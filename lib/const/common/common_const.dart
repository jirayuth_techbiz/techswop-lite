// ignore_for_file: constant_identifier_names

import 'package:techswop_lite/const/enums/routing_enums.dart';
import 'package:techswop_lite/features/result/domain/model/checklist_result_properties.dart';
import 'package:techswop_lite/generated/assets.gen.dart';

class CommonConst {
  static const String MODELID = "model_id";
  static const String MODELNAME = "model_name";
  static const String MODEL_IMG = "image_url";
  static const String MODEL_DISK_SPACE = "disk_space";
  static const String TOKEN = "token";
  static const String EXPIRY_TIME = "expiry_time";

  ///เอาไว้ลบคำว่า Asset ออก (Known Issues เวลาใช้ Asset เสียง + Flutter_Gen)
  static const String ASSET =
          "assets/"; 
  
  ///ใช้เฉพาะกับ Web
  static const String CREATE_FEEDBACK = "Create Feedback";
  
  ///ใช้เฉพาะหน้า Result
  // ignore: non_constant_identifier_names
  static List<CheckListResultProperties> CHECKLIST_PROPERTIES = [
      CheckListResultProperties(
        icon: Assets.icon.techBizCustom.wifi.path,
        appRoutesPage: AppRoutes.wifiCheckPage,
      ),
      CheckListResultProperties(
        icon: Assets.icon.techBizCustom.bluetooth.path,
        appRoutesPage: AppRoutes.bluetoothCheckPage,
      ),
      CheckListResultProperties(
        icon: Assets.icon.techBizCustom.location.path,
        appRoutesPage: AppRoutes.geolocationCheckPage,
      ),
      CheckListResultProperties(
        icon: Assets.icon.techBizCustom.frontCamera.path,
        appRoutesPage: AppRoutes.frontCameraCheckPage,
      ),
      CheckListResultProperties(
        icon: Assets.icon.techBizCustom.backCamera.path,
        appRoutesPage: AppRoutes.backCameraCheckPage,
      ),
      CheckListResultProperties(
        icon: Assets.icon.techBizCustom.microphone.path,
        appRoutesPage: AppRoutes.microphoneCheckPage,
      ),
      CheckListResultProperties(
        icon: Assets.icon.techBizCustom.fingerprint.path,
        appRoutesPage: AppRoutes.biometricsCheckPage,
      ),
      CheckListResultProperties(
        icon: Assets.icon.techBizCustom.proximitySensor.path,
        appRoutesPage: AppRoutes.proximitySensorCheckPage,
      ),
      CheckListResultProperties(
        icon: Assets.icon.techBizCustom.volume.path,
        appRoutesPage: AppRoutes.volumeCheckPage,
      ),
      CheckListResultProperties(
        icon: Assets.icon.techBizCustom.motion.path,
        appRoutesPage: AppRoutes.motionCheckPage,
      ),
      CheckListResultProperties(
        icon: Assets.icon.techBizCustom.touchScreen.path,
        appRoutesPage: AppRoutes.screenCheckStartPage,
      ),
    ];

}
