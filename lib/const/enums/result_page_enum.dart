///Used in [EnumTestResult] extension class
enum TestResult {
  complete,
  skip,
  failed,
}