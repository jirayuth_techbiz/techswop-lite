import 'package:flutter/material.dart';
import 'package:techswop_lite/features/device_test/biometrics/presentation/view/biometrics_check_page.dart';
import 'package:techswop_lite/features/device_test/bluetooth/presentation/view/bluetooth_check_page.dart';
import 'package:techswop_lite/features/device_test/camera/presentation/view/back_camera_check_page.dart';
import 'package:techswop_lite/features/device_test/camera/presentation/view/front_camera_check_page.dart';
import 'package:techswop_lite/features/device_test/geo_location/presentation/view/geolocation_check_page.dart';
import 'package:techswop_lite/features/device_test/microphone/presentation/view/microphone_check_page.dart';
import 'package:techswop_lite/features/device_test/motion/presentation/view/motion_check_page.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/presentation/view/proximity_check_page.dart';
import 'package:techswop_lite/features/device_test/screen_check/presentation/views/screen_check_page.dart';
import 'package:techswop_lite/features/device_test/screen_check/presentation/views/screen_check_start_page.dart';
import 'package:techswop_lite/features/device_test/volume/presentation/view/volume_check_page.dart';
import 'package:techswop_lite/features/device_test/wifi/presentation/view/wifi_check_page.dart';
import 'package:techswop_lite/features/preparation/presentation/view/preparing_page.dart';
import 'package:techswop_lite/features/result/presentation/view/result_page_view.dart';

enum AppRoutes {
  preparingPage,
  resultPage,
  wifiCheckPage,
  bluetoothCheckPage,
  geolocationCheckPage,
  frontCameraCheckPage,
  backCameraCheckPage,
  microphoneCheckPage,
  biometricsCheckPage,
  proximitySensorCheckPage,
  volumeCheckPage,
  motionCheckPage,
  screenCheckStartPage,
  screenCheckPage,
}

///เอาไว้ Map กับ API
///ห้ามใช้ทำอย่างอื่น และห้ามเอาไปใช้ใน Routing
extension AppRoutesQuestionTitle on AppRoutes {
  String get titleAPI {
    switch (this) {
      case AppRoutes.wifiCheckPage:
        return 'WiFi';
      case AppRoutes.bluetoothCheckPage:
        return 'Bluetooth';
      case AppRoutes.geolocationCheckPage:
        return 'GPS';
      case AppRoutes.frontCameraCheckPage:
        return 'Camera';
      case AppRoutes.backCameraCheckPage:
        return 'Camera';
      case AppRoutes.microphoneCheckPage:
        return 'Microphone and Speaker';
      case AppRoutes.biometricsCheckPage:
        return 'Biometric';
      case AppRoutes.proximitySensorCheckPage:
        return 'Proximity';
      case AppRoutes.volumeCheckPage:
        return 'Volume';
      case AppRoutes.motionCheckPage:
        return 'Motion';
      case AppRoutes.screenCheckStartPage:
        return 'Screen';
      case AppRoutes.screenCheckPage:
        return 'Screen';
      default:
        return "";
    }
  }

  int get apiID {
    switch (this) {
      case AppRoutes.wifiCheckPage:
        return 7;
      case AppRoutes.bluetoothCheckPage:
        return 8;
      case AppRoutes.geolocationCheckPage:
        return 9;
      case AppRoutes.frontCameraCheckPage:
        return 10;
      case AppRoutes.backCameraCheckPage:
        return 10;
      case AppRoutes.microphoneCheckPage:
        return 1;
      case AppRoutes.biometricsCheckPage:
        return 6;
      case AppRoutes.proximitySensorCheckPage:
        return 4;
      case AppRoutes.volumeCheckPage:
        return 5;
      case AppRoutes.motionCheckPage:
        return 3;
      case AppRoutes.screenCheckPage:
        return 2;
      default:
        return 0;
    }
  }
}

///เอาไว้ผูก Map Path
extension AppRoutePathName on AppRoutes {
  String get path {
    switch (this) {
      case AppRoutes.preparingPage:
        return "preparingPage";
      case AppRoutes.resultPage:
        return "resultPage";
      case AppRoutes.wifiCheckPage:
        return "wifiCheckPage";
      case AppRoutes.bluetoothCheckPage:
        return "bluetoothCheckPage";
      case AppRoutes.geolocationCheckPage:
        return "geolocationCheckPage";
      case AppRoutes.frontCameraCheckPage:
        return "frontCameraCheckPage";
      case AppRoutes.backCameraCheckPage:
        return "backCameraCheckPage";
      case AppRoutes.microphoneCheckPage:
        return "microphoneCheckPage";
      case AppRoutes.biometricsCheckPage:
        return "biometricsCheckPage";
      case AppRoutes.proximitySensorCheckPage:
        return "proximitySensorCheckPage";
      case AppRoutes.volumeCheckPage:
        return "volumeCheckPage";
      case AppRoutes.motionCheckPage:
        return "motionCheckPage";
      case AppRoutes.screenCheckStartPage:
        return "screenCheckStartPage";
      case AppRoutes.screenCheckPage:
        return "screenCheckPage";
      default:
        return "";
    }
  }
}

///เอาไว้ Map กับหน้า Routing
extension AppRoutesWidget on AppRoutes {
  
  static String isFromResult = "false";

  Widget get widgetPages {
    switch (this) {
      case AppRoutes.preparingPage:
        return const PreparingPage();
      case AppRoutes.resultPage:
        return const ResultPage();
      case AppRoutes.wifiCheckPage:
        return const WifiCheckPage();
      case AppRoutes.bluetoothCheckPage:
        return const BluetoothCheckPage();
      case AppRoutes.geolocationCheckPage:
        return const GeolocationCheckPage();
      case AppRoutes.frontCameraCheckPage:
        return const FrontCameraCheckPage();
      case AppRoutes.backCameraCheckPage:
        return const BackCameraCheckPage();
      case AppRoutes.microphoneCheckPage:
        return const MicrophoneCheckPage();
      case AppRoutes.biometricsCheckPage:
        return const BiometricsCheckPage();
      case AppRoutes.proximitySensorCheckPage:
        return const ProximitySensorCheckPage();
      case AppRoutes.volumeCheckPage:
        return const VolumeCheckPage();
      case AppRoutes.motionCheckPage:
        return const MotionPage();
      case AppRoutes.screenCheckStartPage:
        return const ScreenCheckStartPage();
      case AppRoutes.screenCheckPage:
        return const ScreenCheckPage();
    }
  }
}
