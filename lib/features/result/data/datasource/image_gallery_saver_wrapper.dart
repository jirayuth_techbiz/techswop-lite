import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:screenshot/screenshot.dart';
import 'package:techswop_lite/shared/shared.dart';

class ImageGallerySaverWrapper {
  FutureOr<dynamic> saveImage(
    Uint8List? imageBytes, {
    required int quality,
    String? name,
  }) => ImageGallerySaver.saveImage(imageBytes!, quality: quality, name: name);

  Future<Uint8List> captureFromWidget(
    Widget widget, {
    Duration delay = const Duration(seconds: 1),
    double? pixelRatio,
    BuildContext? context,
    Size? targetSize,
  }) => ScreenshotController().captureFromWidget(widget, delay: delay, pixelRatio: pixelRatio, context: context, targetSize: targetSize);
}
