import 'package:flutter/widgets.dart';
import 'package:techswop_lite/features/result/data/datasource/image_gallery_saver_wrapper.dart';
import 'package:techswop_lite/features/result/domain/repository/result_page_repository.dart';
import 'package:techswop_lite/shared/shared.dart';

class ResultPageRepositoryImpl implements ResultPageRepository {
  final ImageGallerySaverWrapper imageGallerySaverWrapper;
  final LoggerRepository loggerRepository;

  ResultPageRepositoryImpl({
    required this.imageGallerySaverWrapper,
    required this.loggerRepository,
  });

  @override
  Future<void> saveQRCodeImage({required Widget widget, required String datetime, required BuildContext context}) async {
    try {
      final capture = await _saveWidgetImage(widget, context: context);

      await imageGallerySaverWrapper.saveImage(
        capture,
        quality: 95,
        name: 'tech_swop_$datetime',
      );

    } catch (e) {
      loggerRepository.logError(e.toString());
    }
  }

  Future<Uint8List> _saveWidgetImage(
    Widget widget, {
    Duration delay = const Duration(seconds: 1),
    double? pixelRatio,
    BuildContext? context,
    Size? targetSize,
  }) async {
    final result = await imageGallerySaverWrapper.captureFromWidget(
      widget,
      delay: delay,
      pixelRatio: pixelRatio,
      context: context,
      targetSize: targetSize,
    );

    loggerRepository.logInfo("Image saved successfully");

    return result;
  }
}
