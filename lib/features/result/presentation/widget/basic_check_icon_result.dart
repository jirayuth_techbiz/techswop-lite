import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/features/result/domain/model/checklist_result_properties.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/shared.dart';

class BasicCheckIconResult extends ConsumerWidget {
  final Color color;
  final bool? result;
  final CheckListResultProperties checkListProperties;

  const BasicCheckIconResult({
    super.key,
    required this.checkListProperties,
    this.result,
    required this.color,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = context.theme;

    Color shouldReturnColorBasedOnResultFont(bool? result) {
      if (result == true) {
        return theme.colorScheme.primary;
      } else if (result == false) {
        return theme.colorScheme.error;
      } else {
        return theme.colorScheme.onSurfaceVariant;
      }
    }

    return GestureDetector(
      onTap: () {
        //// NOTHING TO DO FOR NOW
        // const bool isRetest = true;
        // if (result == null || result == false) {
        //   context.pushNamed(checkListProperties.appRoutesPage.name,
        //       extra: isRetest);
        // }
      },
      child: Card(
        elevation: 0,
        color: color,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 25.r,
              height: 25.r,
              constraints: BoxConstraints(
                maxWidth: 30.w,
              ),
              margin: const EdgeInsets.all(15),
              child: SvgPicture.asset(
                checkListProperties!.icon,
                width: 20.r,
                height: 20.r,
                colorFilter: ColorFilter.mode(
                    shouldReturnColorBasedOnResultFont(result),
                    BlendMode.srcATop),
              ),
            ),
            Flexible(
              fit: FlexFit.tight,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 10,
                ),
                child: Text(
                  checkListProperties!.appRoutesPage.titleAPI,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.visible,
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                        color: shouldReturnColorBasedOnResultFont(result),
                        fontWeight: FontWeight.bold,
                        overflow: TextOverflow.ellipsis,
                        fontSize: 12.sp,
                      ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
