import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/result/domain/model/checklist_result_properties.dart';
import 'package:techswop_lite/features/result/domain/model/issues_model.dart';
import 'package:techswop_lite/features/result/domain/service/result_page_create_trade_in_service.dart';
import 'package:techswop_lite/features/result/domain/service/result_page_history_service.dart';

part 'result_page_state.g.dart';

@riverpod
class ListPropertiesTrueState extends _$ListPropertiesTrueState {
  @override
  List<Map<CheckListResultProperties, bool>> build(
      List<TestResultModel> testResult) {
    final resultPageHistoryService =
        ref.watch(resultPageHistoryServiceProvider);
    return resultPageHistoryService.buildPassListProperties(testResult);
  }
}

@riverpod
class ListPropertiesFalseState extends _$ListPropertiesFalseState {
  @override
  List<Map<CheckListResultProperties, bool>> build(
      List<TestResultModel> testResult) {
    final resultPageHistoryService =
        ref.watch(resultPageHistoryServiceProvider);
    return resultPageHistoryService.buildFailListProperties(testResult);
  }
}

@riverpod 
class IssueResultState extends _$IssueResultState {
  @override
  Future<IssuesModel> build() async {
    final resultPageCreateTradeInService =
        ref.watch(resultPageCreateTradeInServiceProvider);
    return resultPageCreateTradeInService.createTradeinCheckListResult();
  }
}