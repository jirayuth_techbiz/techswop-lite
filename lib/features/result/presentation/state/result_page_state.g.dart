// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'result_page_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$listPropertiesTrueStateHash() =>
    r'ecd7f1cfd04a9be0197aea946e898512d769aa95';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$ListPropertiesTrueState extends BuildlessAutoDisposeNotifier<
    List<Map<CheckListResultProperties, bool>>> {
  late final List<TestResultModel> testResult;

  List<Map<CheckListResultProperties, bool>> build(
    List<TestResultModel> testResult,
  );
}

/// See also [ListPropertiesTrueState].
@ProviderFor(ListPropertiesTrueState)
const listPropertiesTrueStateProvider = ListPropertiesTrueStateFamily();

/// See also [ListPropertiesTrueState].
class ListPropertiesTrueStateFamily
    extends Family<List<Map<CheckListResultProperties, bool>>> {
  /// See also [ListPropertiesTrueState].
  const ListPropertiesTrueStateFamily();

  /// See also [ListPropertiesTrueState].
  ListPropertiesTrueStateProvider call(
    List<TestResultModel> testResult,
  ) {
    return ListPropertiesTrueStateProvider(
      testResult,
    );
  }

  @override
  ListPropertiesTrueStateProvider getProviderOverride(
    covariant ListPropertiesTrueStateProvider provider,
  ) {
    return call(
      provider.testResult,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'listPropertiesTrueStateProvider';
}

/// See also [ListPropertiesTrueState].
class ListPropertiesTrueStateProvider extends AutoDisposeNotifierProviderImpl<
    ListPropertiesTrueState, List<Map<CheckListResultProperties, bool>>> {
  /// See also [ListPropertiesTrueState].
  ListPropertiesTrueStateProvider(
    List<TestResultModel> testResult,
  ) : this._internal(
          () => ListPropertiesTrueState()..testResult = testResult,
          from: listPropertiesTrueStateProvider,
          name: r'listPropertiesTrueStateProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$listPropertiesTrueStateHash,
          dependencies: ListPropertiesTrueStateFamily._dependencies,
          allTransitiveDependencies:
              ListPropertiesTrueStateFamily._allTransitiveDependencies,
          testResult: testResult,
        );

  ListPropertiesTrueStateProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.testResult,
  }) : super.internal();

  final List<TestResultModel> testResult;

  @override
  List<Map<CheckListResultProperties, bool>> runNotifierBuild(
    covariant ListPropertiesTrueState notifier,
  ) {
    return notifier.build(
      testResult,
    );
  }

  @override
  Override overrideWith(ListPropertiesTrueState Function() create) {
    return ProviderOverride(
      origin: this,
      override: ListPropertiesTrueStateProvider._internal(
        () => create()..testResult = testResult,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        testResult: testResult,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<ListPropertiesTrueState,
      List<Map<CheckListResultProperties, bool>>> createElement() {
    return _ListPropertiesTrueStateProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ListPropertiesTrueStateProvider &&
        other.testResult == testResult;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, testResult.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin ListPropertiesTrueStateRef on AutoDisposeNotifierProviderRef<
    List<Map<CheckListResultProperties, bool>>> {
  /// The parameter `testResult` of this provider.
  List<TestResultModel> get testResult;
}

class _ListPropertiesTrueStateProviderElement
    extends AutoDisposeNotifierProviderElement<ListPropertiesTrueState,
        List<Map<CheckListResultProperties, bool>>>
    with ListPropertiesTrueStateRef {
  _ListPropertiesTrueStateProviderElement(super.provider);

  @override
  List<TestResultModel> get testResult =>
      (origin as ListPropertiesTrueStateProvider).testResult;
}

String _$listPropertiesFalseStateHash() =>
    r'aa66e5eb68b15623b22321864ce3caed3d41e6a5';

abstract class _$ListPropertiesFalseState extends BuildlessAutoDisposeNotifier<
    List<Map<CheckListResultProperties, bool>>> {
  late final List<TestResultModel> testResult;

  List<Map<CheckListResultProperties, bool>> build(
    List<TestResultModel> testResult,
  );
}

/// See also [ListPropertiesFalseState].
@ProviderFor(ListPropertiesFalseState)
const listPropertiesFalseStateProvider = ListPropertiesFalseStateFamily();

/// See also [ListPropertiesFalseState].
class ListPropertiesFalseStateFamily
    extends Family<List<Map<CheckListResultProperties, bool>>> {
  /// See also [ListPropertiesFalseState].
  const ListPropertiesFalseStateFamily();

  /// See also [ListPropertiesFalseState].
  ListPropertiesFalseStateProvider call(
    List<TestResultModel> testResult,
  ) {
    return ListPropertiesFalseStateProvider(
      testResult,
    );
  }

  @override
  ListPropertiesFalseStateProvider getProviderOverride(
    covariant ListPropertiesFalseStateProvider provider,
  ) {
    return call(
      provider.testResult,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'listPropertiesFalseStateProvider';
}

/// See also [ListPropertiesFalseState].
class ListPropertiesFalseStateProvider extends AutoDisposeNotifierProviderImpl<
    ListPropertiesFalseState, List<Map<CheckListResultProperties, bool>>> {
  /// See also [ListPropertiesFalseState].
  ListPropertiesFalseStateProvider(
    List<TestResultModel> testResult,
  ) : this._internal(
          () => ListPropertiesFalseState()..testResult = testResult,
          from: listPropertiesFalseStateProvider,
          name: r'listPropertiesFalseStateProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$listPropertiesFalseStateHash,
          dependencies: ListPropertiesFalseStateFamily._dependencies,
          allTransitiveDependencies:
              ListPropertiesFalseStateFamily._allTransitiveDependencies,
          testResult: testResult,
        );

  ListPropertiesFalseStateProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.testResult,
  }) : super.internal();

  final List<TestResultModel> testResult;

  @override
  List<Map<CheckListResultProperties, bool>> runNotifierBuild(
    covariant ListPropertiesFalseState notifier,
  ) {
    return notifier.build(
      testResult,
    );
  }

  @override
  Override overrideWith(ListPropertiesFalseState Function() create) {
    return ProviderOverride(
      origin: this,
      override: ListPropertiesFalseStateProvider._internal(
        () => create()..testResult = testResult,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        testResult: testResult,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<ListPropertiesFalseState,
      List<Map<CheckListResultProperties, bool>>> createElement() {
    return _ListPropertiesFalseStateProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ListPropertiesFalseStateProvider &&
        other.testResult == testResult;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, testResult.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin ListPropertiesFalseStateRef on AutoDisposeNotifierProviderRef<
    List<Map<CheckListResultProperties, bool>>> {
  /// The parameter `testResult` of this provider.
  List<TestResultModel> get testResult;
}

class _ListPropertiesFalseStateProviderElement
    extends AutoDisposeNotifierProviderElement<ListPropertiesFalseState,
        List<Map<CheckListResultProperties, bool>>>
    with ListPropertiesFalseStateRef {
  _ListPropertiesFalseStateProviderElement(super.provider);

  @override
  List<TestResultModel> get testResult =>
      (origin as ListPropertiesFalseStateProvider).testResult;
}

String _$issueResultStateHash() => r'512db85eafbbf34d5aae68037eda1ef6f23c91bd';

/// See also [IssueResultState].
@ProviderFor(IssueResultState)
final issueResultStateProvider =
    AutoDisposeAsyncNotifierProvider<IssueResultState, IssuesModel>.internal(
  IssueResultState.new,
  name: r'issueResultStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$issueResultStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$IssueResultState = AutoDisposeAsyncNotifier<IssuesModel>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
