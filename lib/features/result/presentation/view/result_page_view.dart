// ignore_for_file: non_constant_identifier_names
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:techswop_lite/const/enums/routing_enums.dart';
import 'package:techswop_lite/features/result/domain/model/checklist_result_properties.dart';
import 'package:techswop_lite/features/result/domain/model/issues_model.dart';
import 'package:techswop_lite/features/result/domain/service/result_page_history_service.dart';
import 'package:techswop_lite/features/result/presentation/state/result_page_state.dart';
import 'package:techswop_lite/features/result/presentation/widget/basic_check_icon_result.dart';
import 'package:techswop_lite/generated/app_localizations.dart';
import 'package:techswop_lite/generated/assets.gen.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/route/state/route_state.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/shared/utils/format/result_format_utils.dart';

class ResultPage extends ConsumerWidget {
  const ResultPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = context.theme;
    final translation = context.localization;
    final route = ref.watch(routeStateProvider);
    final issueResult = ref.watch(issueResultStateProvider);

    return LayoutBuilder(builder: (ctx, constraints) {
      bool shouldAdaptive = false;
      const double tabletWidth = 425;

      if (constraints.maxWidth <= tabletWidth) {
        shouldAdaptive = true;
      }

      return AsyncValueWidget(
          value: issueResult,
          data: (resultMap) {
            final testModelResultMap = resultMap.questionChoiceItem.map((answerChoices) => TestResultModel.fromJson(answerChoices.toJson()));

            return ResultPageWidget(
              result: resultMap,
              resultMap: testModelResultMap,
            );
          });
    });
  }
}

class ResultPageWidget extends ConsumerStatefulWidget {
  final IssuesModel result;
  final Iterable<TestResultModel> resultMap;
  const ResultPageWidget(
      {super.key, required this.result, required this.resultMap});

  @override
  ConsumerState<ResultPageWidget> createState() => _ResultPageWidgetState();
}

class _ResultPageWidgetState extends ConsumerState<ResultPageWidget> {
  @override
  Widget build(BuildContext context) {
    final globalKey = GlobalKey();
    final translation = context.localization;
    final theme = context.theme;
    final resultHistoryService = ref.watch(resultPageHistoryServiceProvider);
    // final logger = ref.watch(loggerRepositoryProvider);

    var listPropertiesTrueState = ref.watch(
      listPropertiesTrueStateProvider(widget.result.questionChoiceItem),
    );
    var listPropertiesFalseState = ref.watch(
      listPropertiesFalseStateProvider(widget.result.questionChoiceItem),
    );

    final qrDataResult = ResultFormatUtils.toFormat(widget.result.toJson());

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: theme.colorScheme.primary,
        centerTitle: true,
        title: Padding(
          padding: EdgeInsets.only(left: 7.0.w),
          child: Text(
            translation.resultPageTitle,
            style: theme.textTheme.titleLarge!.copyWith(
              fontWeight: FontWeight.bold,
              color: theme.colorScheme.onPrimary,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        leading: IconButton(
          icon: const FaIcon(FontAwesomeIcons.arrowLeft, color: Colors.white,),
          onPressed: () {
            context.goNamed(AppRoutes.preparingPage.name);
          },
        )
      ),
      body: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        child: Align(
          heightFactor: 1,
          child: Visibility(
            visible: widget.result.code !=
                translation.resultPageNoDataError,
            replacement: Column(
              children: [
                const Icon(
                  Icons.warning_amber_rounded,
                  size: 100,
                  color: Colors.amber,
                ),
                Text(translation.resultPageNoDataError),
              ],
            ),
            child: Column(
              children: [
                _PleaseShowQRTitleText(
                  translation: translation.resultPageCreateQRCode,
                  theme: theme,
                ),
                _TestDateTimeText(
                  translation: translation.resultPageDateOfTest,
                  result: widget.result.createdAt.toString(),
                ),
                _ShowQRCodeHero(
                  qrData: qrDataResult,
                  onTap: () async {
                    _zoomQR(
                      context,
                      globalKey: globalKey,
                      qrData: qrDataResult,
                      qrTextData: qrDataResult,
                    );

                    await resultHistoryService
                        .checkIsQRImageDuplicated(
                            qrDataResult)
                        .then(
                      (result) async {
                        _showSnackbarSaveQRComplete(context, result);
                        if (!result) {
                          await resultHistoryService.saveQRCodeImage(
                            widget: _HeroQRCodeForSave(
                              translation: translation,
                              theme: theme,
                              globalKey: globalKey,
                              qrcodeData: qrDataResult,
                              time: widget.result.createdAt.toString(),
                            ),
                            datetime: widget.result.createdAt.toString(),
                            context: context,
                          );
                        }
                      },
                    );
                  },
                ),
                SizedBox(
                  height: 15.h,
                ),
                _ResultTable(
                  theme: theme,
                  passText: translation.resultPageTestPassed,
                  failText: translation.resultPageTestFailed,
                  allPageText: translation.resultPagePageTotal,
                  listPropertiesTrueState: listPropertiesTrueState,
                  listPropertiesFalseState: listPropertiesFalseState,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _zoomQR(
    BuildContext context, {
    required String qrTextData,
    required String qrData,
    required GlobalKey<State<StatefulWidget>> globalKey,
  }) {
    final theme = context.theme;
    final translation = context.localization;
    final resultHistoryService = ref.watch(resultPageHistoryServiceProvider);

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => Scaffold(
          body: _HeroQRCode(
            translation: translation,
            widget: widget,
            theme: theme,
            resultService: resultHistoryService,
            globalKey: globalKey,
            qrTextData: qrTextData,
            qrData: qrData,
          ),
        ),
      ),
    );
  }

  void _showSnackbarSaveQRComplete(
      BuildContext context, bool isQRImageDuplicated) {
    final translations = context.localization;
    final snackBar = SnackBar(
      content: Text(isQRImageDuplicated
          ? translations.resultPageDuplicatedQRSave
          : translations.resultPageQRSaveSuccess),
      duration: const Duration(seconds: 2),
      // action: SnackBarAction(
      //   label: translations.confirmTxt,
      //   onPressed: () {}, //Just close it.
      // ),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}

////////////////////////////////////////////////////////////////////////////////

class _HeroQRCode extends ConsumerWidget {
  const _HeroQRCode({
    required this.translation,
    required this.widget,
    required this.theme,
    required this.resultService,
    required this.globalKey,
    required this.qrTextData,
    required this.qrData,
  });

  final AppLocalizations translation;
  final ResultPageWidget widget;
  final ThemeData theme;
  final ResultPageHistoryService resultService;
  final GlobalKey<State<StatefulWidget>> globalKey;
  final String qrTextData;
  final String qrData;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Hero(
            tag: 'qr',
            child: QrImageView(
              data: qrData,
              size: 300.0.spMax,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(7.r),
            child: Text(
              "${translation.resultPageDateOfTest} : ${timeConverter(db_date: widget.result.createdAt.toString())}",
              style: theme.textTheme.titleMedium!.copyWith(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 16.sp,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Text(
              translation.resultPagePleaseShowQR,
              textAlign: TextAlign.center,
              style: theme.textTheme.titleMedium!.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          ConstrainedBox(
            constraints: BoxConstraints(minWidth: 150.spMax),
            child: ElevatedButton(
              onPressed: () {
                context.pop();
              },
              child: Text(translation.resultPageClosedHero),
            ),
          ),
        ],
      ),
    );
  }
}

class _ResultTable extends ConsumerWidget {
  const _ResultTable({
    required this.theme,
    required this.passText,
    required this.failText,
    required this.allPageText,
    required this.listPropertiesTrueState,
    required this.listPropertiesFalseState,
  });

  final ThemeData theme;
  final String passText;
  final String failText;
  final String allPageText;
  final List<Map<CheckListResultProperties, bool>> listPropertiesTrueState;
  final List<Map<CheckListResultProperties, bool>> listPropertiesFalseState;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /// Test Failed
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            children: [
              WidgetSpan(
                  alignment: PlaceholderAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0.w),
                    child: FaIcon(
                      FontAwesomeIcons.circleXmark,
                      size: 35.r,
                      color: theme.colorScheme.error,
                    ),
                  )),
              TextSpan(
                text: "$failText ",
                style: theme.textTheme.bodyLarge!.copyWith(
                    fontWeight: FontWeight.bold,
                    color: theme.colorScheme.onPrimaryContainer,
                    fontSize: 18.sp),
              ),
              TextSpan(
                text: "${listPropertiesFalseState.length}",
                style: theme.textTheme.bodyLarge!.copyWith(
                    fontWeight: FontWeight.bold,
                    color: theme.colorScheme.error,
                    fontSize: 18.sp),
              ),
              TextSpan(
                text: " $allPageText",
                style: theme.textTheme.bodyLarge!.copyWith(
                    fontWeight: FontWeight.bold,
                    color: theme.colorScheme.onPrimaryContainer,
                    fontSize: 18.sp),
              ),
            ],
          ),
        ),

        SizedBox(
          height: 10.h,
        ),

        _ResultListProperties(
          bgcolor: theme.colorScheme.errorContainer,
          listProperties: listPropertiesFalseState,
        ),

        SizedBox(
          height: 10.h,
        ),

        ///Test Passed
        RichText(
          text: TextSpan(
            children: [
              WidgetSpan(
                  alignment: PlaceholderAlignment.middle,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0.w),
                    child: FaIcon(
                      FontAwesomeIcons.circleCheck,
                      size: 35.r,
                      color: theme.colorScheme.primary,
                    ),
                  )),
              TextSpan(
                text: "$passText ",
                style: theme.textTheme.bodyLarge!.copyWith(
                  fontWeight: FontWeight.bold,
                  color: theme.colorScheme.onPrimaryContainer,
                  fontSize: 18.sp,
                ),
              ),
              TextSpan(
                text: "${listPropertiesTrueState.length}",
                style: theme.textTheme.bodyLarge!.copyWith(
                  fontWeight: FontWeight.bold,
                  color: theme.colorScheme.primary,
                  fontSize: 18.sp,
                ),
              ),
              TextSpan(
                text: " $allPageText",
                style: theme.textTheme.bodyLarge!.copyWith(
                  fontWeight: FontWeight.bold,
                  color: theme.colorScheme.onPrimaryContainer,
                  fontSize: 18.sp,
                ),
              ),
            ],
          ),
        ),

        SizedBox(
          height: 10.h,
        ),

        _ResultListProperties(
          bgcolor: theme.colorScheme.tertiaryContainer,
          listProperties: listPropertiesTrueState,
        ),
      ],
    );
  }
}

class _PressToCreateQRText extends ConsumerWidget {
  const _PressToCreateQRText({
    required this.theme,
    required this.translation,
  });

  final ThemeData theme;
  final String translation;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.all(7),
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minWidth: 200.w,
          maxWidth: ScreenUtil().screenWidth.w / 1.2.w,
        ),
        child: Text(
          translation,
          textAlign: TextAlign.center,
          style: theme.textTheme.bodyMedium!.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}

class _ShowQRCodeHero extends ConsumerWidget {
  const _ShowQRCodeHero({
    required this.qrData,
    required this.onTap,
  });

  final String qrData;
  final void Function() onTap;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Hero(
          tag: "qr",
          child: GestureDetector(
            onTap: onTap,
            child: QrImageView(
              data: qrData,
              size: 200.0,
            ),
          ),
        ),
      ],
    );
  }
}

class _TestDateTimeText extends ConsumerWidget {
  const _TestDateTimeText({
    required this.translation,
    required this.result,
  });

  final String translation;
  final String result;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 15.h),
      child: Text(
        "$translation : ${timeConverter(db_date: result)}",
        style: Theme.of(context).textTheme.titleSmall!.copyWith(
              color: Colors.black,
              fontWeight: FontWeight.w500,
              fontSize: 13.sp,
            ),
      ),
    );
  }
}

class _PleaseShowQRTitleText extends ConsumerWidget {
  const _PleaseShowQRTitleText({
    required this.translation,
    required this.theme,
  });

  final String translation;
  final ThemeData theme;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      padding: EdgeInsets.all(7.h),
      width: ScreenUtil().screenWidth.w / 1.4,
      child: Text(
        translation,
        style: theme.textTheme.titleSmall!.copyWith(
          fontWeight: FontWeight.bold,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}

class _ResultListProperties extends ConsumerWidget {
  const _ResultListProperties({
    required this.bgcolor,
    required this.listProperties,
  });

  final Color bgcolor;
  final List<Map<CheckListResultProperties, bool>> listProperties;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SizedBox(
      width: MediaQuery.of(context).size.width.w,
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: listProperties.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          int elementIndexList = 0;
          CheckListResultProperties checkListProperty =
              listProperties[index].keys.elementAt(elementIndexList);
          bool result =
              listProperties[index].values.elementAt(elementIndexList);

          return Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 7,
              vertical: 1,
            ),
            child: BasicCheckIconResult(
              color: bgcolor,
              checkListProperties: checkListProperty,
              result: result,
            ),
          );
        },
      ),
    );
  }
}

class _HeroQRCodeForSave extends ConsumerWidget {
  const _HeroQRCodeForSave({
    required this.translation,
    required this.theme,
    required this.globalKey,
    required this.qrcodeData,
    required this.time,
  });

  final AppLocalizations translation;
  final ThemeData theme;
  final GlobalKey<State<StatefulWidget>> globalKey;
  final String qrcodeData;
  final String time;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      backgroundColor: theme.colorScheme.surface,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Image.asset(
                Assets.design.techbizLogo512Lite.path,
                colorBlendMode: BlendMode.dstIn,
                width: 100.r,
                height: 100.r,
              ),
            ),
            QrImageView(
              backgroundColor: theme.colorScheme.surface,
              data: qrcodeData,
              size: 300.0.spMax,
            ),
            _TestDateTimeText(
              translation: translation.resultPageDateOfTest,
              result: time,
            ),
          ],
        ),
      ),
    );
  }
}
