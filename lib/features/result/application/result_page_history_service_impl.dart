import 'package:collection/collection.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/features/result/domain/model/checklist_result_properties.dart';
import 'package:techswop_lite/features/result/domain/model/issues_model.dart';
import 'package:techswop_lite/features/result/domain/repository/result_page_repository.dart';
import 'package:techswop_lite/features/result/domain/service/result_page_history_service.dart';
import 'package:techswop_lite/shared/domain/repository/device_info_repository.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/shared/domain/repository/trade_in_secure_storage_repository.dart';

class ResultPageHistoryServiceImpl implements ResultPageHistoryService {
  final Ref ref;
  final ResultPageRepository resultPageRepository;
  final TradeinSecureStorageRepository tradeinSecureStorageRepository;
  final TradeInSharePreferencesRepository tradeInSharePreferencesRepository;
  final DeviceInfoRepository deviceInfoRepository;
  final LoggerRepository logger;

  ResultPageHistoryServiceImpl({
    required this.ref,
    required this.logger,
    required this.resultPageRepository,
    required this.tradeinSecureStorageRepository,
    required this.tradeInSharePreferencesRepository,
    required this.deviceInfoRepository,
  });

  final String _keyResult = "key_result";

  @override
  List<Map<CheckListResultProperties, bool>> buildPassListProperties(
      List<TestResultModel> testResult) {

    final listPassResult = <Map<CheckListResultProperties, bool>>[];

    if (listPassResult.isNotEmpty) {
      listPassResult.clear();
    }

    for (var t = 0; t < testResult.length; t++) {
      if (testResult[t].result == true) {
        var key = CommonConst.CHECKLIST_PROPERTIES.firstWhereOrNull(
            (data) => data.appRoutesPage.titleAPI == testResult[t].title);

        if (key != null) {
          listPassResult.add({key: true});
          tradeInSharePreferencesRepository.saveBool(testResult[t].title, true);
        }
      }
    }

    return listPassResult;
  }

  @override
  List<Map<CheckListResultProperties, bool>> buildFailListProperties(
      List<TestResultModel> testResult) {

    final listFailedResult = <Map<CheckListResultProperties, bool>>[];

    if (listFailedResult.isNotEmpty) {
      listFailedResult.clear();
    }

    for (var t = 0; t < testResult.length; t++) {
      if (testResult[t].result == false) {
        var key = CommonConst.CHECKLIST_PROPERTIES.firstWhereOrNull(
            (data) => data.appRoutesPage.titleAPI == testResult[t].title);

        if (key != null) {
          listFailedResult.add({key: false});
          tradeInSharePreferencesRepository.saveBool(testResult[t].title, false);
        }
      }
    }
    return listFailedResult;
  }

  @override
  Future<bool> checkIsQRImageDuplicated(String keyResult) async {
    final savedQrCode = await _getSavedQrCode();
    logger.logDebug(savedQrCode.toString());

    if (savedQrCode == null ||
        savedQrCode != keyResult ||
        savedQrCode.isEmpty) {
      await _saveQRCodeCompleteStatus(keyResult);
      return false;
    } else if (savedQrCode == keyResult) {
      return true;
    }

    return true;
  }

  @override
  Future<void> saveQRCodeImage({
    required Widget widget,
    required String datetime,
    required BuildContext context,
  }) async {
    logger.logInfo("Saving QR Code Image");
    await resultPageRepository.saveQRCodeImage(
        widget: widget, datetime: datetime, context: context);
  }

  ///เก็บข้อมูลว่า QR Code ภาพนี้ถูกบันทึกไปแล้ว
  Future<void> _saveQRCodeCompleteStatus(String keyResult) async {
    await tradeInSharePreferencesRepository.saveString(_keyResult, keyResult);
  }

  Future<String?> _getSavedQrCode() async {
    return await tradeInSharePreferencesRepository.getString(_keyResult);
  }
}