import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/result/domain/model/issues_model.dart';
import 'package:techswop_lite/features/result/domain/repository/result_page_repository.dart';
import 'package:techswop_lite/features/result/domain/service/result_page_create_trade_in_service.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:uuid/uuid.dart';

class ResultPageCreateTradeInServiceImpl
    implements ResultPageCreateTradeInService {
  final Ref ref;
  final ResultPageRepository resultPageRepository;
  final TradeinSecureStorageRepository tradeinSecureStorageRepository;
  final TradeInSharePreferencesRepository tradeInSharePreferencesRepository;
  final DeviceInfoRepository deviceInfoRepository;
  final LoggerRepository logger;
  final Uuid uuid;

  ResultPageCreateTradeInServiceImpl({
    required this.ref,
    required this.logger,
    required this.resultPageRepository,
    required this.tradeinSecureStorageRepository,
    required this.tradeInSharePreferencesRepository,
    required this.deviceInfoRepository,
    required this.uuid,
  });

  @override
  Future<IssuesModel> createTradeinCheckListResult() async {
    final code = uuid.v4();
    final DateTime now = DateTime.now();
    final hardwareResult = await _createHardwareResult();
    final result = IssuesModel(
      code: code,
      createdAt: now,
      questionChoiceItem: hardwareResult,
    );

    return result;
  }

  Future<List<TestResultModel>> _createHardwareResult() async {
    final List<String> issues = [
      AppRoutes.wifiCheckPage.titleAPI,
      AppRoutes.bluetoothCheckPage.titleAPI,
      AppRoutes.geolocationCheckPage.titleAPI,
      AppRoutes.frontCameraCheckPage.titleAPI,
      // AppRoutes.backCameraCheckPage.titleAPI,
      AppRoutes.microphoneCheckPage.titleAPI,
      AppRoutes.biometricsCheckPage.titleAPI,
      AppRoutes.proximitySensorCheckPage.titleAPI,
      AppRoutes.volumeCheckPage.titleAPI,
      AppRoutes.motionCheckPage.titleAPI,
      AppRoutes.screenCheckPage.titleAPI,
    ];

    final List<int> hardwareID = [
      AppRoutes.wifiCheckPage.apiID,
      AppRoutes.bluetoothCheckPage.apiID,
      AppRoutes.geolocationCheckPage.apiID,
      AppRoutes.frontCameraCheckPage.apiID,
      // AppRoutes.backCameraCheckPage.APIID,
      AppRoutes.microphoneCheckPage.apiID,
      AppRoutes.biometricsCheckPage.apiID,
      AppRoutes.proximitySensorCheckPage.apiID,
      AppRoutes.volumeCheckPage.apiID,
      AppRoutes.motionCheckPage.apiID,
      AppRoutes.screenCheckPage.apiID,
    ];

    List<TestResultModel> resultModel = [];

    var issuesResultModel = TestResultModel();
    for (var a = 0; a < issues.length; a++) {
      final result = await tradeInSharePreferencesRepository.getBool(issues[a]);
      issuesResultModel = issuesResultModel.copyWith(
        title: issues[a],
        hardware_id: hardwareID[a],
        result: result!,
      );

      resultModel.add(issuesResultModel);
    }

    return resultModel;
  }

  // Future<AnswerMapper> _createTradeinCheckListResult() async {
  //   final question = await deviceModelService.getQuestionList();
  //   final modelId =
  //       await tradeInSharePreferencesRepository.getString(CommonConst.MODELID);
  //   final deviceInfo = await deviceInfoRepository.getDeviceInfo();
  //   final List<AnswerChoices> answerChoices = [];
  //   const answerChoice = AnswerChoices();
  //   const answerResult = AnswerMapper();
  //   const answerCorrect = 1;
  //   const answerWrong = 0;

  //   for (var a = 0; a < question!.length; a++) {
  //     final title = question[a].title;
  //     final result = await tradeInSharePreferencesRepository.getBool(title);
  //     final choiceId = (result == true)
  //         ? question[a].choices[answerCorrect].choiceId
  //         : question[a].choices[answerWrong].choiceId;
  //     final choiceValue = (result == true)
  //         ? question[a].choices[answerCorrect].choiceValue
  //         : question[a].choices[answerWrong].choiceValue;
  //     final choice = (result == true)
  //         ? question[a].choices[answerCorrect].choice
  //         : question[a].choices[answerWrong].choice;

  //     answerChoices.add(
  //       answerChoice.copyWith(
  //         questionId: question[a].questionId,
  //         questionValue: question[a].questionValue,
  //         title: question[a].title,
  //         choiceId: choiceId,
  //         choiceValue: choiceValue,
  //         choice: choice,
  //       ),
  //     );
  //   }

  //   return answerResult.copyWith(
  //     modelId: modelId!,
  //     uuid: deviceInfo!.id!,
  //     questionChoiceItem: answerChoices,
  //   );
  // }
}
