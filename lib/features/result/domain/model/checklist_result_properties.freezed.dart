// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'checklist_result_properties.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CheckListResultProperties {
  ///เป็น dynamic ก็จริง แต่อย่าใส่อะไรนอกจาก [String] หรือ [IconData]
  ///
  ///ไม่งั้นพังแน่นอน
  dynamic get icon => throw _privateConstructorUsedError;
  AppRoutes get appRoutesPage => throw _privateConstructorUsedError;
  String get textUpper => throw _privateConstructorUsedError;
  String get textLower => throw _privateConstructorUsedError;
  PermissionToCheckStatusEnum? get permissionStatusEnum =>
      throw _privateConstructorUsedError;
  AsyncValue<PermissionToCheckStatus>? get state =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CheckListResultPropertiesCopyWith<CheckListResultProperties> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CheckListResultPropertiesCopyWith<$Res> {
  factory $CheckListResultPropertiesCopyWith(CheckListResultProperties value,
          $Res Function(CheckListResultProperties) then) =
      _$CheckListResultPropertiesCopyWithImpl<$Res, CheckListResultProperties>;
  @useResult
  $Res call(
      {dynamic icon,
      AppRoutes appRoutesPage,
      String textUpper,
      String textLower,
      PermissionToCheckStatusEnum? permissionStatusEnum,
      AsyncValue<PermissionToCheckStatus>? state});
}

/// @nodoc
class _$CheckListResultPropertiesCopyWithImpl<$Res,
        $Val extends CheckListResultProperties>
    implements $CheckListResultPropertiesCopyWith<$Res> {
  _$CheckListResultPropertiesCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? icon = freezed,
    Object? appRoutesPage = null,
    Object? textUpper = null,
    Object? textLower = null,
    Object? permissionStatusEnum = freezed,
    Object? state = freezed,
  }) {
    return _then(_value.copyWith(
      icon: freezed == icon
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as dynamic,
      appRoutesPage: null == appRoutesPage
          ? _value.appRoutesPage
          : appRoutesPage // ignore: cast_nullable_to_non_nullable
              as AppRoutes,
      textUpper: null == textUpper
          ? _value.textUpper
          : textUpper // ignore: cast_nullable_to_non_nullable
              as String,
      textLower: null == textLower
          ? _value.textLower
          : textLower // ignore: cast_nullable_to_non_nullable
              as String,
      permissionStatusEnum: freezed == permissionStatusEnum
          ? _value.permissionStatusEnum
          : permissionStatusEnum // ignore: cast_nullable_to_non_nullable
              as PermissionToCheckStatusEnum?,
      state: freezed == state
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as AsyncValue<PermissionToCheckStatus>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CheckListResultPropertiesImplCopyWith<$Res>
    implements $CheckListResultPropertiesCopyWith<$Res> {
  factory _$$CheckListResultPropertiesImplCopyWith(
          _$CheckListResultPropertiesImpl value,
          $Res Function(_$CheckListResultPropertiesImpl) then) =
      __$$CheckListResultPropertiesImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {dynamic icon,
      AppRoutes appRoutesPage,
      String textUpper,
      String textLower,
      PermissionToCheckStatusEnum? permissionStatusEnum,
      AsyncValue<PermissionToCheckStatus>? state});
}

/// @nodoc
class __$$CheckListResultPropertiesImplCopyWithImpl<$Res>
    extends _$CheckListResultPropertiesCopyWithImpl<$Res,
        _$CheckListResultPropertiesImpl>
    implements _$$CheckListResultPropertiesImplCopyWith<$Res> {
  __$$CheckListResultPropertiesImplCopyWithImpl(
      _$CheckListResultPropertiesImpl _value,
      $Res Function(_$CheckListResultPropertiesImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? icon = freezed,
    Object? appRoutesPage = null,
    Object? textUpper = null,
    Object? textLower = null,
    Object? permissionStatusEnum = freezed,
    Object? state = freezed,
  }) {
    return _then(_$CheckListResultPropertiesImpl(
      icon: freezed == icon
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as dynamic,
      appRoutesPage: null == appRoutesPage
          ? _value.appRoutesPage
          : appRoutesPage // ignore: cast_nullable_to_non_nullable
              as AppRoutes,
      textUpper: null == textUpper
          ? _value.textUpper
          : textUpper // ignore: cast_nullable_to_non_nullable
              as String,
      textLower: null == textLower
          ? _value.textLower
          : textLower // ignore: cast_nullable_to_non_nullable
              as String,
      permissionStatusEnum: freezed == permissionStatusEnum
          ? _value.permissionStatusEnum
          : permissionStatusEnum // ignore: cast_nullable_to_non_nullable
              as PermissionToCheckStatusEnum?,
      state: freezed == state
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as AsyncValue<PermissionToCheckStatus>?,
    ));
  }
}

/// @nodoc

class _$CheckListResultPropertiesImpl extends _CheckListResultProperties {
  const _$CheckListResultPropertiesImpl(
      {required this.icon,
      required this.appRoutesPage,
      this.textUpper = "",
      this.textLower = "",
      this.permissionStatusEnum,
      this.state})
      : super._();

  ///เป็น dynamic ก็จริง แต่อย่าใส่อะไรนอกจาก [String] หรือ [IconData]
  ///
  ///ไม่งั้นพังแน่นอน
  @override
  final dynamic icon;
  @override
  final AppRoutes appRoutesPage;
  @override
  @JsonKey()
  final String textUpper;
  @override
  @JsonKey()
  final String textLower;
  @override
  final PermissionToCheckStatusEnum? permissionStatusEnum;
  @override
  final AsyncValue<PermissionToCheckStatus>? state;

  @override
  String toString() {
    return 'CheckListResultProperties(icon: $icon, appRoutesPage: $appRoutesPage, textUpper: $textUpper, textLower: $textLower, permissionStatusEnum: $permissionStatusEnum, state: $state)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckListResultPropertiesImpl &&
            const DeepCollectionEquality().equals(other.icon, icon) &&
            (identical(other.appRoutesPage, appRoutesPage) ||
                other.appRoutesPage == appRoutesPage) &&
            (identical(other.textUpper, textUpper) ||
                other.textUpper == textUpper) &&
            (identical(other.textLower, textLower) ||
                other.textLower == textLower) &&
            (identical(other.permissionStatusEnum, permissionStatusEnum) ||
                other.permissionStatusEnum == permissionStatusEnum) &&
            (identical(other.state, state) || other.state == state));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(icon),
      appRoutesPage,
      textUpper,
      textLower,
      permissionStatusEnum,
      state);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckListResultPropertiesImplCopyWith<_$CheckListResultPropertiesImpl>
      get copyWith => __$$CheckListResultPropertiesImplCopyWithImpl<
          _$CheckListResultPropertiesImpl>(this, _$identity);
}

abstract class _CheckListResultProperties extends CheckListResultProperties {
  const factory _CheckListResultProperties(
          {required final dynamic icon,
          required final AppRoutes appRoutesPage,
          final String textUpper,
          final String textLower,
          final PermissionToCheckStatusEnum? permissionStatusEnum,
          final AsyncValue<PermissionToCheckStatus>? state}) =
      _$CheckListResultPropertiesImpl;
  const _CheckListResultProperties._() : super._();

  @override

  ///เป็น dynamic ก็จริง แต่อย่าใส่อะไรนอกจาก [String] หรือ [IconData]
  ///
  ///ไม่งั้นพังแน่นอน
  dynamic get icon;
  @override
  AppRoutes get appRoutesPage;
  @override
  String get textUpper;
  @override
  String get textLower;
  @override
  PermissionToCheckStatusEnum? get permissionStatusEnum;
  @override
  AsyncValue<PermissionToCheckStatus>? get state;
  @override
  @JsonKey(ignore: true)
  _$$CheckListResultPropertiesImplCopyWith<_$CheckListResultPropertiesImpl>
      get copyWith => throw _privateConstructorUsedError;
}
