// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'issues_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$IssuesModelImpl _$$IssuesModelImplFromJson(Map<String, dynamic> json) =>
    _$IssuesModelImpl(
      code: json['code'] as String? ?? "",
      createdAt: DateTime.parse(json['created_at'] as String),
      questionChoiceItem: (json['test_results'] as List<dynamic>)
          .map((e) => TestResultModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$IssuesModelImplToJson(_$IssuesModelImpl instance) =>
    <String, dynamic>{
      'code': instance.code,
      'created_at': instance.createdAt.toIso8601String(),
      'test_results': instance.questionChoiceItem,
    };

_$TestResultModelImpl _$$TestResultModelImplFromJson(
        Map<String, dynamic> json) =>
    _$TestResultModelImpl(
      title: json['title'] as String? ?? "",
      hardware_id: (json['hardware_id'] as num?)?.toInt(),
      result: json['result'] as bool? ?? false,
    );

Map<String, dynamic> _$$TestResultModelImplToJson(
        _$TestResultModelImpl instance) =>
    <String, dynamic>{
      'title': instance.title,
      'hardware_id': instance.hardware_id,
      'result': instance.result,
    };
