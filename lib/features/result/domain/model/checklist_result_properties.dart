import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:techswop_lite/const/enums/routing_enums.dart';
import 'package:techswop_lite/features/preparation/domain/permission_status.dart';
import 'package:techswop_lite/features/preparation/presentation/state/permission_check_state.dart';

part 'checklist_result_properties.freezed.dart';

@freezed
sealed class CheckListResultProperties with _$CheckListResultProperties {
  const factory CheckListResultProperties({
    ///เป็น dynamic ก็จริง แต่อย่าใส่อะไรนอกจาก [String] หรือ [IconData]
    ///
    ///ไม่งั้นพังแน่นอน
    required dynamic icon,
    required AppRoutes appRoutesPage,
    @Default("") String textUpper,
    @Default("") String textLower,
    PermissionToCheckStatusEnum? permissionStatusEnum,
    AsyncValue<PermissionToCheckStatus>? state,
  }) = _CheckListResultProperties;

  const CheckListResultProperties._();
}