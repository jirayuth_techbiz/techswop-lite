// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'issues_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

IssuesModel _$IssuesModelFromJson(Map<String, dynamic> json) {
  return _IssuesModel.fromJson(json);
}

/// @nodoc
mixin _$IssuesModel {
  String get code => throw _privateConstructorUsedError;
  @JsonKey(name: "created_at")
  DateTime get createdAt => throw _privateConstructorUsedError;
  @JsonKey(name: "test_results")
  List<TestResultModel> get questionChoiceItem =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $IssuesModelCopyWith<IssuesModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IssuesModelCopyWith<$Res> {
  factory $IssuesModelCopyWith(
          IssuesModel value, $Res Function(IssuesModel) then) =
      _$IssuesModelCopyWithImpl<$Res, IssuesModel>;
  @useResult
  $Res call(
      {String code,
      @JsonKey(name: "created_at") DateTime createdAt,
      @JsonKey(name: "test_results") List<TestResultModel> questionChoiceItem});
}

/// @nodoc
class _$IssuesModelCopyWithImpl<$Res, $Val extends IssuesModel>
    implements $IssuesModelCopyWith<$Res> {
  _$IssuesModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? code = null,
    Object? createdAt = null,
    Object? questionChoiceItem = null,
  }) {
    return _then(_value.copyWith(
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      questionChoiceItem: null == questionChoiceItem
          ? _value.questionChoiceItem
          : questionChoiceItem // ignore: cast_nullable_to_non_nullable
              as List<TestResultModel>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$IssuesModelImplCopyWith<$Res>
    implements $IssuesModelCopyWith<$Res> {
  factory _$$IssuesModelImplCopyWith(
          _$IssuesModelImpl value, $Res Function(_$IssuesModelImpl) then) =
      __$$IssuesModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String code,
      @JsonKey(name: "created_at") DateTime createdAt,
      @JsonKey(name: "test_results") List<TestResultModel> questionChoiceItem});
}

/// @nodoc
class __$$IssuesModelImplCopyWithImpl<$Res>
    extends _$IssuesModelCopyWithImpl<$Res, _$IssuesModelImpl>
    implements _$$IssuesModelImplCopyWith<$Res> {
  __$$IssuesModelImplCopyWithImpl(
      _$IssuesModelImpl _value, $Res Function(_$IssuesModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? code = null,
    Object? createdAt = null,
    Object? questionChoiceItem = null,
  }) {
    return _then(_$IssuesModelImpl(
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as String,
      createdAt: null == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime,
      questionChoiceItem: null == questionChoiceItem
          ? _value._questionChoiceItem
          : questionChoiceItem // ignore: cast_nullable_to_non_nullable
              as List<TestResultModel>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$IssuesModelImpl extends _IssuesModel {
  _$IssuesModelImpl(
      {this.code = "",
      @JsonKey(name: "created_at") required this.createdAt,
      @JsonKey(name: "test_results")
      required final List<TestResultModel> questionChoiceItem})
      : _questionChoiceItem = questionChoiceItem,
        super._();

  factory _$IssuesModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$IssuesModelImplFromJson(json);

  @override
  @JsonKey()
  final String code;
  @override
  @JsonKey(name: "created_at")
  final DateTime createdAt;
  final List<TestResultModel> _questionChoiceItem;
  @override
  @JsonKey(name: "test_results")
  List<TestResultModel> get questionChoiceItem {
    if (_questionChoiceItem is EqualUnmodifiableListView)
      return _questionChoiceItem;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_questionChoiceItem);
  }

  @override
  String toString() {
    return 'IssuesModel(code: $code, createdAt: $createdAt, questionChoiceItem: $questionChoiceItem)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$IssuesModelImpl &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            const DeepCollectionEquality()
                .equals(other._questionChoiceItem, _questionChoiceItem));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, code, createdAt,
      const DeepCollectionEquality().hash(_questionChoiceItem));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$IssuesModelImplCopyWith<_$IssuesModelImpl> get copyWith =>
      __$$IssuesModelImplCopyWithImpl<_$IssuesModelImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$IssuesModelImplToJson(
      this,
    );
  }
}

abstract class _IssuesModel extends IssuesModel {
  factory _IssuesModel(
          {final String code,
          @JsonKey(name: "created_at") required final DateTime createdAt,
          @JsonKey(name: "test_results")
          required final List<TestResultModel> questionChoiceItem}) =
      _$IssuesModelImpl;
  _IssuesModel._() : super._();

  factory _IssuesModel.fromJson(Map<String, dynamic> json) =
      _$IssuesModelImpl.fromJson;

  @override
  String get code;
  @override
  @JsonKey(name: "created_at")
  DateTime get createdAt;
  @override
  @JsonKey(name: "test_results")
  List<TestResultModel> get questionChoiceItem;
  @override
  @JsonKey(ignore: true)
  _$$IssuesModelImplCopyWith<_$IssuesModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

TestResultModel _$TestResultModelFromJson(Map<String, dynamic> json) {
  return _TestResultModel.fromJson(json);
}

/// @nodoc
mixin _$TestResultModel {
  String get title =>
      throw _privateConstructorUsedError; // ignore: non_constant_identifier_names
  int? get hardware_id => throw _privateConstructorUsedError;
  bool get result => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TestResultModelCopyWith<TestResultModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TestResultModelCopyWith<$Res> {
  factory $TestResultModelCopyWith(
          TestResultModel value, $Res Function(TestResultModel) then) =
      _$TestResultModelCopyWithImpl<$Res, TestResultModel>;
  @useResult
  $Res call({String title, int? hardware_id, bool result});
}

/// @nodoc
class _$TestResultModelCopyWithImpl<$Res, $Val extends TestResultModel>
    implements $TestResultModelCopyWith<$Res> {
  _$TestResultModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? hardware_id = freezed,
    Object? result = null,
  }) {
    return _then(_value.copyWith(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      hardware_id: freezed == hardware_id
          ? _value.hardware_id
          : hardware_id // ignore: cast_nullable_to_non_nullable
              as int?,
      result: null == result
          ? _value.result
          : result // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$TestResultModelImplCopyWith<$Res>
    implements $TestResultModelCopyWith<$Res> {
  factory _$$TestResultModelImplCopyWith(_$TestResultModelImpl value,
          $Res Function(_$TestResultModelImpl) then) =
      __$$TestResultModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String title, int? hardware_id, bool result});
}

/// @nodoc
class __$$TestResultModelImplCopyWithImpl<$Res>
    extends _$TestResultModelCopyWithImpl<$Res, _$TestResultModelImpl>
    implements _$$TestResultModelImplCopyWith<$Res> {
  __$$TestResultModelImplCopyWithImpl(
      _$TestResultModelImpl _value, $Res Function(_$TestResultModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
    Object? hardware_id = freezed,
    Object? result = null,
  }) {
    return _then(_$TestResultModelImpl(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      hardware_id: freezed == hardware_id
          ? _value.hardware_id
          : hardware_id // ignore: cast_nullable_to_non_nullable
              as int?,
      result: null == result
          ? _value.result
          : result // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$TestResultModelImpl extends _TestResultModel {
  _$TestResultModelImpl(
      {this.title = "", this.hardware_id, this.result = false})
      : super._();

  factory _$TestResultModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$TestResultModelImplFromJson(json);

  @override
  @JsonKey()
  final String title;
// ignore: non_constant_identifier_names
  @override
  final int? hardware_id;
  @override
  @JsonKey()
  final bool result;

  @override
  String toString() {
    return 'TestResultModel(title: $title, hardware_id: $hardware_id, result: $result)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TestResultModelImpl &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.hardware_id, hardware_id) ||
                other.hardware_id == hardware_id) &&
            (identical(other.result, result) || other.result == result));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, title, hardware_id, result);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$TestResultModelImplCopyWith<_$TestResultModelImpl> get copyWith =>
      __$$TestResultModelImplCopyWithImpl<_$TestResultModelImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$TestResultModelImplToJson(
      this,
    );
  }
}

abstract class _TestResultModel extends TestResultModel {
  factory _TestResultModel(
      {final String title,
      final int? hardware_id,
      final bool result}) = _$TestResultModelImpl;
  _TestResultModel._() : super._();

  factory _TestResultModel.fromJson(Map<String, dynamic> json) =
      _$TestResultModelImpl.fromJson;

  @override
  String get title;
  @override // ignore: non_constant_identifier_names
  int? get hardware_id;
  @override
  bool get result;
  @override
  @JsonKey(ignore: true)
  _$$TestResultModelImplCopyWith<_$TestResultModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
