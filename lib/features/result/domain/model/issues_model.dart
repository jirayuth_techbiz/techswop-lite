import 'package:freezed_annotation/freezed_annotation.dart';

part 'issues_model.g.dart';
part 'issues_model.freezed.dart';

@freezed
class IssuesModel with _$IssuesModel {
  factory IssuesModel({
    @Default("") String code,
    @JsonKey(name: "created_at") required DateTime createdAt,
    @JsonKey(name: "test_results") required List<TestResultModel> questionChoiceItem,
  }) = _IssuesModel;

  const IssuesModel._();

  factory IssuesModel.fromJson(Map<String, dynamic> json) =>
      _$IssuesModelFromJson(json);
} 

@freezed
class TestResultModel with _$TestResultModel {
  factory TestResultModel({
    @Default("") String title,
    // ignore: non_constant_identifier_names
    int? hardware_id,
    @Default(false) bool result,
  }) = _TestResultModel;

  const TestResultModel._();

  factory TestResultModel.fromJson(Map<String, dynamic> json) =>
      _$TestResultModelFromJson(json);
}