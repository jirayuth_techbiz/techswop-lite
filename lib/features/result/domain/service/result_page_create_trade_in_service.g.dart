// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'result_page_create_trade_in_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$resultPageCreateTradeInServiceHash() =>
    r'4943d5e51977447d0ca42a9358476799caabe6b4';

/// See also [resultPageCreateTradeInService].
@ProviderFor(resultPageCreateTradeInService)
final resultPageCreateTradeInServiceProvider =
    AutoDisposeProvider<ResultPageCreateTradeInService>.internal(
  resultPageCreateTradeInService,
  name: r'resultPageCreateTradeInServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$resultPageCreateTradeInServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef ResultPageCreateTradeInServiceRef
    = AutoDisposeProviderRef<ResultPageCreateTradeInService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
