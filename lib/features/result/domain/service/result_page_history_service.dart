import 'package:flutter/widgets.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/result/application/result_page_history_service_impl.dart';
import 'package:techswop_lite/features/result/domain/model/checklist_result_properties.dart';
import 'package:techswop_lite/features/result/domain/model/issues_model.dart';
import 'package:techswop_lite/features/result/domain/repository/result_page_repository.dart';
import 'package:techswop_lite/shared/domain/repository/device_info_repository.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/shared/domain/repository/trade_in_secure_storage_repository.dart';

part 'result_page_history_service.g.dart';

@riverpod 
ResultPageHistoryService resultPageHistoryService(ResultPageHistoryServiceRef ref) {
    final resultPageRepository = ref.watch(resultPageRepositoryProvider);
  final tradeinSecureStorageRepository =
      ref.watch(tradeinSecureStorageRepositoryProvider);
  final tradeInSharePreferencesRepository =
      ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final deviceInfoRepository = ref.watch(deviceInfoRepositoryProvider);
  final loggerRepository = ref.watch(loggerRepositoryProvider);

  return ResultPageHistoryServiceImpl(
    ref: ref,
    logger: loggerRepository,
    resultPageRepository: resultPageRepository,
    tradeInSharePreferencesRepository: tradeInSharePreferencesRepository,
    tradeinSecureStorageRepository: tradeinSecureStorageRepository,
    deviceInfoRepository: deviceInfoRepository,
  );
}

abstract class ResultPageHistoryService {
  List<Map<CheckListResultProperties, bool>> buildPassListProperties(
      List<TestResultModel> testResult);
  List<Map<CheckListResultProperties, bool>> buildFailListProperties(
      List<TestResultModel> testResult);
  Future<void> saveQRCodeImage(
      {required Widget widget,
      required String datetime,
      required BuildContext context});

  Future<bool> checkIsQRImageDuplicated(String keyResult);
}
