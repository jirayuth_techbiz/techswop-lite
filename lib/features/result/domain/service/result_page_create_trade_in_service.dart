import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/result/application/result_page_create_trade_in_service_impl.dart';
import 'package:techswop_lite/features/result/domain/model/issues_model.dart';
import 'package:techswop_lite/features/result/domain/repository/result_page_repository.dart';
import 'package:techswop_lite/shared/domain/repository/device_info_repository.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/shared/domain/repository/trade_in_secure_storage_repository.dart';
import 'package:uuid/uuid.dart';

part 'result_page_create_trade_in_service.g.dart';

@riverpod
ResultPageCreateTradeInService resultPageCreateTradeInService(ResultPageCreateTradeInServiceRef ref) {
  final resultPageRepository = ref.watch(resultPageRepositoryProvider);
  final tradeinSecureStorageRepository =
      ref.watch(tradeinSecureStorageRepositoryProvider);
  final tradeInSharePreferencesRepository =
      ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final deviceInfoRepository = ref.watch(deviceInfoRepositoryProvider);
  final loggerRepository = ref.watch(loggerRepositoryProvider);
  const Uuid uuid = Uuid();

  return ResultPageCreateTradeInServiceImpl(
    ref: ref,
    logger: loggerRepository,
    resultPageRepository: resultPageRepository,
    tradeInSharePreferencesRepository: tradeInSharePreferencesRepository,
    tradeinSecureStorageRepository: tradeinSecureStorageRepository,
    deviceInfoRepository: deviceInfoRepository,
    uuid: uuid,
  );
}

abstract class ResultPageCreateTradeInService {
  Future<IssuesModel> createTradeinCheckListResult();
}
