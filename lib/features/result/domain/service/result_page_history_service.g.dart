// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'result_page_history_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$resultPageHistoryServiceHash() =>
    r'572f0592965a4b4d3185835879e52f266d7b2495';

/// See also [resultPageHistoryService].
@ProviderFor(resultPageHistoryService)
final resultPageHistoryServiceProvider =
    AutoDisposeProvider<ResultPageHistoryService>.internal(
  resultPageHistoryService,
  name: r'resultPageHistoryServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$resultPageHistoryServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef ResultPageHistoryServiceRef
    = AutoDisposeProviderRef<ResultPageHistoryService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
