import 'package:flutter/widgets.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/result/data/datasource/image_gallery_saver_wrapper.dart';
import 'package:techswop_lite/features/result/data/repository/result_page_repository_impl.dart';
import 'package:techswop_lite/shared/shared.dart';

part 'result_page_repository.g.dart';

@riverpod
ResultPageRepository resultPageRepository(ResultPageRepositoryRef ref) {
  final imageGallerySaver = ImageGallerySaverWrapper();
  final loggerRepository = ref.watch(loggerRepositoryProvider);

  return ResultPageRepositoryImpl(
    imageGallerySaverWrapper: imageGallerySaver,
    loggerRepository: loggerRepository,
  );
}

abstract class ResultPageRepository {
  Future<void> saveQRCodeImage({required Widget widget, required String datetime, required BuildContext context});
}
