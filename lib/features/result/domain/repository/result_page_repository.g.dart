// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'result_page_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$resultPageRepositoryHash() =>
    r'62a846c47119f92cb28f1862916b90192d6dfe73';

/// See also [resultPageRepository].
@ProviderFor(resultPageRepository)
final resultPageRepositoryProvider =
    AutoDisposeProvider<ResultPageRepository>.internal(
  resultPageRepository,
  name: r'resultPageRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$resultPageRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef ResultPageRepositoryRef = AutoDisposeProviderRef<ResultPageRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
