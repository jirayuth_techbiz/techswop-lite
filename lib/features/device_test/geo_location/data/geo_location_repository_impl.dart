import 'dart:async';
import 'dart:io';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:geolocator/geolocator.dart';
import 'package:techswop_lite/features/device_test/geo_location/data/geo_location_wrapper.dart';
import 'package:techswop_lite/features/device_test/geo_location/domain/repository/geo_location_repository.dart';
import 'package:techswop_lite/shared/shared.dart';

class GeolocationCheckRepositoryImpl implements GeolocationCheckRepository {
  final Ref ref;
  final GeolocatorWrapper geolocator;
  final LoggerRepository loggerRepository;
  final DeviceInfoRepository deviceInfo;

  GeolocationCheckRepositoryImpl(
    this.ref, 
    {
    required this.geolocator,
    required this.loggerRepository,
    required this.deviceInfo,
  });

  @override
  Stream<ServiceStatus> isGPSEnabled() async* {
    ///อ่าน Service Status จาก Notification Bar หรือจากหน้าการตั้งค่า
    ///เอาไว้ใช้ในหน้า Preparation

    final StreamController<ServiceStatus> statusStream =
        StreamController<ServiceStatus>();
    StreamSubscription? serviceStatusSubscription;

    ///https://github.com/Baseflow/flutter-geolocator/issues/1180
    ///[getServiceStatusStream] จะไม่คืนค่าถ้าไม่มีการเปลี่ยนแปลง เป็น
    ///Expected Behavior ของ Geolocator
    ///
    ///วิธีแก้คือให้ yield ค่าเริ่มต้นคืนไปหา Listener ข้างนอกก่อน
    ///แล้วค่อย Delegate Stream Subscription ตามเข้าไป
    yield await geolocator.isLocationServiceEnabled().then(
        (value) => value ? ServiceStatus.enabled : ServiceStatus.disabled);

    serviceStatusSubscription =
        geolocator.getServiceStatusStream().listen((ServiceStatus status) {
      loggerRepository.logInfo("isGPSEnabled : ${status.toString()}");
      statusStream.sink.add(status);
    });

    ref.onDispose(() {
      serviceStatusSubscription?.cancel();
      statusStream.close();
      loggerRepository.logDebug('GeolocationRepository disposed');
    });

    yield* statusStream.stream;
  }

  @override
  Stream<Position> getCurrentLocation() async* {
    final currentPosition = StreamController<Position>();
    final device = await deviceInfo.getDeviceInfo();

    if (Platform.isAndroid) {
      geolocator
          .getPositionStream(
              locationSettings: AndroidSettings(
        forceLocationManager: true,
      ))
          .listen((event) {
        loggerRepository.logInfo("getCurrentLocation : ${event.toString()}");
        currentPosition.sink.add(event);
      });
    } else {
      geolocator.getPositionStream().listen((event) {
        loggerRepository.logInfo("getCurrentLocation : ${event.toString()}");
        currentPosition.sink.add(event);
      });
    }

    ref.onDispose(() {
      currentPosition.close();
      log('GeolocationRepository disposed');
    });

    yield* currentPosition.stream;
  }

  @override
  Future<bool> isLocationServiceEnabled() async {
    final status = await geolocator.isLocationServiceEnabled();
    loggerRepository.logInfo("isLocationServiceEnabled : ${status.toString()}");
    return status;
  }

  @override
  Future<bool> isLocationPermissionGranted() async {
    final permission = await geolocator.checkPermission();
    loggerRepository
        .logInfo("isLocationPermissionGranted : ${permission.toString()}");
    if (permission == LocationPermission.denied ||
        permission == LocationPermission.deniedForever) {
      return false;
    }
    return true;
  }
}
