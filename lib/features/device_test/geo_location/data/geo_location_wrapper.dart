import 'package:geolocator/geolocator.dart';

class GeolocatorWrapper {
  Stream<ServiceStatus> getServiceStatusStream() => Geolocator.getServiceStatusStream();
  Stream<Position> getPositionStream({LocationSettings? locationSettings}) => Geolocator.getPositionStream(locationSettings: locationSettings);
  Future<bool> isLocationServiceEnabled() => Geolocator.isLocationServiceEnabled();
  Future<LocationPermission> checkPermission() => Geolocator.checkPermission();
}