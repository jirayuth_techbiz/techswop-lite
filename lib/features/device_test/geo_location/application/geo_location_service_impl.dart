import 'dart:async';
import 'dart:developer' as developer;

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/geo_location/domain/geo_location_check.dart';
import 'package:techswop_lite/features/device_test/geo_location/domain/repository/geo_location_repository.dart';
import 'package:techswop_lite/features/device_test/geo_location/domain/service/geo_location_service.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/const/const.dart';

class GeolocationCheckServiceImpl implements GeolocationCheckService {
  final Ref ref;
  final GeolocationCheckRepository _locationRepository;
  final TradeInSharePreferencesRepository _sharePreferencesRepositoryProvider;
  final LoggerRepository _loggerRepository;

  GeolocationCheckServiceImpl(this.ref, this._locationRepository,
      this._sharePreferencesRepositoryProvider, this._loggerRepository,);

  LoggerRepository get logger => _loggerRepository;

  @override
  Stream<LocationCheck> isGPSCheckComplete() async* {
    ///เอา Permission มาเช็คว่าเปิด GPS + อนุญาติสิทธิ์แล้วหรือยัง

    StreamController<LocationCheck> locationCheckController =
        StreamController<LocationCheck>();
    LocationCheck result = const LocationCheck();

    final isLocationPermissionGrantedResult =
        await _locationRepository.isLocationPermissionGranted();
    final isLocationServiceEnabledResult =
        await _locationRepository.isLocationServiceEnabled();

    try {
      result = result.copyWith(
        isLocationPermissionGranted: isLocationPermissionGrantedResult,
        isLocationServiceEnabled: isLocationServiceEnabledResult,
      );

      _locationRepository.getCurrentLocation().listen((event) {
        if (event.latitude != 0.0 && event.longitude != 0.0) {
          result = result.copyWith(
            isLocationDataReturn: true,
          );
          locationCheckController.sink.add(result);

          locationCheckController.close();
        }
      });

      locationCheckController.sink.add(result);

      yield* locationCheckController.stream;

      ref.onDispose(() {
        locationCheckController.close();
      });
    } catch (e) {
      logger.logDebug(e.toString());
      rethrow;
    }
  }

  @override
  Future<void> saveLocationCheck(bool status) async {
    ///บันทึกสถานะการเช็ค GPS ลง SharedPreference

    final pref = _sharePreferencesRepositoryProvider;
    try {
      await pref.saveBool(AppRoutes.geolocationCheckPage.titleAPI, status);
    } catch (e) {
      logger.logDebug(e.toString());
      rethrow;
    }
  }

  @override
  Future<bool?> getLocationCheck() async {
    ///ดึงสถานะการเช็ค GPS จาก SharedPreference

    final pref = _sharePreferencesRepositoryProvider;
    try {
      return pref.getBool(AppRoutes.geolocationCheckPage.titleAPI);
    } catch (e) {
      logger.logDebug(e.toString());
      rethrow;
    }
  }
}
