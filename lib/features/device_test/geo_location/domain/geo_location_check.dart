import 'package:freezed_annotation/freezed_annotation.dart';

part 'geo_location_check.freezed.dart';

@freezed
class LocationCheck with _$LocationCheck {
  const factory LocationCheck({
    @Default(false) bool isLocationPermissionGranted,
    @Default(false) bool isLocationServiceEnabled,
    @Default(false) bool isLocationDataReturn,
  }) = _LocationCheck;

  const LocationCheck._();
}