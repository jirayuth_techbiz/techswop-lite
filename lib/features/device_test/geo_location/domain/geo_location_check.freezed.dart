// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'geo_location_check.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$LocationCheck {
  bool get isLocationPermissionGranted => throw _privateConstructorUsedError;
  bool get isLocationServiceEnabled => throw _privateConstructorUsedError;
  bool get isLocationDataReturn => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LocationCheckCopyWith<LocationCheck> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LocationCheckCopyWith<$Res> {
  factory $LocationCheckCopyWith(
          LocationCheck value, $Res Function(LocationCheck) then) =
      _$LocationCheckCopyWithImpl<$Res, LocationCheck>;
  @useResult
  $Res call(
      {bool isLocationPermissionGranted,
      bool isLocationServiceEnabled,
      bool isLocationDataReturn});
}

/// @nodoc
class _$LocationCheckCopyWithImpl<$Res, $Val extends LocationCheck>
    implements $LocationCheckCopyWith<$Res> {
  _$LocationCheckCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLocationPermissionGranted = null,
    Object? isLocationServiceEnabled = null,
    Object? isLocationDataReturn = null,
  }) {
    return _then(_value.copyWith(
      isLocationPermissionGranted: null == isLocationPermissionGranted
          ? _value.isLocationPermissionGranted
          : isLocationPermissionGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isLocationServiceEnabled: null == isLocationServiceEnabled
          ? _value.isLocationServiceEnabled
          : isLocationServiceEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isLocationDataReturn: null == isLocationDataReturn
          ? _value.isLocationDataReturn
          : isLocationDataReturn // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$LocationCheckImplCopyWith<$Res>
    implements $LocationCheckCopyWith<$Res> {
  factory _$$LocationCheckImplCopyWith(
          _$LocationCheckImpl value, $Res Function(_$LocationCheckImpl) then) =
      __$$LocationCheckImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool isLocationPermissionGranted,
      bool isLocationServiceEnabled,
      bool isLocationDataReturn});
}

/// @nodoc
class __$$LocationCheckImplCopyWithImpl<$Res>
    extends _$LocationCheckCopyWithImpl<$Res, _$LocationCheckImpl>
    implements _$$LocationCheckImplCopyWith<$Res> {
  __$$LocationCheckImplCopyWithImpl(
      _$LocationCheckImpl _value, $Res Function(_$LocationCheckImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLocationPermissionGranted = null,
    Object? isLocationServiceEnabled = null,
    Object? isLocationDataReturn = null,
  }) {
    return _then(_$LocationCheckImpl(
      isLocationPermissionGranted: null == isLocationPermissionGranted
          ? _value.isLocationPermissionGranted
          : isLocationPermissionGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isLocationServiceEnabled: null == isLocationServiceEnabled
          ? _value.isLocationServiceEnabled
          : isLocationServiceEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isLocationDataReturn: null == isLocationDataReturn
          ? _value.isLocationDataReturn
          : isLocationDataReturn // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$LocationCheckImpl extends _LocationCheck {
  const _$LocationCheckImpl(
      {this.isLocationPermissionGranted = false,
      this.isLocationServiceEnabled = false,
      this.isLocationDataReturn = false})
      : super._();

  @override
  @JsonKey()
  final bool isLocationPermissionGranted;
  @override
  @JsonKey()
  final bool isLocationServiceEnabled;
  @override
  @JsonKey()
  final bool isLocationDataReturn;

  @override
  String toString() {
    return 'LocationCheck(isLocationPermissionGranted: $isLocationPermissionGranted, isLocationServiceEnabled: $isLocationServiceEnabled, isLocationDataReturn: $isLocationDataReturn)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LocationCheckImpl &&
            (identical(other.isLocationPermissionGranted,
                    isLocationPermissionGranted) ||
                other.isLocationPermissionGranted ==
                    isLocationPermissionGranted) &&
            (identical(
                    other.isLocationServiceEnabled, isLocationServiceEnabled) ||
                other.isLocationServiceEnabled == isLocationServiceEnabled) &&
            (identical(other.isLocationDataReturn, isLocationDataReturn) ||
                other.isLocationDataReturn == isLocationDataReturn));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isLocationPermissionGranted,
      isLocationServiceEnabled, isLocationDataReturn);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LocationCheckImplCopyWith<_$LocationCheckImpl> get copyWith =>
      __$$LocationCheckImplCopyWithImpl<_$LocationCheckImpl>(this, _$identity);
}

abstract class _LocationCheck extends LocationCheck {
  const factory _LocationCheck(
      {final bool isLocationPermissionGranted,
      final bool isLocationServiceEnabled,
      final bool isLocationDataReturn}) = _$LocationCheckImpl;
  const _LocationCheck._() : super._();

  @override
  bool get isLocationPermissionGranted;
  @override
  bool get isLocationServiceEnabled;
  @override
  bool get isLocationDataReturn;
  @override
  @JsonKey(ignore: true)
  _$$LocationCheckImplCopyWith<_$LocationCheckImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
