import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/geo_location/application/geo_location_service_impl.dart';
import 'package:techswop_lite/features/device_test/geo_location/domain/geo_location_check.dart';
import 'package:techswop_lite/features/device_test/geo_location/domain/repository/geo_location_repository.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

part 'geo_location_service.g.dart';

@riverpod
GeolocationCheckService geolocationService(GeolocationServiceRef ref) {
  final locationRepository = ref.watch(geoLocationCheckRepositoryProvider);
  final preferencesRepository =
      ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final loggerRepository = ref.watch(loggerRepositoryProvider);

  return GeolocationCheckServiceImpl(
    ref, 
    locationRepository, 
    preferencesRepository,
    loggerRepository,);
}

abstract class GeolocationCheckService {
  Stream<LocationCheck> isGPSCheckComplete();
  Future<bool?> getLocationCheck();
  Future<void> saveLocationCheck(bool status);
}
