// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geo_location_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$geolocationServiceHash() =>
    r'c67184cbc271895b01c643b9269ccf7975eb2266';

/// See also [geolocationService].
@ProviderFor(geolocationService)
final geolocationServiceProvider =
    AutoDisposeProvider<GeolocationCheckService>.internal(
  geolocationService,
  name: r'geolocationServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$geolocationServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef GeolocationServiceRef = AutoDisposeProviderRef<GeolocationCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
