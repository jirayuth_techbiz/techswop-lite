import 'package:geolocator/geolocator.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/geo_location/data/geo_location_repository_impl.dart';
import 'package:techswop_lite/features/device_test/geo_location/data/geo_location_wrapper.dart';
import 'package:techswop_lite/shared/domain/repository/device_info_repository.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';

part 'geo_location_repository.g.dart';

@riverpod
GeolocationCheckRepository geoLocationCheckRepository(
    GeoLocationCheckRepositoryRef ref) {
  final geolocator = GeolocatorWrapper();
  final logger = ref.watch(loggerRepositoryProvider);
  final deviceInfo = ref.watch(deviceInfoRepositoryProvider);

  return GeolocationCheckRepositoryImpl(ref, 
    geolocator: geolocator,
    loggerRepository: logger,
    deviceInfo: deviceInfo,
  );
}

abstract class GeolocationCheckRepository {
  Stream<Position> getCurrentLocation();
  Stream<ServiceStatus> isGPSEnabled();
  Future<bool> isLocationServiceEnabled();
  Future<bool> isLocationPermissionGranted();
}
