// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geo_location_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$geoLocationCheckRepositoryHash() =>
    r'a17018090b631a600cf0ad98ee248571b3f38750';

/// See also [geoLocationCheckRepository].
@ProviderFor(geoLocationCheckRepository)
final geoLocationCheckRepositoryProvider =
    AutoDisposeProvider<GeolocationCheckRepository>.internal(
  geoLocationCheckRepository,
  name: r'geoLocationCheckRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$geoLocationCheckRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef GeoLocationCheckRepositoryRef
    = AutoDisposeProviderRef<GeolocationCheckRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
