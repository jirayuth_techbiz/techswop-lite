// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'geolocation_check_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$geoLocationCheckStateHash() =>
    r'382c902ba588bc70cfc2b657221de0e9c144c56f';

/// See also [GeoLocationCheckState].
@ProviderFor(GeoLocationCheckState)
final geoLocationCheckStateProvider = AutoDisposeStreamNotifierProvider<
    GeoLocationCheckState, LocationCheck>.internal(
  GeoLocationCheckState.new,
  name: r'geoLocationCheckStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$geoLocationCheckStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$GeoLocationCheckState = AutoDisposeStreamNotifier<LocationCheck>;
String _$geoLocationCheckManagerHash() =>
    r'ae81d42395dc0cfb353eeeb24b0289b55211531f';

/// See also [GeoLocationCheckManager].
@ProviderFor(GeoLocationCheckManager)
final geoLocationCheckManagerProvider =
    AutoDisposeAsyncNotifierProvider<GeoLocationCheckManager, void>.internal(
  GeoLocationCheckManager.new,
  name: r'geoLocationCheckManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$geoLocationCheckManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$GeoLocationCheckManager = AutoDisposeAsyncNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
