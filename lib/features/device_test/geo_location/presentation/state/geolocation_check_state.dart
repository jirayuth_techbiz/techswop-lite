// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'dart:async';
import 'dart:developer' as developer;

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/geo_location/domain/geo_location_check.dart';
import 'package:techswop_lite/features/device_test/geo_location/domain/service/geo_location_service.dart';

part 'geolocation_check_state.g.dart';

@riverpod
class GeoLocationCheckState extends _$GeoLocationCheckState {
  @override
  Stream<LocationCheck> build() async* {
    final geoLocationService = ref.watch(geolocationServiceProvider);
    final res = geoLocationService.isGPSCheckComplete();
    yield* res;

    ref.onDispose(() {
      developer.log("LocationCheckState disposed");
    });
  }
}

@riverpod
class GeoLocationCheckManager extends _$GeoLocationCheckManager {
  @override 
  FutureOr<void> build() async {}

  Future<bool?> getLocationCheck() async {
    final geoLocationService = ref.watch(geolocationServiceProvider);
    return await geoLocationService.getLocationCheck();
  }

  Future<void> saveLocationCheck(bool status) async {
    final geoLocationService = ref.watch(geolocationServiceProvider);
    state = const AsyncLoading();
    state = await AsyncValue.guard(() => geoLocationService.saveLocationCheck(status));
  }
}