import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/test_result/test_failed_status_icon.dart';
import 'package:techswop_lite/features/device_test/geo_location/presentation/state/geolocation_check_state.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';

class GeolocationCheckPage extends ConsumerWidget {
  const GeolocationCheckPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = context.theme;
    final translation = context.localization;

    final geoLocationState = ref.watch(geoLocationCheckStateProvider);
    final getGeolocationPageNumber = ref.watch(
        getCurrentPageNumberProvider(AppRoutes.geolocationCheckPage.path));
    final geoLocationCheckManager =
        ref.watch(geoLocationCheckManagerProvider.notifier);

    ref.listen(geoLocationCheckStateProvider, (prev, next) {
      if (next.asData!.value.isLocationPermissionGranted &&
          next.asData!.value.isLocationServiceEnabled &&
          next.asData!.value.isLocationDataReturn) {
        handlingGeoLocationCheck(
          context: context,
          ref: ref,
          isCheck: true,
        );
      } else if (!next.asData!.value.isLocationServiceEnabled) {
        handlingGeoLocationCheck(
          context: context,
          ref: ref,
          isCheck: false,
          failedReason: translation.gpsPageTestFailed,
        );
      }
    });

    return TestPageWidgetLayout(
      nextPage: getGeolocationPageNumber,
      processNumber: "${translation.layoutStep} $getGeolocationPageNumber",
      title: translation.gpsPageTitle,
      pictureAssetDirectory: CommonConst.CHECKLIST_PROPERTIES
          .firstWhere((element) =>
              element.appRoutesPage == AppRoutes.geolocationCheckPage)
          .icon,
      underCard: AsyncValueWidget(
        value: geoLocationState,
        data: (result) {
          if (result.isLocationPermissionGranted &&
              result.isLocationServiceEnabled &&
              result.isLocationDataReturn) {
            return Center(
              child: TestSuccessStatusIcon(
                theme: theme,
              ),
            );
          } else if (!result.isLocationServiceEnabled) {
            return Center(
              child: TestFailedStatusIcon(
                theme: theme,
              ),
            );
          }

          return const SizedBox();
        },
        loading: () => const SizedBox(),
      ),
      cardBodyText: translation.gpsPageDescription,
      onSkip: () async {
        await geoLocationCheckManager.saveLocationCheck(false);
      },
    );
  }

  void handlingGeoLocationCheck(
      {required BuildContext context,
      required WidgetRef ref,
      required bool isCheck,
      String? failedReason}) {
    final isExecuted = ref.watch(resultIsExecutedStateProvider);
    final geoLocationCheckManager =
        ref.watch(geoLocationCheckManagerProvider.notifier);
    final resultHandler = TestCheckHandler(
      context: context,
      ref: ref,
      isCheck: isCheck,
      appRoutes: AppRoutes.geolocationCheckPage,
      stateProvider: geoLocationCheckStateProvider,
      isExecuted: isExecuted,
    );

    if (isExecuted) return;

    Future.delayed(
            const Duration(seconds: AppConfig.futureDelayedTestAwaitDuration))
        .then((_) async {
      await geoLocationCheckManager.saveLocationCheck(isCheck);
      log("geoLocationService : ${await geoLocationCheckManager.getLocationCheck()}");

      if (context.mounted) {
        await resultHandler.handlingStatus(
          onSkip: () async {
            await geoLocationCheckManager.saveLocationCheck(false);
          },
          failedReason: failedReason,
        );
      }
    });
  }
}
