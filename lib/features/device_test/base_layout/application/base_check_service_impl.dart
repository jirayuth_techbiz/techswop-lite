import 'package:techswop_lite/features/device_test/base_layout/domain/base_check_service.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/shared/domain/repository/test_timeout_repository.dart';

class BaseCheckServiceImpl implements BaseCheckService {
  final TradeInSharePreferencesRepository tradeinPrefsRepository;
  final TestTimerRepository testTimerRepository;

  BaseCheckServiceImpl({
    required this.tradeinPrefsRepository,
    required this.testTimerRepository,
  });

  @override
  Future<void> stopTimer() async {
    await testTimerRepository.stopTimer();
  }
  
  @override
  Future<void> resetData() async {
    await tradeinPrefsRepository.resetSharePrefs();
  }
}
