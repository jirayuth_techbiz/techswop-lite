import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/layout_widgets/process_number.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/layout_widgets/test_app_icon.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/layout_widgets/test_app_icon_svg.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/layout_widgets/test_guide_card.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/layout_widgets/test_title.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/test_result/test_cancelling_widget.dart';
import 'package:techswop_lite/features/result/domain/service/result_page_history_service.dart';
import 'package:techswop_lite/generated/app_localizations.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/route/state/route_state.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';

import 'package:techswop_lite/shared/shared.dart';

///สร้าง Layout สำหรับการทดสอบ โดยมีการแสดงหน้าจอ และปุ่มข้ามการทดสอบ
class TestPageWidgetLayout extends ConsumerStatefulWidget {
  ///แสดง Widget หลัก (ตรงคำแนะนำ/กลางจอ)
  final Widget? cardBody;

  ///Text ด้านใน Card คำแนะนำ
  final String? cardBodyText;

  ///สำหรับแสดง [Widget] ใส่ Icon จาก [icondata] หรือ [pictureAssetDirectory]
  final Widget? underCard;

  ///เลขหน้าขั้นตอนที่กำลังทำอยู่
  final String processNumber;

  ///สำหรับแสดง [Widget] ที่ต้องอยู่ด้านบนโซนคำแนะนำ
  final Widget? iconReplacer;

  ///สำหรับ Icon ใส่เป็น [IconData]
  ///
  ///https://api.flutter.dev/flutter/widgets/IconData-class.html
  final IconData? icondata;

  ///สำหรับ Svg ใส่เป็น [String] Directory จากโฟลเดอร์ assets
  ///
  ///https://pub.dev/packages/flutter_svg
  final String? pictureAssetDirectory;

  ///ชื่อหน้า
  final String title;

  ///เลขหน้าถัดไป ใช้กรณีที่ผู้ใช้กดปุ่มข้ามการทดสอบ
  final int nextPage;

  final void Function() onSkip;

  const TestPageWidgetLayout({
    super.key,
    this.cardBody,
    this.cardBodyText,
    this.underCard,
    this.iconReplacer,
    required this.nextPage,
    required this.title,
    this.icondata,
    this.pictureAssetDirectory = "",
    required this.onSkip,
    required this.processNumber,
  });

  @override
  ConsumerState<TestPageWidgetLayout> createState() =>
      _TestPageWidgetLayoutState();
}

class _TestPageWidgetLayoutState extends ConsumerState<TestPageWidgetLayout> {
  @override
  Widget build(BuildContext context) {
    final translation = context.localization;
    final theme = context.theme;
    final totalPageNumber =
        GoRouter.of(context).routerDelegate.currentConfiguration.matches;
    final prefs =
        ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
    final baseCheckService = ref.watch(baseCheckServiceProvider);
    final extra = GoRouterState.of(context).extra;
    final isFromResultPage = extra is bool ? extra : false;
    final routeState = ref.watch(routeStateProvider);

    return PopScope(
      canPop: false,
      onPopInvoked: (bool result) async {
        if (result == true) {
          if (context.mounted) {
            await prefs.resetSharePrefs();
            await baseCheckService.stopTimer();
            if (context.mounted) {
              context.goNamed(AppRoutes.preparingPage.name);
            }
          }
        }
        if (context.mounted) {
          await showDialog(
            context: context,
            builder: (context) {
              return TestCancellingPopScope(
                isFromResultPage: isFromResultPage,
              );
            },
          );
        }
      },
      child: SafeArea(
        top: false,
        child: Scaffold(
          extendBody: true,
          appBar: AppBar(
            backgroundColor: theme.colorScheme.primary,
            centerTitle: true,
            title: Padding(
              padding: EdgeInsets.only(left: 7.w),
              child: Text(
                translation.layoutPageTitle,
                textAlign: TextAlign.justify,
                style: theme.textTheme.titleMedium!.copyWith(
                  color: theme.colorScheme.onPrimary,
                ),
              ),
            ),
            leading: IconButton(
              color: theme.colorScheme.onPrimary,
              icon: const FaIcon(
                Icons.arrow_back_ios_new_outlined,
              ),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) => TestCancellingPopScope(
                    isFromResultPage: isFromResultPage,
                  ),
                );
              },
            ),
          ),
          resizeToAvoidBottomInset: false,
          body: LayoutBuilder(
            builder: (context, constraints) {
              bool shouldRenderSmallLayout = false;
              const double smallScreenHeight = 788;

              if (constraints.maxHeight <= smallScreenHeight) {
                shouldRenderSmallLayout = true;
              }

              return Visibility(
                visible: shouldRenderSmallLayout,
                replacement: _NormalLayoutTestPage(
                  isFromResultPage: isFromResultPage,
                  widget: widget,
                  totalPageNumber: totalPageNumber,
                  theme: theme,
                  translation: translation,
                ),
                child: _SmallLayoutTestPage(
                  isFromResultPage: isFromResultPage,
                  widget: widget,
                  totalPageNumber: totalPageNumber,
                  theme: theme,
                  translation: translation,
                ),
              );
            },
          ),
          bottomNavigationBar: _BottomTestButton(
            routeState: routeState,
            widget: widget,
            translation: translation,
            getNextPageNumber: widget.nextPage,
          ),
        ),
      ),
    );
  }
}

class _SmallLayoutTestPage extends ConsumerWidget {
  const _SmallLayoutTestPage({
    required this.widget,
    required this.totalPageNumber,
    required this.theme,
    required this.translation,
    required this.isFromResultPage,
  });

  final TestPageWidgetLayout widget;

  ///ต้องเอาจำนวนหน้าด้วยการดึงหา Length ของ [List]<[RouteBase]>
  ///
  ///ดังนั้นไม่ควร Refactor เป็น double หรือดึง length จาก [RouteMatchBase]
  final List<RouteMatchBase> totalPageNumber;
  final ThemeData theme;
  final AppLocalizations translation;
  final bool isFromResultPage;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: EdgeInsets.only(top: 15.sp),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          ProcessNumber(
            isFromResultPage: isFromResultPage,
            widget: widget,
            totalPageNumber: totalPageNumber,
            theme: theme,
            padding: EdgeInsets.only(top: 10.w, bottom: 12.h),
          ),

          ///ถ้ามีไอคอน
          TestAppIconSvg(
            widget: widget,
            padding: EdgeInsets.symmetric(vertical: 10.w),
            cardHeight: 85.h,
            cardWidth: 85.w,
            borderCircularRadius: 85.sp,
            iconSizeHeight: 48.r,
          ),

          TestAppIcon(
            widget: widget,
            theme: theme,
            padding: EdgeInsets.symmetric(vertical: 10.w),
            cardHeight: 85.h,
            cardWidth: 85.w,
            borderCircularRadius: 85.sp,
          ),

          TestTitle(
            padding: EdgeInsets.symmetric(vertical: 10.w),
            widget: widget,
            theme: theme,
          ),

          ///ถ้าไม่มีไอคอนด้านบน ให้แสดง Widget นี้แทน
          ///ตอนนี้ใช้แค่ในหน้า Front/Back Camera Check
          Padding(
            padding: EdgeInsets.only(top: 8.0.sp),
            child: widget.iconReplacer ?? const SizedBox(),
          ),

          ///คำแนะนำ
          TestGuideCard(
            theme: theme,
            translation: translation,
            widget: widget,
            containerMargin: EdgeInsets.symmetric(horizontal: 16.h),
            containerPadding:
                EdgeInsets.symmetric(vertical: 8.h, horizontal: 8.w),
            cardPadding: EdgeInsets.symmetric(vertical: 6.h, horizontal: 8.w),
            cardConstraints: BoxConstraints(minHeight: 165.h, minWidth: 225.w),
            guidePadding: EdgeInsets.only(bottom: 5.h),
          ),
        ],
      ),
    );
  }
}

class _NormalLayoutTestPage extends ConsumerWidget {
  const _NormalLayoutTestPage({
    required this.widget,
    required this.totalPageNumber,
    required this.theme,
    required this.translation,
    required this.isFromResultPage,
  });

  final TestPageWidgetLayout widget;
  final List<RouteMatchBase> totalPageNumber;
  final ThemeData theme;
  final AppLocalizations translation;
  final bool isFromResultPage;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: EdgeInsets.only(top: 25.sp),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          ///Process Number
          ProcessNumber(
            widget: widget,
            totalPageNumber: totalPageNumber,
            theme: theme,
            padding: EdgeInsets.only(top: 10.w, bottom: 16.h),
            isFromResultPage: isFromResultPage,
          ),

          ///Title
          TestTitle(
            widget: widget,
            theme: theme,
            padding: EdgeInsets.symmetric(vertical: 10.w),
          ),
          TestAppIconSvg(
            widget: widget,
            padding: EdgeInsets.all(20.sp),
            cardHeight: 145.h,
            cardWidth: 145.w,
            borderCircularRadius: 150.sp,
            iconSizeHeight: 72.sp,
          ),
          TestAppIcon(
            widget: widget,
            theme: theme,
            padding: EdgeInsets.all(20.sp),
            cardHeight: 145.h,
            cardWidth: 145.w,
            borderCircularRadius: 150.sp,
          ),

          ///ถ้าไม่มีไอคอนด้านบน ให้แสดง Widget นี้แทน
          ///ตอนนี้ใช้แค่ในหน้า Front/Back Camera Check
          Padding(
            padding: EdgeInsets.only(top: 16.0.sp),
            child: widget.iconReplacer ?? const SizedBox(),
          ),

          ///คำแนะนำ
          TestGuideCard(
            theme: theme,
            translation: translation,
            widget: widget,
            containerMargin: EdgeInsets.symmetric(horizontal: 24.h),
            containerPadding:
                EdgeInsets.symmetric(vertical: 24.h, horizontal: 16.w),
            cardPadding: EdgeInsets.symmetric(vertical: 24.h, horizontal: 16.w),
            cardConstraints: BoxConstraints(minHeight: 180.h, minWidth: 450.w),
            guidePadding: EdgeInsets.only(bottom: 16.h),
          ),
        ],
      ),
    );
  }
}

class _BottomTestButton extends ConsumerWidget {
  const _BottomTestButton({
    required this.routeState,
    required this.widget,
    required this.translation,
    required this.getNextPageNumber,
  });

  final AsyncValue<List<GoRoute>> routeState;
  final TestPageWidgetLayout widget;
  final AppLocalizations translation;
  final int getNextPageNumber;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = context.theme;
    final logger = ref.watch(loggerRepositoryProvider);
    final prefs =
        ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
    final extra = GoRouterState.of(context).extra;
    final isFromResultPage = extra is bool ? extra : false;
    final safeOnTapState = ref.watch(safeOnTapStateProvider);

    return AsyncValueWidget(
      value: routeState,
      data: (route) => Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            width: MediaQuery.of(context).size.width.w,
            padding: EdgeInsets.symmetric(vertical: 16.spMin),
            margin: EdgeInsets.symmetric(horizontal: 24.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.sp),
            ),
            child: widget.underCard ?? const SizedBox(),
          ),
          Container(
            width: MediaQuery.of(context).size.width.w,
            padding: EdgeInsets.only(bottom: 16.spMin),
            margin: EdgeInsets.symmetric(horizontal: 24.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.sp),
            ),
            child: TextButton(
              onPressed: () {
                if (safeOnTapState) return;
                ref.watch(safeOnTapStateProvider.notifier).safeOnTap();
                widget.onSkip.call();
                //TODO
                if (getNextPageNumber >= route.length || isFromResultPage) {
                  context.goNamed(AppRoutes.resultPage.name);
                } else {
                  context.goNamed(route[getNextPageNumber].name!);
                }
              },
              style: theme.textButtonTheme.style!.copyWith(
                minimumSize: WidgetStateProperty.resolveWith(
                  (_) {
                    return Size(MediaQuery.of(context).size.width.w, 45.sp);
                  },
                ),
                elevation: WidgetStateProperty.resolveWith((_) => 0),
                shape: WidgetStateProperty.resolveWith(
                  (_) {
                    return RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.sp),
                    );
                  },
                ),
                backgroundColor: WidgetStateProperty.resolveWith(
                  (_) {
                    return Colors.grey[100];
                  },
                ),
              ),
              child: Text(
                translation.layoutSkipButton,
                style: theme.textTheme.bodyMedium!.copyWith(
                  fontWeight: FontWeight.bold,
                  color: theme.colorScheme.onPrimaryContainer,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
