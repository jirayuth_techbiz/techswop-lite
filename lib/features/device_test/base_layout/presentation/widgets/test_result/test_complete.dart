import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lottie/lottie.dart';
import 'package:techswop_lite/generated/assets.gen.dart';
import 'package:techswop_lite/shared/shared.dart';

class TestCompletePage extends ConsumerWidget {
  const TestCompletePage({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = context.theme;
    final translation = context.localization;

    return PopScope(
      canPop: false,
      onPopInvoked: (result) async => false,
      child: Stack(
        children: [
          Opacity(
            opacity: 0.7,
            child: ModalBarrier(
              color: theme.colorScheme.shadow,
              dismissible: false,
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  height: 60.sp,
                  width: 60.sp,
                  child: Lottie.asset(
                    Assets.animation.testPassed,
                    frameRate: FrameRate.composition,
                    renderCache: RenderCache.raster,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                DefaultTextStyle(
                  style: theme.textTheme.headlineMedium!.copyWith(
                    color: theme.colorScheme.surface,
                  ),
                  child: Text(
                    translation.testSuccess,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
