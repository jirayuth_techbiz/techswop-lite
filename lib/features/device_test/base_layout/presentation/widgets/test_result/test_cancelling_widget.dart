import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/features/device_test/base_layout/domain/base_check_service.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/shared.dart';

class TestCancellingPopScope extends ConsumerWidget {
  const TestCancellingPopScope({
    super.key,
    required this.isFromResultPage,
  });

  final bool isFromResultPage;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = context.theme;
    final translation = context.localization;
    final baseCheckService = ref.watch(baseCheckServiceProvider);
    // final extra = GoRouterState.of(context).extra;
    // final isFromResultPage = extra is bool ? extra : false;

    return PopScope(
      canPop: false,
      onPopInvoked: (result) => false,
      child: Stack(
        children: [
          Opacity(
            opacity: 0.7,
            child: ModalBarrier(
              color: theme.colorScheme.shadow,
              dismissible: false,
            ),
          ),
          SizedBox(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                DecoratedBox(
                  decoration: BoxDecoration(
                    color: theme.colorScheme.errorContainer,
                    shape: BoxShape.circle,
                  ),
                  child: FaIcon(
                    FontAwesomeIcons.circleXmark,
                    size: 60.r,
                    color: theme.colorScheme.error,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(translation.testCancel,
                    textAlign: TextAlign.center,
                    style: theme.textTheme.bodyLarge!.copyWith(
                      color: theme.colorScheme.surface,
                    )),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 22, horizontal: 14),
                  child: Text(
                    translation.testCancelDescription,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.visible,
                    style: theme.textTheme.bodyMedium!.copyWith(
                      color: theme.colorScheme.surface,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 100.w,
                      constraints: BoxConstraints(
                        maxWidth: 120.w,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2.r),
                      ),
                      child: FilledButton(
                        onPressed: () async {
                          context.goNamed(AppRoutes.preparingPage.name);
                        },
                        style: theme.filledButtonTheme.style,
                        child: Text(translation.confirmTxt),
                      ),
                    ),
                    Container(
                      width: 100.w,
                      constraints: BoxConstraints(
                        maxWidth: 120.w,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.sp),
                      ),
                      child: ElevatedButton(
                        onPressed: () => context.pop(),
                        child: Text(translation.closeTxt),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
