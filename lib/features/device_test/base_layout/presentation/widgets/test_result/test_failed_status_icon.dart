
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/shared/shared.dart';

class TestFailedStatusIcon extends ConsumerWidget {
  const TestFailedStatusIcon({
    super.key,
    required this.theme,
  });

  final ThemeData theme;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return FaIcon(
      FontAwesomeIcons.circleXmark,
      color: theme.colorScheme.error,
      size: 40.r,
    );
  }
}
