
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/shared/shared.dart';

class TestSuccessStatusIcon extends ConsumerStatefulWidget {
  const TestSuccessStatusIcon({
    super.key,
    required this.theme,
  });

  final ThemeData theme;

  @override
  ConsumerState<TestSuccessStatusIcon> createState() => _TestSuccessStatusIconState();
}

class _TestSuccessStatusIconState extends ConsumerState<TestSuccessStatusIcon> {
  @override
  initState() {
    super.initState();

  }
  
  @override
  Widget build(BuildContext context) {
    final theme = context.theme;

    return FaIcon(
      FontAwesomeIcons.circleCheck,
      color: theme.colorScheme.primary,
      size: 40.r,
    );
  }
}
