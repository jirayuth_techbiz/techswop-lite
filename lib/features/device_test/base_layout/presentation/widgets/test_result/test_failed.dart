import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:lottie/lottie.dart';
import 'package:techswop_lite/const/enums/routing_enums.dart';
import 'package:techswop_lite/generated/app_localizations.dart';
import 'package:techswop_lite/generated/assets.gen.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/common_widgets/gradient_elevated_long_button.dart';
import 'package:techswop_lite/shared/shared.dart';

class TestFailedPage extends ConsumerWidget {
  final String? titleAPI;
  final GoRoute nextPageRoute;
  final Function? onNextPage;
  final String? failReason;
  final void Function()? onRestart;
  final Function onDialogClose;
  final bool isFromResultPage;

  const TestFailedPage({
    this.titleAPI,
    required this.nextPageRoute,
    this.onNextPage,
    this.failReason,
    this.onRestart,
    required this.onDialogClose,
    required this.isFromResultPage,
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final prefs =
        ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
    final theme = context.theme;
    final translation = context.localization;

    return PopScope(
      canPop: false,
      onPopInvoked: (result) => false,
      child: Stack(
        children: [
          Opacity(
            opacity: .7,
            child: ModalBarrier(
              color: theme.colorScheme.shadow,
              dismissible: false,
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _TestFailedErrorIcon(
                  theme: theme,
                ),
                const SizedBox(
                  height: 20,
                ),
                DefaultTextStyle(
                  style: theme.textTheme.titleMedium!.copyWith(
                    color: theme.colorScheme.surface,
                  ),
                  child: Column(
                    children: [
                      _TestFailureReasonWidget(
                        failReason: failReason,
                      ),
                      _TestFailedTextWidget(
                        translation: translation,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          _RestartButtonWidget(
                            prefs: prefs,
                            titleAPI: titleAPI ?? "",
                            onRestart: onRestart,
                            translation: translation,
                            onDialogClose: onDialogClose,
                          ),
                          SizedBox(
                            height: 8.h,
                          ),
                          _SkipButtonWidget(
                            isFromResultPage: isFromResultPage,
                            prefs: prefs,
                            titleAPI: titleAPI ?? "",
                            onNextPage: onNextPage,
                            theme: theme,
                            translation: translation,
                            onDialogClose: onDialogClose,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _TestFailedErrorIcon extends ConsumerStatefulWidget {
  const _TestFailedErrorIcon({
    super.key,
    required this.theme,
  });

  final ThemeData theme;

  @override
  ConsumerState<_TestFailedErrorIcon> createState() =>
      _TestFailedErrorIconState();
}

class _TestFailedErrorIconState extends ConsumerState<_TestFailedErrorIcon>
    with TickerProviderStateMixin {
  late final AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: 60.sp,
      width: 60.sp,
      child: Lottie.asset(
        Assets.animation.testFailed,
        frameRate: FrameRate.composition,
        renderCache: RenderCache.raster,
        controller: _animationController,
        onLoaded: (composition) {
          _animationController
            ..duration = const Duration(milliseconds: 900)
            ..forward();
        },
      ),
      // FaIcon(
      //   FontAwesomeIcons.xmark,
      //   size: 40,
      //   color: theme.colorScheme.error,
      // ),
    );
  }
}

class _TestFailedTextWidget extends ConsumerWidget {
  const _TestFailedTextWidget({
    required this.translation,
  });

  final AppLocalizations translation;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      padding: const EdgeInsets.only(bottom: 10),
      width: 200.w,
      child: Text(
        translation.testFailure,
        overflow: TextOverflow.clip,
        textAlign: TextAlign.center,
      ),
    );
  }
}

class _TestFailureReasonWidget extends ConsumerWidget {
  const _TestFailureReasonWidget({
    required this.failReason,
  });

  final String? failReason;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      padding: const EdgeInsets.only(bottom: 10),
      width: 200.w,
      child: Text(
        failReason ?? "",
        overflow: TextOverflow.clip,
        textAlign: TextAlign.center,
      ),
    );
  }
}

class _RestartButtonWidget extends ConsumerWidget {
  const _RestartButtonWidget({
    required this.prefs,
    required this.titleAPI,
    required this.onRestart,
    required this.translation,
    required this.onDialogClose,
  });

  final TradeInSharePreferencesRepository prefs;
  final String titleAPI;
  final void Function()? onRestart;
  final AppLocalizations translation;
  final Function onDialogClose;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      constraints: BoxConstraints(
        minWidth: 200.w,
        maxWidth: 400.w,
      ),
      child: SharedElevatedButton(
        onPressed: () async {
          context.pop();

          if (context.mounted) {
            await prefs.remove(
              titleAPI,
            );
          }
          onRestart?.call();
          onDialogClose.call();
        },
        text: translation.layoutPageRestart,
      ),
    );
  }
}

class _SkipButtonWidget extends ConsumerWidget {
  const _SkipButtonWidget({
    required this.prefs,
    required this.titleAPI,
    required this.onNextPage,
    required this.theme,
    required this.translation,
    required this.onDialogClose,
    required this.isFromResultPage,
  });

  final TradeInSharePreferencesRepository prefs;
  final String titleAPI;
  final Function? onNextPage;
  final ThemeData theme;
  final AppLocalizations translation;
  final Function onDialogClose;
  final bool isFromResultPage;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      constraints: BoxConstraints(
        minWidth: 200.w,
        maxWidth: 400.w,
      ),
      height: 45.sp,
      child: TextButton(
        onPressed: () async {
          await prefs.saveBool(
            titleAPI,
            false,
          );
          if (context.mounted && !isFromResultPage) {
            onDialogClose.call();
            onNextPage?.call();
          } else if (context.mounted && isFromResultPage) {
            
            context.goNamed(AppRoutes.preparingPage.name);
          }
        },
        style: theme.textButtonTheme.style!.copyWith(
          elevation: WidgetStateProperty.resolveWith((_) => 0),
          shape: WidgetStateProperty.resolveWith(
            (_) {
              return RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.sp),
              );
            },
          ),
          backgroundColor: WidgetStateProperty.resolveWith(
            (_) {
              return Colors.grey[100];
            },
          ),
        ),
        child: Text(
          translation.layoutSkipButton,
          style: theme.textTheme.bodyMedium!.copyWith(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
