import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';
import 'package:techswop_lite/shared/common_widgets/gradient_elevated_long_button.dart';
import 'package:techswop_lite/shared/shared.dart';

// ignore: must_be_immutable
class CountdownNumberQuestionWidget extends CountdownWidget {
  CountdownNumberQuestionWidget({
    super.key,
    required super.isVisible,
    required super.translation,
    required super.theme,
    required super.countdownNumberString,
    required super.startButtonOnPressed,
    required super.restartButtonOnPressed,
    this.numberButtonOnPressed,
  });

  void Function(int index)? numberButtonOnPressed;

  @override
  ConsumerState<CountdownNumberQuestionWidget> createState() =>
      _CoundownNumberQuestionWidgetState();
}

class _CoundownNumberQuestionWidgetState
    extends ConsumerState<CountdownNumberQuestionWidget> {
  @override
  Widget build(BuildContext context) {
    final logger = ref.watch(loggerRepositoryProvider);

    logger.logDebug(widget.isVisible.toString());

    return Visibility(
      visible: widget.isVisible,
      replacement: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Text.rich(
              TextSpan(
                text: "${widget.translation.countdownWidgetDescription} ",
                style: widget.theme.textTheme.bodyMedium,
                children: <TextSpan>[
                  TextSpan(
                    text: widget.countdownNumberString,
                    style: widget.theme.textTheme.bodyMedium!,
                    // .copyWith(color: Colors.red),
                  ),
                  TextSpan(
                    text: " ${widget.translation.countdownWidgetSeconds}",
                    style: widget.theme.textTheme.bodyMedium,
                  ),
                ],
              ),
            ),
          ),

          /// https://pub.dev/packages/flutter_layout_grid
          LayoutBuilder(
            builder: (ctx, constraints) {
              return Column(
                // mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 10.0.h),
                    alignment: Alignment.center,
                    height: 90.r,
                    width: constraints.maxWidth,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        for (var i = 0;
                            i < AppConfig.defaultRandomMaxNumber;
                            i++)
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 2.0.h, vertical: 6.0.w),
                            child: NumberButton(
                              number: i + 1,
                              onPressed: () => widget.numberButtonOnPressed!(i),
                            ),
                          ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width.w,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.sp),
                    ),
                    child: SizedBox(
                      child: SharedElevatedButton(
                        onPressed: widget.restartButtonOnPressed,
                        text: widget.translation.countdownWidgetReset,
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ],
      ),
      child: SharedElevatedButton(
        onPressed: widget.startButtonOnPressed,
        text: widget.translation.layoutPageStartTest,
      ),
    );
  }
}
