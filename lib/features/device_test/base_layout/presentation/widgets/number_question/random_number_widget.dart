import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/shared/shared.dart';

///ปุ่มช้อยส์ตัวเลข
///- สร้างขึ้นสำหรับหน้าที่ใช้ระบบการทดสอบแบบตอบคำถาม
class NumberButton extends ConsumerWidget {
  const NumberButton({super.key, required this.number, required this.onPressed});

  /// ตัวเลขที่จะแสดงบนปุ่ม
  final int number;

  /// ฟังก์ชันที่จะทำงานเมื่อผู้ใช้กดปุ่มตัวเลข
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = context.theme;
    
    return SizedBox(
      height: 60.h,
      child: FilledButton(
          // style: ElevatedButton.styleFrom(
          //   padding: EdgeInsets.zero,
          //   shape:
          //       RoundedRectangleBorder(borderRadius: BorderRadius.circular(100),),
          //   elevation: 5,
          //   textStyle: theme.
          //       textTheme
          //       .bodySmall!,
          //   enableFeedback: true,
          // ),
          style: theme.filledButtonTheme.style!.copyWith(
            shape: WidgetStateProperty.resolveWith((_) => RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(80.r),
              side: const BorderSide(color: Colors.transparent),
            ),),
          ),
          onPressed: onPressed,
          child: Text(
            number.toString(),
            textAlign: TextAlign.center,
            style: theme
                .textTheme
                .titleMedium!.copyWith(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.sp,
                  color: theme.colorScheme.onPrimary,
                )
                // .copyWith(color: Colors.white, fontSize: 13.5.sp),
          ),
        ),
    );
  }
}
