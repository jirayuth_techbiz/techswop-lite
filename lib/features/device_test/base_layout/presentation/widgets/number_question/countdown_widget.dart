import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/generated/app_localizations.dart';
import 'package:techswop_lite/shared/common_widgets/gradient_elevated_long_button.dart';
import 'package:techswop_lite/shared/shared.dart';

// ignore: must_be_immutable
class CountdownWidget extends ConsumerStatefulWidget {
  CountdownWidget({
    super.key,
    required this.isVisible,
    required this.translation,
    required this.theme,
    required this.countdownNumberString,
    required this.startButtonOnPressed,
    required this.restartButtonOnPressed,
  });
  final bool isVisible;
  final AppLocalizations translation;
  final ThemeData theme;
  final String countdownNumberString;
  FutureOr<void> Function()? startButtonOnPressed;
  FutureOr<void> Function()? restartButtonOnPressed;

  @override
  ConsumerState<CountdownWidget> createState() =>
      _CoundownNumberQuestionWidgetState();
}

class _CoundownNumberQuestionWidgetState
    extends ConsumerState<CountdownWidget> {
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: widget.isVisible,
      replacement: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Text.rich(
              TextSpan(
                text: "${widget.translation.countdownWidgetDescription} ",
                style: widget.theme.textTheme.bodyMedium,
                children: <TextSpan>[
                  TextSpan(
                    text: widget.countdownNumberString,
                    style: widget.theme.textTheme.bodyMedium!,
                    // .copyWith(color: Colors.red),
                  ),
                  TextSpan(
                    text: " ${widget.translation.countdownWidgetSeconds}",
                    style: widget.theme.textTheme.bodyMedium,
                  ),
                ],
              ),
            ),
          ),

          /// https://pub.dev/packages/flutter_layout_grid
          LayoutBuilder(
            builder: (ctx, constraints) {
              return Column(
                // mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width.w,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.sp),
                    ),
                    padding: const EdgeInsets.only(top: 4.0),
                    child: SizedBox(
                      child: SharedElevatedButton(
                        onPressed: widget.restartButtonOnPressed,
                        text: widget.translation.countdownWidgetReset,
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ],
      ),
      child: SharedElevatedButton(
        onPressed: widget.startButtonOnPressed,
        text: widget.translation.layoutPageStartTest,
      ),
    );
  }
}
