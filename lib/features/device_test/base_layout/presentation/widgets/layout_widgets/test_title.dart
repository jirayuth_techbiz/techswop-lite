import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/layout/test_layout.dart';

class TestTitle extends ConsumerWidget {
  const TestTitle({
    super.key,
    required this.widget,
    required this.theme,
    required this.padding,
  });

  final TestPageWidgetLayout widget;
  final ThemeData theme;
  final EdgeInsetsGeometry padding;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: padding,
      child: Text(
        widget.title,
        textAlign: TextAlign.center,
        style: theme.textTheme.titleMedium!,
      ),
    );
  }
}