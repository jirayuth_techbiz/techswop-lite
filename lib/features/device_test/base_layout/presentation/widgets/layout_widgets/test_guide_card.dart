import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';
import 'package:techswop_lite/generated/app_localizations.dart';
import 'package:techswop_lite/shared/shared.dart';

class TestGuideCard extends ConsumerWidget {
  const TestGuideCard({
    super.key,
    required this.theme,
    required this.translation,
    required this.widget,
    required this.containerMargin,
    required this.containerPadding,
    required this.cardPadding,
    required this.cardConstraints,
    required this.guidePadding,
  });

  final ThemeData theme;
  final AppLocalizations translation;
  final TestPageWidgetLayout widget;
  final EdgeInsetsGeometry containerMargin;
  final EdgeInsetsGeometry containerPadding;
  final EdgeInsetsGeometry cardPadding;
  final BoxConstraints cardConstraints;
  final EdgeInsetsGeometry guidePadding;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      margin: containerMargin,
      padding: containerPadding,
      child: Column(
        children: [
          Container(
            decoration: ShapeDecoration(
              color: theme.colorScheme.onPrimary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.r),
              ),
              shadows: [
                BoxShadow(
                  color: theme.colorScheme.shadow.withOpacity(.2),
                  offset: const Offset(0, 5),
                  blurRadius: 6,
                ),
              ],
            ),
            padding: cardPadding,
            child: Container(
              constraints: cardConstraints,
              child: Column(
                children: [
                  Padding(
                    padding: guidePadding,
                    child: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: translation.layoutTips,
                            style: theme.textTheme.bodyMedium!.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          WidgetSpan(
                            child: FaIcon(
                              FontAwesomeIcons.commentQuestion,
                              color: const Color(0xFF515151),
                              size: 20.r,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  widget.cardBody ??
                      Text(
                        widget.cardBodyText ?? '',
                        style: theme.textTheme.bodyMedium!.copyWith(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w400,
                        ),
                        textAlign: TextAlign.start,
                      ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 30.h,
                    ),
                    child: SizedBox(
                      height: 30.h,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
