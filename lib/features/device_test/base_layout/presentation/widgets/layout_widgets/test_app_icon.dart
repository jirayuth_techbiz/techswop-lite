import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';
import 'package:techswop_lite/shared/shared.dart';

class TestAppIcon extends ConsumerWidget {
  const TestAppIcon({
    super.key,
    required this.widget,
    required this.theme,
    required this.padding,
    required this.cardHeight,
    required this.cardWidth,
    required this.borderCircularRadius,
  });

  final TestPageWidgetLayout widget;
  final ThemeData theme;
  final EdgeInsetsGeometry padding;
  final double cardHeight;
  final double cardWidth;
  final double borderCircularRadius;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Visibility(
      visible: widget.icondata != null,
      child: Padding(
        padding: padding,
        child: SizedBox(
          height: cardHeight,
          width: cardWidth,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(borderCircularRadius),
            child: Container(
              alignment: Alignment.center,
              // color: Colors.grey.shade300,
              child: FaIcon(
                widget.icondata,
                color: theme.colorScheme.primary,
                size: 48.r,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
