import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';

class TestAppIconSvg extends ConsumerWidget {
  const TestAppIconSvg({
    super.key,
    required this.widget,
    required this.padding,
    required this.cardHeight,
    required this.cardWidth,
    required this.borderCircularRadius,
    required this.iconSizeHeight,
  });

  final TestPageWidgetLayout widget;
  final EdgeInsetsGeometry padding;
  final double cardHeight;
  final double cardWidth;
  final double borderCircularRadius;
  final double iconSizeHeight;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Visibility(
      visible: widget.pictureAssetDirectory != "",
      child: Padding(
        padding: padding,
        child: SizedBox(
          height: cardHeight,
          width: cardWidth,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(borderCircularRadius),
            child: Container(
              alignment: Alignment.center,
              child: SvgPicture.asset(
                widget.pictureAssetDirectory!,
                height: iconSizeHeight,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
