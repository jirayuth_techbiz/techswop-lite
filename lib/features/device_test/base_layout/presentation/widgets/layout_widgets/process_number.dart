import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/extensions/app_locale_extensions.dart';

class ProcessNumber extends ConsumerWidget {
  const ProcessNumber({
    super.key,
    required this.widget,
    required this.totalPageNumber,
    required this.theme,
    required this.padding,
    required this.isFromResultPage,
  });

  final TestPageWidgetLayout widget;
  final List<RouteMatchBase> totalPageNumber;
  final ThemeData theme;
  final EdgeInsetsGeometry padding;
  final bool isFromResultPage;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final translation = context.localization;

    ///ตัดคำว่า 'ขั้นตอนที่ : ' ออก
    ///
    ///เอาไว้แก้เวลาเจอหน้าสุดท้ายแล้ว [totalPageNumber[1].route.routes.length] กลายเป็น 0
    final processNumber = widget.processNumber
        .split(translation.layoutStep)[1]
        .replaceAll(' ', '');

    return Visibility(
      visible: !isFromResultPage,
      replacement: Padding(
        padding: padding,
        child: Text(widget.processNumber,
            textAlign: TextAlign.center, style: theme.textTheme.titleMedium!),
      ),
      child: Padding(
        padding: padding,
        child: Text(
          totalPageNumber[0].route.routes.isEmpty
              ? "${widget.processNumber} / $processNumber"
              : "${widget.processNumber} / ${totalPageNumber[0].route.routes.length}",
          textAlign: TextAlign.center,
          style: theme.textTheme.titleMedium!,
        ),
      ),
    );
  }
}
