import 'dart:async';

import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'test_result_state.g.dart';

@riverpod
class SafeOnTapState extends _$SafeOnTapState {
  @override
  bool build() {
    return false;
  }

  safeOnTap({int intervalMs = 500}) {
    state = !state;
    Timer(Duration(milliseconds: intervalMs), () {
      state = !state;
    });
  }
}

@riverpod 
class ResultIsExecutedState extends _$ResultIsExecutedState {
  @override
  bool build() {
    return false;
  }

  ///สลับค่า state จาก false เป็น true เพื่อป้องกันการรันฟังค์ชั่นซ้ำ
  ///
  ///หรือสลับค่าจาก true เป็น false เพื่อเปิดให้ฟังค์ชั่นกดได้อีกครั้ง
  void setExecuted() {
    state = !state;
  }
}
