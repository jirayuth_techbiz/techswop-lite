import 'dart:async';

import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'countdown_state.g.dart';

///ควบคุมระบบนับถอยหลัง
@riverpod
class CountdownState extends _$CountdownState {
  // ignore: avoid_public_notifier_properties
  StreamSubscription<int>? _tickerSubscription;
  int? _seconds;

  ///สร้าง Countdown Timer
  ///
  ///มี [ref.onDispose] เช็คด้วยว่า Countdown Timer นี้ยังทำงานอยู่หรือไม่
  @override
  String build({required int seconds}) {
    _seconds = seconds;
    ref.onDispose(() {
      _tickerSubscription?.cancel();
    });

    return seconds.toString();
  }

  ///เริ่มเวลานับถอยหลัง
  ///
  ///[seconds] เป็น Named Parameter คือระยะเวลาที่ต้องการให้นับถอยหลัง
  ///
  ///แต่ละหน้าจะใช้เวลานับถอยหลังไม่เหมือนกัน
  void startCountdownTimer() async {
    _tickerSubscription = Stream.periodic(
      const Duration(seconds: 1),
      (timer) => _seconds! - timer - 1,
    ).take(_seconds!).listen((duration) {
      state = duration.toString();
    });
  }

  ///หยุดเวลานับถอยหลัง
  void stopCountdownTimer() {
    _tickerSubscription?.cancel();
    state = _seconds.toString();
    // state = "0";
  }
}
