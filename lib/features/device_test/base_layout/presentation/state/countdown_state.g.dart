// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'countdown_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$countdownStateHash() => r'c0fb1642fd5be3c51befc77725ba85f3c3f152cd';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$CountdownState extends BuildlessAutoDisposeNotifier<String> {
  late final int seconds;

  String build({
    required int seconds,
  });
}

///ควบคุมระบบนับถอยหลัง
///
/// Copied from [CountdownState].
@ProviderFor(CountdownState)
const countdownStateProvider = CountdownStateFamily();

///ควบคุมระบบนับถอยหลัง
///
/// Copied from [CountdownState].
class CountdownStateFamily extends Family<String> {
  ///ควบคุมระบบนับถอยหลัง
  ///
  /// Copied from [CountdownState].
  const CountdownStateFamily();

  ///ควบคุมระบบนับถอยหลัง
  ///
  /// Copied from [CountdownState].
  CountdownStateProvider call({
    required int seconds,
  }) {
    return CountdownStateProvider(
      seconds: seconds,
    );
  }

  @override
  CountdownStateProvider getProviderOverride(
    covariant CountdownStateProvider provider,
  ) {
    return call(
      seconds: provider.seconds,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'countdownStateProvider';
}

///ควบคุมระบบนับถอยหลัง
///
/// Copied from [CountdownState].
class CountdownStateProvider
    extends AutoDisposeNotifierProviderImpl<CountdownState, String> {
  ///ควบคุมระบบนับถอยหลัง
  ///
  /// Copied from [CountdownState].
  CountdownStateProvider({
    required int seconds,
  }) : this._internal(
          () => CountdownState()..seconds = seconds,
          from: countdownStateProvider,
          name: r'countdownStateProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$countdownStateHash,
          dependencies: CountdownStateFamily._dependencies,
          allTransitiveDependencies:
              CountdownStateFamily._allTransitiveDependencies,
          seconds: seconds,
        );

  CountdownStateProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.seconds,
  }) : super.internal();

  final int seconds;

  @override
  String runNotifierBuild(
    covariant CountdownState notifier,
  ) {
    return notifier.build(
      seconds: seconds,
    );
  }

  @override
  Override overrideWith(CountdownState Function() create) {
    return ProviderOverride(
      origin: this,
      override: CountdownStateProvider._internal(
        () => create()..seconds = seconds,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        seconds: seconds,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<CountdownState, String> createElement() {
    return _CountdownStateProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CountdownStateProvider && other.seconds == seconds;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, seconds.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin CountdownStateRef on AutoDisposeNotifierProviderRef<String> {
  /// The parameter `seconds` of this provider.
  int get seconds;
}

class _CountdownStateProviderElement
    extends AutoDisposeNotifierProviderElement<CountdownState, String>
    with CountdownStateRef {
  _CountdownStateProviderElement(super.provider);

  @override
  int get seconds => (origin as CountdownStateProvider).seconds;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
