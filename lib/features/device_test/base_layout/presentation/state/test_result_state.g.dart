// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_result_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$safeOnTapStateHash() => r'733987472c04181127b7d6783706e55524290f2a';

/// See also [SafeOnTapState].
@ProviderFor(SafeOnTapState)
final safeOnTapStateProvider =
    AutoDisposeNotifierProvider<SafeOnTapState, bool>.internal(
  SafeOnTapState.new,
  name: r'safeOnTapStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$safeOnTapStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SafeOnTapState = AutoDisposeNotifier<bool>;
String _$resultIsExecutedStateHash() =>
    r'79fb6d27f3edc9bd093526885dcfbfa7920117e3';

/// See also [ResultIsExecutedState].
@ProviderFor(ResultIsExecutedState)
final resultIsExecutedStateProvider =
    AutoDisposeNotifierProvider<ResultIsExecutedState, bool>.internal(
  ResultIsExecutedState.new,
  name: r'resultIsExecutedStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$resultIsExecutedStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ResultIsExecutedState = AutoDisposeNotifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
