// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_check_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$baseCheckServiceHash() => r'e173380745f7993cfa0c42129835b3dd2e775632';

/// See also [baseCheckService].
@ProviderFor(baseCheckService)
final baseCheckServiceProvider = AutoDisposeProvider<BaseCheckService>.internal(
  baseCheckService,
  name: r'baseCheckServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$baseCheckServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef BaseCheckServiceRef = AutoDisposeProviderRef<BaseCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
