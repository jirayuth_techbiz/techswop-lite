import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/base_layout/application/base_check_service_impl.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/shared/domain/repository/test_timeout_repository.dart';

part 'base_check_service.g.dart';

@riverpod
BaseCheckService baseCheckService(BaseCheckServiceRef ref) {
  final tradeInSharePreferencesRepository = ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final testTimerRepository = ref.watch(testTimerRepositoryProvider);
  return BaseCheckServiceImpl(
    tradeinPrefsRepository: tradeInSharePreferencesRepository,
    testTimerRepository: testTimerRepository,
  );
}

abstract class BaseCheckService {
  Future<void> stopTimer();
  Future<void> resetData();
}
