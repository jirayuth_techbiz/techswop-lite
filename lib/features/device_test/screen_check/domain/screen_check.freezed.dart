// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'screen_check.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ScreenCheck {
  bool get isScreenBlockVisible => throw _privateConstructorUsedError;
  double get currentScreenCoveredAreaPercentage =>
      throw _privateConstructorUsedError;
  bool get isScreenCheckComplete => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ScreenCheckCopyWith<ScreenCheck> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ScreenCheckCopyWith<$Res> {
  factory $ScreenCheckCopyWith(
          ScreenCheck value, $Res Function(ScreenCheck) then) =
      _$ScreenCheckCopyWithImpl<$Res, ScreenCheck>;
  @useResult
  $Res call(
      {bool isScreenBlockVisible,
      double currentScreenCoveredAreaPercentage,
      bool isScreenCheckComplete});
}

/// @nodoc
class _$ScreenCheckCopyWithImpl<$Res, $Val extends ScreenCheck>
    implements $ScreenCheckCopyWith<$Res> {
  _$ScreenCheckCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isScreenBlockVisible = null,
    Object? currentScreenCoveredAreaPercentage = null,
    Object? isScreenCheckComplete = null,
  }) {
    return _then(_value.copyWith(
      isScreenBlockVisible: null == isScreenBlockVisible
          ? _value.isScreenBlockVisible
          : isScreenBlockVisible // ignore: cast_nullable_to_non_nullable
              as bool,
      currentScreenCoveredAreaPercentage: null ==
              currentScreenCoveredAreaPercentage
          ? _value.currentScreenCoveredAreaPercentage
          : currentScreenCoveredAreaPercentage // ignore: cast_nullable_to_non_nullable
              as double,
      isScreenCheckComplete: null == isScreenCheckComplete
          ? _value.isScreenCheckComplete
          : isScreenCheckComplete // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ScreenCheckImplCopyWith<$Res>
    implements $ScreenCheckCopyWith<$Res> {
  factory _$$ScreenCheckImplCopyWith(
          _$ScreenCheckImpl value, $Res Function(_$ScreenCheckImpl) then) =
      __$$ScreenCheckImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool isScreenBlockVisible,
      double currentScreenCoveredAreaPercentage,
      bool isScreenCheckComplete});
}

/// @nodoc
class __$$ScreenCheckImplCopyWithImpl<$Res>
    extends _$ScreenCheckCopyWithImpl<$Res, _$ScreenCheckImpl>
    implements _$$ScreenCheckImplCopyWith<$Res> {
  __$$ScreenCheckImplCopyWithImpl(
      _$ScreenCheckImpl _value, $Res Function(_$ScreenCheckImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isScreenBlockVisible = null,
    Object? currentScreenCoveredAreaPercentage = null,
    Object? isScreenCheckComplete = null,
  }) {
    return _then(_$ScreenCheckImpl(
      isScreenBlockVisible: null == isScreenBlockVisible
          ? _value.isScreenBlockVisible
          : isScreenBlockVisible // ignore: cast_nullable_to_non_nullable
              as bool,
      currentScreenCoveredAreaPercentage: null ==
              currentScreenCoveredAreaPercentage
          ? _value.currentScreenCoveredAreaPercentage
          : currentScreenCoveredAreaPercentage // ignore: cast_nullable_to_non_nullable
              as double,
      isScreenCheckComplete: null == isScreenCheckComplete
          ? _value.isScreenCheckComplete
          : isScreenCheckComplete // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$ScreenCheckImpl extends _ScreenCheck {
  const _$ScreenCheckImpl(
      {this.isScreenBlockVisible = true,
      this.currentScreenCoveredAreaPercentage = 0.00,
      this.isScreenCheckComplete = false})
      : super._();

  @override
  @JsonKey()
  final bool isScreenBlockVisible;
  @override
  @JsonKey()
  final double currentScreenCoveredAreaPercentage;
  @override
  @JsonKey()
  final bool isScreenCheckComplete;

  @override
  String toString() {
    return 'ScreenCheck(isScreenBlockVisible: $isScreenBlockVisible, currentScreenCoveredAreaPercentage: $currentScreenCoveredAreaPercentage, isScreenCheckComplete: $isScreenCheckComplete)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ScreenCheckImpl &&
            (identical(other.isScreenBlockVisible, isScreenBlockVisible) ||
                other.isScreenBlockVisible == isScreenBlockVisible) &&
            (identical(other.currentScreenCoveredAreaPercentage,
                    currentScreenCoveredAreaPercentage) ||
                other.currentScreenCoveredAreaPercentage ==
                    currentScreenCoveredAreaPercentage) &&
            (identical(other.isScreenCheckComplete, isScreenCheckComplete) ||
                other.isScreenCheckComplete == isScreenCheckComplete));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isScreenBlockVisible,
      currentScreenCoveredAreaPercentage, isScreenCheckComplete);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ScreenCheckImplCopyWith<_$ScreenCheckImpl> get copyWith =>
      __$$ScreenCheckImplCopyWithImpl<_$ScreenCheckImpl>(this, _$identity);
}

abstract class _ScreenCheck extends ScreenCheck {
  const factory _ScreenCheck(
      {final bool isScreenBlockVisible,
      final double currentScreenCoveredAreaPercentage,
      final bool isScreenCheckComplete}) = _$ScreenCheckImpl;
  const _ScreenCheck._() : super._();

  @override
  bool get isScreenBlockVisible;
  @override
  double get currentScreenCoveredAreaPercentage;
  @override
  bool get isScreenCheckComplete;
  @override
  @JsonKey(ignore: true)
  _$$ScreenCheckImplCopyWith<_$ScreenCheckImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
