// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'sketch_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Sketch {
  List<Offset> get points => throw _privateConstructorUsedError;
  set points(List<Offset> value) => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SketchCopyWith<Sketch> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SketchCopyWith<$Res> {
  factory $SketchCopyWith(Sketch value, $Res Function(Sketch) then) =
      _$SketchCopyWithImpl<$Res, Sketch>;
  @useResult
  $Res call({List<Offset> points});
}

/// @nodoc
class _$SketchCopyWithImpl<$Res, $Val extends Sketch>
    implements $SketchCopyWith<$Res> {
  _$SketchCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? points = null,
  }) {
    return _then(_value.copyWith(
      points: null == points
          ? _value.points
          : points // ignore: cast_nullable_to_non_nullable
              as List<Offset>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SketchImplCopyWith<$Res> implements $SketchCopyWith<$Res> {
  factory _$$SketchImplCopyWith(
          _$SketchImpl value, $Res Function(_$SketchImpl) then) =
      __$$SketchImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<Offset> points});
}

/// @nodoc
class __$$SketchImplCopyWithImpl<$Res>
    extends _$SketchCopyWithImpl<$Res, _$SketchImpl>
    implements _$$SketchImplCopyWith<$Res> {
  __$$SketchImplCopyWithImpl(
      _$SketchImpl _value, $Res Function(_$SketchImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? points = null,
  }) {
    return _then(_$SketchImpl(
      points: null == points
          ? _value.points
          : points // ignore: cast_nullable_to_non_nullable
              as List<Offset>,
    ));
  }
}

/// @nodoc

class _$SketchImpl extends _Sketch {
  _$SketchImpl({this.points = const []}) : super._();

  @override
  @JsonKey()
  List<Offset> points;

  @override
  String toString() {
    return 'Sketch(points: $points)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SketchImplCopyWith<_$SketchImpl> get copyWith =>
      __$$SketchImplCopyWithImpl<_$SketchImpl>(this, _$identity);
}

abstract class _Sketch extends Sketch {
  factory _Sketch({List<Offset> points}) = _$SketchImpl;
  _Sketch._() : super._();

  @override
  List<Offset> get points;
  set points(List<Offset> value);
  @override
  @JsonKey(ignore: true)
  _$$SketchImplCopyWith<_$SketchImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
