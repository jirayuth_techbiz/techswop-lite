// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'screen_check_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$screenCheckServiceHash() =>
    r'2e79c28db013adc26d7b510a2b273b80698e0067';

/// See also [screenCheckService].
@ProviderFor(screenCheckService)
final screenCheckServiceProvider =
    AutoDisposeProvider<ScreenCheckService>.internal(
  screenCheckService,
  name: r'screenCheckServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$screenCheckServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef ScreenCheckServiceRef = AutoDisposeProviderRef<ScreenCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
