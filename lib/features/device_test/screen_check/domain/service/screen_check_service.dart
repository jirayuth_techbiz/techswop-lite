import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/screen_check/application/screen_check_service_impl.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

part 'screen_check_service.g.dart';

@riverpod 
ScreenCheckService screenCheckService(ScreenCheckServiceRef ref) {
  final tradeinSharepreferencesRepository =
      ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  return ScreenCheckServiceImpl(tradeinSharepreferencesRepository);
}

abstract class ScreenCheckService {
  Future<Uint8List> readSnapshot(GlobalKey globalkey);
  Future<bool?> getScreenCheckStatus();
  Future<void> saveScreenCheckStatus(bool status);
}
