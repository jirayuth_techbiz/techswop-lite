import 'package:freezed_annotation/freezed_annotation.dart';

part 'screen_check.freezed.dart';

@freezed
class ScreenCheck with _$ScreenCheck {
  const factory ScreenCheck({
    @Default(true) bool isScreenBlockVisible,
    @Default(0.00) double currentScreenCoveredAreaPercentage,
    @Default(false) bool isScreenCheckComplete,
  }) = _ScreenCheck;

  const ScreenCheck._();
}