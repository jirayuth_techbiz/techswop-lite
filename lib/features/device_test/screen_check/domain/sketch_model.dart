import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'sketch_model.freezed.dart';

@unfreezed
class Sketch with _$Sketch {
  factory Sketch({@Default([]) List<Offset> points}) = _Sketch;

  Sketch._();
}
