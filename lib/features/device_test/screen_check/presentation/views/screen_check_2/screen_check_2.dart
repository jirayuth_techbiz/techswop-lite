import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/screen_check/presentation/state/screen_check_2/screen_check_state_2.dart';
import 'package:techswop_lite/shared/shared.dart';

class ScreenTestSecondOption extends ConsumerStatefulWidget {
  const ScreenTestSecondOption({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _ScreenTestSecondOptionState();
}

class _ScreenTestSecondOptionState
    extends ConsumerState<ScreenTestSecondOption> {

  @override
  Widget build(BuildContext context) {
    ///Provider and screensize
    final isTouch = ref.watch(checkScreenTouchProvider);
    final screenWidth = MediaQuery.of(context).size.width ~/ 50;

    ///Calculating user touch pos
    //final ValueNotifier<Sketch?> currentSketch = useState(null);

    return Flex(
      direction: Axis.horizontal,
      children: [
        Expanded(
          child: ColoredBox(
            color: Colors.white,
            child: GridView.builder(
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: screenWidth,
                childAspectRatio: 50 / 70,
              ),
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      ref
                          .read(checkScreenTouchProvider.notifier)
                          .setTrue(true, index);
                    });
                  },
                  child: ColorContainer(
                    color: isTouch[index] ? Colors.white : Colors.amber,
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}

class ColorContainer extends ConsumerWidget {
  final Color? color;
  final String? text;
  const ColorContainer({super.key, this.color, this.text});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(),
        color: color,
      ),
    );
  }
}

class CountdownWidget extends ConsumerWidget {
  const CountdownWidget({super.key});

  int get appTestTimeout => AppConfig.defaultTimeout;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final translation = context.localization;
    return Container(
      alignment: Alignment.bottomCenter,
      margin: const EdgeInsets.only(bottom: 50),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            translation.countdownWidgetDescription,
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                  fontSize: 18,
                  color: const Color.fromARGB(255, 0, 64, 255),
                  fontWeight: FontWeight.bold,
                ),
          ),
          Text(
            appTestTimeout.toString(),
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                  fontSize: 20,
                  color: Colors.blue[700],
                  fontWeight: FontWeight.bold,
                ),
          ),
          Text(
            translation.countdownWidgetSeconds,
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                  fontSize: 18,
                  color: const Color.fromARGB(255, 0, 64, 255),
                  fontWeight: FontWeight.bold,
                ),
          ),
        ],
      ),
    );
  }
}

//class ScrennTestSecondOptions extends StatefulHookWidget {
//  const ScrennTestSecondOptions({super.key});
//
//  @override
//  State<StatefulHookWidget> createState() => _ScrennTestSecondOptionsState();
//}
//
//class _ScrennTestSecondOptionsState extends State<ScrennTestSecondOptions> {
//  final List<Offset> points = <Offset>[];
//
//  Widget buildCurrentPath(
//      BuildContext context,
//      ValueNotifier<Sketch?> currentSketch,
//      ValueNotifier<List<Sketch>> currentListSketch) {
//    int _pointers = 0;
//
//    return GestureDetector(
//      onPanDown: (details) {
//        final box = context.findRenderObject() as RenderBox;
//        final offset = box.globalToLocal(details.localPosition);
//
//        currentSketch.value = Sketch(points: [offset]);
//        _pointers++;
//      },
//      onPanUpdate: (details) {
//        final box = context.findRenderObject() as RenderBox;
//        final offset = box.globalToLocal(details.localPosition);
//
//        final points = List<Offset>.from(currentSketch.value?.points ?? [])
//          ..add(offset);
//        currentSketch.value = Sketch(points: points);
//        _pointers--;
//      },
//      onPanEnd: (details) {
//        currentListSketch.value = List<Sketch>.from(currentListSketch.value)
//          ..add(currentSketch.value!);
//      },
//      child: IgnorePointer(
//        ignoring: _pointers > 1,
//        child: _pointers > 1
//            ? null
//            : RepaintBoundary(
//                child: SizedBox(
//                  height: MediaQuery.of(context).size.height,
//                  width: MediaQuery.of(context).size.width,
//                  child: CustomPaint(
//                    isComplex: true,
//                    willChange: true,
//                    painter: ScreenCheckContainerPainter(
//                      points: currentSketch.value == null
//                          ? []
//                          : [currentSketch.value!],
//                    ),
//                  ),
//                ),
//              ),
//      ),
//    );
//  }
//
//  Widget buildAllPath(
//      BuildContext context, ValueNotifier<List<Sketch>> currentListSketch) {
//    return RepaintBoundary(
//      child: CustomPaint(
//        painter: ScreenCheckContainerPainter(points: currentListSketch.value),
//      ),
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    final ValueNotifier<Sketch?> currentSketch = useState(null);
//    final ValueNotifier<List<Sketch>> currentListSketch = useState([]);
//    return Stack(children: [
//      GridView.builder(
//        physics: const NeverScrollableScrollPhysics(),
//        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
//          crossAxisCount: 5,
//          childAspectRatio: (90 / 120),
//        ),
//        shrinkWrap: true,
//        itemCount: 40,
//        itemBuilder: (context, index) {
//          return const Stack(
//            children: [
//              ColorContainer(),
//            ],
//          );
//        },
//      ),
//      buildAllPath(context, currentListSketch),
//      buildCurrentPath(context, currentSketch, currentListSketch),
//    ]);
//  }
//}
//
//class ColorContainer extends HookWidget {
//  const ColorContainer({super.key});
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      decoration: BoxDecoration(
//        color: Colors.amber,
//        border: Border.all(width: 1, color: Colors.white),
//      ),
//    );
//  }
//}
//
//class ScreenCheckContainerPainter extends CustomPainter {
//  final List<Sketch> points;
//
//  ScreenCheckContainerPainter({required this.points});
//
//  @override
//  void paint(Canvas canvas, Size size) {
//    Paint paint = Paint()..color = Colors.white;
//
//    for (Sketch sketch in points) {
//      final point = sketch.points;
//
//      final path = Path();
//      path.moveTo(point.first.dx, point.first.dy);
//
//      for (int i = 0; i < point.length - 1; ++i) {
//        final p0 = point[i];
//        final p1 = point[i + 1];
//        path.quadraticBezierTo(
//          p0.dx,
//          p0.dy,
//          (p0.dx + p1.dx) / 2,
//          (p0.dy + p1.dy) / 2,
//        );
//      }
//      canvas.drawPaint(paint);
//    }
//  }
//
//  @override
//  bool shouldRepaint(ScreenCheckContainerPainter oldDelegate) {
//    return oldDelegate.points != points;
//  }
//}
