import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/countdown_state.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/screen_check/presentation/state/screen_check_state.dart';
import 'package:techswop_lite/features/device_test/screen_check/presentation/widget/builder/build_all_path.dart';
import 'package:techswop_lite/features/device_test/screen_check/presentation/widget/builder/build_current_path.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';

class ScreenCheckPage extends ConsumerStatefulWidget {
  const ScreenCheckPage({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _ScreenCheckPageState();
}

class _ScreenCheckPageState extends ConsumerState<ScreenCheckPage> {
  /// https://notes.tst.sh/flutter/efficient-painter/
  final List<Offset> points = <Offset>[];
  final GlobalKey globalkey = GlobalKey();
  Offset localPos = const Offset(-1, -1);
  Color color = const Color(0x00000000);

  @override
  Widget build(BuildContext context) {
    final theme = context.theme;
    final translation = context.localization;

    final screenCheckState = ref.watch(screenCheckStateProvider);
    final screenCheckStateNotifier =
        ref.watch(screenCheckStateProvider.notifier);
    final screenCheckCountdownStateNotifier = ref.watch(
        countdownStateProvider(seconds: AppConfig.defaultScreenCheckCountdown)
            .notifier);

    ref.listen(screenCheckStateProvider, (prev, next) {
      if (next.asData!.value.isScreenCheckComplete) {
        _handlingScreenCheck(
          context: context,
          ref: ref,
          isCheck: true,
        );
      }
    });

    ref.listen(
        countdownStateProvider(seconds: AppConfig.defaultScreenCheckCountdown),
        (prev, next) {
      if (int.parse(next) <= 0) {
        _handlingScreenCheck(
          context: context,
          ref: ref,
          isCheck: false,
          failedReason: translation.screenTouchStartPageTestFailed,
        );
      }
    });

    return PopScope(
      canPop: false,
      child: AsyncValueWidget(
        value: screenCheckState,
        data: (screenCheck) {
          return Visibility(
            visible: screenCheck.isScreenBlockVisible,
            replacement: SafeArea(
              child: ColoredBox(
                color: theme.colorScheme.primary,
                child: Stack(
                  children: [
                    BuildAllPath(
                      globalkey: globalkey,
                    ),
                    BuildCurrentPath(
                      globalKey: globalkey,
                    ),
                  ],
                ),
              ),
            ),
            child: GestureDetector(
              onPanStart: (_) {
                screenCheckCountdownStateNotifier.startCountdownTimer();
                screenCheckStateNotifier.startTest();
              },
              child: Container(
                color: theme.colorScheme.primary.withOpacity(.6),
                child: Container(
                  padding: const EdgeInsets.only(bottom: 60),
                  alignment: Alignment.bottomCenter,
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      maxWidth: ScreenUtil().screenWidth / 1.5,
                    ),
                    child: Text(
                      translation.screenTouchStartTest,
                      style: theme.textTheme.bodyLarge!.copyWith(
                        color: theme.colorScheme.primaryContainer,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  void _handlingScreenCheck(
      {required BuildContext context,
      required WidgetRef ref,
      required bool isCheck,
      String? failedReason}) {
    final isExecuted = ref.watch(resultIsExecutedStateProvider);
    final screenCheckManager = ref.watch(screenCheckManagerProvider.notifier);
    final screenCheckCountdownStateNotifier = ref.watch(
        countdownStateProvider(seconds: AppConfig.defaultScreenCheckCountdown)
            .notifier);
    final resultHandler = TestCheckHandler(
      context: context,
      ref: ref,
      isCheck: isCheck,
      appRoutes: AppRoutes.screenCheckStartPage,
      stateProvider: screenCheckStateProvider,
      isExecuted: isExecuted,
    );

    if (isExecuted) return;
    Future.delayed(
            const Duration(milliseconds: AppConfig.futureSuccessAwaitDuration))
        .then(
      (_) async {
        await screenCheckManager.saveScreenCheckResult(isCheck);

        screenCheckCountdownStateNotifier.stopCountdownTimer();

        if (context.mounted) {
          await resultHandler.handlingStatus(
            ///ใช้ screenCheckStartPage เพื่อป้องกันไม่ให้ Page Number => 0
            failedReason: failedReason,
            onSkip: () async {
              await screenCheckManager.saveScreenCheckResult(false);
            },
          );
        }
      },
    );
  }
}
