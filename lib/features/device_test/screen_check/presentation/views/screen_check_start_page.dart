import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/screen_check/presentation/state/screen_check_state.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/common_widgets/gradient_elevated_long_button.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';

class ScreenCheckStartPage extends ConsumerWidget {
  const ScreenCheckStartPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final translation = context.localization;
    final getScreenCheckPageNumber = ref.watch(
        getCurrentPageNumberProvider(AppRoutes.screenCheckStartPage.path));
    final screenCheckManager = ref.watch(screenCheckManagerProvider.notifier);

    return TestPageWidgetLayout(
      nextPage: getScreenCheckPageNumber,
      processNumber: "${translation.layoutStep} $getScreenCheckPageNumber",
      title: translation.screenTouchStartPageTitle,
      pictureAssetDirectory: CommonConst.CHECKLIST_PROPERTIES
          .firstWhere((element) =>
              element.appRoutesPage == AppRoutes.screenCheckStartPage)
          .icon,
      cardBodyText: translation.screenTouchStartPageDescription,
      underCard: SizedBox(
        width: ScreenUtil().screenWidth.w,
        child: SharedElevatedButton(
          onPressed: () {
            context.pushNamed(AppRoutes.screenCheckPage.name);
          },
          text: translation.layoutPageStartTest,
        ),
      ),
      onSkip: () async {
        await screenCheckManager.saveScreenCheckResult(false);
      },
    );
  }
}
