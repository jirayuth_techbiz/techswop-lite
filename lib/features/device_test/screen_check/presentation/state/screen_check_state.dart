import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/screen_check/domain/screen_check.dart';
import 'package:techswop_lite/features/device_test/screen_check/domain/service/screen_check_service.dart';
import 'package:techswop_lite/features/device_test/screen_check/domain/sketch_model.dart';

part 'screen_check_state.g.dart';

///For check 1 only
@riverpod
class SketchController extends _$SketchController {
  Sketch? _currentSketch;

  @override
  Sketch? build() {
    return _currentSketch;
  }

  // ignore: use_setters_to_change_properties
  void updateCurrentSketch(Sketch? newSketchPos) {
    state = newSketchPos;
  }

  Sketch? getCurrentSketch() {
    return state;
  }

  void clearSketch() {
    state = null;
  }
}

@riverpod
class SketchListController extends _$SketchListController {
  final List<Sketch> _sketchList = [];

  @override
  List<Sketch> build() {
    return _sketchList;
  }

  void addSketch(Sketch newSketchData) {
    state = [...state, newSketchData];
  }

  void clearSketch() {
    state = [];
  }
}

@riverpod
class ScreenCheckState extends _$ScreenCheckState {
  @override
  FutureOr<ScreenCheck> build() async {
    return const ScreenCheck();
  }

  Future<void> startTest() async {
    state =
        state.whenData((value) => value.copyWith(isScreenBlockVisible: false));
  }

  updatePercentage(double percentage) {
    double testThresholds = 95.0;
    state = state.whenData((value) {
      if (percentage >= testThresholds) {
        return value = value.copyWith(
          currentScreenCoveredAreaPercentage: percentage,
          isScreenCheckComplete: true,
        );
      }
      return value = value.copyWith(
        currentScreenCoveredAreaPercentage: percentage,
      );
    });
  }
}

@riverpod
class ScreenCheckManager extends _$ScreenCheckManager {
  @override
  Future<void> build() async {}

  Future<bool?> getScreenCheckResult() async {
    final screenCheckService = ref.watch(screenCheckServiceProvider);
    return await screenCheckService.getScreenCheckStatus();
  }

  Future<void> saveScreenCheckResult(bool status) async {
    final screenCheckService = ref.watch(screenCheckServiceProvider);
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(
      () => screenCheckService.saveScreenCheckStatus(status),
    );
  }
}
