// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'screen_check_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sketchControllerHash() => r'd8ce2d9002491ca11ffe4fce04baa1fe0d42b778';

///For check 1 only
///
/// Copied from [SketchController].
@ProviderFor(SketchController)
final sketchControllerProvider =
    AutoDisposeNotifierProvider<SketchController, Sketch?>.internal(
  SketchController.new,
  name: r'sketchControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sketchControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SketchController = AutoDisposeNotifier<Sketch?>;
String _$sketchListControllerHash() =>
    r'c8b5660ae889bd9b0e7dd78c099cb0be4de2daef';

/// See also [SketchListController].
@ProviderFor(SketchListController)
final sketchListControllerProvider =
    AutoDisposeNotifierProvider<SketchListController, List<Sketch>>.internal(
  SketchListController.new,
  name: r'sketchListControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sketchListControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SketchListController = AutoDisposeNotifier<List<Sketch>>;
String _$screenCheckStateHash() => r'dbd56d67e816356d0134739d03c7ef8d4bfd3012';

/// See also [ScreenCheckState].
@ProviderFor(ScreenCheckState)
final screenCheckStateProvider =
    AutoDisposeAsyncNotifierProvider<ScreenCheckState, ScreenCheck>.internal(
  ScreenCheckState.new,
  name: r'screenCheckStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$screenCheckStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ScreenCheckState = AutoDisposeAsyncNotifier<ScreenCheck>;
String _$screenCheckManagerHash() =>
    r'126ee33b759c2b1b25d846a6f5040fcbb9604dda';

/// See also [ScreenCheckManager].
@ProviderFor(ScreenCheckManager)
final screenCheckManagerProvider =
    AutoDisposeAsyncNotifierProvider<ScreenCheckManager, void>.internal(
  ScreenCheckManager.new,
  name: r'screenCheckManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$screenCheckManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ScreenCheckManager = AutoDisposeAsyncNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
