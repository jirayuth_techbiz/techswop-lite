// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'screen_check_state_2.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$checkScreenTouchHash() => r'd1a014afa81fdc2b6fd995eb9c41688ee389b915';

///TODO : add this [https://github.com/igniti0n/draw_application/tree/master]
///
//////For check 2 only
///
/// Copied from [CheckScreenTouch].
@ProviderFor(CheckScreenTouch)
final checkScreenTouchProvider =
    AutoDisposeNotifierProvider<CheckScreenTouch, List<bool>>.internal(
  CheckScreenTouch.new,
  name: r'checkScreenTouchProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$checkScreenTouchHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$CheckScreenTouch = AutoDisposeNotifier<List<bool>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
