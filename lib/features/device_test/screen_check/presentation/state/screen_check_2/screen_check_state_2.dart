
import 'package:flutter/material.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'screen_check_state_2.g.dart';

///TODO : add this [https://github.com/igniti0n/draw_application/tree/master]
///
//////For check 2 only
@riverpod
class CheckScreenTouch extends _$CheckScreenTouch {
  final List<bool> _isTouch = [];
  // ignore: avoid_public_notifier_properties
  List<bool> get isTouch => _isTouch;

  @override
  List<bool> build() {
    final screenWidth = WidgetsBinding
            .instance.platformDispatcher.views.first.physicalSize.width ~/
        50;
    final screenHeight = WidgetsBinding
            .instance.platformDispatcher.views.first.physicalSize.height ~/
        70;
    final totalItems = screenHeight * screenWidth ~/ 10 * 2;
    isTouch.addAll(List.generate(totalItems, (index) => false));
    return isTouch;
  }

  Future<void> setTrue(bool condition, int index) async {
    state[index] = condition;
  }
}
