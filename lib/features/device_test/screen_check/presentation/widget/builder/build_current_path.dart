import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/countdown_state.dart';
import 'package:techswop_lite/features/device_test/screen_check/domain/service/screen_check_service.dart';
import 'package:techswop_lite/features/device_test/screen_check/domain/sketch_model.dart';
import 'package:techswop_lite/features/device_test/screen_check/presentation/state/screen_check_state.dart';
import 'package:techswop_lite/features/device_test/screen_check/presentation/widget/screen_check_painter.dart';
import 'package:techswop_lite/shared/shared.dart';

class BuildCurrentPath extends ConsumerStatefulWidget {
  final GlobalKey globalKey;
  const BuildCurrentPath({super.key, required this.globalKey});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _BuildCurrentPathState();
}

class _BuildCurrentPathState extends ConsumerState<BuildCurrentPath> {
  ui.Image? image;
  var points = <Sketch>[];
  var baking = false;

  void clear() {
    points = <Sketch>[];
    baking = false;
    image = null;
  }

  Size? size;
  double? scale;

  void bake() async {
    if (points.length > 50 && !baking) {
      baking = true;
      var points = this.points;
      var numPoints = points.length;
      var recorder = ui.PictureRecorder();
      var canvas = ui.Canvas(recorder);

      canvas.scale(scale!);

      ScreenCheckPainter(backgroundImage: image, points: points)
          .paint(canvas, size!);

      var picture = recorder.endRecording();
      var newImage = await picture.toImage(
          (size!.width * scale!).ceil(), (size!.height * scale!).ceil());

      if (points == this.points) {
        image?.dispose();
        image = newImage;
        points.removeRange(0, numPoints);

        baking = false;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    int pointers = 0;
    final translation = context.localization;
    final theme = context.theme;
    final sketchController = ref.watch(sketchControllerProvider);
    final countdown = ref.watch(
        countdownStateProvider(seconds: AppConfig.defaultScreenCheckCountdown));
    final screenCheckService = ref.watch(screenCheckServiceProvider);
    final screenCheckStateNotifier =
        ref.watch(screenCheckStateProvider.notifier);

    return GestureDetector(
      onTap: () async {
        await screenCheckService.readSnapshot(widget.globalKey);
      },
      onPanStart: (details) {
        pointers++;

        final box = context.findRenderObject()! as RenderBox;
        final offset = box.globalToLocal(details.localPosition);
        ref
            .watch(sketchControllerProvider.notifier)
            .updateCurrentSketch(Sketch(points: [offset]));
      },
      onPanUpdate: (details) {
        final box = context.findRenderObject()! as RenderBox;
        final offset = box.globalToLocal(details.localPosition);

        final points = List<Offset>.from(sketchController?.points ?? [])
          ..add(offset);

        ref
            .watch(sketchControllerProvider.notifier)
            .updateCurrentSketch(Sketch(points: points));

        bake();
      },
      onPanEnd: (details) async {
        ref
            .watch(sketchControllerProvider.notifier)
            .updateCurrentSketch(sketchController);
        ref
            .watch(sketchListControllerProvider.notifier)
            .addSketch(sketchController!);

        ///รอ Snapshot จาก [sketchController] อัพเดตให้เสร็จก่อน
        await Future.delayed(const Duration(milliseconds: 100));

        final result = await screenCheckService.readSnapshot(widget.globalKey);

        final currentAmouth = result.where((element) => element == 255).length;
        final totalAmouth = result.length;
        final percentage = (currentAmouth / totalAmouth) * 100;

        screenCheckStateNotifier.updatePercentage(percentage);
      },
      child: IgnorePointer(
        ignoring: pointers > 1,
        child: pointers > 1
            ? null
            : RepaintBoundary(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: CustomPaint(
                    isComplex: true,
                    willChange: true,
                    painter: ScreenCheckPainter(
                      points:
                          sketchController == null ? [] : [sketchController],
                    ),
                    child: Container(
                      alignment: Alignment.bottomCenter,
                      margin: const EdgeInsets.only(bottom: 50),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "${translation.countdownWidgetDescription} ",
                            style: theme.textTheme.bodyLarge!.copyWith(
                                color: theme.colorScheme.onPrimaryContainer),
                          ),
                          Text(
                            (int.parse(countdown) == -1) ? "0" : "$countdown ",
                            style: theme.textTheme.titleMedium!.copyWith(
                                color: theme.colorScheme.onPrimaryContainer),
                          ),
                          Text(
                            translation.countdownWidgetSeconds,
                            style: theme.textTheme.bodyLarge!.copyWith(
                                color: theme.colorScheme.onPrimaryContainer),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
