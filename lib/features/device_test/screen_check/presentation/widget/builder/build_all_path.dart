import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/screen_check/presentation/state/screen_check_state.dart';
import 'package:techswop_lite/features/device_test/screen_check/presentation/widget/screen_check_painter.dart';

class BuildAllPath extends ConsumerStatefulWidget {
  final GlobalKey globalkey;
  const BuildAllPath({super.key, required this.globalkey});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _BuildAllPathState();
}

class _BuildAllPathState extends ConsumerState<BuildAllPath> {

  @override
  Widget build(BuildContext context) {

    ///ทุกครั้งที่ User สัมผัสจอ BuildAllPath จะถูกเรียกใช้งาน
    ///โดยจะเรียกใช้งาน ScreenCheckPainter ในการวาด Path ใหม่
    ///ดึงข้อมูลจาก SketchListController ที่เก็บข้อมูล Path ทั้งหมด
    ///จากนั้นจะสร้าง Path ใหม่จากข้อมูลที่ได้มา

    final getSketchList = ref.watch(sketchListControllerProvider);

    return SizedBox(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: RepaintBoundary(
        key: widget.globalkey,
        child: CustomPaint(
          isComplex: true,
          willChange: true,
          foregroundPainter: ScreenCheckPainter(
            points: getSketchList,
          ),
        ),
      ),
    );
  }
}