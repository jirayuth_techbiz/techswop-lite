import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:techswop_lite/features/device_test/screen_check/domain/sketch_model.dart';

class ScreenCheckPainter extends CustomPainter {
  final List<Sketch> points;
  final ui.Image? backgroundImage;

  ScreenCheckPainter({required this.points, this.backgroundImage});

  @override
  void paint(Canvas canvas, Size size) {
    //if (backgroundImage != null) {
    //  canvas.drawImageRect(
    //      backgroundImage!,
    //      Rect.fromLTWH(0, 0, backgroundImage!.width.toDouble(),
    //          backgroundImage!.height.toDouble()),
    //      Rect.fromLTWH(0, 0, size.width, size.height),
    //      Paint()..color = Colors.amber.shade800);
    //}

    final Paint paint = Paint()
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 65.0
      ..color = Colors.white
      //..blendMode = BlendMode.srcIn
      ..style = PaintingStyle.stroke;

    for (final Sketch sketch in points) {
      final point = sketch.points;

      final path = Path();
      path.moveTo(point.first.dx, point.first.dy);

      for (int i = 0; i < point.length - 1; ++i) {
        final p0 = point[i];
        final p1 = point[i + 1];
        path.quadraticBezierTo(
          p0.dx,
          p0.dy,
          (p0.dx + p1.dx) / 2,
          (p0.dy + p1.dy) / 2,
        );
        canvas.drawCircle(point[i], 5, paint);
      }

      canvas.drawPath(path, paint);
    }
  }

  @override
  bool shouldRepaint(covariant ScreenCheckPainter oldDelegate) {
    return oldDelegate.points != points;
  }
}
