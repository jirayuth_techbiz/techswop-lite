import 'dart:ui' as ui;

import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:techswop_lite/features/device_test/screen_check/domain/service/screen_check_service.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';

class ScreenCheckServiceImpl implements ScreenCheckService {
  final TradeInSharePreferencesRepository _tradeInSharePreferencesRepository;
  ScreenCheckServiceImpl(
    this._tradeInSharePreferencesRepository,
  );

  @override
  Future<bool?> getScreenCheckStatus() async {
    final prefs = _tradeInSharePreferencesRepository;
    return await prefs.getBool(AppRoutes.screenCheckPage.titleAPI);
  }

  @override
  Future<void> saveScreenCheckStatus(bool status) async {
    final prefs = _tradeInSharePreferencesRepository;
    await prefs.saveBool(AppRoutes.screenCheckPage.titleAPI, status);
  }

  ///อ่านข้อมูลที่ User เพิ่งลากไปบนจอจาก RenderRepaintBoundary มาแปลงเป็น Uint8List
  @override
  Future<Uint8List> readSnapshot(GlobalKey globalkey) async {
    final RenderRepaintBoundary boundary=
        globalkey.currentContext!.findRenderObject()! as RenderRepaintBoundary;
    final ui.Image img = await boundary.toImage();
    final ByteData? bytes = await img.toByteData();
    final Uint8List memoryImageData = bytes!.buffer.asUint8List();
    img.dispose();
    return memoryImageData;
  }
}
