import 'package:camera/camera.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'package:google_mlkit_object_detection/google_mlkit_object_detection.dart';
import 'package:techswop_lite/features/device_test/camera/domain/repository/camera_check_repository.dart';

class CameraCheckRepositoryImpl implements CameraCheckRepository {
  final Ref ref;
  final FaceDetector faceDetector;
  final ObjectDetector objectDetector;

  CameraCheckRepositoryImpl(this.ref, this.faceDetector, this.objectDetector);

  @override
  Future<List<CameraDescription>> getAvailableCameras() async {
    final cameraList = await availableCameras();
    return cameraList;
  }

  @override
  Future<List<Face>> getFacesData(InputImage inputimg) async {

    return await faceDetector.processImage(inputimg);
  }

  @override
  Future<List<DetectedObject>> getObjectData(InputImage inputimg) async {
    return await objectDetector.processImage(inputimg);
  }
}
