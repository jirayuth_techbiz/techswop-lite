import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/services.dart';
import 'package:google_mlkit_object_detection/google_mlkit_object_detection.dart';
import 'package:techswop_lite/features/device_test/camera/domain/repository/camera_check_repository.dart';
import 'package:techswop_lite/features/device_test/camera/domain/service/back_camera_check_service.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/shared/shared.dart';

class BackCameraCheckServiceImpl extends BackCameraCheckService {
  final TradeInSharePreferencesRepository tradeInSharePreferencesRepository;
  final CameraCheckRepository cameraCheckRepository;
  final LoggerRepository loggerRepository;

  BackCameraCheckServiceImpl({
    required this.tradeInSharePreferencesRepository,
    required this.cameraCheckRepository,
    required this.loggerRepository,
  });

  CameraController? backCameraController;

  LoggerRepository get logger => loggerRepository;

  final _orientations = {
    DeviceOrientation.portraitUp: 0,
    DeviceOrientation.landscapeLeft: 90,
    DeviceOrientation.portraitDown: 180,
    DeviceOrientation.landscapeRight: 270,
  };

  @override
  Future<bool> checkObjectData(InputImage inputimg) async {
    final objectResult = await cameraCheckRepository.getObjectData(inputimg);

    ///TODO : delete
    print("objects: ${objectResult.toString()}");

    if (objectResult.isNotEmpty) {
      return true;
    }

    return false;
  }

  @override
  Future<bool?> getBackCameraCheckResult() async {
    return await tradeInSharePreferencesRepository
        .getBool(AppRoutes.backCameraCheckPage.titleAPI);
  }

  @override
  Future<void> saveBackCameraCheckResult(bool status) async {
    await tradeInSharePreferencesRepository.saveBool(
        AppRoutes.backCameraCheckPage.titleAPI, status);
  }

  @override
  Future<List<CameraDescription>> getAvailableCameras() async {
    return await cameraCheckRepository.getAvailableCameras();
  }

  @override
  InputImage? readInputImageFromBackCameraImage(
    CameraImage image,
    CameraDescription camera,
    CameraController? cameraController,
  ) {
    try {
      //nv21 and bgra8888 return only one plane.
      final plane = image.planes.first;
      Uint8List _yuv420ToNV21(CameraImage image) {
        ///WORKAROUND UNTIL https://github.com/flutter-ml/google_ml_kit_flutter/issues/628 fixed
        var nv21 = Uint8List(image.planes[0].bytes.length +
            image.planes[1].bytes.length +
            image.planes[2].bytes.length);

        var yBuffer = image.planes[0].bytes;
        var uBuffer = image.planes[1].bytes;
        var vBuffer = image.planes[2].bytes;

        nv21.setRange(0, yBuffer.length, yBuffer);

        int i = 0;
        while (i < uBuffer.length) {
          nv21[yBuffer.length + i] = vBuffer[i];
          nv21[yBuffer.length + i + 1] = uBuffer[i];
          i += 2;
        }

        return nv21;
      }

      Uint8List? newImg;

      if (Platform.isAndroid) {
        if (image.format.group != ImageFormatGroup.nv21) {
          newImg = _yuv420ToNV21(image);
        }

        final format =
            InputImageFormatValue.fromRawValue(image.format.raw as int);
        InputImageRotation? rotation =
            _getInputImgRotation(camera, cameraController);
        if (!_shouldReturnNull(image, rotation, format)) return null;

        return InputImage.fromBytes(
          bytes: newImg ?? image.planes.first.bytes,
          metadata: InputImageMetadata(
            size: Size(image.width.toDouble(), image.height.toDouble()),
            rotation: rotation!,
            format: format!,
            bytesPerRow: plane.bytesPerRow,
          ),
        );
      }
    } catch (e) {
      logger.logWTF(e.toString());
      rethrow;
    }
    return null;
  }

  InputImageRotation? _getInputImgRotation(
    CameraDescription cameraDescription,
    CameraController? cameraController,
  ) {
    InputImageRotation? rotation;
    if (Platform.isIOS) {
      rotation = InputImageRotationValue.fromRawValue(
          cameraDescription.sensorOrientation);
    } else if (Platform.isAndroid) {
      var rotationCompensation =
          _orientations[cameraController?.value.deviceOrientation];
      if (rotationCompensation == null) return null;

      //front only
      rotationCompensation =
          (cameraDescription.sensorOrientation + rotationCompensation) % 360;

      rotation = InputImageRotationValue.fromRawValue(rotationCompensation);
    }
    return rotation;
  }

  ///เช็คว่ากล้องหน้าส่งข้อมูลมาหรือไม่ ถ้าไม่ให้คืนค่า null
  ///
  ///พอมันมีหลาย Condition ที่ต้องเช็คแล้วไม่แยกออกมา อาจจะงงตอนมาอ่านทีหลัง
  bool _shouldReturnNull(
    CameraImage image,
    InputImageRotation? rotation,
    InputImageFormat? format,
  ) {
    List<bool> conditions = [
      rotation != null,
      format != null,
      // (Platform.isAndroid && format != InputImageFormat.nv21),
      // (Platform.isIOS && format != InputImageFormat.bgra8888),
      image.planes.length != 1,
    ];

    return conditions.every((element) => element);
  }

  @override
  Future<void> disposeCameraController(
      CameraController? cameraController) async {
    await cameraController?.stopImageStream();
    await cameraController?.dispose();
  }
}
