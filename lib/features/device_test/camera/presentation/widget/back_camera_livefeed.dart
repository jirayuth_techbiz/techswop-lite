import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'package:techswop_lite/features/device_test/camera/presentation/state/camera_check_state.dart';
import 'package:techswop_lite/shared/shared.dart';

class BackCamDetectorViewWidget extends ConsumerStatefulWidget {
  const BackCamDetectorViewWidget({
    super.key,
    required this.onImage,
    this.onCameraFeedReady,
  });

  final Function(InputImage inputImage) onImage;
  final Function()? onCameraFeedReady;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _BackCamDetectorViewState();
}

class _BackCamDetectorViewState
    extends ConsumerState<BackCamDetectorViewWidget> {
  CameraController? backCameraController;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      if (context.mounted) {
        await _startLiveFeed();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final logger = ref.watch(loggerRepositoryProvider);
    final size = MediaQuery.of(context).size.aspectRatio;
    if (backCameraController == null ||
        !backCameraController!.value.isInitialized) {
      return Container();
    }

    var cameraAspectRatio = backCameraController!.value.aspectRatio * size;
    if (cameraAspectRatio < 1) {
      cameraAspectRatio = 1 / cameraAspectRatio;
    }

    return Transform.scale(
      scale: cameraAspectRatio,
      child: _CameraLiveFeedBody(backCameraController: backCameraController),
    );
  }

  Future<void> _startLiveFeed() async {
    final cameraController =
        ref.watch(cameraCheckStateProvider).cameraController;
    final backCameraCheckManager =
        ref.watch(backCameraCheckManagerProvider.notifier);
    final backCameraCheckStateNotifier =
        ref.watch(cameraCheckStateProvider.notifier);
    final avaliableCameras = await backCameraCheckManager.getAvailableCameras();

    if (cameraController != null) {
      cameraController.dispose();
    }

    backCameraController = CameraController(
      avaliableCameras.firstWhere(
          (element) => element.lensDirection == CameraLensDirection.back),
      ResolutionPreset.high,
      enableAudio: false,
      imageFormatGroup: Platform.isAndroid
          ? ImageFormatGroup.nv21
          : ImageFormatGroup.bgra8888,
    );

    backCameraCheckStateNotifier.updateCameraController(backCameraController!);

    await backCameraController!.initialize().then((_) {
      backCameraController!
          .startImageStream(_processBackCameraImage)
          .then((value) {
        setState(() {});
        // backCameraCheckStateNotifier.updateCameraCheckState(true);
        if (widget.onCameraFeedReady != null) {
          widget.onCameraFeedReady?.call();
        }
      });
    });
  }

  Future<void> _processBackCameraImage(CameraImage image) async {
    ///ฟังค์ชั่นนี้อาจจะยัง Trigger แม้ว่า [backCameraController] กลายเป็น [null] แล้ว
    ///
    ///เพื่อป้องกันไม่ให้เกิด Unhandled Exception เลยเช็คว่าถ้าเป็น Null แล้วไม่ต้องทำอะไรเพิ่ม
    if (backCameraController == null) return;

    final backCameraCheckManager =
        ref.watch(backCameraCheckManagerProvider.notifier);
    final availiableCameras =
        await backCameraCheckManager.getAvailableCameras();
    final backCameraDescription = availiableCameras.firstWhere(
        (element) => element.lensDirection == CameraLensDirection.back);

    final inputimage = backCameraCheckManager.readInputImageFromCameraImage(
      image,
      backCameraDescription,
      backCameraController,
    );
    if (inputimage == null) return;
    widget.onImage(inputimage);
  }

  void _stopLiveFeed() async {
    try {
      await backCameraController?.stopImageStream();
      if (backCameraController != null) {
        backCameraController?.dispose();
      }
      
      backCameraController = null;
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void dispose() {
    try {
      _stopLiveFeed();
    } catch (e) {
      log(e.toString());
    } finally {
      super.dispose();
    }
  }
}

class _CameraLiveFeedBody extends ConsumerWidget {
  const _CameraLiveFeedBody({
    required this.backCameraController,
  });

  final CameraController? backCameraController;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final cameraControllerWidth =
        backCameraController!.value.previewSize!.height;
    final cameraControllerHeight =
        backCameraController!.value.previewSize!.width;
    return SizedBox(
      width: cameraControllerWidth,
      height: cameraControllerHeight,
      child: CameraPreview(backCameraController!),
    );
  }
}
