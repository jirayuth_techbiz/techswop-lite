import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'package:techswop_lite/features/device_test/camera/domain/service/front_camera_check_service.dart';
import 'package:techswop_lite/features/device_test/camera/presentation/state/camera_check_state.dart';
import 'package:techswop_lite/shared/shared.dart';

class FrontCamDetectorViewWidget extends ConsumerStatefulWidget {
  const FrontCamDetectorViewWidget({
    super.key,
    required this.onImage,
    this.onCameraFeedReady,
  });

  final Function(InputImage inputImage) onImage;
  final Function()? onCameraFeedReady;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _FrontCamDetectorViewState();
}

class _FrontCamDetectorViewState
    extends ConsumerState<FrontCamDetectorViewWidget> {
  CameraController? frontCameraController;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      try {
        await _startLiveFeed();
      } catch (e) {
        log(e.toString());
      }
    });
  }

  Future _startLiveFeed() async {
    final cameraController = ref.watch(cameraCheckStateProvider).cameraController;
    final frontCameraCheckManager =
        ref.watch(frontCameraCheckManagerProvider.notifier);
    final frontCameraCheckStateNotifier =
        ref.watch(cameraCheckStateProvider.notifier);
    final avaliableCameras =
        await frontCameraCheckManager.getAvailableCameras();
    try {
      if (cameraController != null) {
      cameraController.dispose();
    }

    frontCameraController = CameraController(
      avaliableCameras.firstWhere(
          (element) => element.lensDirection == CameraLensDirection.front),
      ResolutionPreset.high,
      enableAudio: false,
      imageFormatGroup: Platform.isAndroid
          ? ImageFormatGroup.nv21
          : ImageFormatGroup.bgra8888,
    );

    frontCameraCheckStateNotifier
        .updateCameraController(frontCameraController!);

    frontCameraController!.initialize().then((_) {
      frontCameraController!
          .startImageStream(_processFrontCameraImage)
          .then((value) {
        setState(() {});
        if (widget.onCameraFeedReady != null) {
          widget.onCameraFeedReady?.call();
        }
      });
    });
    } catch (e) {
      log(e.toString());
    }
    
  }

  Future<void> _processFrontCameraImage(CameraImage image) async {
    ///ฟังค์ชั่นนี้อาจจะยัง Trigger แม้ว่า [backCameraController] กลายเป็น [null] แล้ว
    ///
    ///เพื่อป้องกันไม่ให้เกิด Unhandled Exception เลยเช็คว่าถ้าเป็น Null แล้วไม่ต้องทำอะไรเพิ่ม
    if (frontCameraController == null) return;
    final frontCameraCheckManager =
        ref.watch(frontCameraCheckManagerProvider.notifier);
    final avaliableCameras =
        await frontCameraCheckManager.getAvailableCameras();
    final frontCameraDescription = avaliableCameras.firstWhere(
        (element) => element.lensDirection == CameraLensDirection.front);

    final inputimage = frontCameraCheckManager.readInputImageFromCameraImage(
      image,
      frontCameraDescription,
      frontCameraController,
    );
    if (inputimage == null) return;
    widget.onImage(inputimage);
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size.aspectRatio;
    if (frontCameraController == null ||
        !frontCameraController!.value.isInitialized) {
      return const SizedBox();
    }

    var cameraAspectRatio = frontCameraController!.value.aspectRatio * size;
    if (cameraAspectRatio < 1) {
      cameraAspectRatio = 1 / cameraAspectRatio;
    }

    ///According to : https://stackoverflow.com/questions/49946153/flutter-camera-appears-stretched
    ///
    ///Do a calculate between camera aspect ratio and divide it with
    ///Screen Size ratio
    ///
    ///This way, you get an accurate scale for the widget.
    return Transform.scale(
      scale: cameraAspectRatio,
      child: _CameraLiveFeedBody(frontCameraController: frontCameraController),
    );
  }

  FutureOr<void> _stopLiveFeed() async {
    try {
      await frontCameraController?.stopImageStream();
      if (frontCameraController != null) {
        frontCameraController?.dispose();
      }
      frontCameraController = null;
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void dispose() {
    try {
      _stopLiveFeed();
    } catch (e) {
      log(e.toString());
    } finally {
      super.dispose();
    }
  }
}

class _CameraLiveFeedBody extends ConsumerWidget {
  const _CameraLiveFeedBody({
    required this.frontCameraController,
  });

  final CameraController? frontCameraController;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final cameraControllerWidth =
        frontCameraController!.value.previewSize!.height;
    final cameraControllerHeight =
        frontCameraController!.value.previewSize!.width;
        
    return SizedBox(
      width: cameraControllerWidth,
      height: cameraControllerHeight,
      child: CameraPreview(frontCameraController!),
    );
  }
}
