// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:camera/camera.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_mlkit_object_detection/google_mlkit_object_detection.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/test_result/test_failed_status_icon.dart';
import 'package:techswop_lite/features/device_test/camera/domain/service/back_camera_check_service.dart';
import 'package:techswop_lite/features/device_test/camera/presentation/state/camera_check_state.dart';
import 'package:techswop_lite/features/device_test/camera/presentation/widget/back_camera_livefeed.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';

class BackCameraCheckPage extends ConsumerStatefulWidget {
  const BackCameraCheckPage({super.key});

  @override
  ConsumerState<BackCameraCheckPage> createState() =>
      _BackCameraTestPageState();
}

class _BackCameraTestPageState extends ConsumerState<BackCameraCheckPage> {
  @override
  Widget build(BuildContext context) {
    final translation = context.localization;

    ///อย่าเอาออกเพราะตัว State ใข้ใน [BackCamDetectorViewWidget]
    // ignore: unused_local_variable
    final backCameraCheckState = ref.watch(cameraCheckStateProvider);
    final backCameraStateNotifier =
        ref.watch(cameraCheckStateProvider.notifier);
    final backCameraCheckManager =
        ref.watch(backCameraCheckManagerProvider.notifier);
    final getBackCameraPageNumber = ref.watch(
        getCurrentPageNumberProvider(AppRoutes.backCameraCheckPage.path));
    final theme = context.theme;
    final logger = ref.watch(loggerRepositoryProvider);

    ref.listen(cameraCheckStateProvider, (prev, next) async {
      if (next.isBackCameraScanned && !prev!.isBackCameraScanned) {
        _handlingBackCameraCheckPage(
          context: context,
          ref: ref,
          isCheck: true,
        );
      }

      if (next.cameras.isNotEmpty &&
          next.cameras.firstWhereOrNull((cameraLens) =>
                  cameraLens.lensDirection == CameraLensDirection.back) ==
              null) {
        _handlingBackCameraCheckPage(
          context: context,
          ref: ref,
          isCheck: false,
          failedReason: translation.frontCameraPageTestFailed,
        );
      }

      if (!next.isFrontCameraScanned) {
        await backCameraCheckManager.saveCameraCheckStatus(false);
      }
    });

    

    Future<void> _processImage(InputImage inputImage) async {
      final bool objects =
          await backCameraCheckManager.checkObjectData(inputImage);

      if (inputImage.metadata?.size != null &&
          inputImage.metadata?.rotation != null &&
          objects &&
          context.mounted) {
        backCameraStateNotifier.isBackCameraScanned(true);
      }
    }

    return TestPageWidgetLayout(
      nextPage: getBackCameraPageNumber,
      // ignore: avoid_redundant_argument_values
      processNumber: "${translation.layoutStep} $getBackCameraPageNumber",
      title: translation.backCameraPageTitle,
      cardBodyText: translation.backCameraPageDescription,
      iconReplacer: LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxHeight < AppConfig.smallScreenHeight) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 10.h),
              color: Colors.transparent,
              child: _SmallBackCameraPreview(
                onImage: _processImage,
              ),
            );
          }
          return Container(
            padding: EdgeInsets.symmetric(vertical: 10.h),
            color: Colors.transparent,
            child: _NormalBackCameraPreview(
              onImage: _processImage,
            ),
          );
        },
      ),
      underCard: backCameraCheckState.isBackCameraScanned
          ? Center(
              child: TestSuccessStatusIcon(theme: theme),
            )
          : const SizedBox(),
      onSkip: () async {
        await backCameraCheckManager.saveCameraCheckStatus(false);
      },
    );
  }

  void _handlingBackCameraCheckPage(
      {required BuildContext context,
      required WidgetRef ref,
      required bool isCheck,
      String? failedReason}) {
    final isExecuted = ref.watch(resultIsExecutedStateProvider);
    final backCameraService = ref.watch(backCameraCheckServiceProvider);
    final resultHandler = TestCheckHandler(
      context: context,
      ref: ref,
      isCheck: isCheck,
      appRoutes: AppRoutes.backCameraCheckPage,
      stateProvider: cameraCheckStateProvider,
      isExecuted: isExecuted,
    );

    if (isExecuted) return;
    Future.delayed(
            const Duration(milliseconds: AppConfig.futureSuccessAwaitDuration))
        .then((_) async {
      await backCameraService.saveBackCameraCheckResult(isCheck);

      log("Back Camera Check: ${(await backCameraService.getBackCameraCheckResult()).toString()}");

      if (context.mounted) {
        await resultHandler.handlingStatus(
          failedReason: failedReason,
          onSkip: () async {
            await backCameraService.saveBackCameraCheckResult(false);
          },
        );
      }
    });
  }
}

class _SmallBackCameraPreview extends StatelessWidget {
  const _SmallBackCameraPreview({
    required this.onImage,
  });

  final Function(InputImage) onImage;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 145.sp,
      height: 145.sp,
      child: AspectRatio(
        aspectRatio: 1,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(250),
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: BackCamDetectorViewWidget(
              onImage: onImage,
            ),
          ),
        ),
      ),
    );
  }
}

class _NormalBackCameraPreview extends StatelessWidget {
  const _NormalBackCameraPreview({
    required this.onImage,
  });

  final Function(InputImage) onImage;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 175.sp,
      height: 175.sp,
      child: AspectRatio(
        aspectRatio: 1,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(250),
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: BackCamDetectorViewWidget(
              onImage: onImage,
            ),
          ),
        ),
      ),
    );
  }
}
