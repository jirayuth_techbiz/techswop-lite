// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:camera/camera.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/test_result/test_failed_status_icon.dart';
import 'package:techswop_lite/features/device_test/camera/presentation/state/camera_check_state.dart';
import 'package:techswop_lite/features/device_test/camera/presentation/widget/front_camera_livefeed.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';

class FrontCameraCheckPage extends ConsumerStatefulWidget {
  const FrontCameraCheckPage({super.key});

  @override
  ConsumerState<FrontCameraCheckPage> createState() =>
      _FrontCameraTestPageState();
}

class _FrontCameraTestPageState extends ConsumerState<FrontCameraCheckPage> {
  @override
  Widget build(BuildContext context) {
    final translation = context.localization;

    ///อย่าเอาออกเพราะตัว State ใข้ใน [FrontCamDetectorViewWidget]
    // ignore: unused_local_variable
    final frontCameraCheckState = ref.watch(cameraCheckStateProvider);
    final frontCameraStateNotifier =
        ref.watch(cameraCheckStateProvider.notifier);
    final frontCameraCheckManager =
        ref.watch(frontCameraCheckManagerProvider.notifier);
    final getFrontCameraPageNumber = ref.watch(
        getCurrentPageNumberProvider(AppRoutes.frontCameraCheckPage.path));
    final theme = context.theme;

    ref.listen(cameraCheckStateProvider, (prev, next) {
      if (next.isFrontCameraScanned && !prev!.isFrontCameraScanned) {
        _handlingFrontCameraCheckPage(
          context: context,
          ref: ref,
          isCheck: true,
        );
      }

      if (next.cameras.isNotEmpty &&
          next.cameras.firstWhereOrNull((cameraLens) =>
                  cameraLens.lensDirection == CameraLensDirection.front) ==
              null) {
        _handlingFrontCameraCheckPage(
          context: context,
          ref: ref,
          isCheck: false,
          failedReason: translation.frontCameraPageTestFailed,
        );
      }
    });

    // bool hasExecuted = false;
    Future<void> _processImage(InputImage inputImage) async {
      // if (hasExecuted) return;
      final bool result =
          await frontCameraCheckManager.checkFaceData(inputImage);

      if (result && context.mounted) {
        frontCameraStateNotifier.isFrontCameraScanned(true);
      }
    }

    return TestPageWidgetLayout(
        nextPage: getFrontCameraPageNumber,
        processNumber: "${translation.layoutStep} $getFrontCameraPageNumber",
        title: translation.frontCameraPageTitle,
        cardBodyText: translation.frontCameraPageDescription,
        iconReplacer: LayoutBuilder(
          builder: (context, constraints) {
            if (constraints.maxHeight < AppConfig.smallScreenHeight) {
              return Container(
                padding: EdgeInsets.symmetric(vertical: 10.h),
                color: Colors.transparent,
                child: _SmallFrontCameraPreview(
                  onImage: _processImage,
                ),
              );
            }

            return Container(
              padding: EdgeInsets.symmetric(vertical: 10.h),
              color: Colors.transparent,
              child: _NormaFrontCameraPreview(
                onImage: _processImage,
              ),
            );
          },
        ),
        underCard: frontCameraCheckState.isFrontCameraScanned
            ? Center(
                child: TestSuccessStatusIcon(theme: theme),
              )
            : const SizedBox(),
        onSkip: () async {
          await frontCameraCheckManager.saveCameraCheckStatus(false);
        });
  }

  void _handlingFrontCameraCheckPage({
    required BuildContext context,
    required WidgetRef ref,
    required bool isCheck,
    String? failedReason,
  }) {
    final isExecuted = ref.watch(resultIsExecutedStateProvider);
    final frontCameraCheckManager =
        ref.watch(frontCameraCheckManagerProvider.notifier);
    final resultHandler = TestCheckHandler(
      context: context,
      ref: ref,
      isCheck: isCheck,
      appRoutes: AppRoutes.frontCameraCheckPage,
      stateProvider: cameraCheckStateProvider,
      isExecuted: isExecuted,
    );

    if (isExecuted) return;
    Future.delayed(
            const Duration(milliseconds: AppConfig.futureSuccessAwaitDuration))
        .then(
      (_) async {
        await frontCameraCheckManager.saveCameraCheckStatus(isCheck);

        log("Front Camera Check: ${(await frontCameraCheckManager.getCameraCheckStatus()).toString()}");

        if (context.mounted) {
          await resultHandler.handlingStatus(
            failedReason: failedReason,
            onSkip: () async {
              await frontCameraCheckManager.saveCameraCheckStatus(false);
            },
          );
        }
      },
    );
  }
}

class _SmallFrontCameraPreview extends ConsumerWidget {
  const _SmallFrontCameraPreview({
    required this.onImage,
  });

  final Function(InputImage) onImage;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SizedBox(
      width: 145.w,
      height: 145.h,
      child: AspectRatio(
        aspectRatio: 1,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(250),
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: FrontCamDetectorViewWidget(
              onImage: onImage,
            ),
          ),
        ),
      ),
    );
  }
}

class _NormaFrontCameraPreview extends ConsumerWidget {
  const _NormaFrontCameraPreview({
    required this.onImage,
  });

  final Function(InputImage) onImage;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SizedBox(
      width: 175.w,
      height: 175.h,
      child: AspectRatio(
        aspectRatio: 1,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(250),
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: FrontCamDetectorViewWidget(
              onImage: onImage,
            ),
          ),
        ),
      ),
    );
  }
}
