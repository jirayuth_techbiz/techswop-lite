import 'package:camera/camera.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/camera/domain/camera_check.dart';
import 'package:techswop_lite/features/device_test/camera/domain/service/back_camera_check_service.dart';
import 'package:techswop_lite/features/device_test/camera/domain/service/front_camera_check_service.dart';

part 'camera_check_state.g.dart';

@riverpod
class CameraCheckState extends _$CameraCheckState {
  @override 
  CameraCheck build() {
    return const CameraCheck();
  }

  void updateInputImg(InputImage inputimg) {
    state = state.copyWith(inputimg: inputimg);
  }

  void updateCameraController(CameraController cameraController) {
    state = state.copyWith(cameraController: cameraController);
  }

  void isFrontCameraScanned(bool isScanned) {
    state = state.copyWith(isFrontCameraScanned: isScanned);
  }

  void isBackCameraScanned(bool isScanned) {
    state = state.copyWith(isBackCameraScanned: isScanned);
  }

  void updateCameraList(List<CameraDescription> cameras) {
    state = state.copyWith(cameras: cameras);
  }
}

@riverpod 
class FrontCameraCheckManager extends _$FrontCameraCheckManager {
  @override  
  FutureOr<void> build() async {}

  Future<bool?> getCameraCheckStatus() async {
    final frontCameraCheckService = ref.watch(frontCameraCheckServiceProvider);
    return await frontCameraCheckService.getFrontCameraCheckResult();
  }

  Future<void> saveCameraCheckStatus(bool status) async {
    final frontCameraCheckService = ref.watch(frontCameraCheckServiceProvider);
    state = const AsyncLoading();
    state = await AsyncValue.guard(() => frontCameraCheckService.saveFrontCameraCheckResult(status));
  }

  Future<List<CameraDescription>> getAvailableCameras() async {
    final frontCameraCheckService = ref.watch(frontCameraCheckServiceProvider);
    return await frontCameraCheckService.getAvailableCameras();
  }

  InputImage? readInputImageFromCameraImage(
    CameraImage image,
    CameraDescription camera,
    CameraController? cameraController,
  ) {
    final frontCameraCheckService = ref.watch(frontCameraCheckServiceProvider);
    return frontCameraCheckService.readInputImageFromFrontCameraImage(image, camera, cameraController);
  }

  Future<bool> checkFaceData(InputImage inputimg) async {
    final frontCameraCheckService = ref.watch(frontCameraCheckServiceProvider);
    return await frontCameraCheckService.checkFaceData(inputimg);
  }
}


@riverpod 
class BackCameraCheckManager extends _$BackCameraCheckManager {
  @override  
  FutureOr<void> build() async {}

  Future<bool?> getCameraCheckStatus() async {
    final backCameraCheckService = ref.watch(backCameraCheckServiceProvider);
    return await backCameraCheckService.getBackCameraCheckResult();
  }

  Future<void> saveCameraCheckStatus(bool status) async {
    final backCameraCheckService = ref.watch(backCameraCheckServiceProvider);
    state = const AsyncLoading();
    state = await AsyncValue.guard(() => backCameraCheckService.saveBackCameraCheckResult(status));
  }

  Future<List<CameraDescription>> getAvailableCameras() async {
    final backCameraCheckService = ref.watch(backCameraCheckServiceProvider);
    return await backCameraCheckService.getAvailableCameras();
  }

  InputImage? readInputImageFromCameraImage(
    CameraImage image,
    CameraDescription camera,
    CameraController? cameraController,
  ) {
    final backCameraCheckService = ref.watch(backCameraCheckServiceProvider);
    return backCameraCheckService.readInputImageFromBackCameraImage(image, camera, cameraController);
  }

  Future<bool> checkObjectData(InputImage inputimg) async {
    final backCameraCheckService = ref.watch(backCameraCheckServiceProvider);
    return await backCameraCheckService.checkObjectData(inputimg);
  }
}