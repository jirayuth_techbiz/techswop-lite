// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'camera_check_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$cameraCheckStateHash() => r'4f11300fb0ded895b00a8db9e61be9ed507c7db1';

/// See also [CameraCheckState].
@ProviderFor(CameraCheckState)
final cameraCheckStateProvider =
    AutoDisposeNotifierProvider<CameraCheckState, CameraCheck>.internal(
  CameraCheckState.new,
  name: r'cameraCheckStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$cameraCheckStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$CameraCheckState = AutoDisposeNotifier<CameraCheck>;
String _$frontCameraCheckManagerHash() =>
    r'46df71d54b7b0cb8c306bad9a96b86974b602582';

/// See also [FrontCameraCheckManager].
@ProviderFor(FrontCameraCheckManager)
final frontCameraCheckManagerProvider =
    AutoDisposeAsyncNotifierProvider<FrontCameraCheckManager, void>.internal(
  FrontCameraCheckManager.new,
  name: r'frontCameraCheckManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$frontCameraCheckManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FrontCameraCheckManager = AutoDisposeAsyncNotifier<void>;
String _$backCameraCheckManagerHash() =>
    r'18d576d27f5ce992f3e78943aadc2ac5a7c0d4b3';

/// See also [BackCameraCheckManager].
@ProviderFor(BackCameraCheckManager)
final backCameraCheckManagerProvider =
    AutoDisposeAsyncNotifierProvider<BackCameraCheckManager, void>.internal(
  BackCameraCheckManager.new,
  name: r'backCameraCheckManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$backCameraCheckManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$BackCameraCheckManager = AutoDisposeAsyncNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
