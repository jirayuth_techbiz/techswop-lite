import 'package:camera/camera.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:google_mlkit_object_detection/google_mlkit_object_detection.dart';

part 'camera_check.freezed.dart';

@freezed
class CameraCheck with _$CameraCheck {
  const factory CameraCheck({
    @Default([]) List<CameraDescription> cameras,
    @Default(false) bool isFrontCameraScanned,
    @Default(false) bool isBackCameraScanned,
    InputImage? inputimg,
    CameraController? cameraController,
  }) = _CameraCheck;

  const CameraCheck._();
}