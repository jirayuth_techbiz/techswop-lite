import 'package:camera/camera.dart';

abstract class CameraCheckService {
  Future<List<CameraDescription>> getAvailableCameras();
  Future<void> disposeCameraController(CameraController cameraController);
}
