import 'package:camera/camera.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/camera/application/front_camera_check_service_impl.dart';
import 'package:techswop_lite/features/device_test/camera/domain/repository/camera_check_repository.dart';
import 'package:techswop_lite/features/device_test/camera/domain/service/base/camera_check_service.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/shared/shared.dart';

part 'front_camera_check_service.g.dart';

@riverpod
FrontCameraCheckService frontCameraCheckService(
  FrontCameraCheckServiceRef ref,
) {
  final tradeinShareRepository =
      ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final cameraCheckRepository = ref.watch(cameraCheckRepositoryProvider);
  final loggerRepository = ref.watch(loggerRepositoryProvider);

  return FrontCameraCheckServiceImpl(
      ref: ref, 
      tradeInSharePreferencesRepository: tradeinShareRepository,
      cameraCheckRepository: cameraCheckRepository,
      loggerRepository: loggerRepository);
}

abstract class FrontCameraCheckService extends CameraCheckService {
  InputImage? readInputImageFromFrontCameraImage(
    CameraImage image,
    CameraDescription camera,
    CameraController? cameraController,
  );
  Future<bool?> getFrontCameraCheckResult();
  Future<void> saveFrontCameraCheckResult(bool status);
  Future<bool> checkFaceData(InputImage inputimg);
}
