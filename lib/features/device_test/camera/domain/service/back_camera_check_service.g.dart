// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'back_camera_check_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$backCameraCheckServiceHash() =>
    r'74620ff2dc14824e1e82faf4be2c390a2caa93d2';

/// See also [backCameraCheckService].
@ProviderFor(backCameraCheckService)
final backCameraCheckServiceProvider =
    AutoDisposeProvider<BackCameraCheckService>.internal(
  backCameraCheckService,
  name: r'backCameraCheckServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$backCameraCheckServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef BackCameraCheckServiceRef
    = AutoDisposeProviderRef<BackCameraCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
