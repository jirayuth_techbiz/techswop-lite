// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'front_camera_check_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$frontCameraCheckServiceHash() =>
    r'd150d6e6d6367c32c69429da9235080356bc78cf';

/// See also [frontCameraCheckService].
@ProviderFor(frontCameraCheckService)
final frontCameraCheckServiceProvider =
    AutoDisposeProvider<FrontCameraCheckService>.internal(
  frontCameraCheckService,
  name: r'frontCameraCheckServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$frontCameraCheckServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef FrontCameraCheckServiceRef
    = AutoDisposeProviderRef<FrontCameraCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
