import 'package:camera/camera.dart';
import 'package:google_mlkit_object_detection/google_mlkit_object_detection.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/camera/application/back_camera_check_service_impl.dart';
import 'package:techswop_lite/features/device_test/camera/domain/repository/camera_check_repository.dart';
import 'package:techswop_lite/features/device_test/camera/domain/service/base/camera_check_service.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

part 'back_camera_check_service.g.dart';

@riverpod
BackCameraCheckService backCameraCheckService(BackCameraCheckServiceRef ref) {
  final tradeinShareRepository =
      ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final cameraCheckRepository = ref.watch(cameraCheckRepositoryProvider);
  final loggerRepository = ref.watch(loggerRepositoryProvider);

  return BackCameraCheckServiceImpl(
      tradeInSharePreferencesRepository: tradeinShareRepository, 
      cameraCheckRepository: cameraCheckRepository,
      loggerRepository: loggerRepository);
}

abstract class BackCameraCheckService extends CameraCheckService {
  InputImage? readInputImageFromBackCameraImage(
    CameraImage image,
    CameraDescription camera,
    CameraController? cameraController,
  );
  Future<bool?> getBackCameraCheckResult();
  Future<void> saveBackCameraCheckResult(bool status);
  Future<bool> checkObjectData(InputImage inputimg);
}
