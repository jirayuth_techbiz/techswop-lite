// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'camera_check_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$cameraCheckRepositoryHash() =>
    r'42fa7a3208ca20f7cbc977947e2b410d2d990e93';

/// See also [cameraCheckRepository].
@ProviderFor(cameraCheckRepository)
final cameraCheckRepositoryProvider =
    AutoDisposeProvider<CameraCheckRepository>.internal(
  cameraCheckRepository,
  name: r'cameraCheckRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$cameraCheckRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef CameraCheckRepositoryRef
    = AutoDisposeProviderRef<CameraCheckRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
