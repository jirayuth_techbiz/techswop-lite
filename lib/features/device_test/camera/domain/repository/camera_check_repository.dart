import 'package:camera/camera.dart';
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'package:google_mlkit_object_detection/google_mlkit_object_detection.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/camera/data/camera_repository_impl.dart';

part 'camera_check_repository.g.dart';

@riverpod
CameraCheckRepository cameraCheckRepository(CameraCheckRepositoryRef ref) {
  final FaceDetector faceDetector = FaceDetector(
    options: FaceDetectorOptions(
      performanceMode: FaceDetectorMode.accurate,
      enableContours: true,
      enableLandmarks: true,
      enableTracking: true,
      enableClassification: true,
    ),
  );

  final ObjectDetector objectDetector = ObjectDetector(
    options: ObjectDetectorOptions(
      mode: DetectionMode.stream,
      classifyObjects: false,
      multipleObjects: true,
    ),
  );

  return CameraCheckRepositoryImpl(ref, faceDetector, objectDetector);
}

abstract class CameraCheckRepository {
  Future<List<CameraDescription>> getAvailableCameras();
  Future<List<Face>> getFacesData(InputImage inputimg);
  Future<List<DetectedObject>> getObjectData(InputImage inputimg);
}
