// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'camera_check.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CameraCheck {
  List<CameraDescription> get cameras => throw _privateConstructorUsedError;
  bool get isFrontCameraScanned => throw _privateConstructorUsedError;
  bool get isBackCameraScanned => throw _privateConstructorUsedError;
  InputImage? get inputimg => throw _privateConstructorUsedError;
  CameraController? get cameraController => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CameraCheckCopyWith<CameraCheck> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CameraCheckCopyWith<$Res> {
  factory $CameraCheckCopyWith(
          CameraCheck value, $Res Function(CameraCheck) then) =
      _$CameraCheckCopyWithImpl<$Res, CameraCheck>;
  @useResult
  $Res call(
      {List<CameraDescription> cameras,
      bool isFrontCameraScanned,
      bool isBackCameraScanned,
      InputImage? inputimg,
      CameraController? cameraController});
}

/// @nodoc
class _$CameraCheckCopyWithImpl<$Res, $Val extends CameraCheck>
    implements $CameraCheckCopyWith<$Res> {
  _$CameraCheckCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? cameras = null,
    Object? isFrontCameraScanned = null,
    Object? isBackCameraScanned = null,
    Object? inputimg = freezed,
    Object? cameraController = freezed,
  }) {
    return _then(_value.copyWith(
      cameras: null == cameras
          ? _value.cameras
          : cameras // ignore: cast_nullable_to_non_nullable
              as List<CameraDescription>,
      isFrontCameraScanned: null == isFrontCameraScanned
          ? _value.isFrontCameraScanned
          : isFrontCameraScanned // ignore: cast_nullable_to_non_nullable
              as bool,
      isBackCameraScanned: null == isBackCameraScanned
          ? _value.isBackCameraScanned
          : isBackCameraScanned // ignore: cast_nullable_to_non_nullable
              as bool,
      inputimg: freezed == inputimg
          ? _value.inputimg
          : inputimg // ignore: cast_nullable_to_non_nullable
              as InputImage?,
      cameraController: freezed == cameraController
          ? _value.cameraController
          : cameraController // ignore: cast_nullable_to_non_nullable
              as CameraController?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CameraCheckImplCopyWith<$Res>
    implements $CameraCheckCopyWith<$Res> {
  factory _$$CameraCheckImplCopyWith(
          _$CameraCheckImpl value, $Res Function(_$CameraCheckImpl) then) =
      __$$CameraCheckImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<CameraDescription> cameras,
      bool isFrontCameraScanned,
      bool isBackCameraScanned,
      InputImage? inputimg,
      CameraController? cameraController});
}

/// @nodoc
class __$$CameraCheckImplCopyWithImpl<$Res>
    extends _$CameraCheckCopyWithImpl<$Res, _$CameraCheckImpl>
    implements _$$CameraCheckImplCopyWith<$Res> {
  __$$CameraCheckImplCopyWithImpl(
      _$CameraCheckImpl _value, $Res Function(_$CameraCheckImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? cameras = null,
    Object? isFrontCameraScanned = null,
    Object? isBackCameraScanned = null,
    Object? inputimg = freezed,
    Object? cameraController = freezed,
  }) {
    return _then(_$CameraCheckImpl(
      cameras: null == cameras
          ? _value._cameras
          : cameras // ignore: cast_nullable_to_non_nullable
              as List<CameraDescription>,
      isFrontCameraScanned: null == isFrontCameraScanned
          ? _value.isFrontCameraScanned
          : isFrontCameraScanned // ignore: cast_nullable_to_non_nullable
              as bool,
      isBackCameraScanned: null == isBackCameraScanned
          ? _value.isBackCameraScanned
          : isBackCameraScanned // ignore: cast_nullable_to_non_nullable
              as bool,
      inputimg: freezed == inputimg
          ? _value.inputimg
          : inputimg // ignore: cast_nullable_to_non_nullable
              as InputImage?,
      cameraController: freezed == cameraController
          ? _value.cameraController
          : cameraController // ignore: cast_nullable_to_non_nullable
              as CameraController?,
    ));
  }
}

/// @nodoc

class _$CameraCheckImpl extends _CameraCheck {
  const _$CameraCheckImpl(
      {final List<CameraDescription> cameras = const [],
      this.isFrontCameraScanned = false,
      this.isBackCameraScanned = false,
      this.inputimg,
      this.cameraController})
      : _cameras = cameras,
        super._();

  final List<CameraDescription> _cameras;
  @override
  @JsonKey()
  List<CameraDescription> get cameras {
    if (_cameras is EqualUnmodifiableListView) return _cameras;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_cameras);
  }

  @override
  @JsonKey()
  final bool isFrontCameraScanned;
  @override
  @JsonKey()
  final bool isBackCameraScanned;
  @override
  final InputImage? inputimg;
  @override
  final CameraController? cameraController;

  @override
  String toString() {
    return 'CameraCheck(cameras: $cameras, isFrontCameraScanned: $isFrontCameraScanned, isBackCameraScanned: $isBackCameraScanned, inputimg: $inputimg, cameraController: $cameraController)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CameraCheckImpl &&
            const DeepCollectionEquality().equals(other._cameras, _cameras) &&
            (identical(other.isFrontCameraScanned, isFrontCameraScanned) ||
                other.isFrontCameraScanned == isFrontCameraScanned) &&
            (identical(other.isBackCameraScanned, isBackCameraScanned) ||
                other.isBackCameraScanned == isBackCameraScanned) &&
            (identical(other.inputimg, inputimg) ||
                other.inputimg == inputimg) &&
            (identical(other.cameraController, cameraController) ||
                other.cameraController == cameraController));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_cameras),
      isFrontCameraScanned,
      isBackCameraScanned,
      inputimg,
      cameraController);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CameraCheckImplCopyWith<_$CameraCheckImpl> get copyWith =>
      __$$CameraCheckImplCopyWithImpl<_$CameraCheckImpl>(this, _$identity);
}

abstract class _CameraCheck extends CameraCheck {
  const factory _CameraCheck(
      {final List<CameraDescription> cameras,
      final bool isFrontCameraScanned,
      final bool isBackCameraScanned,
      final InputImage? inputimg,
      final CameraController? cameraController}) = _$CameraCheckImpl;
  const _CameraCheck._() : super._();

  @override
  List<CameraDescription> get cameras;
  @override
  bool get isFrontCameraScanned;
  @override
  bool get isBackCameraScanned;
  @override
  InputImage? get inputimg;
  @override
  CameraController? get cameraController;
  @override
  @JsonKey(ignore: true)
  _$$CameraCheckImplCopyWith<_$CameraCheckImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
