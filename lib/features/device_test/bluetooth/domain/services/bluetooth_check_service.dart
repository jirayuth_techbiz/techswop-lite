import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/bluetooth/application/bluetooth_check_service_impl.dart';
import 'package:techswop_lite/features/device_test/bluetooth/domain/bluetooth_check.dart';
import 'package:techswop_lite/features/device_test/bluetooth/domain/repository/bluetooth_check_repository.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

part 'bluetooth_check_service.g.dart';

@riverpod
BluetoothCheckService bluetoothCheckService(BluetoothCheckServiceRef ref) {
  final bluetoothRepository = ref.watch(bluetoothCheckRepositoryProvider);
  final sharePreferencesRepository =
      ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final loggerRepository = ref.watch(loggerRepositoryProvider);

  return BluetoothCheckServiceImpl(
    ref,
    logger: loggerRepository,
    bluetoothCheckRepository: bluetoothRepository,
    prefs: sharePreferencesRepository,
  );
}

abstract class BluetoothCheckService {
  Stream<BluetoothCheck> updateBluetoothCheck();
  Future<bool?> getBluetoothCheckStatus();
  Future<void> saveBluetoothCheckStatus(bool status);
}
