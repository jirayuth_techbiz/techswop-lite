// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bluetooth_check_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bluetoothCheckServiceHash() =>
    r'c509c29b419f58a33733d83a4d8d7452d5fdbc48';

/// See also [bluetoothCheckService].
@ProviderFor(bluetoothCheckService)
final bluetoothCheckServiceProvider =
    AutoDisposeProvider<BluetoothCheckService>.internal(
  bluetoothCheckService,
  name: r'bluetoothCheckServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$bluetoothCheckServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef BluetoothCheckServiceRef
    = AutoDisposeProviderRef<BluetoothCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
