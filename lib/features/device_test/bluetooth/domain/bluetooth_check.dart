import 'package:freezed_annotation/freezed_annotation.dart';

part 'bluetooth_check.freezed.dart';

@freezed
class BluetoothCheck with _$BluetoothCheck {
  factory BluetoothCheck({
    @Default(true) bool isBluetoothOn,
    // @Default(false) bool isBluetoothSupported,
    @Default(false) bool isBluetoothDeviceScanFound,
  }) = _BluetoothCheck;

  BluetoothCheck._();
}
