// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'bluetooth_check.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$BluetoothCheck {
  bool get isBluetoothOn =>
      throw _privateConstructorUsedError; // @Default(false) bool isBluetoothSupported,
  bool get isBluetoothDeviceScanFound => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $BluetoothCheckCopyWith<BluetoothCheck> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BluetoothCheckCopyWith<$Res> {
  factory $BluetoothCheckCopyWith(
          BluetoothCheck value, $Res Function(BluetoothCheck) then) =
      _$BluetoothCheckCopyWithImpl<$Res, BluetoothCheck>;
  @useResult
  $Res call({bool isBluetoothOn, bool isBluetoothDeviceScanFound});
}

/// @nodoc
class _$BluetoothCheckCopyWithImpl<$Res, $Val extends BluetoothCheck>
    implements $BluetoothCheckCopyWith<$Res> {
  _$BluetoothCheckCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isBluetoothOn = null,
    Object? isBluetoothDeviceScanFound = null,
  }) {
    return _then(_value.copyWith(
      isBluetoothOn: null == isBluetoothOn
          ? _value.isBluetoothOn
          : isBluetoothOn // ignore: cast_nullable_to_non_nullable
              as bool,
      isBluetoothDeviceScanFound: null == isBluetoothDeviceScanFound
          ? _value.isBluetoothDeviceScanFound
          : isBluetoothDeviceScanFound // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$BluetoothCheckImplCopyWith<$Res>
    implements $BluetoothCheckCopyWith<$Res> {
  factory _$$BluetoothCheckImplCopyWith(_$BluetoothCheckImpl value,
          $Res Function(_$BluetoothCheckImpl) then) =
      __$$BluetoothCheckImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isBluetoothOn, bool isBluetoothDeviceScanFound});
}

/// @nodoc
class __$$BluetoothCheckImplCopyWithImpl<$Res>
    extends _$BluetoothCheckCopyWithImpl<$Res, _$BluetoothCheckImpl>
    implements _$$BluetoothCheckImplCopyWith<$Res> {
  __$$BluetoothCheckImplCopyWithImpl(
      _$BluetoothCheckImpl _value, $Res Function(_$BluetoothCheckImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isBluetoothOn = null,
    Object? isBluetoothDeviceScanFound = null,
  }) {
    return _then(_$BluetoothCheckImpl(
      isBluetoothOn: null == isBluetoothOn
          ? _value.isBluetoothOn
          : isBluetoothOn // ignore: cast_nullable_to_non_nullable
              as bool,
      isBluetoothDeviceScanFound: null == isBluetoothDeviceScanFound
          ? _value.isBluetoothDeviceScanFound
          : isBluetoothDeviceScanFound // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$BluetoothCheckImpl extends _BluetoothCheck {
  _$BluetoothCheckImpl(
      {this.isBluetoothOn = true, this.isBluetoothDeviceScanFound = false})
      : super._();

  @override
  @JsonKey()
  final bool isBluetoothOn;
// @Default(false) bool isBluetoothSupported,
  @override
  @JsonKey()
  final bool isBluetoothDeviceScanFound;

  @override
  String toString() {
    return 'BluetoothCheck(isBluetoothOn: $isBluetoothOn, isBluetoothDeviceScanFound: $isBluetoothDeviceScanFound)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BluetoothCheckImpl &&
            (identical(other.isBluetoothOn, isBluetoothOn) ||
                other.isBluetoothOn == isBluetoothOn) &&
            (identical(other.isBluetoothDeviceScanFound,
                    isBluetoothDeviceScanFound) ||
                other.isBluetoothDeviceScanFound ==
                    isBluetoothDeviceScanFound));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, isBluetoothOn, isBluetoothDeviceScanFound);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$BluetoothCheckImplCopyWith<_$BluetoothCheckImpl> get copyWith =>
      __$$BluetoothCheckImplCopyWithImpl<_$BluetoothCheckImpl>(
          this, _$identity);
}

abstract class _BluetoothCheck extends BluetoothCheck {
  factory _BluetoothCheck(
      {final bool isBluetoothOn,
      final bool isBluetoothDeviceScanFound}) = _$BluetoothCheckImpl;
  _BluetoothCheck._() : super._();

  @override
  bool get isBluetoothOn;
  @override // @Default(false) bool isBluetoothSupported,
  bool get isBluetoothDeviceScanFound;
  @override
  @JsonKey(ignore: true)
  _$$BluetoothCheckImplCopyWith<_$BluetoothCheckImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
