// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bluetooth_check_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bluetoothCheckRepositoryHash() =>
    r'0f567fcf6763b60c1a6a29098fdca6ad217d6032';

/// See also [bluetoothCheckRepository].
@ProviderFor(bluetoothCheckRepository)
final bluetoothCheckRepositoryProvider =
    AutoDisposeProvider<BluetoothCheckRepository>.internal(
  bluetoothCheckRepository,
  name: r'bluetoothCheckRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$bluetoothCheckRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef BluetoothCheckRepositoryRef
    = AutoDisposeProviderRef<BluetoothCheckRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
