import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/bluetooth/data/bluetooth_check_repository_impl.dart';
import 'package:techswop_lite/features/device_test/bluetooth/data/bluetooth_check_wrapper.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';

part 'bluetooth_check_repository.g.dart';

@riverpod
BluetoothCheckRepository bluetoothCheckRepository(
    BluetoothCheckRepositoryRef ref) {
  final FlutterBluePlusWrapper flutterBluePlus = FlutterBluePlusWrapper();
  final loggerRepository = ref.watch(loggerRepositoryProvider);

  return BluetoothCheckRepositoryImpl(
    ref,
    flutterBluePlus: flutterBluePlus,
    logger: loggerRepository,
  );
}

abstract class BluetoothCheckRepository {
  Stream<BluetoothAdapterState> isBluetoothEnabled();
  Stream<Set<DeviceIdentifier>> getBluetoothDevice();
  Future<bool> getBluetoothSupportedStatus();
}
