import 'package:flutter_blue_plus/flutter_blue_plus.dart';

class FlutterBluePlusWrapper {
  Future<bool> get isSupported async => await FlutterBluePlus.isSupported;
  Stream<BluetoothAdapterState> get adapterState => FlutterBluePlus.adapterState;
  Future<void> startScan() => FlutterBluePlus.startScan();
  Future<void> stopScan() => FlutterBluePlus.stopScan();
  Stream<List<ScanResult>> get scanResults => FlutterBluePlus.scanResults;
}
