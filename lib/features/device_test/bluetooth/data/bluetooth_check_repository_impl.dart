import 'dart:async';

import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/bluetooth/data/bluetooth_check_wrapper.dart';
import 'package:techswop_lite/features/device_test/bluetooth/domain/repository/bluetooth_check_repository.dart';
import 'package:techswop_lite/shared/shared.dart';

class BluetoothCheckRepositoryImpl implements BluetoothCheckRepository {
  final FlutterBluePlusWrapper flutterBluePlus;
  final Ref ref;
  final LoggerRepository logger;
  BluetoothCheckRepositoryImpl(
    this.ref, {
    required this.flutterBluePlus,
    required this.logger,
  });

  @override
  Stream<Set<DeviceIdentifier>> getBluetoothDevice() async* {
    final Set<DeviceIdentifier> seen = {};
    final StreamController<Set<DeviceIdentifier>> bluetoothStream =
        StreamController();
    StreamSubscription? adapterStateSubscription;
    StreamSubscription? scanResultsSubscription;
    adapterStateSubscription =
        flutterBluePlus.adapterState.listen((blState) async {
      try {
        await flutterBluePlus.startScan();
        scanResultsSubscription = flutterBluePlus.scanResults.listen(
          (results) async {
            logger.logDebug('Scan results: $results');
            for (final ScanResult r in results) {
              if (!seen.contains(r.device.remoteId)) {
                seen.add(r.device.remoteId);
              }
            }
            bluetoothStream.sink.add(seen);

            if (seen.isNotEmpty) {
              await flutterBluePlus.stopScan();
              await adapterStateSubscription?.cancel();
              await scanResultsSubscription?.cancel();
            }
          },
          onError: (e) {
            bluetoothStream.sink.addError(e);
            logger.logWTF(e);
          },
        );
      } catch (e) {
        bluetoothStream.sink.addError(e);
      }
    });

    ref.onDispose(() {
      adapterStateSubscription?.cancel();
      scanResultsSubscription?.cancel();
      bluetoothStream.close();
      log('BluetoothCheckRepository disposed');
    });

    yield* bluetoothStream.stream;
  }

  @override
  Future<bool> getBluetoothSupportedStatus() async {
    var result = await flutterBluePlus.isSupported;
    return result;
  }

  @override
  Stream<BluetoothAdapterState> isBluetoothEnabled() async* {
    StreamController<BluetoothAdapterState> bluetoothState =
        StreamController<BluetoothAdapterState>();
    StreamSubscription? bluetoothStateSubscription;

    bluetoothStateSubscription = flutterBluePlus.adapterState.listen((element) {
      bluetoothState.sink.add(element);
    });

    ref.onDispose(() {
      bluetoothStateSubscription?.cancel();
      bluetoothState.close();
      logger.logDebug('BluetoothCheckRepository disposed');
    });

    yield* bluetoothState.stream;
  }
}
