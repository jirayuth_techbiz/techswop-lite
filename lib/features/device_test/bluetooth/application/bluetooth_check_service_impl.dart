import 'dart:async';

import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/features/device_test/bluetooth/domain/bluetooth_check.dart';
import 'package:techswop_lite/features/device_test/bluetooth/domain/repository/bluetooth_check_repository.dart';
import 'package:techswop_lite/features/device_test/bluetooth/domain/services/bluetooth_check_service.dart';
import 'package:techswop_lite/shared/shared.dart';

class BluetoothCheckServiceImpl implements BluetoothCheckService {
  final Ref ref;
  final BluetoothCheckRepository bluetoothCheckRepository;
  final TradeInSharePreferencesRepository prefs;
  final LoggerRepository logger;

  BluetoothCheckServiceImpl(
      this.ref,{
      required this.bluetoothCheckRepository,
      required this.prefs,
      required this.logger,
    });

  @override
  Future<bool?> getBluetoothCheckStatus() async {
    final result = await prefs.getBool(AppRoutes.bluetoothCheckPage.titleAPI);
    return result;
  }

  @override
  Future<void> saveBluetoothCheckStatus(bool status) async {
    await prefs.saveBool(AppRoutes.bluetoothCheckPage.titleAPI, status);
  }

  @override
  Stream<BluetoothCheck> updateBluetoothCheck() async* {
    final StreamController<BluetoothCheck> bluetoothStatusController =
        StreamController<BluetoothCheck>();

    var result = BluetoothCheck();

    try {
      final isBluetoothOn =
          bluetoothCheckRepository.isBluetoothEnabled().listen((event) {
        _checkIsBluetoothOn(event, result, bluetoothStatusController);
      });

      logger.logInfo('isBluetoothOn : $result');

      final blueToothDeviceScanStream =
          bluetoothCheckRepository.getBluetoothDevice().listen((event) {
        _checkIsBluetoothDeviceScanFound(
            event, result, bluetoothStatusController);
      });

      logger.logInfo('blueToothDeviceScanStream : $result');

      ///TODO : find out why this is returning false
      ///
      ///Even though the device is supported and turn Bluetooth on.
      // final isBluetoothSupported =
      //     await bluetoothCheckRepository.getBluetoothSupportedStatus();

      // _checkIsBluetoothSupported(
      //     isBluetoothSupported, result, bluetoothStatusController);

      logger.logInfo('isBluetoothSupported : $result');

      bluetoothStatusController.sink.add(result);

      if (result.isBluetoothDeviceScanFound && result.isBluetoothOn) {
        bluetoothStatusController.close();
      }

      ref.onDispose(() {
        isBluetoothOn.cancel();
        bluetoothStatusController.close();
        blueToothDeviceScanStream.cancel();
      });

      yield* bluetoothStatusController.stream;
    } catch (e) {
      result = result.copyWith(isBluetoothDeviceScanFound: false);
      bluetoothStatusController.sink.addError(e);
      rethrow;
    }
  }

  void _checkIsBluetoothOn(BluetoothAdapterState event, BluetoothCheck result,
      StreamController<BluetoothCheck> bluetoothStatusController) {

    if (event == BluetoothAdapterState.on) {
      result = result.copyWith(isBluetoothOn: true);
    } else if (event == BluetoothAdapterState.off) {
      result = result.copyWith(isBluetoothOn: false);
    }
    
    bluetoothStatusController.sink.add(result);
  }

  ///TODO : Find a way to make this not returning false
  // void _checkIsBluetoothSupported(
  //     bool isBluetoothSupported,
  //     BluetoothCheck result,
  //     StreamController<BluetoothCheck> bluetoothStatusController) {
  //   if (isBluetoothSupported) {
  //     result = result.copyWith(isBluetoothSupported: true);
  //   } else {
  //     result = result.copyWith(isBluetoothSupported: false);
  //   }
  //   bluetoothStatusController.sink.add(result);
  // }

  void _checkIsBluetoothDeviceScanFound(
      Set<DeviceIdentifier> event,
      BluetoothCheck result,
      StreamController<BluetoothCheck> bluetoothStatusController) {
    if (event.isNotEmpty) {
      result = result.copyWith(isBluetoothDeviceScanFound: true);
    } else {
      result = result.copyWith(isBluetoothDeviceScanFound: false);
    }
    bluetoothStatusController.sink.add(result);
  }
}
