import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/test_result/test_failed_status_icon.dart';
import 'package:techswop_lite/features/device_test/bluetooth/presentation/state/bluetooth_check_state.dart';
import 'package:techswop_lite/route/mobile_route.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';

class BluetoothCheckPage extends ConsumerWidget {
  const BluetoothCheckPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final translation = context.localization;
    final bluetoothCheckState = ref.watch(bluetoothCheckStateProvider);
    final getBluetoothPageNumber = ref
        .watch(getCurrentPageNumberProvider(AppRoutes.bluetoothCheckPage.path));
    final theme = context.theme;
    final bluetoothCheckManager =
        ref.watch(bluetoothCheckManagerProvider.notifier);

    ref.listen(bluetoothCheckStateProvider, (prev, next) {
      if (next.asData!.value.isBluetoothOn &&
          next.asData!.value.isBluetoothDeviceScanFound) {
        _handlingBluetoothStatus(
          context: context,
          ref: ref,
          isCheck: true,
        );
      } else if (!next.asData!.value.isBluetoothOn && !prev!.isLoading) {
        _handlingBluetoothStatus(
          context: context,
          ref: ref,
          isCheck: false,
          failedReason: translation.bluetoothPageTestFailed,
        );
      }
    });

    return TestPageWidgetLayout(
        nextPage: getBluetoothPageNumber,
        processNumber: "${translation.layoutStep} $getBluetoothPageNumber",
        title: translation.bluetoothPageTitle,
        pictureAssetDirectory: CommonConst.CHECKLIST_PROPERTIES
            .firstWhere((element) =>
                element.appRoutesPage == AppRoutes.bluetoothCheckPage)
            .icon,
        underCard: AsyncValueWidget(
          value: bluetoothCheckState,
          data: (result) {
            if (result.isBluetoothOn && result.isBluetoothDeviceScanFound) {
              return Center(child: TestSuccessStatusIcon(theme: theme));
            } else if (!result.isBluetoothOn) {
              return Center(child: TestFailedStatusIcon(theme: theme));
            }
            return const SizedBox();
          },
          loading: () => const SizedBox(),
        ),
        cardBodyText: translation.bluetoothPageDescription,
        onSkip: () async {
          await bluetoothCheckManager.saveBluetoothCheckStatus(false);
        });
  }

  /// จัดการผลการทดสอบระบบ Bluetooth
  ///
  /// [context] คือ BuildContext ของหน้าจอ ใช้เข้าถึง Theme และ Localization
  ///
  /// [ref] คือ WidgetRef ของหน้าจอ เอาไว้เรียกใช้ Provider
  ///
  /// [isCheck] คือ ผลการทดสอบระบบ Bluetooth ถ้าผู้ใช้ตอบคำถามถูกต้องจะเป็น true ถ้าตอบผิดจะเป็น false
  ///
  /// [failedReason] คือ ข้อความที่แสดงถ้าผู้ใช้ทดสอบไม่ผ่าน
  void _handlingBluetoothStatus({
    required BuildContext context,
    required WidgetRef ref,
    required bool isCheck,
    String? failedReason,
  }) async {
    final isExecuted = ref.watch(resultIsExecutedStateProvider);
    final bluetoothCheckManager =
        ref.watch(bluetoothCheckManagerProvider.notifier);
    final resultHandler = TestCheckHandler(
      context: context,
      ref: ref,
      isCheck: isCheck,
      appRoutes: AppRoutes.bluetoothCheckPage,
      stateProvider: bluetoothCheckStateProvider,
      isExecuted: isExecuted,
    );

    if (isExecuted) return;
    Future.delayed(
        const Duration(seconds: AppConfig.futureDelayedTestAwaitDuration),
        () async {
      await bluetoothCheckManager.saveBluetoothCheckStatus(isCheck);

      if (context.mounted) {
        await resultHandler.handlingStatus(
          onSkip: () async {
            await bluetoothCheckManager.saveBluetoothCheckStatus(false);
          },
          failedReason: failedReason,
        );
      }
    });
  }
}
