// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bluetooth_check_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bluetoothCheckStateHash() =>
    r'0aa57971dfae67164cb93ebc38e3839d7f4c0a2e';

/// See also [BluetoothCheckState].
@ProviderFor(BluetoothCheckState)
final bluetoothCheckStateProvider = AutoDisposeStreamNotifierProvider<
    BluetoothCheckState, BluetoothCheck>.internal(
  BluetoothCheckState.new,
  name: r'bluetoothCheckStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$bluetoothCheckStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$BluetoothCheckState = AutoDisposeStreamNotifier<BluetoothCheck>;
String _$bluetoothCheckManagerHash() =>
    r'0ee1b9b76cb7c73f390c0d778fe9acb04bfdd2d3';

/// See also [BluetoothCheckManager].
@ProviderFor(BluetoothCheckManager)
final bluetoothCheckManagerProvider =
    AutoDisposeAsyncNotifierProvider<BluetoothCheckManager, void>.internal(
  BluetoothCheckManager.new,
  name: r'bluetoothCheckManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$bluetoothCheckManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$BluetoothCheckManager = AutoDisposeAsyncNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
