import 'dart:async';

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/bluetooth/domain/bluetooth_check.dart';
import 'package:techswop_lite/features/device_test/bluetooth/domain/services/bluetooth_check_service.dart';

part 'bluetooth_check_state.g.dart';

@riverpod
class BluetoothCheckState extends _$BluetoothCheckState {
  @override
  Stream<BluetoothCheck> build() async* {
    final bluetoothCheckService = ref.watch(bluetoothCheckServiceProvider);

    yield* bluetoothCheckService.updateBluetoothCheck();
  }
}

@riverpod 
class BluetoothCheckManager extends _$BluetoothCheckManager {
  @override
  FutureOr<void> build() async {}

  Future<bool?> getBluetoothCheckStatus() async {
    final bluetoothCheckService = ref.watch(bluetoothCheckServiceProvider);
    return await bluetoothCheckService.getBluetoothCheckStatus();
  }

  Future<void> saveBluetoothCheckStatus(bool status) async {
    final bluetoothCheckService = ref.watch(bluetoothCheckServiceProvider);
    state = const AsyncLoading();
    state = await AsyncValue.guard(() => bluetoothCheckService.saveBluetoothCheckStatus(status));
  }
}