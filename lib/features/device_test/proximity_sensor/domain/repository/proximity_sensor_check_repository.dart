import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/data/proximity_sensor_check_repository_impl.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/data/proximity_sensor_check_wrapper.dart';

part 'proximity_sensor_check_repository.g.dart';

@riverpod
ProximitySensorCheckRepository proximitySensorCheckRepository(
    ProximitySensorCheckRepositoryRef ref) {
  final ProximitySensorCheckWrapper proximitySensor = ProximitySensorCheckWrapper();
  return ProximitySensorCheckRepositoryImpl(ref, proximitySensor);
}

abstract class ProximitySensorCheckRepository {
  Stream<int> getProximitySensor();
}
