// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'proximity_sensor_check_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$proximitySensorCheckRepositoryHash() =>
    r'51ccaddd252d5d4adc7b329fe6f3dac7d095dd37';

/// See also [proximitySensorCheckRepository].
@ProviderFor(proximitySensorCheckRepository)
final proximitySensorCheckRepositoryProvider =
    AutoDisposeProvider<ProximitySensorCheckRepository>.internal(
  proximitySensorCheckRepository,
  name: r'proximitySensorCheckRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$proximitySensorCheckRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef ProximitySensorCheckRepositoryRef
    = AutoDisposeProviderRef<ProximitySensorCheckRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
