// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'proximity_sensor_check_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$proximitySensorCheckServiceHash() =>
    r'a48239ccb9c4f0696f6be30f31a7b3893f98da9f';

/// See also [proximitySensorCheckService].
@ProviderFor(proximitySensorCheckService)
final proximitySensorCheckServiceProvider =
    AutoDisposeProvider<ProximitySensorCheckService>.internal(
  proximitySensorCheckService,
  name: r'proximitySensorCheckServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$proximitySensorCheckServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef ProximitySensorCheckServiceRef
    = AutoDisposeProviderRef<ProximitySensorCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
