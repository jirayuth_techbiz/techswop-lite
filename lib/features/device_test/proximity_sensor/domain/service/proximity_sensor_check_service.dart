import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/application/proximity_sensor_check_service_impl.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/domain/proximity_sensor_check.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/domain/repository/proximity_sensor_check_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

part 'proximity_sensor_check_service.g.dart';

@riverpod
ProximitySensorCheckService proximitySensorCheckService(
    ProximitySensorCheckServiceRef ref) {
  final TradeInSharePreferencesRepository tradeinSharePreferencesRepository =
      ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final ProximitySensorCheckRepository proximitySensorCheckRepository =
      ref.watch(proximitySensorCheckRepositoryProvider);

  return ProximitySensorCheckServiceImpl(
    ref,
    proximitySensorCheckRepository: proximitySensorCheckRepository,
    tradeinSharePreferencesRepository: tradeinSharePreferencesRepository,
  );
}

abstract class ProximitySensorCheckService {
  Stream<ProximitySensorCheck> getProximitySensor();
  Future<bool?> getProximitySensorCheckResult();
  Future<void> saveProximitySensorCheckResult(bool status);
}
