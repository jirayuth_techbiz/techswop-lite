import 'package:freezed_annotation/freezed_annotation.dart';

part 'proximity_sensor_check.freezed.dart';

@freezed
class ProximitySensorCheck with _$ProximitySensorCheck {
  const factory ProximitySensorCheck({
    @Default(false) bool isNear,
  }) = _ProximitySensorCheck;

  const ProximitySensorCheck._();
}