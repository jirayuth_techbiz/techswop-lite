// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'proximity_sensor_check.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ProximitySensorCheck {
  bool get isNear => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProximitySensorCheckCopyWith<ProximitySensorCheck> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProximitySensorCheckCopyWith<$Res> {
  factory $ProximitySensorCheckCopyWith(ProximitySensorCheck value,
          $Res Function(ProximitySensorCheck) then) =
      _$ProximitySensorCheckCopyWithImpl<$Res, ProximitySensorCheck>;
  @useResult
  $Res call({bool isNear});
}

/// @nodoc
class _$ProximitySensorCheckCopyWithImpl<$Res,
        $Val extends ProximitySensorCheck>
    implements $ProximitySensorCheckCopyWith<$Res> {
  _$ProximitySensorCheckCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isNear = null,
  }) {
    return _then(_value.copyWith(
      isNear: null == isNear
          ? _value.isNear
          : isNear // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ProximitySensorCheckImplCopyWith<$Res>
    implements $ProximitySensorCheckCopyWith<$Res> {
  factory _$$ProximitySensorCheckImplCopyWith(_$ProximitySensorCheckImpl value,
          $Res Function(_$ProximitySensorCheckImpl) then) =
      __$$ProximitySensorCheckImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isNear});
}

/// @nodoc
class __$$ProximitySensorCheckImplCopyWithImpl<$Res>
    extends _$ProximitySensorCheckCopyWithImpl<$Res, _$ProximitySensorCheckImpl>
    implements _$$ProximitySensorCheckImplCopyWith<$Res> {
  __$$ProximitySensorCheckImplCopyWithImpl(_$ProximitySensorCheckImpl _value,
      $Res Function(_$ProximitySensorCheckImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isNear = null,
  }) {
    return _then(_$ProximitySensorCheckImpl(
      isNear: null == isNear
          ? _value.isNear
          : isNear // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$ProximitySensorCheckImpl extends _ProximitySensorCheck {
  const _$ProximitySensorCheckImpl({this.isNear = false}) : super._();

  @override
  @JsonKey()
  final bool isNear;

  @override
  String toString() {
    return 'ProximitySensorCheck(isNear: $isNear)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProximitySensorCheckImpl &&
            (identical(other.isNear, isNear) || other.isNear == isNear));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isNear);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProximitySensorCheckImplCopyWith<_$ProximitySensorCheckImpl>
      get copyWith =>
          __$$ProximitySensorCheckImplCopyWithImpl<_$ProximitySensorCheckImpl>(
              this, _$identity);
}

abstract class _ProximitySensorCheck extends ProximitySensorCheck {
  const factory _ProximitySensorCheck({final bool isNear}) =
      _$ProximitySensorCheckImpl;
  const _ProximitySensorCheck._() : super._();

  @override
  bool get isNear;
  @override
  @JsonKey(ignore: true)
  _$$ProximitySensorCheckImplCopyWith<_$ProximitySensorCheckImpl>
      get copyWith => throw _privateConstructorUsedError;
}
