import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/domain/proximity_sensor_check.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/domain/repository/proximity_sensor_check_repository.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/domain/service/proximity_sensor_check_service.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';

class ProximitySensorCheckServiceImpl implements ProximitySensorCheckService {
  final Ref ref;
  final TradeInSharePreferencesRepository tradeinSharePreferencesRepository;
  final ProximitySensorCheckRepository proximitySensorCheckRepository;
  ProximitySensorCheckServiceImpl(
    this.ref, {
    required this.proximitySensorCheckRepository,
    required this.tradeinSharePreferencesRepository,
  });

  @override
  Stream<ProximitySensorCheck> getProximitySensor() async* {
    var proximitySensorCheck = const ProximitySensorCheck();
    StreamController<ProximitySensorCheck> proximitysensor =
        StreamController<ProximitySensorCheck>();

    final sensorStream =
        proximitySensorCheckRepository.getProximitySensor().listen((event) {
      if (event == 1) {
        proximitySensorCheck = proximitySensorCheck.copyWith(
          isNear: true,
        );
        proximitysensor.sink.add(proximitySensorCheck);
      }
    });

    ref.onDispose(() async {
      await sensorStream.cancel();
      await proximitysensor.close();
    });

    yield* proximitysensor.stream;
  }

  @override
  Future<bool?> getProximitySensorCheckResult() async {
    return await tradeinSharePreferencesRepository
        .getBool(AppRoutes.proximitySensorCheckPage.titleAPI);
  }

  @override
  Future<void> saveProximitySensorCheckResult(bool status) async {
    await tradeinSharePreferencesRepository.saveBool(
        AppRoutes.proximitySensorCheckPage.titleAPI, status);
  }
}
