import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/data/proximity_sensor_check_wrapper.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/domain/repository/proximity_sensor_check_repository.dart';
import 'package:techswop_lite/shared/shared.dart';

class ProximitySensorCheckRepositoryImpl
    implements ProximitySensorCheckRepository {
  final Ref ref;
  final ProximitySensorCheckWrapper _proximitySensor;
  ProximitySensorCheckRepositoryImpl(this.ref, this._proximitySensor);

  ///จับ Event แล้วคืนค่าเป็น Stream<int>
  /// 0 = ไม่มีอะไรอยู่ใกล้เครื่อง
  /// 1 = มีอะไรอยู่ใกล้เครื่อง
  @override
  Stream<int> getProximitySensor() async* {
    /// proximity event will return as stream of int
    /// it work like bool where 0 mean no sensor detected
    /// and 1 mean sensor is detecting something near it.

    final StreamController<int> proximitysensor = StreamController<int>();
    StreamSubscription? _proximitySensorSubscription;

    _proximitySensorSubscription = _proximitySensor.events.listen((event) {
      log(event.toString());
      proximitysensor.sink.add(event);
    });

    ref.onDispose(() {
      log('ProximitySensorCheckRepository disposed');
      _proximitySensorSubscription?.cancel();
      proximitysensor.close();
    });

    yield* proximitysensor.stream;
  }
}
