import 'package:proximity_sensor/proximity_sensor.dart';

class ProximitySensorCheckWrapper {
  Stream<int> get events => ProximitySensor.events;
  
}
