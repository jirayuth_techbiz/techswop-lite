import 'dart:async';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/domain/proximity_sensor_check.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/domain/service/proximity_sensor_check_service.dart';

part 'proximity_check_state.g.dart';

@riverpod
class ProximitySensorCheckState extends _$ProximitySensorCheckState {
  @override
  Future<ProximitySensorCheck> build() async {
    return const ProximitySensorCheck();
  }

  Future<void> startTest() async {
    final proximityCheckService =
        ref.watch(proximitySensorCheckServiceProvider);
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() {
      return proximityCheckService.getProximitySensor().firstWhere(
            (proximityCheck) => proximityCheck.isNear,
          );
    });
  }
}

@riverpod
class ProximitySensorCheckManager extends _$ProximitySensorCheckManager {
  @override
  Future<void> build() async {}

  Future<bool?> getProximitySensorCheckResult() async {
    final proximityCheckService =
        ref.watch(proximitySensorCheckServiceProvider);
    return await proximityCheckService.getProximitySensorCheckResult();
  }

  Future<void> saveProximitySensorCheckResult(bool status) async {
    final proximityCheckService =
        ref.watch(proximitySensorCheckServiceProvider);
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() => proximityCheckService.saveProximitySensorCheckResult(status));
  }
}