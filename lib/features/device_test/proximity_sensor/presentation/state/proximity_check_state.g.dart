// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'proximity_check_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$proximitySensorCheckStateHash() =>
    r'ad6d95c3bc1d81b1434e4310c13f3251b1a61902';

/// See also [ProximitySensorCheckState].
@ProviderFor(ProximitySensorCheckState)
final proximitySensorCheckStateProvider = AutoDisposeAsyncNotifierProvider<
    ProximitySensorCheckState, ProximitySensorCheck>.internal(
  ProximitySensorCheckState.new,
  name: r'proximitySensorCheckStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$proximitySensorCheckStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ProximitySensorCheckState
    = AutoDisposeAsyncNotifier<ProximitySensorCheck>;
String _$proximitySensorCheckManagerHash() =>
    r'2c43cd3d326f4e9dca2ebf4134e5624b43aec9fa';

/// See also [ProximitySensorCheckManager].
@ProviderFor(ProximitySensorCheckManager)
final proximitySensorCheckManagerProvider = AutoDisposeAsyncNotifierProvider<
    ProximitySensorCheckManager, void>.internal(
  ProximitySensorCheckManager.new,
  name: r'proximitySensorCheckManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$proximitySensorCheckManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ProximitySensorCheckManager = AutoDisposeAsyncNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
