import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/domain/service/proximity_sensor_check_service.dart';
import 'package:techswop_lite/features/device_test/proximity_sensor/presentation/state/proximity_check_state.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';

class ProximitySensorCheckPage extends ConsumerStatefulWidget {
  const ProximitySensorCheckPage({super.key});

  @override
  ConsumerState<ProximitySensorCheckPage> createState() =>
      _ProximitySensorCheckPageState();
}

class _ProximitySensorCheckPageState
    extends ConsumerState<ProximitySensorCheckPage> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await ref.read(proximitySensorCheckStateProvider.notifier).startTest();
    });
  }

  @override
  Widget build(BuildContext context) {
    final translation = context.localization;
    final sensorCheckState = ref.watch(proximitySensorCheckStateProvider);
    final getProximityPageNumber = ref.watch(
        getCurrentPageNumberProvider(AppRoutes.proximitySensorCheckPage.path));
    final theme = context.theme;
    final proximitySensorCheckManager =
        ref.watch(proximitySensorCheckManagerProvider.notifier);

    ref.listen(proximitySensorCheckStateProvider, (prev, next) {
      if (next.asData!.value.isNear) {
        _handlingProximityCheck(
          context: context,
          ref: ref,
          isCheck: true,
        );
      }
    });

    return TestPageWidgetLayout(
      title: translation.proximitySensorPageTitle,
      pictureAssetDirectory: CommonConst.CHECKLIST_PROPERTIES
          .firstWhere((element) =>
              element.appRoutesPage == AppRoutes.proximitySensorCheckPage)
          .icon, //TODO
      processNumber: "${translation.layoutStep} $getProximityPageNumber",
      nextPage: getProximityPageNumber,
      cardBodyText: translation.proximitySensorPageDescription,
      underCard: AsyncValueWidget(
        value: sensorCheckState,
        data: (proximity) {
          return Column(
            children: [
              _ProximityCheckResultWidget(
                theme: theme,
                text: translation.proximitySensorPageIsNear,
                isProximitySensorNear: proximity.isNear,
              ),
            ],
          );
        },
        loading: () {
          return Column(
            children: [
              _ProximityCheckResultWidget(
                theme: theme,
                text: translation.proximitySensorPageIsNear,
                isProximitySensorNear: false,
              ),
            ],
          );
        },
      ),
      onSkip: () async {
        await proximitySensorCheckManager.saveProximitySensorCheckResult(false);
      },
    );
  }

  void _handlingProximityCheck({
    required BuildContext context,
    required WidgetRef ref,
    required bool isCheck,
    String? failedReason,
  }) {
    final isExecuted = ref.watch(resultIsExecutedStateProvider);
    final proximitySensorCheckManager =
        ref.watch(proximitySensorCheckManagerProvider.notifier);
    final resultHandler = TestCheckHandler(
      context: context,
      ref: ref,
      isCheck: isCheck,
      appRoutes: AppRoutes.proximitySensorCheckPage,
      stateProvider: proximitySensorCheckStateProvider,
      isExecuted: isExecuted,
    );

    if (isExecuted) return;
    Future.delayed(
            const Duration(milliseconds: AppConfig.futureSuccessAwaitDuration))
        .then((_) async {
      await proximitySensorCheckManager.saveProximitySensorCheckResult(isCheck);

      if (context.mounted) {
        await resultHandler.handlingStatus(
          onSkip: () async {
            await proximitySensorCheckManager
                .saveProximitySensorCheckResult(false);
          },
          failedReason: failedReason,
        );
      }
    });
  }
}

class _ProximityCheckResultWidget extends ConsumerStatefulWidget {
  final bool isProximitySensorNear;
  final ThemeData theme;
  final String text;

  const _ProximityCheckResultWidget({
    required this.theme,
    required this.text,
    required this.isProximitySensorNear,
  });

  @override
  ConsumerState<_ProximityCheckResultWidget> createState() =>
      _ProximityCheckResultWidgetState();
}

class _ProximityCheckResultWidgetState
    extends ConsumerState<_ProximityCheckResultWidget> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 6.0.w),
        child: CheckboxListTile(
          onChanged:
              (result) {}, // ไม่ได้ใช้ แต่ [CheckboxListTile] ต้องใส่ [onChanged] ไม่งั้นจะ Error,
          value: widget.isProximitySensorNear,
          title: Text(widget.text),
        ),
      ),
    );
  }
}
