// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'microphone_check.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$MicrophoneCheck {
  bool get isMicrophoneCheckStartButtonVisible =>
      throw _privateConstructorUsedError;
  bool get isMicrophoneAvailable => throw _privateConstructorUsedError;
  bool get isUserSpoken => throw _privateConstructorUsedError;

  /// [isMicrophoneAvailable] กับ [isUserSpoken] คืนข้อมูลเป็นรูปแบบ Stream
  /// หมายความว่าถ้า Condition ด้านบนสามารถ Yield ได้หลายครั้ง
  /// จึงต้องมี Condition ด้านล่างมาป้องกันไม่ให้ Yield มากกว่า 1 ครั้ง
  bool get hasExecuted => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MicrophoneCheckCopyWith<MicrophoneCheck> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MicrophoneCheckCopyWith<$Res> {
  factory $MicrophoneCheckCopyWith(
          MicrophoneCheck value, $Res Function(MicrophoneCheck) then) =
      _$MicrophoneCheckCopyWithImpl<$Res, MicrophoneCheck>;
  @useResult
  $Res call(
      {bool isMicrophoneCheckStartButtonVisible,
      bool isMicrophoneAvailable,
      bool isUserSpoken,
      bool hasExecuted});
}

/// @nodoc
class _$MicrophoneCheckCopyWithImpl<$Res, $Val extends MicrophoneCheck>
    implements $MicrophoneCheckCopyWith<$Res> {
  _$MicrophoneCheckCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isMicrophoneCheckStartButtonVisible = null,
    Object? isMicrophoneAvailable = null,
    Object? isUserSpoken = null,
    Object? hasExecuted = null,
  }) {
    return _then(_value.copyWith(
      isMicrophoneCheckStartButtonVisible: null ==
              isMicrophoneCheckStartButtonVisible
          ? _value.isMicrophoneCheckStartButtonVisible
          : isMicrophoneCheckStartButtonVisible // ignore: cast_nullable_to_non_nullable
              as bool,
      isMicrophoneAvailable: null == isMicrophoneAvailable
          ? _value.isMicrophoneAvailable
          : isMicrophoneAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
      isUserSpoken: null == isUserSpoken
          ? _value.isUserSpoken
          : isUserSpoken // ignore: cast_nullable_to_non_nullable
              as bool,
      hasExecuted: null == hasExecuted
          ? _value.hasExecuted
          : hasExecuted // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$MicrophoneCheckImplCopyWith<$Res>
    implements $MicrophoneCheckCopyWith<$Res> {
  factory _$$MicrophoneCheckImplCopyWith(_$MicrophoneCheckImpl value,
          $Res Function(_$MicrophoneCheckImpl) then) =
      __$$MicrophoneCheckImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool isMicrophoneCheckStartButtonVisible,
      bool isMicrophoneAvailable,
      bool isUserSpoken,
      bool hasExecuted});
}

/// @nodoc
class __$$MicrophoneCheckImplCopyWithImpl<$Res>
    extends _$MicrophoneCheckCopyWithImpl<$Res, _$MicrophoneCheckImpl>
    implements _$$MicrophoneCheckImplCopyWith<$Res> {
  __$$MicrophoneCheckImplCopyWithImpl(
      _$MicrophoneCheckImpl _value, $Res Function(_$MicrophoneCheckImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isMicrophoneCheckStartButtonVisible = null,
    Object? isMicrophoneAvailable = null,
    Object? isUserSpoken = null,
    Object? hasExecuted = null,
  }) {
    return _then(_$MicrophoneCheckImpl(
      isMicrophoneCheckStartButtonVisible: null ==
              isMicrophoneCheckStartButtonVisible
          ? _value.isMicrophoneCheckStartButtonVisible
          : isMicrophoneCheckStartButtonVisible // ignore: cast_nullable_to_non_nullable
              as bool,
      isMicrophoneAvailable: null == isMicrophoneAvailable
          ? _value.isMicrophoneAvailable
          : isMicrophoneAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
      isUserSpoken: null == isUserSpoken
          ? _value.isUserSpoken
          : isUserSpoken // ignore: cast_nullable_to_non_nullable
              as bool,
      hasExecuted: null == hasExecuted
          ? _value.hasExecuted
          : hasExecuted // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$MicrophoneCheckImpl extends _MicrophoneCheck {
  const _$MicrophoneCheckImpl(
      {this.isMicrophoneCheckStartButtonVisible = true,
      this.isMicrophoneAvailable = false,
      this.isUserSpoken = false,
      this.hasExecuted = false})
      : super._();

  @override
  @JsonKey()
  final bool isMicrophoneCheckStartButtonVisible;
  @override
  @JsonKey()
  final bool isMicrophoneAvailable;
  @override
  @JsonKey()
  final bool isUserSpoken;

  /// [isMicrophoneAvailable] กับ [isUserSpoken] คืนข้อมูลเป็นรูปแบบ Stream
  /// หมายความว่าถ้า Condition ด้านบนสามารถ Yield ได้หลายครั้ง
  /// จึงต้องมี Condition ด้านล่างมาป้องกันไม่ให้ Yield มากกว่า 1 ครั้ง
  @override
  @JsonKey()
  final bool hasExecuted;

  @override
  String toString() {
    return 'MicrophoneCheck(isMicrophoneCheckStartButtonVisible: $isMicrophoneCheckStartButtonVisible, isMicrophoneAvailable: $isMicrophoneAvailable, isUserSpoken: $isUserSpoken, hasExecuted: $hasExecuted)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MicrophoneCheckImpl &&
            (identical(other.isMicrophoneCheckStartButtonVisible,
                    isMicrophoneCheckStartButtonVisible) ||
                other.isMicrophoneCheckStartButtonVisible ==
                    isMicrophoneCheckStartButtonVisible) &&
            (identical(other.isMicrophoneAvailable, isMicrophoneAvailable) ||
                other.isMicrophoneAvailable == isMicrophoneAvailable) &&
            (identical(other.isUserSpoken, isUserSpoken) ||
                other.isUserSpoken == isUserSpoken) &&
            (identical(other.hasExecuted, hasExecuted) ||
                other.hasExecuted == hasExecuted));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      isMicrophoneCheckStartButtonVisible,
      isMicrophoneAvailable,
      isUserSpoken,
      hasExecuted);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MicrophoneCheckImplCopyWith<_$MicrophoneCheckImpl> get copyWith =>
      __$$MicrophoneCheckImplCopyWithImpl<_$MicrophoneCheckImpl>(
          this, _$identity);
}

abstract class _MicrophoneCheck extends MicrophoneCheck {
  const factory _MicrophoneCheck(
      {final bool isMicrophoneCheckStartButtonVisible,
      final bool isMicrophoneAvailable,
      final bool isUserSpoken,
      final bool hasExecuted}) = _$MicrophoneCheckImpl;
  const _MicrophoneCheck._() : super._();

  @override
  bool get isMicrophoneCheckStartButtonVisible;
  @override
  bool get isMicrophoneAvailable;
  @override
  bool get isUserSpoken;
  @override

  /// [isMicrophoneAvailable] กับ [isUserSpoken] คืนข้อมูลเป็นรูปแบบ Stream
  /// หมายความว่าถ้า Condition ด้านบนสามารถ Yield ได้หลายครั้ง
  /// จึงต้องมี Condition ด้านล่างมาป้องกันไม่ให้ Yield มากกว่า 1 ครั้ง
  bool get hasExecuted;
  @override
  @JsonKey(ignore: true)
  _$$MicrophoneCheckImplCopyWith<_$MicrophoneCheckImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
