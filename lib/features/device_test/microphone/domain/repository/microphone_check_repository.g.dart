// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'microphone_check_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$microphoneCheckRepositoryHash() =>
    r'a752fde4d2709d922eb95c6ec3452350e3f133ba';

/// See also [microphoneCheckRepository].
@ProviderFor(microphoneCheckRepository)
final microphoneCheckRepositoryProvider =
    AutoDisposeProvider<MicrophoneCheckRepository>.internal(
  microphoneCheckRepository,
  name: r'microphoneCheckRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$microphoneCheckRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef MicrophoneCheckRepositoryRef
    = AutoDisposeProviderRef<MicrophoneCheckRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
