import 'package:audio_session/audio_session.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:noise_meter/noise_meter.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/microphone/data/microphone_check_repository_impl.dart';
import 'package:techswop_lite/features/device_test/microphone/domain/repository/speaker_check_repository.dart';
import 'package:techswop_lite/shared/shared.dart';

part 'microphone_check_repository.g.dart';

@riverpod
MicrophoneCheckRepository microphoneCheckRepository(
    MicrophoneCheckRepositoryRef ref) {
  final androidAudioManager = AndroidAudioManager();
  final NoiseMeter noiseMeter = NoiseMeter();
  final AudioPlayer audioPlayer = AudioPlayer();
  final deviceInfo = ref.watch(deviceInfoRepositoryProvider);
  return MicrophoneCheckRepositoryImpl(
    ref,
    noiseMeter,
    androidAudioManager,
    deviceInfo,
    audioPlayer,
  );
}

abstract class MicrophoneCheckRepository implements SpeakerCheckRepository {
  Stream<NoiseReading> checkMicrophone();
  Future<void> playAudio({required String audioAsset});
  Future<void> stopAudio();
  void disposeSpeaker();
}
