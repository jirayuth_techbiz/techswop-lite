import 'package:freezed_annotation/freezed_annotation.dart';

part 'microphone_check.freezed.dart';

@freezed 
class MicrophoneCheck with _$MicrophoneCheck {
  const factory MicrophoneCheck({
    @Default(true) bool isMicrophoneCheckStartButtonVisible,
    @Default(false) bool isMicrophoneAvailable,
    @Default(false) bool isUserSpoken,
    /// [isMicrophoneAvailable] กับ [isUserSpoken] คืนข้อมูลเป็นรูปแบบ Stream
    /// หมายความว่าถ้า Condition ด้านบนสามารถ Yield ได้หลายครั้ง
    /// จึงต้องมี Condition ด้านล่างมาป้องกันไม่ให้ Yield มากกว่า 1 ครั้ง
    @Default(false) bool hasExecuted,
  }) = _MicrophoneCheck;

  const MicrophoneCheck._();
}