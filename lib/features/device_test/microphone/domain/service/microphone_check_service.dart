import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/microphone/application/microphone_check_service_impl.dart';
import 'package:techswop_lite/features/device_test/microphone/domain/microphone_check.dart';
import 'package:techswop_lite/features/device_test/microphone/domain/repository/microphone_check_repository.dart';
import 'package:techswop_lite/features/preparation/domain/repository/permission_status_repository.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

part 'microphone_check_service.g.dart';

@riverpod 
MicrophoneCheckService microphoneCheckService(MicrophoneCheckServiceRef ref) {
  final microphoneCheckRepository = ref.watch(microphoneCheckRepositoryProvider);
  final tradeInSharePreferencesRepository = ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final permisisonStatusRepository = ref.watch(permissionStatusRepositoryProvider);
  final loggerRepository = ref.watch(loggerRepositoryProvider);

  return MicrophoneCheckServiceImpl(
    ref,
    microphoneCheckRepository: microphoneCheckRepository, 
    tradeInSharePreferencesRepository: tradeInSharePreferencesRepository, 
    permissionStatusRepository: permisisonStatusRepository,
    logger: loggerRepository
  );
}

abstract class MicrophoneCheckService {
  Future<bool> isMicrophoneAvailable();
  Stream<MicrophoneCheck> getMicrophoneCheck();
  Future<bool?> getMicrophoneCheckResult();
  Future<void> saveMicrophoneCheckResult(bool status);
}