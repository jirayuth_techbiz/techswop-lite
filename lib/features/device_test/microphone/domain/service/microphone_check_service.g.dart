// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'microphone_check_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$microphoneCheckServiceHash() =>
    r'a98afd5144ef83a73f15577edd2fed9160d497d2';

/// See also [microphoneCheckService].
@ProviderFor(microphoneCheckService)
final microphoneCheckServiceProvider =
    AutoDisposeProvider<MicrophoneCheckService>.internal(
  microphoneCheckService,
  name: r'microphoneCheckServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$microphoneCheckServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef MicrophoneCheckServiceRef
    = AutoDisposeProviderRef<MicrophoneCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
