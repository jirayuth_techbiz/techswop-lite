import 'dart:async';

import 'package:audio_session/audio_session.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:collection/collection.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:noise_meter/noise_meter.dart';
import 'package:techswop_lite/features/device_test/microphone/domain/repository/microphone_check_repository.dart';

class MicrophoneCheckRepositoryImpl implements MicrophoneCheckRepository {
  final Ref ref;
  final NoiseMeter _noiseMeter;
  final AndroidAudioManager androidAudioManager;
  final DeviceInfoRepository deviceInfoRepository;
  final AudioPlayer audioPlayer;

  MicrophoneCheckRepositoryImpl(
    this.ref,
    this._noiseMeter,
    this.androidAudioManager,
    this.deviceInfoRepository,
    this.audioPlayer,
  );

  int get defaultDecibel => 80;

  @override
  Stream<NoiseReading> checkMicrophone() async* {
    final StreamController<NoiseReading> noiseStreamController =
        StreamController<NoiseReading>();
    final Stream<NoiseReading> noisemeter = _noiseMeter.noise;
    StreamSubscription<NoiseReading>? noiseStream;

    noiseStream = noisemeter.listen((noise) {
      noiseStreamController.sink.add(noise);
    });

    ref.onDispose(() {
      log("Dispose Noise Stream");
      noiseStream?.cancel();
      // noiseStreamController.close();
    });

    yield* noiseStreamController.stream;
  }

  @override
  Future<bool> switchToLoudSpeaker() async {
    final androidSdklvl = await deviceInfoRepository.getDeviceInfo();
    if (androidSdklvl!.version < 33) {
      await androidAudioManager.setMode(AndroidAudioHardwareMode.normal);
      await androidAudioManager.stopBluetoothSco();
      await androidAudioManager.setBluetoothScoOn(false);
      await androidAudioManager.setSpeakerphoneOn(true);
      return true;
    } else {
      final devices =
          await androidAudioManager.getAvailableCommunicationDevices();
      Map<int, AndroidAudioDeviceInfo> devicesMap = {
        for (var device in devices) device.id: device
      };

      if (devicesMap.values
              .firstWhereOrNull(
                (element) =>
                    element.type == AndroidAudioDeviceType.bluetoothSco,
              )
              ?.type ==
          AndroidAudioDeviceType.bluetoothSco) {
        await androidAudioManager.setCommunicationDevice(devicesMap.values
            .where((element) =>
                element.type == AndroidAudioDeviceType.builtInSpeaker)
            .first);
        await androidAudioManager.setStreamVolume(
            AndroidStreamType.music,
            await androidAudioManager
                .getStreamMaxVolume(AndroidStreamType.music),
            AndroidAudioVolumeFlags.playSound);
        return true;
      } else {
        final getBuiltinSpeaker = devicesMap.values.firstWhereOrNull(
          (element) => element.type == AndroidAudioDeviceType.builtInSpeaker,
        );

        if (getBuiltinSpeaker != null) {
          try {
            await androidAudioManager.setCommunicationDevice(getBuiltinSpeaker);
            await androidAudioManager.setStreamVolume(
                AndroidStreamType.music,
                await androidAudioManager
                    .getStreamMaxVolume(AndroidStreamType.music),
                AndroidAudioVolumeFlags.playSound);
            return true;
          } catch (e) {
            log(e.toString());
            return false;
          }
        }
        await androidAudioManager.setStreamVolume(
            AndroidStreamType.music,
            await androidAudioManager
                .getStreamMaxVolume(AndroidStreamType.music),
            AndroidAudioVolumeFlags.playSound);
        return true;
      }
    }
  }

  @override
  Future<void> playAudio({required String audioAsset}) async {
    await audioPlayer.setAudioContext(AudioContextConfig(
      route: AudioContextConfigRoute.speaker,
      focus: AudioContextConfigFocus.gain,
    ).build());
    await audioPlayer.play(AssetSource(audioAsset));
  }

  @override
  Future<void> stopAudio() async {
    ref.onDispose(() {
      audioPlayer.release();
    });

    await audioPlayer.stop();
  }

  @override
  void disposeSpeaker() {
    audioPlayer.release();
  }
}
