import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/countdown_state.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/test_result/test_failed_status_icon.dart';
import 'package:techswop_lite/features/device_test/microphone/presentation/state/microphone_check_state.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';

class MicrophoneCheckPage extends ConsumerStatefulWidget {
  const MicrophoneCheckPage({super.key});

  @override
  ConsumerState<MicrophoneCheckPage> createState() =>
      _MicrophoneCheckPageState();
}

class _MicrophoneCheckPageState extends ConsumerState<MicrophoneCheckPage> {
  @override
  Widget build(BuildContext context) {
    final theme = context.theme;
    final translation = context.localization;
    final microphoneCheckState = ref.watch(microphoneCheckStateProvider);
    final microphoneCheckStateNotifier =
        ref.watch(microphoneCheckStateProvider.notifier);
    final microphoneCheckCountdown =
        ref.watch(countdownStateProvider(seconds: AppConfig.defaultCountdown));
    final microphoneCheckCountdownState = ref.watch(
        countdownStateProvider(seconds: AppConfig.defaultCountdown).notifier);
    final getMicrophonePageNumber = ref.watch(
        getCurrentPageNumberProvider(AppRoutes.microphoneCheckPage.path));
    final microphoneCheckManager =
        ref.watch(microphoneCheckManagerProvider.notifier);

    ref.listen(microphoneCheckStateProvider, (prev, next) {
      ///ถ้าไม่มีไมค์หรือเข้าถึงระบบภายในไม่ได้ และไม่ได้อยู่ในสถานะโหลด
      if (!next.asData!.value.isMicrophoneAvailable && !prev!.isLoading) {
        _handlingMicrophoneCheckResult(
          context: context,
          ref: ref,
          isCheck: false,
          failedReason: translation.microphoneTestFailed,
        );
      }

      ///ถ้ามีไมค์ และผู้ใช้พูดใส่แล้ว
      if (next.asData!.value.isMicrophoneAvailable &&
          next.asData!.value.isUserSpoken &&
          !next.asData!.value.isMicrophoneCheckStartButtonVisible) {
        _handlingMicrophoneCheckResult(
          context: context,
          ref: ref,
          isCheck: true,
        );
      }
    });

    ref.listen(countdownStateProvider(seconds: AppConfig.defaultCountdown), (prev, next) {
      if (int.parse(next) <= 0) {
        _handlingMicrophoneCheckResult(
          context: context,
          ref: ref,
          isCheck: false,
          failedReason: translation.microphoneTestFailed,
        );
      }
    });

    return TestPageWidgetLayout(
      nextPage: getMicrophonePageNumber,
      processNumber: "${translation.layoutStep} $getMicrophonePageNumber",
      title: translation.microphonePageTitle,
      pictureAssetDirectory: CommonConst.CHECKLIST_PROPERTIES
          .firstWhere((element) =>
              element.appRoutesPage == AppRoutes.microphoneCheckPage)
          .icon,
      cardBodyText: translation.microphonePageDescription,
      underCard: AsyncValueWidget(
        value: microphoneCheckState,
        data: (microphoneCheck) {
          if (microphoneCheck.isMicrophoneAvailable &&
              microphoneCheck.isUserSpoken &&
              !microphoneCheck.isMicrophoneCheckStartButtonVisible) {
            return Center(
              child: TestSuccessStatusIcon(
                theme: theme,
              ),
            );
          }
          
          if (!microphoneCheck.isMicrophoneAvailable) {
            return Center(
              child: TestFailedStatusIcon(
                theme: theme,
              ),
            );
          }

          return CountdownWidget(
            isVisible: microphoneCheck.isMicrophoneCheckStartButtonVisible,
            translation: translation,
            theme: theme,
            countdownNumberString: microphoneCheckCountdown,
            startButtonOnPressed: () async {
              await microphoneCheckStateNotifier.toggle();
              microphoneCheckStateNotifier.getMicrophoneResult();
              microphoneCheckCountdownState.startCountdownTimer();
            },
            restartButtonOnPressed: () async {
              await microphoneCheckStateNotifier.toggle();
              microphoneCheckStateNotifier.getMicrophoneResult();
              microphoneCheckCountdownState.stopCountdownTimer();
              // ref.invalidate(microphoneCheckStateProvider);
            },
          );
        },
      ),
      onSkip: () async {
        await microphoneCheckManager.saveMicrophoneCheckResult(false);
      },
    );
  }

  void _handlingMicrophoneCheckResult({
    required BuildContext context,
    required WidgetRef ref,
    required bool isCheck,
    String? failedReason,
  }) {
    final isExecuted = ref.watch(resultIsExecutedStateProvider);
    final microphoneCheckManager =
        ref.watch(microphoneCheckManagerProvider.notifier);
    final microphoneCheckCountdownState = ref.watch(
        countdownStateProvider(seconds: AppConfig.defaultCountdown).notifier);
    final resultHandler = TestCheckHandler(
      context: context,
      ref: ref,
      isCheck: isCheck,
      appRoutes: AppRoutes.microphoneCheckPage,
      stateProvider: microphoneCheckStateProvider,
      isExecuted: isExecuted,
    );

    if (isExecuted) return;
    Future.delayed(
            const Duration(milliseconds: AppConfig.futureSuccessAwaitDuration))
        .then(
      (_) async {
        await microphoneCheckManager.saveMicrophoneCheckResult(isCheck);

        microphoneCheckCountdownState.stopCountdownTimer();

        if (context.mounted) {
          await resultHandler.handlingStatus(
            onSkip: () async {
              await microphoneCheckManager.saveMicrophoneCheckResult(false);
            },
            failedReason: failedReason,
          );
        }
      },
    );
  }
}
