import 'dart:async';

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/microphone/domain/microphone_check.dart';
import 'package:techswop_lite/features/device_test/microphone/domain/service/microphone_check_service.dart';

part 'microphone_check_state.g.dart';

@riverpod
class MicrophoneCheckState extends _$MicrophoneCheckState {
  @override
  FutureOr<MicrophoneCheck> build() async {
    final microphoneCheckService = ref.watch(microphoneCheckServiceProvider);
    return MicrophoneCheck(
        
        isMicrophoneAvailable:
            await microphoneCheckService.isMicrophoneAvailable());
  }

  Future<void> toggle() async {
    state = state.whenData((value) => value.copyWith(
          isMicrophoneCheckStartButtonVisible: !state.asData!.value.isMicrophoneCheckStartButtonVisible,
        ));
  }

  Future<void> testExecuted() async {
    state = state.whenData((value) => value.copyWith(
          hasExecuted: true,
        ));
  }

  Future<void> getMicrophoneResult() async {
    final microphoneCheckService = ref.watch(microphoneCheckServiceProvider);

    final result = await AsyncValue.guard(() {
      return microphoneCheckService.getMicrophoneCheck().firstWhere(
            (microphoneCheck) => microphoneCheck.isUserSpoken,
          );
    });

    result.whenData((microphoneCheck) {
      state = state.whenData((value) => value.copyWith(
            isUserSpoken: microphoneCheck.isUserSpoken,
          ));
    });
  }
}

@riverpod
class MicrophoneCheckManager extends _$MicrophoneCheckManager {
  @override
  FutureOr<void> build() async {}

  Future<bool?> getMicrophoneCheckResult() async {
    final microphoneCheckService = ref.watch(microphoneCheckServiceProvider);
    return await microphoneCheckService.getMicrophoneCheckResult();
  }

  Future<void> saveMicrophoneCheckResult(bool status) async {
    final microphoneCheckService = ref.watch(microphoneCheckServiceProvider);
    state = const AsyncLoading();
    state = await AsyncValue.guard(
        () => microphoneCheckService.saveMicrophoneCheckResult(status));
  }
}
