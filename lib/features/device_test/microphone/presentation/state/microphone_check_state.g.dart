// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'microphone_check_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$microphoneCheckStateHash() =>
    r'1dc16b05587354a8a17021cff6cf29f705688b79';

/// See also [MicrophoneCheckState].
@ProviderFor(MicrophoneCheckState)
final microphoneCheckStateProvider = AutoDisposeAsyncNotifierProvider<
    MicrophoneCheckState, MicrophoneCheck>.internal(
  MicrophoneCheckState.new,
  name: r'microphoneCheckStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$microphoneCheckStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$MicrophoneCheckState = AutoDisposeAsyncNotifier<MicrophoneCheck>;
String _$microphoneCheckManagerHash() =>
    r'e92922f2b8d879d2e4daeb1a6d92da4b93e32a37';

/// See also [MicrophoneCheckManager].
@ProviderFor(MicrophoneCheckManager)
final microphoneCheckManagerProvider =
    AutoDisposeAsyncNotifierProvider<MicrophoneCheckManager, void>.internal(
  MicrophoneCheckManager.new,
  name: r'microphoneCheckManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$microphoneCheckManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$MicrophoneCheckManager = AutoDisposeAsyncNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
