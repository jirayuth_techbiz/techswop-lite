import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/microphone/domain/microphone_check.dart';
import 'package:techswop_lite/features/device_test/microphone/domain/repository/microphone_check_repository.dart';
import 'package:techswop_lite/features/device_test/microphone/domain/service/microphone_check_service.dart';
import 'package:techswop_lite/features/preparation/domain/repository/permission_status_repository.dart';
import 'package:techswop_lite/generated/assets.gen.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';

class MicrophoneCheckServiceImpl implements MicrophoneCheckService {
  final Ref ref;
  final MicrophoneCheckRepository microphoneCheckRepository;
  final TradeInSharePreferencesRepository tradeInSharePreferencesRepository;
  final PermisisonStatusRepository permissionStatusRepository;
  final LoggerRepository logger;

  MicrophoneCheckServiceImpl(
    this.ref, {
    required this.microphoneCheckRepository,
    required this.tradeInSharePreferencesRepository,
    required this.permissionStatusRepository,
    required this.logger,
  });

  int get defaultDecibel => AppConfig.defaultDecibel;

  @override
  Stream<MicrophoneCheck> getMicrophoneCheck() async* {
    StreamController<MicrophoneCheck> microphoneController =
        StreamController<MicrophoneCheck>();
    var microphoneCheck = const MicrophoneCheck();
    const String asset = CommonConst.ASSET;

    try {
      final microphoneStream = microphoneCheckRepository
          .checkMicrophone()
          .map((noise) => noise.meanDecibel >= defaultDecibel)
          .distinct()
          .listen((isNoiseDetected) async {
        await microphoneCheckRepository.playAudio(
            audioAsset: Assets.sound.testsound.startsWith(asset)
                ? Assets.sound.testsound.replaceFirst(asset, "")
                : Assets.sound.testsound,);
        Timer(Duration(seconds: AppConfig.defaultCountdown), () {
          microphoneController.addError("Timeout");
          microphoneController.close();
        });
        Future.delayed(const Duration(seconds: 3), () async {
          if (isNoiseDetected) {
            
            microphoneCheck = microphoneCheck.copyWith(isUserSpoken: true);
            microphoneController.sink.add(microphoneCheck);
          }
        });
      });

      microphoneController.sink.add(microphoneCheck);

      ref.onDispose(() {
        microphoneStream.cancel();
        microphoneController.close();
        microphoneCheckRepository.stopAudio();
      });

      yield* microphoneController.stream;
    } catch (e) {
      log(e.toString());
      microphoneController.addError(e);
      rethrow;
    }
  }

  @override
  Future<bool?> getMicrophoneCheckResult() async {
    return await tradeInSharePreferencesRepository
        .getBool(AppRoutes.microphoneCheckPage.titleAPI);
  }

  @override
  Future<void> saveMicrophoneCheckResult(bool status) async {
    await tradeInSharePreferencesRepository.saveBool(
        AppRoutes.microphoneCheckPage.titleAPI, status);
  }

  @override
  Future<bool> isMicrophoneAvailable() async {
    final permission =
        permissionStatusRepository.isPermissionGranted(Permission.microphone);
    return await permission;
  }
}
