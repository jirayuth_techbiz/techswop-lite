// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'dart:async';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/volume/domain/service/volume_check_service.dart';
import 'package:techswop_lite/features/device_test/volume/domain/volume_check.dart';

part 'volume_check_state.g.dart';

@riverpod
class VolumeCheckState extends _$VolumeCheckState {
  @override
  Stream<VolumeCheck> build() async* {
    final volumeService = ref.watch(volumeCheckServiceProvider);
    final result = volumeService.checkVolume();
    yield* result;
  }
}

@riverpod 
class VolumeCheckManager extends _$VolumeCheckManager {
  @override 
  Future<void> build() async {}

  Future<bool?> getVolumeCheckResult() async {
    final volumeService = ref.watch(volumeCheckServiceProvider);
    return await volumeService.getVolumeCheckResult();
  }

  Future<void> saveVolumeCheckResult(bool status) async {
    final volumeService = ref.watch(volumeCheckServiceProvider);
    state = const AsyncLoading();
    state = await AsyncValue.guard(() => volumeService.saveVolumeCheckResult(status));
  }
}