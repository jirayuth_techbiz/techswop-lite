// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'volume_check_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$volumeCheckStateHash() => r'83c8b5a806e8093d6b5d819f7c252774be77a717';

/// See also [VolumeCheckState].
@ProviderFor(VolumeCheckState)
final volumeCheckStateProvider =
    AutoDisposeStreamNotifierProvider<VolumeCheckState, VolumeCheck>.internal(
  VolumeCheckState.new,
  name: r'volumeCheckStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$volumeCheckStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$VolumeCheckState = AutoDisposeStreamNotifier<VolumeCheck>;
String _$volumeCheckManagerHash() =>
    r'0f7036618548233bc28c901b66ce5f69bd7624bc';

/// See also [VolumeCheckManager].
@ProviderFor(VolumeCheckManager)
final volumeCheckManagerProvider =
    AutoDisposeAsyncNotifierProvider<VolumeCheckManager, void>.internal(
  VolumeCheckManager.new,
  name: r'volumeCheckManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$volumeCheckManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$VolumeCheckManager = AutoDisposeAsyncNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
