// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/volume/presentation/state/volume_check_state.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';

class VolumeCheckPage extends ConsumerWidget {
  const VolumeCheckPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final translation = context.localization;
    final theme = context.theme;
    final volumeCheckState = ref.watch(volumeCheckStateProvider);
    final getVolumeCheckPageNumber =
        ref.watch(getCurrentPageNumberProvider(AppRoutes.volumeCheckPage.path));
    final volumeCheckManager = ref.watch(volumeCheckManagerProvider.notifier);

    ref.listen(volumeCheckStateProvider, (prev, next) {
      if (next.asData!.value.isVolumeUpButtonPress &&
          next.asData!.value.isVolumeDownButtonPress) {
        _handlingVolumeStatus(
          context: context,
          ref: ref,
          isCheck: true,
          failedReason: "",
        );
      }
    });

    return TestPageWidgetLayout(
      processNumber: "${translation.layoutStep} $getVolumeCheckPageNumber",
      title: translation.volumeButtonPageTitle,
      pictureAssetDirectory: CommonConst.CHECKLIST_PROPERTIES
          .firstWhere(
              (element) => element.appRoutesPage == AppRoutes.volumeCheckPage)
          .icon,
      nextPage: getVolumeCheckPageNumber,
      cardBodyText: translation.volumeButtonPageDescription,
      underCard: AsyncValueWidget(
        value: volumeCheckState,
        data: (volumeCheck) {
          return Column(
            children: [
              _VolumeCheckResultWidget(
                theme: theme,
                text: translation.volumeUpButton,
                isVolumeButtonPress: volumeCheck.isVolumeUpButtonPress,
              ),
              _VolumeCheckResultWidget(
                theme: theme,
                text: translation.volumeDownButton,
                isVolumeButtonPress: volumeCheck.isVolumeDownButtonPress,
              ),
            ],
          );
        },
        loading: () {
          return Column(
            children: [
              _VolumeCheckResultWidget(
                theme: theme,
                text: translation.volumeUpButton,
                isVolumeButtonPress: false,
              ),
              _VolumeCheckResultWidget(
                theme: theme,
                text: translation.volumeDownButton,
                isVolumeButtonPress: false,
              ),
            ],
          );
        },
      ),
      onSkip: () async {
        await volumeCheckManager.saveVolumeCheckResult(false);
      },
    );
  }

  void _handlingVolumeStatus({
    required BuildContext context,
    required WidgetRef ref,
    required bool isCheck,
    required String failedReason,
  }) {
    final isExecuted = ref.watch(resultIsExecutedStateProvider);
    final volumeCheckManager = ref.watch(volumeCheckManagerProvider.notifier);
    final resultHandler = TestCheckHandler(
      context: context,
      ref: ref,
      isCheck: isCheck,
      appRoutes: AppRoutes.volumeCheckPage,
      stateProvider: volumeCheckStateProvider,
      isExecuted: isExecuted,
    );

    if (isExecuted) return;
    Future.delayed(
            const Duration(milliseconds: AppConfig.futureSuccessAwaitDuration))
        .then(
      (_) async {
        await volumeCheckManager.saveVolumeCheckResult(isCheck);
        if (context.mounted) {
          await resultHandler.handlingStatus(
            onSkip: () async {
              await volumeCheckManager.saveVolumeCheckResult(false);
            },
          );
        }
      },
    );
  }
}

class _VolumeCheckResultWidget extends ConsumerStatefulWidget {
  final bool isVolumeButtonPress;
  final ThemeData theme;
  final String text;

  const _VolumeCheckResultWidget({
    required this.theme,
    required this.text,
    required this.isVolumeButtonPress,
  });

  @override
  ConsumerState<_VolumeCheckResultWidget> createState() =>
      _VolumeCheckResultWidgetState();
}

class _VolumeCheckResultWidgetState
    extends ConsumerState<_VolumeCheckResultWidget> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 6.0.w),
        child: CheckboxListTile(
          onChanged:
              (result) {}, // ไม่ได้ใช้ แต่ [CheckboxListTile] ต้องใส่ [onChanged] ไม่งั้นจะ Error,
          value: widget.isVolumeButtonPress,
          title: Text(widget.text),
        ),
      ),
    );
  }
}
