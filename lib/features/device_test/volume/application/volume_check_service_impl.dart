import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/volume/domain/repository/volume_check_repository.dart';
import 'package:techswop_lite/features/device_test/volume/domain/service/volume_check_service.dart';
import 'package:techswop_lite/features/device_test/volume/domain/volume_check.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/const/const.dart';

class VolumeCheckServiceImpl implements VolumeCheckService {
  final TradeInSharePreferencesRepository tradeInSharePreferencesRepository;
  final VolumeCheckRepository volumeCheckRepository;
  final Ref ref;
  final LoggerRepository logger;

  VolumeCheckServiceImpl(
  this.ref,{
    required this.tradeInSharePreferencesRepository,
    required this.volumeCheckRepository,
    required this.logger,
  });

  @override
  Future<bool?> getVolumeCheckResult() async {
    return await tradeInSharePreferencesRepository
        .getBool(AppRoutes.volumeCheckPage.titleAPI);
  }

  @override
  Future<void> saveVolumeCheckResult(bool result) async {
    await tradeInSharePreferencesRepository.saveBool(
        AppRoutes.volumeCheckPage.titleAPI, result);
  }

  @override
  Stream<VolumeCheck> checkVolume() async* {
    final resultController = StreamController<VolumeCheck>();
    var result = const VolumeCheck();

    double getCurrentVol = await volumeCheckRepository.getCurrentVolume();

    if (getCurrentVol == 0 || getCurrentVol == 10) {
      volumeCheckRepository.setCurrentVolume(.5);

      ///get the current volume again
      getCurrentVol = await volumeCheckRepository.getCurrentVolume();
    }

    final volumeStream = volumeCheckRepository.getVolume().listen((event) {
      if (event > getCurrentVol) {
        result = result.copyWith(
          isVolumeUpButtonPress: true,
        );
        resultController.sink.add(result);
        getCurrentVol = event;
      } else if (event < getCurrentVol) {
        result = result.copyWith(
          isVolumeDownButtonPress: true,
        );
        resultController.sink.add(result);
        getCurrentVol = event;
      }
    });

    ref.onDispose(() {
      volumeStream.cancel();
      resultController.close();
    });

    yield* resultController.stream;
  }
}
