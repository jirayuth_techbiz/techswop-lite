// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'volume_check.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$VolumeCheck {
  bool get isVolumeUpButtonPress => throw _privateConstructorUsedError;
  bool get isVolumeDownButtonPress => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $VolumeCheckCopyWith<VolumeCheck> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VolumeCheckCopyWith<$Res> {
  factory $VolumeCheckCopyWith(
          VolumeCheck value, $Res Function(VolumeCheck) then) =
      _$VolumeCheckCopyWithImpl<$Res, VolumeCheck>;
  @useResult
  $Res call({bool isVolumeUpButtonPress, bool isVolumeDownButtonPress});
}

/// @nodoc
class _$VolumeCheckCopyWithImpl<$Res, $Val extends VolumeCheck>
    implements $VolumeCheckCopyWith<$Res> {
  _$VolumeCheckCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isVolumeUpButtonPress = null,
    Object? isVolumeDownButtonPress = null,
  }) {
    return _then(_value.copyWith(
      isVolumeUpButtonPress: null == isVolumeUpButtonPress
          ? _value.isVolumeUpButtonPress
          : isVolumeUpButtonPress // ignore: cast_nullable_to_non_nullable
              as bool,
      isVolumeDownButtonPress: null == isVolumeDownButtonPress
          ? _value.isVolumeDownButtonPress
          : isVolumeDownButtonPress // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$VolumeCheckImplCopyWith<$Res>
    implements $VolumeCheckCopyWith<$Res> {
  factory _$$VolumeCheckImplCopyWith(
          _$VolumeCheckImpl value, $Res Function(_$VolumeCheckImpl) then) =
      __$$VolumeCheckImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isVolumeUpButtonPress, bool isVolumeDownButtonPress});
}

/// @nodoc
class __$$VolumeCheckImplCopyWithImpl<$Res>
    extends _$VolumeCheckCopyWithImpl<$Res, _$VolumeCheckImpl>
    implements _$$VolumeCheckImplCopyWith<$Res> {
  __$$VolumeCheckImplCopyWithImpl(
      _$VolumeCheckImpl _value, $Res Function(_$VolumeCheckImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isVolumeUpButtonPress = null,
    Object? isVolumeDownButtonPress = null,
  }) {
    return _then(_$VolumeCheckImpl(
      isVolumeUpButtonPress: null == isVolumeUpButtonPress
          ? _value.isVolumeUpButtonPress
          : isVolumeUpButtonPress // ignore: cast_nullable_to_non_nullable
              as bool,
      isVolumeDownButtonPress: null == isVolumeDownButtonPress
          ? _value.isVolumeDownButtonPress
          : isVolumeDownButtonPress // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$VolumeCheckImpl extends _VolumeCheck {
  const _$VolumeCheckImpl(
      {this.isVolumeUpButtonPress = false,
      this.isVolumeDownButtonPress = false})
      : super._();

  @override
  @JsonKey()
  final bool isVolumeUpButtonPress;
  @override
  @JsonKey()
  final bool isVolumeDownButtonPress;

  @override
  String toString() {
    return 'VolumeCheck(isVolumeUpButtonPress: $isVolumeUpButtonPress, isVolumeDownButtonPress: $isVolumeDownButtonPress)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$VolumeCheckImpl &&
            (identical(other.isVolumeUpButtonPress, isVolumeUpButtonPress) ||
                other.isVolumeUpButtonPress == isVolumeUpButtonPress) &&
            (identical(
                    other.isVolumeDownButtonPress, isVolumeDownButtonPress) ||
                other.isVolumeDownButtonPress == isVolumeDownButtonPress));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, isVolumeUpButtonPress, isVolumeDownButtonPress);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$VolumeCheckImplCopyWith<_$VolumeCheckImpl> get copyWith =>
      __$$VolumeCheckImplCopyWithImpl<_$VolumeCheckImpl>(this, _$identity);
}

abstract class _VolumeCheck extends VolumeCheck {
  const factory _VolumeCheck(
      {final bool isVolumeUpButtonPress,
      final bool isVolumeDownButtonPress}) = _$VolumeCheckImpl;
  const _VolumeCheck._() : super._();

  @override
  bool get isVolumeUpButtonPress;
  @override
  bool get isVolumeDownButtonPress;
  @override
  @JsonKey(ignore: true)
  _$$VolumeCheckImplCopyWith<_$VolumeCheckImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
