import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/volume/data/volume_check_repository_impl.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:volume_controller/volume_controller.dart';

part 'volume_check_repository.g.dart';

@riverpod
VolumeCheckRepository volumeCheckRepository(VolumeCheckRepositoryRef ref) {
  final VolumeController volumeController = VolumeController();
  final logger = ref.watch(loggerRepositoryProvider);
  return VolumeCheckRepositoryImpl(ref, volumeController: volumeController, logger: logger);
}

abstract class VolumeCheckRepository {
  Future<double> getCurrentVolume();
  void setCurrentVolume(double volume);
  String getVolumeToString();
  Stream<double> getVolume();
}
