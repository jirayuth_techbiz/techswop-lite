// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'volume_check_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$volumeCheckRepositoryHash() =>
    r'd98a3cacc2f571eaf5bc7b64da55c3f6db8cb15e';

/// See also [volumeCheckRepository].
@ProviderFor(volumeCheckRepository)
final volumeCheckRepositoryProvider =
    AutoDisposeProvider<VolumeCheckRepository>.internal(
  volumeCheckRepository,
  name: r'volumeCheckRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$volumeCheckRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef VolumeCheckRepositoryRef
    = AutoDisposeProviderRef<VolumeCheckRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
