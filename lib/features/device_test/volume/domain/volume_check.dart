import 'package:freezed_annotation/freezed_annotation.dart';

part 'volume_check.freezed.dart';

@freezed
class VolumeCheck with _$VolumeCheck {
  const factory VolumeCheck({
    @Default(false) bool isVolumeUpButtonPress,
    @Default(false) bool isVolumeDownButtonPress,
  }) = _VolumeCheck;

  const VolumeCheck._();
}
