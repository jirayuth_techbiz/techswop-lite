import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/volume/application/volume_check_service_impl.dart';
import 'package:techswop_lite/features/device_test/volume/domain/repository/volume_check_repository.dart';
import 'package:techswop_lite/features/device_test/volume/domain/volume_check.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

part 'volume_check_service.g.dart';

@riverpod
VolumeCheckService volumeCheckService(VolumeCheckServiceRef ref) {
  final tradeInSharePreferencesRepository =
      ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final volumeCheckRepository = ref.watch(volumeCheckRepositoryProvider);
  final logger = ref.watch(loggerRepositoryProvider);

  return VolumeCheckServiceImpl(
    ref,
    logger: logger,
    tradeInSharePreferencesRepository: tradeInSharePreferencesRepository,
    volumeCheckRepository: volumeCheckRepository,
  );
}

abstract class VolumeCheckService {
  Stream<VolumeCheck> checkVolume();
  Future<bool?> getVolumeCheckResult();
  Future<void> saveVolumeCheckResult(bool result);
}
