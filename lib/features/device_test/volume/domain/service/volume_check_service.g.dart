// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'volume_check_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$volumeCheckServiceHash() =>
    r'b49e5bebc7591a96f723639748981c032a7e7027';

/// See also [volumeCheckService].
@ProviderFor(volumeCheckService)
final volumeCheckServiceProvider =
    AutoDisposeProvider<VolumeCheckService>.internal(
  volumeCheckService,
  name: r'volumeCheckServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$volumeCheckServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef VolumeCheckServiceRef = AutoDisposeProviderRef<VolumeCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
