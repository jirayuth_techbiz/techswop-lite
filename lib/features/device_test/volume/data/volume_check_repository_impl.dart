import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/volume/domain/repository/volume_check_repository.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:volume_controller/volume_controller.dart';

class VolumeCheckRepositoryImpl implements VolumeCheckRepository {
  final Ref ref;
  final VolumeController volumeController;
  final LoggerRepository logger;

  VolumeCheckRepositoryImpl(
    this.ref,{
    required this.volumeController,
    required this.logger,
  });

  @override
  String getVolumeToString() {
    const volumeProvider = 0;
    return (10 - (volumeProvider * 10)).toStringAsFixed(0);
  }

  @override
  Stream<double> getVolume() async* {
    final VolumeController volcontroller = VolumeController();
    final StreamController<double> volumestream = StreamController<double>();

    volcontroller.listener((p0) {
      volumestream.sink.add(p0 * 10);
    });

    ref.onDispose(() {
      log('VolumeCheckRepositoryImpl disposed');
      volumestream.close();
    });

    yield* volumestream.stream;
  }

  @override
  Future<double> getCurrentVolume() async {
    final result = await volumeController.getVolume();
    return (result * 10);
  }

  @override
  void setCurrentVolume(double volume) {
    try {
      volumeController.setVolume(volume, showSystemUI: false);
    } catch (e) {
      log(e.toString());
    }
  }
}
