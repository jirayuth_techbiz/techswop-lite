// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wifi_check_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$wifiCheckStateHash() => r'084fa5074dffe09b77f0ada721e8e1d5cd8f3950';

/// See also [WifiCheckState].
@ProviderFor(WifiCheckState)
final wifiCheckStateProvider =
    AutoDisposeStreamNotifierProvider<WifiCheckState, WifiStatus>.internal(
  WifiCheckState.new,
  name: r'wifiCheckStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$wifiCheckStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$WifiCheckState = AutoDisposeStreamNotifier<WifiStatus>;
String _$wifiCheckManagerHash() => r'2f23d7ee9bb1360cfe7e071d36639bb70c697d0b';

/// See also [WifiCheckManager].
@ProviderFor(WifiCheckManager)
final wifiCheckManagerProvider =
    AutoDisposeAsyncNotifierProvider<WifiCheckManager, void>.internal(
  WifiCheckManager.new,
  name: r'wifiCheckManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$wifiCheckManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$WifiCheckManager = AutoDisposeAsyncNotifier<void>;
String _$wifiTimeoutStateHash() => r'b91082daf4e7fafd5305128c43f4c6ac1c9c3f77';

/// See also [WifiTimeoutState].
@ProviderFor(WifiTimeoutState)
final wifiTimeoutStateProvider =
    AutoDisposeStreamNotifierProvider<WifiTimeoutState, int>.internal(
  WifiTimeoutState.new,
  name: r'wifiTimeoutStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$wifiTimeoutStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$WifiTimeoutState = AutoDisposeStreamNotifier<int>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
