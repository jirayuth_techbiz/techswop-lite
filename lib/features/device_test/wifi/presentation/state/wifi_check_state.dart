import 'dart:async';

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/services/wifi_check_service.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/wifi_status.dart';
import 'package:techswop_lite/shared/shared.dart';

part 'wifi_check_state.g.dart';

@riverpod
class WifiCheckState extends _$WifiCheckState {
  @override
  Stream<WifiStatus> build() async* {
    final wifiService = ref.watch(wifiCheckServiceProvider);
    
    yield* wifiService.updateWifiStatus();

    ref.onDispose(() {
      wifiService.stopTimer();
      log("WifiCheckState disposed");
    });
  }
}

@riverpod
class WifiCheckManager extends _$WifiCheckManager {
  @override
  FutureOr<void> build() async {}

  Future<void> saveWifiCheckStatus(bool status) async {
    final wifiService = ref.watch(wifiCheckServiceProvider);

    state = const AsyncLoading();
    state =
        await AsyncValue.guard(() => wifiService.saveWifiCheckStatus(status));
  }

  Future<bool?> getWifiCheckStatus() async {
    final wifiService = ref.watch(wifiCheckServiceProvider);

    return await wifiService.getWifiCheckStatus();
  }
}

@riverpod
class WifiTimeoutState extends _$WifiTimeoutState {
  @override
  Stream<int> build() async* {
    final wifiService = ref.watch(wifiCheckServiceProvider);
    yield* wifiService.getTimer();
  }
}
