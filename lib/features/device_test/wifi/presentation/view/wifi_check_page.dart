import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/test_result/test_failed_status_icon.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/wifi_status.dart';
import 'package:techswop_lite/features/device_test/wifi/presentation/state/wifi_check_state.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';

class WifiCheckPage extends ConsumerStatefulWidget {
  const WifiCheckPage({super.key});

  @override
  ConsumerState<WifiCheckPage> createState() => _WifiCheckPageState();
}

class _WifiCheckPageState extends ConsumerState<WifiCheckPage> {
  @override
  Widget build(BuildContext context) {
    final theme = context.theme;
    final translation = context.localization;
    final wifiCheckState = ref.watch(wifiCheckStateProvider);
    final getWifiCheckPageNumber =
        ref.watch(getCurrentPageNumberProvider(AppRoutes.wifiCheckPage.path));
    final wifiCheckManager = ref.watch(wifiCheckManagerProvider.notifier);

    ref.listen(wifiCheckStateProvider, (prev, next) {
      if (next.asData!.value.isWifiConnected &&
          next.asData!.value.isWifiPermissionGranted) {
          _handlingWifiStatus(
            context: context,
            ref: ref,
            isCheck: true,
          );

      } else if (next.asData!.value.isWifiConnected == false) {
        _handlingWifiStatus(
          context: context,
          ref: ref,
          isCheck: false,
          failedReason: translation.wifiPageTestFailed,
        );
      }
    });

    return TestPageWidgetLayout(
      processNumber: "${translation.layoutStep} $getWifiCheckPageNumber",
      pictureAssetDirectory: CommonConst.CHECKLIST_PROPERTIES
          .firstWhere(
              (element) => element.appRoutesPage == AppRoutes.wifiCheckPage)
          .icon,
      title: translation.wifiPageTitle,
      nextPage: getWifiCheckPageNumber,
      underCard: AsyncValueWidget<WifiStatus>(
        value: wifiCheckState,
        data: (result) {
          if (result.isWifiConnected && result.isWifiPermissionGranted) {
            return Center(child: TestSuccessStatusIcon(theme: theme));
          } else if (!result.isWifiConnected) {
            return Center(child: TestFailedStatusIcon(theme: theme));
          }
          return const SizedBox();
        },
        loading: () => const SizedBox(),
        error: (err, stk) {
            _handlingWifiStatus(
                context: context,
                ref: ref,
                isCheck: false,
                failedReason: err.toString());
          return Center(child: TestFailedStatusIcon(theme: theme));
        },
      ),
      cardBodyText: translation.wifiPageDescription,
      onSkip: () async {
        await wifiCheckManager.saveWifiCheckStatus(false);
      },
    );
  }

  void _handlingWifiStatus({
    required BuildContext context,
    required WidgetRef ref,
    required bool isCheck,
    String? failedReason,
  }) {
    final isExecuted = ref.watch(resultIsExecutedStateProvider);
    final wifiCheckManager = ref.watch(wifiCheckManagerProvider.notifier);
    final resultHandler = TestCheckHandler(
      context: context,
      ref: ref,
      isCheck: isCheck,
      appRoutes: AppRoutes.wifiCheckPage,
      stateProvider: wifiCheckStateProvider,
      isExecuted: isExecuted,
    );

    if (isExecuted) return;
    Future.delayed(
            const Duration(seconds: AppConfig.futureDelayedTestAwaitDuration))
        .then(
      (_) async {
        await wifiCheckManager.saveWifiCheckStatus(isCheck);
        if (context.mounted) {
          await resultHandler.handlingStatus(
            failedReason: failedReason,
            onSkip: () async {
              await wifiCheckManager.saveWifiCheckStatus(false);
            },
          );
        }
      },
    );
  }
}
