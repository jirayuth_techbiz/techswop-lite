import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:rxdart/rxdart.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/repositories/wifi_check_repository.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/wifi_status.dart';
import 'package:techswop_lite/shared/shared.dart';

class WifiCheckRepositoryImpl implements WifiCheckRepository {
  final Ref ref;
  final Connectivity connectivity;
  final LoggerRepository logger;

  WifiCheckRepositoryImpl(this.ref, {
    required this.connectivity, 
    required this.logger
  });

  @override
  Stream<ConnectivityResult> connectivityResultStreamController() async* {
    final BehaviorSubject<ConnectivityResult> wifiStreamController =
        BehaviorSubject<ConnectivityResult>();
    StreamSubscription? wifiSubscription;

    wifiSubscription = connectivity.onConnectivityChanged.listen((results) {
      logger.logDebug('result = ${results.toString()}');
      for (final connectivityResult in results) {
        wifiStreamController.sink.add(connectivityResult);
      }
    });

    ref.onDispose(() {
      wifiSubscription?.cancel();
      wifiStreamController.close();
      logger.logDebug('wifiStreamController disposed');
    });

    yield* wifiStreamController.stream;
  }

  @override
  Future<List<ConnectivityResult>> isWifiOn() async {
    return await connectivity.checkConnectivity();
  }

  @override
  Future<WifiStatus> setWifiStatus(WifiStatus wifiStatus) async {
    return wifiStatus.copyWith(
        isWifiConnected: wifiStatus.isWifiConnected,
        isWifiPermissionGranted: wifiStatus.isWifiPermissionGranted);
  }
}
