// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'wifi_status.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$WifiStatus {
  bool get isWifiConnected => throw _privateConstructorUsedError;
  bool get isWifiPermissionGranted => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $WifiStatusCopyWith<WifiStatus> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WifiStatusCopyWith<$Res> {
  factory $WifiStatusCopyWith(
          WifiStatus value, $Res Function(WifiStatus) then) =
      _$WifiStatusCopyWithImpl<$Res, WifiStatus>;
  @useResult
  $Res call({bool isWifiConnected, bool isWifiPermissionGranted});
}

/// @nodoc
class _$WifiStatusCopyWithImpl<$Res, $Val extends WifiStatus>
    implements $WifiStatusCopyWith<$Res> {
  _$WifiStatusCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isWifiConnected = null,
    Object? isWifiPermissionGranted = null,
  }) {
    return _then(_value.copyWith(
      isWifiConnected: null == isWifiConnected
          ? _value.isWifiConnected
          : isWifiConnected // ignore: cast_nullable_to_non_nullable
              as bool,
      isWifiPermissionGranted: null == isWifiPermissionGranted
          ? _value.isWifiPermissionGranted
          : isWifiPermissionGranted // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$WifiStatusImplCopyWith<$Res>
    implements $WifiStatusCopyWith<$Res> {
  factory _$$WifiStatusImplCopyWith(
          _$WifiStatusImpl value, $Res Function(_$WifiStatusImpl) then) =
      __$$WifiStatusImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isWifiConnected, bool isWifiPermissionGranted});
}

/// @nodoc
class __$$WifiStatusImplCopyWithImpl<$Res>
    extends _$WifiStatusCopyWithImpl<$Res, _$WifiStatusImpl>
    implements _$$WifiStatusImplCopyWith<$Res> {
  __$$WifiStatusImplCopyWithImpl(
      _$WifiStatusImpl _value, $Res Function(_$WifiStatusImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isWifiConnected = null,
    Object? isWifiPermissionGranted = null,
  }) {
    return _then(_$WifiStatusImpl(
      isWifiConnected: null == isWifiConnected
          ? _value.isWifiConnected
          : isWifiConnected // ignore: cast_nullable_to_non_nullable
              as bool,
      isWifiPermissionGranted: null == isWifiPermissionGranted
          ? _value.isWifiPermissionGranted
          : isWifiPermissionGranted // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$WifiStatusImpl extends _WifiStatus {
  const _$WifiStatusImpl(
      {this.isWifiConnected = false, this.isWifiPermissionGranted = false})
      : super._();

  @override
  @JsonKey()
  final bool isWifiConnected;
  @override
  @JsonKey()
  final bool isWifiPermissionGranted;

  @override
  String toString() {
    return 'WifiStatus(isWifiConnected: $isWifiConnected, isWifiPermissionGranted: $isWifiPermissionGranted)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$WifiStatusImpl &&
            (identical(other.isWifiConnected, isWifiConnected) ||
                other.isWifiConnected == isWifiConnected) &&
            (identical(
                    other.isWifiPermissionGranted, isWifiPermissionGranted) ||
                other.isWifiPermissionGranted == isWifiPermissionGranted));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, isWifiConnected, isWifiPermissionGranted);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$WifiStatusImplCopyWith<_$WifiStatusImpl> get copyWith =>
      __$$WifiStatusImplCopyWithImpl<_$WifiStatusImpl>(this, _$identity);
}

abstract class _WifiStatus extends WifiStatus {
  const factory _WifiStatus(
      {final bool isWifiConnected,
      final bool isWifiPermissionGranted}) = _$WifiStatusImpl;
  const _WifiStatus._() : super._();

  @override
  bool get isWifiConnected;
  @override
  bool get isWifiPermissionGranted;
  @override
  @JsonKey(ignore: true)
  _$$WifiStatusImplCopyWith<_$WifiStatusImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
