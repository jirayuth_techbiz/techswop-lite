import 'dart:async';

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/wifi/application/wifi_check_service_impl.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/repositories/wifi_check_repository.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/wifi_status.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/shared/domain/repository/test_timeout_repository.dart';

part 'wifi_check_service.g.dart';

@riverpod
WifiCheckService wifiCheckService(WifiCheckServiceRef ref) {
  final wifiCheckRepository = ref.watch(wifiCheckRepositoryProvider);
  final tradeIsSharePreferencecsRepository =
      ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final testTimerRepository = ref.watch(testTimerRepositoryProvider);
  final logger = ref.watch(loggerRepositoryProvider);

  return WifiCheckServiceImpl(
    ref: ref,
    wifiCheckRepository: wifiCheckRepository,
    tradeInSharePreferencesRepository: tradeIsSharePreferencecsRepository,
    testTimerRepository: testTimerRepository,
    logger: logger,
  );
}

abstract class WifiCheckService {
  Stream<WifiStatus> updateWifiStatus();
  Future<bool> saveWifiCheckStatus(bool status);
  Future<bool?> getWifiCheckStatus();
  FutureOr<void> startTimer();
  FutureOr<void> stopTimer();
  Stream<int> getTimer();
}
