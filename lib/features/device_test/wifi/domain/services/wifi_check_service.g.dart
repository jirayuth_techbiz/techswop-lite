// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wifi_check_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$wifiCheckServiceHash() => r'f5119e7fe1da5c767f4a09887add80b3e7344c49';

/// See also [wifiCheckService].
@ProviderFor(wifiCheckService)
final wifiCheckServiceProvider = AutoDisposeProvider<WifiCheckService>.internal(
  wifiCheckService,
  name: r'wifiCheckServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$wifiCheckServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef WifiCheckServiceRef = AutoDisposeProviderRef<WifiCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
