import 'package:freezed_annotation/freezed_annotation.dart';

part 'wifi_status.freezed.dart';

@freezed
class WifiStatus with _$WifiStatus {

  const factory WifiStatus({
    @Default(false) bool isWifiConnected,
    @Default(false) bool isWifiPermissionGranted,
  }) = _WifiStatus;

  const WifiStatus._();
}
