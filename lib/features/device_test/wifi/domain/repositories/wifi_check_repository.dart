import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/wifi/data/wifi_check_repository_impl.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/wifi_status.dart';
import 'package:techswop_lite/shared/shared.dart';

part 'wifi_check_repository.g.dart';

@riverpod
WifiCheckRepository wifiCheckRepository(WifiCheckRepositoryRef ref) {
  final Connectivity connectivity = Connectivity();
  final loggerRepository = ref.read(loggerRepositoryProvider);

  return WifiCheckRepositoryImpl(
    ref,
    connectivity: connectivity,
    logger: loggerRepository,
  );
}

abstract class WifiCheckRepository {
  Stream<ConnectivityResult> connectivityResultStreamController();
  Future<List<ConnectivityResult>> isWifiOn();
  Future<WifiStatus> setWifiStatus(WifiStatus wifiStatus);
}
