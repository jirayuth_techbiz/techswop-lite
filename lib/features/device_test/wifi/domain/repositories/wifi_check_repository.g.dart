// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wifi_check_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$wifiCheckRepositoryHash() =>
    r'681c8714fb3a3cd668565c660c5d16ad4f86bb9e';

/// See also [wifiCheckRepository].
@ProviderFor(wifiCheckRepository)
final wifiCheckRepositoryProvider =
    AutoDisposeProvider<WifiCheckRepository>.internal(
  wifiCheckRepository,
  name: r'wifiCheckRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$wifiCheckRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef WifiCheckRepositoryRef = AutoDisposeProviderRef<WifiCheckRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
