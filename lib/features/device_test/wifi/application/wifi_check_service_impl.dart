import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/wifi_status.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/repositories/wifi_check_repository.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/services/wifi_check_service.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';

class WifiCheckServiceImpl implements WifiCheckService {
  final WifiCheckRepository wifiCheckRepository;
  final TradeInSharePreferencesRepository tradeInSharePreferencesRepository;
  final TestTimerRepository testTimerRepository;
  final Ref ref;
  final LoggerRepository logger;

  WifiCheckServiceImpl({
    required this.ref,
    required this.wifiCheckRepository,
    required this.tradeInSharePreferencesRepository,
    required this.testTimerRepository,
    required this.logger,
  });

  @override
  FutureOr<void> startTimer() async {
    await testTimerRepository.startTimer();
  }

  @override
  FutureOr<void> stopTimer() async {
    await testTimerRepository.stopTimer();
  }

  @override
  Stream<int> getTimer() async* {
    yield* testTimerRepository.getTimer();
  }

  @override
  Future<bool> saveWifiCheckStatus(bool status) async {
    final prefs = tradeInSharePreferencesRepository;
    try {
      return await prefs.saveBool(AppRoutes.wifiCheckPage.titleAPI, status);
    } catch (e) {
      logger.logWTF(e.toString());
      rethrow;
    }
  }

  @override
  Future<bool?> getWifiCheckStatus() async {
    final prefs = tradeInSharePreferencesRepository;
    try {
      return await prefs.getBool(AppRoutes.wifiCheckPage.titleAPI);
    } catch (e) {
      logger.logWTF(e.toString());
      rethrow;
    }
  }

  @override
  Stream<WifiStatus> updateWifiStatus() async* {
    StreamController<WifiStatus> wifiStatusController =
        StreamController<WifiStatus>();
    
    var wifiStatus = const WifiStatus();

    try {
      final isWifiOn = await wifiCheckRepository.isWifiOn();

      for (final connectivityResult in isWifiOn) {
        wifiStatus = wifiStatus.copyWith(
          isWifiConnected: connectivityResult == ConnectivityResult.wifi,
          isWifiPermissionGranted:
              connectivityResult == ConnectivityResult.wifi,
        );
      }
      logger.logDebug("wifiStatus: $wifiStatus");

      ref.onDispose(() {
        wifiStatusController.close();
      });

      wifiStatusController.sink.add(wifiStatus);

      yield* wifiStatusController.stream;
    } on Exception catch (e) {
      logger.logWTF(e.toString());
      wifiStatusController.sink.addError(e);
    } catch (e) {
      logger.logWTF(e.toString());
      rethrow;
    }
  }
}
