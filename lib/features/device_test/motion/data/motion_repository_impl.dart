import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:sensors_plus/sensors_plus.dart';
import 'package:techswop_lite/features/device_test/motion/domain/repository/motion_repository.dart';
import 'package:techswop_lite/shared/shared.dart';

class MotionCheckRepositoryImpl implements MotionCheckRepository {
  final Ref ref;
  MotionCheckRepositoryImpl(this.ref);

  @override
  Stream<AccelerometerEvent> getMotion() async* {
    /**
       * ใช้ AccelerometerEvent
       * User ต้องหรือหมุนจอ ให้แกน X หรือ Z 
       * มีค่ามากกว่า 8 จึงจะถือว่าผ่าน
       */

    StreamController<AccelerometerEvent> controller =
        StreamController<AccelerometerEvent>();

    final accelerometerStream = accelerometerEventStream(
      samplingPeriod: SensorInterval.gameInterval,
    ).listen(
      (result) {
        controller.sink.add(result);
      },
      onError: (error) {
        controller.sink.addError(error);
      },
      cancelOnError: true,
    );

    ref.onDispose(() {
      log('motionRepository disposed');
      accelerometerStream.cancel();
      controller.close();
    });

    yield* controller.stream;
  }
}
