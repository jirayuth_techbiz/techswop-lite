import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:sensors_plus/sensors_plus.dart';
import 'package:techswop_lite/features/device_test/motion/data/motion_repository_impl.dart';

part 'motion_repository.g.dart';

@riverpod
MotionCheckRepository motionCheckRepository(MotionCheckRepositoryRef ref) {
  return MotionCheckRepositoryImpl(ref);
}

abstract class MotionCheckRepository {
  Stream<AccelerometerEvent> getMotion();
}
