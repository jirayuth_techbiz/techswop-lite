// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'motion_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$motionCheckRepositoryHash() =>
    r'3b587ec4452af6a3ac08f6ab4524931895a419de';

/// See also [motionCheckRepository].
@ProviderFor(motionCheckRepository)
final motionCheckRepositoryProvider =
    AutoDisposeProvider<MotionCheckRepository>.internal(
  motionCheckRepository,
  name: r'motionCheckRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$motionCheckRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef MotionCheckRepositoryRef
    = AutoDisposeProviderRef<MotionCheckRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
