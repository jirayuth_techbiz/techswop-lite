// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'motion_check.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$MotionCheck {
  bool get isHorizontal => throw _privateConstructorUsedError;
  bool get isVertical => throw _privateConstructorUsedError;
  bool get isStart => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MotionCheckCopyWith<MotionCheck> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MotionCheckCopyWith<$Res> {
  factory $MotionCheckCopyWith(
          MotionCheck value, $Res Function(MotionCheck) then) =
      _$MotionCheckCopyWithImpl<$Res, MotionCheck>;
  @useResult
  $Res call({bool isHorizontal, bool isVertical, bool isStart});
}

/// @nodoc
class _$MotionCheckCopyWithImpl<$Res, $Val extends MotionCheck>
    implements $MotionCheckCopyWith<$Res> {
  _$MotionCheckCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isHorizontal = null,
    Object? isVertical = null,
    Object? isStart = null,
  }) {
    return _then(_value.copyWith(
      isHorizontal: null == isHorizontal
          ? _value.isHorizontal
          : isHorizontal // ignore: cast_nullable_to_non_nullable
              as bool,
      isVertical: null == isVertical
          ? _value.isVertical
          : isVertical // ignore: cast_nullable_to_non_nullable
              as bool,
      isStart: null == isStart
          ? _value.isStart
          : isStart // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$MotionCheckImplCopyWith<$Res>
    implements $MotionCheckCopyWith<$Res> {
  factory _$$MotionCheckImplCopyWith(
          _$MotionCheckImpl value, $Res Function(_$MotionCheckImpl) then) =
      __$$MotionCheckImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isHorizontal, bool isVertical, bool isStart});
}

/// @nodoc
class __$$MotionCheckImplCopyWithImpl<$Res>
    extends _$MotionCheckCopyWithImpl<$Res, _$MotionCheckImpl>
    implements _$$MotionCheckImplCopyWith<$Res> {
  __$$MotionCheckImplCopyWithImpl(
      _$MotionCheckImpl _value, $Res Function(_$MotionCheckImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isHorizontal = null,
    Object? isVertical = null,
    Object? isStart = null,
  }) {
    return _then(_$MotionCheckImpl(
      isHorizontal: null == isHorizontal
          ? _value.isHorizontal
          : isHorizontal // ignore: cast_nullable_to_non_nullable
              as bool,
      isVertical: null == isVertical
          ? _value.isVertical
          : isVertical // ignore: cast_nullable_to_non_nullable
              as bool,
      isStart: null == isStart
          ? _value.isStart
          : isStart // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$MotionCheckImpl extends _MotionCheck {
  const _$MotionCheckImpl(
      {this.isHorizontal = false,
      this.isVertical = false,
      this.isStart = false})
      : super._();

  @override
  @JsonKey()
  final bool isHorizontal;
  @override
  @JsonKey()
  final bool isVertical;
  @override
  @JsonKey()
  final bool isStart;

  @override
  String toString() {
    return 'MotionCheck(isHorizontal: $isHorizontal, isVertical: $isVertical, isStart: $isStart)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MotionCheckImpl &&
            (identical(other.isHorizontal, isHorizontal) ||
                other.isHorizontal == isHorizontal) &&
            (identical(other.isVertical, isVertical) ||
                other.isVertical == isVertical) &&
            (identical(other.isStart, isStart) || other.isStart == isStart));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, isHorizontal, isVertical, isStart);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MotionCheckImplCopyWith<_$MotionCheckImpl> get copyWith =>
      __$$MotionCheckImplCopyWithImpl<_$MotionCheckImpl>(this, _$identity);
}

abstract class _MotionCheck extends MotionCheck {
  const factory _MotionCheck(
      {final bool isHorizontal,
      final bool isVertical,
      final bool isStart}) = _$MotionCheckImpl;
  const _MotionCheck._() : super._();

  @override
  bool get isHorizontal;
  @override
  bool get isVertical;
  @override
  bool get isStart;
  @override
  @JsonKey(ignore: true)
  _$$MotionCheckImplCopyWith<_$MotionCheckImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
