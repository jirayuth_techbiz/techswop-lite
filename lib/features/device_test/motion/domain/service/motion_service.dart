import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/motion/application/motion_check_service_impl.dart';
import 'package:techswop_lite/features/device_test/motion/domain/motion_check.dart';
import 'package:techswop_lite/features/device_test/motion/domain/repository/motion_repository.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

part 'motion_service.g.dart';

@riverpod
MotionCheckService motionCheckService(MotionCheckServiceRef ref) {
  final MotionCheckRepository motionCheckRepository = ref.watch(motionCheckRepositoryProvider);
  final TradeInSharePreferencesRepository prefs = ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
  final LoggerRepository logger = ref.watch(loggerRepositoryProvider);

  return MotionCheckServiceImpl(ref, 
    motionRepository: motionCheckRepository, 
    tradeinSharePreferencesRepository: prefs,
    logger: logger
  );
}

abstract class MotionCheckService {
  Stream<MotionCheck> getMotionCheck();
  Future<bool?> getMotionCheckResult();
  Future<void> saveMotionCheckResult(bool status);
}