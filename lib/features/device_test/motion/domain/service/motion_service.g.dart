// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'motion_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$motionCheckServiceHash() =>
    r'0a205db6283f54b1ff7cbf12727b3a97d85a1026';

/// See also [motionCheckService].
@ProviderFor(motionCheckService)
final motionCheckServiceProvider =
    AutoDisposeProvider<MotionCheckService>.internal(
  motionCheckService,
  name: r'motionCheckServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$motionCheckServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef MotionCheckServiceRef = AutoDisposeProviderRef<MotionCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
