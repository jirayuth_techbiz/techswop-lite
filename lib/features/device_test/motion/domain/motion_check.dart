import 'package:freezed_annotation/freezed_annotation.dart';

part 'motion_check.freezed.dart';

@freezed 
class MotionCheck with _$MotionCheck {
  const factory MotionCheck({
    @Default(false) bool isHorizontal,
    @Default(false) bool isVertical,
    @Default(false) bool isStart,
  }) = _MotionCheck;

  const MotionCheck._();
}