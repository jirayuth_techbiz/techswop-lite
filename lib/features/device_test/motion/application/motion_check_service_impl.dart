import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/device_test/motion/domain/motion_check.dart';
import 'package:techswop_lite/features/device_test/motion/domain/repository/motion_repository.dart';
import 'package:techswop_lite/features/device_test/motion/domain/service/motion_service.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/const/const.dart';

class MotionCheckServiceImpl implements MotionCheckService {
  final Ref ref;
  final TradeInSharePreferencesRepository tradeinSharePreferencesRepository;
  final MotionCheckRepository? motionRepository;
  final LoggerRepository logger;

  MotionCheckServiceImpl(
    this.ref, {
    required this.motionRepository,
    required this.tradeinSharePreferencesRepository,
    required this.logger,
  });

  Orientation? initialOrientation;
  bool isStart = false;

  @override
  Stream<MotionCheck> getMotionCheck() async* {
    StreamController<MotionCheck> motionCheckController =
        StreamController<MotionCheck>();
    StreamSubscription? motionStream;
    MotionCheck orientationCheck = const MotionCheck();

    try {
      motionStream = motionRepository!.getMotion().listen(
        (event) {
          
          
          if (event.y > 8 && orientationCheck.isStart) {
            logger.logInfo("y: ${event.x.toString()}");
            orientationCheck = orientationCheck.copyWith(
              isHorizontal: true,
            );
            motionStream?.cancel();
            motionCheckController.add(orientationCheck);
          }

          if (event.x > 8) {
            logger.logInfo("x: ${event.y.toString()}");
            orientationCheck = orientationCheck.copyWith(
              isVertical: true,
              isStart: true,
            );
            motionCheckController.add(orientationCheck);
          }
        },
      );

      motionCheckController.add(orientationCheck);

      ref.onDispose(() {
        motionStream?.cancel();
        motionCheckController.close();
      });

      yield* motionCheckController.stream;
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<bool?> getMotionCheckResult() async {
    return await tradeinSharePreferencesRepository
        .getBool(AppRoutes.motionCheckPage.titleAPI);
  }

  @override
  Future<void> saveMotionCheckResult(bool status) async {
    await tradeinSharePreferencesRepository.saveBool(
        AppRoutes.motionCheckPage.titleAPI, status);
  }
}
