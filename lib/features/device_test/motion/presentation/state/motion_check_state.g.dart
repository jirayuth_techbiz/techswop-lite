// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'motion_check_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$motionCheckStateHash() => r'3868a4386b357e002e2f0852247aa13805e01b5f';

/// See also [MotionCheckState].
@ProviderFor(MotionCheckState)
final motionCheckStateProvider =
    AutoDisposeStreamNotifierProvider<MotionCheckState, MotionCheck>.internal(
  MotionCheckState.new,
  name: r'motionCheckStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$motionCheckStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$MotionCheckState = AutoDisposeStreamNotifier<MotionCheck>;
String _$motionCheckManagerHash() =>
    r'f9e29349f6272e9e78847d8325288571c35f3a78';

/// See also [MotionCheckManager].
@ProviderFor(MotionCheckManager)
final motionCheckManagerProvider =
    AutoDisposeAsyncNotifierProvider<MotionCheckManager, void>.internal(
  MotionCheckManager.new,
  name: r'motionCheckManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$motionCheckManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$MotionCheckManager = AutoDisposeAsyncNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
