// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/motion/domain/motion_check.dart';
import 'package:techswop_lite/features/device_test/motion/domain/service/motion_service.dart';

part 'motion_check_state.g.dart';

@riverpod
class MotionCheckState extends _$MotionCheckState {
  @override
  Stream<MotionCheck> build() async* {
    final MotionCheckService _motionCheckService =
        ref.watch(motionCheckServiceProvider);

    yield* _motionCheckService.getMotionCheck();
  }
}


@riverpod
class MotionCheckManager extends _$MotionCheckManager {
  @override
  Future<void> build() async {}

  Future<bool?> getMotionCheckResult() async {
    final MotionCheckService _motionCheckService =
        ref.watch(motionCheckServiceProvider);
    return await _motionCheckService.getMotionCheckResult();
  }

  Future<void> saveMotionCheckResult(bool status) async {
    final MotionCheckService _motionCheckService =
        ref.watch(motionCheckServiceProvider);
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(
        () => _motionCheckService.saveMotionCheckResult(status));
  }
}
