// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/motion/presentation/state/motion_check_state.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';

class MotionPage extends ConsumerWidget {
  const MotionPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = context.theme;
    final translation = context.localization;
    final motionCheckState = ref.watch(motionCheckStateProvider);
    final getMotionCheckPageNumber =
        ref.watch(getCurrentPageNumberProvider(AppRoutes.motionCheckPage.path));
    final motionCheckManager = ref.watch(motionCheckManagerProvider.notifier);

    ref.listen(motionCheckStateProvider, (prev, next) {
      if (next.asData!.value.isHorizontal && next.asData!.value.isVertical && next.asData!.value.isStart) {
        _handlingMotionTest(
          context: context,
          ref: ref,
          isCheck: true,
        );
      }
    });

    return TestPageWidgetLayout(
      nextPage: getMotionCheckPageNumber,
      processNumber: "${translation.layoutStep} $getMotionCheckPageNumber",
      title: translation.motionPageTitle,
      pictureAssetDirectory: CommonConst.CHECKLIST_PROPERTIES
          .firstWhere(
              (element) => element.appRoutesPage == AppRoutes.motionCheckPage)
          .icon,
      cardBodyText: translation.motionPageDescription,
      underCard: AsyncValueWidget(
        value: motionCheckState,
        data: (motionCheck) => Column(
          children: [
            _MotionCheckResultWidget(
              theme: theme,
              text: translation.motionPageHorizontal,
              isMotionSensorWorking: motionCheck.isHorizontal,
            ),
            _MotionCheckResultWidget(
              theme: theme,
              text: translation.motionPageVertical,
              isMotionSensorWorking: motionCheck.isVertical,
            ),
          ],
        ),
      ),
      onSkip: () async {
        await motionCheckManager.saveMotionCheckResult(false);
      },
    );
  }

  void _handlingMotionTest(
      {required BuildContext context,
      required WidgetRef ref,
      required bool isCheck,
      String? failedReason}) {
    final isExecuted = ref.watch(resultIsExecutedStateProvider);
    final motionCheckManager = ref.watch(motionCheckManagerProvider.notifier);
    final resultHandler = TestCheckHandler(
      context: context,
      ref: ref,
      isCheck: isCheck,
      appRoutes: AppRoutes.motionCheckPage,
      stateProvider: motionCheckStateProvider,
      isExecuted: isExecuted,
    );
    if (isExecuted) return;

    Future.delayed(
            const Duration(milliseconds: AppConfig.futureSuccessAwaitDuration))
        .then((_) async {
      log("Motion Check: ${(await motionCheckManager.getMotionCheckResult())}");

      await motionCheckManager.saveMotionCheckResult(isCheck);

      if (context.mounted) {
        await resultHandler.handlingStatus(
          onSkip: () async {
            await motionCheckManager.saveMotionCheckResult(false);
          },
          failedReason: failedReason,
        );
      }
    });
  }
}

class _MotionCheckResultWidget extends ConsumerStatefulWidget {
  final bool isMotionSensorWorking;
  final ThemeData theme;
  final String text;

  const _MotionCheckResultWidget({
    required this.theme,
    required this.text,
    required this.isMotionSensorWorking,
  });

  @override
  ConsumerState<_MotionCheckResultWidget> createState() =>
      _ProximityCheckResultWidgetState();
}

class _ProximityCheckResultWidgetState
    extends ConsumerState<_MotionCheckResultWidget> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 6.0.w),
        child: CheckboxListTile(
          onChanged:
              (result) {}, // ไม่ได้ใช้ แต่ [CheckboxListTile] ต้องใส่ [onChanged] ไม่งั้นจะ Error,
          value: widget.isMotionSensorWorking,
          title: Text(widget.text),
        ),
      ),
    );
  }
}
