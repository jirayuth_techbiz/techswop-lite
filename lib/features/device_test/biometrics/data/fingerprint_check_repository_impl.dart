import 'package:local_auth/local_auth.dart';
import 'package:local_auth_android/local_auth_android.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/repository/fingerprint_check_repository.dart';
import 'package:techswop_lite/generated/app_localizations.dart';

class FingerprintCheckRepositoryImpl implements FingerprintCheckRepository {
  final AppLocalizations translation;
  final LocalAuthentication localauth;

  FingerprintCheckRepositoryImpl({required this.translation, required this.localauth});

  @override
  Future<bool> authenticateFingerprint() async {
    final List<BiometricType> availableBiometrics =
        await localauth.getAvailableBiometrics();
    final List<BiometricType> avaliableBiometrics = [];

    for (final biometric in availableBiometrics) {
      if (biometric == BiometricType.weak) {
        avaliableBiometrics.add(biometric);
      }
    }

    if (avaliableBiometrics.isNotEmpty) {
      return await localauth.authenticate(
        authMessages: [
          AndroidAuthMessages(
            cancelButton: translation.cancelTxt,
          ),
        ],
        localizedReason: translation.biometricPagePleaseScanFingerPrint,
        options: const AuthenticationOptions(
          biometricOnly: true,
          useErrorDialogs: false,
        ),
      );
    }

    return false;
  }

  @override
  Future<bool> isFingerprintAvailable() async {
    return await localauth.canCheckBiometrics;
  }
}
