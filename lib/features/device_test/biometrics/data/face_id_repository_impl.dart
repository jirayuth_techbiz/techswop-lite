import 'package:local_auth/local_auth.dart';
import 'package:local_auth_darwin/local_auth_darwin.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/repository/face_id_repository.dart';
import 'package:techswop_lite/generated/app_localizations.dart';

class FaceIdRepositoryImpl implements FaceIdRepository {
  final AppLocalizations translation;
  final LocalAuthentication localauth;
  FaceIdRepositoryImpl({required this.translation, required this.localauth});

  @override
  Future<bool> authenticateFaceId() async {
    final List<BiometricType> availableBiometrics =
        await localauth.getAvailableBiometrics();
    final List<BiometricType> avaliableBiometrics = [];

    for (final biometric in availableBiometrics) {
      if (biometric == BiometricType.face ||
          biometric == BiometricType.iris ||
          biometric == BiometricType.strong) {
        avaliableBiometrics.add(biometric);
      }
    }

    if (avaliableBiometrics.isNotEmpty) {
      return await localauth.authenticate(
        authMessages: [
          IOSAuthMessages(
            cancelButton: translation.cancelTxt,
          ),
        ],
        localizedReason: translation.biometricPagePleaseScanFingerPrint,
        options: const AuthenticationOptions(
          biometricOnly: true,
          useErrorDialogs: false,
        ),
      );
    }

    return false;
  }

  @override
  Future<bool> isFaceIdAvailable() async {
    return await localauth.canCheckBiometrics;
  }
}
