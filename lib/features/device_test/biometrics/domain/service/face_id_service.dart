// import 'package:riverpod_annotation/riverpod_annotation.dart';
// import 'package:techswop_lite/features/device_test/biometrics/application/face_id_service_impl.dart';
// import 'package:techswop_lite/features/device_test/biometrics/domain/face_id_check.dart';
// import 'package:techswop_lite/features/device_test/biometrics/domain/repository/face_id_repository.dart';
// import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

// part 'face_id_service.g.dart';

// @riverpod
// FaceIdService faceIdService(FaceIdServiceRef ref) {
//   final faceIdRepo = ref.watch(faceIdRepositoryProvider);
//   final prefs =
//       ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;
//   return FaceIdServiceImpl(faceIdRepo, prefs);
// }

// abstract class FaceIdService {
//   Future<FaceIdCheck> isFaceIdAvailable();
//   Future<FaceIdCheck> isFaceIdCheck();
//   Future<bool?> getFaceIdCheck();
//   Future<void> saveFaceIdCheck(bool value);
// }
