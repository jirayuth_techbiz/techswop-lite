// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fingerprint_check_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fingerprintCheckServiceHash() =>
    r'6124957e326a0400b9b303b21af8b26db992f151';

/// See also [fingerprintCheckService].
@ProviderFor(fingerprintCheckService)
final fingerprintCheckServiceProvider =
    AutoDisposeProvider<FingerprintCheckService>.internal(
  fingerprintCheckService,
  name: r'fingerprintCheckServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$fingerprintCheckServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef FingerprintCheckServiceRef
    = AutoDisposeProviderRef<FingerprintCheckService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
