import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/biometrics/application/fingerprint_check_service_impl.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/fingerprint_check.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/repository/fingerprint_check_repository.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

part 'fingerprint_check_service.g.dart';

@riverpod
FingerprintCheckService fingerprintCheckService(
    FingerprintCheckServiceRef ref) {
  final fingerprintCheckRepository =
      ref.watch(fingerprintCheckRepositoryProvider);
  final tradeInSharePreferencesRepository =
      ref.watch(tradeInSharePreferencesRepositoryProvider).requireValue;

  return FingerprintCheckServiceImpl(
      fingerprintCheckRepository, tradeInSharePreferencesRepository);
}

abstract class FingerprintCheckService {
  Future<FingerprintCheck> isFingerPrintAvailable();
  Future<FingerprintCheck> isFingerPrintScanSuccess();
  Future<void> saveFingerPrintCheck(bool value);
  Future<bool?> getFingerPrintCheck();
}
