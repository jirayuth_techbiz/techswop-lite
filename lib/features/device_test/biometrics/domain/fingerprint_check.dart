import 'package:freezed_annotation/freezed_annotation.dart';

part 'fingerprint_check.freezed.dart';

@freezed
class FingerprintCheck with _$FingerprintCheck {
  const factory FingerprintCheck({
    @Default(false) bool isFingerPrintAvailable,
    @Default(false) bool isFingerprintScanSuccess,
  }) = _FingerprintCheck;

  const FingerprintCheck._();
}