// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'fingerprint_check.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$FingerprintCheck {
  bool get isFingerPrintAvailable => throw _privateConstructorUsedError;
  bool get isFingerprintScanSuccess => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FingerprintCheckCopyWith<FingerprintCheck> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FingerprintCheckCopyWith<$Res> {
  factory $FingerprintCheckCopyWith(
          FingerprintCheck value, $Res Function(FingerprintCheck) then) =
      _$FingerprintCheckCopyWithImpl<$Res, FingerprintCheck>;
  @useResult
  $Res call({bool isFingerPrintAvailable, bool isFingerprintScanSuccess});
}

/// @nodoc
class _$FingerprintCheckCopyWithImpl<$Res, $Val extends FingerprintCheck>
    implements $FingerprintCheckCopyWith<$Res> {
  _$FingerprintCheckCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isFingerPrintAvailable = null,
    Object? isFingerprintScanSuccess = null,
  }) {
    return _then(_value.copyWith(
      isFingerPrintAvailable: null == isFingerPrintAvailable
          ? _value.isFingerPrintAvailable
          : isFingerPrintAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
      isFingerprintScanSuccess: null == isFingerprintScanSuccess
          ? _value.isFingerprintScanSuccess
          : isFingerprintScanSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FingerprintCheckImplCopyWith<$Res>
    implements $FingerprintCheckCopyWith<$Res> {
  factory _$$FingerprintCheckImplCopyWith(_$FingerprintCheckImpl value,
          $Res Function(_$FingerprintCheckImpl) then) =
      __$$FingerprintCheckImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({bool isFingerPrintAvailable, bool isFingerprintScanSuccess});
}

/// @nodoc
class __$$FingerprintCheckImplCopyWithImpl<$Res>
    extends _$FingerprintCheckCopyWithImpl<$Res, _$FingerprintCheckImpl>
    implements _$$FingerprintCheckImplCopyWith<$Res> {
  __$$FingerprintCheckImplCopyWithImpl(_$FingerprintCheckImpl _value,
      $Res Function(_$FingerprintCheckImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isFingerPrintAvailable = null,
    Object? isFingerprintScanSuccess = null,
  }) {
    return _then(_$FingerprintCheckImpl(
      isFingerPrintAvailable: null == isFingerPrintAvailable
          ? _value.isFingerPrintAvailable
          : isFingerPrintAvailable // ignore: cast_nullable_to_non_nullable
              as bool,
      isFingerprintScanSuccess: null == isFingerprintScanSuccess
          ? _value.isFingerprintScanSuccess
          : isFingerprintScanSuccess // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$FingerprintCheckImpl extends _FingerprintCheck {
  const _$FingerprintCheckImpl(
      {this.isFingerPrintAvailable = false,
      this.isFingerprintScanSuccess = false})
      : super._();

  @override
  @JsonKey()
  final bool isFingerPrintAvailable;
  @override
  @JsonKey()
  final bool isFingerprintScanSuccess;

  @override
  String toString() {
    return 'FingerprintCheck(isFingerPrintAvailable: $isFingerPrintAvailable, isFingerprintScanSuccess: $isFingerprintScanSuccess)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FingerprintCheckImpl &&
            (identical(other.isFingerPrintAvailable, isFingerPrintAvailable) ||
                other.isFingerPrintAvailable == isFingerPrintAvailable) &&
            (identical(
                    other.isFingerprintScanSuccess, isFingerprintScanSuccess) ||
                other.isFingerprintScanSuccess == isFingerprintScanSuccess));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, isFingerPrintAvailable, isFingerprintScanSuccess);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FingerprintCheckImplCopyWith<_$FingerprintCheckImpl> get copyWith =>
      __$$FingerprintCheckImplCopyWithImpl<_$FingerprintCheckImpl>(
          this, _$identity);
}

abstract class _FingerprintCheck extends FingerprintCheck {
  const factory _FingerprintCheck(
      {final bool isFingerPrintAvailable,
      final bool isFingerprintScanSuccess}) = _$FingerprintCheckImpl;
  const _FingerprintCheck._() : super._();

  @override
  bool get isFingerPrintAvailable;
  @override
  bool get isFingerprintScanSuccess;
  @override
  @JsonKey(ignore: true)
  _$$FingerprintCheckImplCopyWith<_$FingerprintCheckImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
