// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'face_id_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$faceIdRepositoryHash() => r'aa7e29b933e92246ffa79d64a80c601562ec0fe2';

/// See also [faceIdRepository].
@ProviderFor(faceIdRepository)
final faceIdRepositoryProvider = AutoDisposeProvider<FaceIdRepository>.internal(
  faceIdRepository,
  name: r'faceIdRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$faceIdRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef FaceIdRepositoryRef = AutoDisposeProviderRef<FaceIdRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
