
import 'package:local_auth/local_auth.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/biometrics/data/fingerprint_check_repository_impl.dart';
import 'package:techswop_lite/shared/domain/providers/app_l10n_provider.dart';

part 'fingerprint_check_repository.g.dart';

@riverpod 
FingerprintCheckRepository fingerprintCheckRepository(FingerprintCheckRepositoryRef ref) {
  final localization = ref.watch(appLocalizationsProvider);
  final LocalAuthentication localauth = LocalAuthentication();
  return FingerprintCheckRepositoryImpl(translation: localization, localauth: localauth);
}

abstract class FingerprintCheckRepository {
  Future<bool> isFingerprintAvailable();
  Future<bool> authenticateFingerprint();
}