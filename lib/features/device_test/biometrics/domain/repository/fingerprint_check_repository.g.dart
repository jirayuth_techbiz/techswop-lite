// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fingerprint_check_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fingerprintCheckRepositoryHash() =>
    r'50000142868b059d06c81166c7ae7eda381ed4cd';

/// See also [fingerprintCheckRepository].
@ProviderFor(fingerprintCheckRepository)
final fingerprintCheckRepositoryProvider =
    AutoDisposeProvider<FingerprintCheckRepository>.internal(
  fingerprintCheckRepository,
  name: r'fingerprintCheckRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$fingerprintCheckRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef FingerprintCheckRepositoryRef
    = AutoDisposeProviderRef<FingerprintCheckRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
