import 'package:local_auth/local_auth.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/biometrics/data/face_id_repository_impl.dart';
import 'package:techswop_lite/shared/domain/providers/app_l10n_provider.dart';

part 'face_id_repository.g.dart';

@riverpod
FaceIdRepository faceIdRepository(FaceIdRepositoryRef ref) {
  final localization = ref.watch(appLocalizationsProvider);
  final LocalAuthentication localAuth = LocalAuthentication();
  return FaceIdRepositoryImpl(translation :localization, localauth: localAuth);
}

abstract class FaceIdRepository {
  Future<bool> isFaceIdAvailable();
  Future<bool> authenticateFaceId();
}

