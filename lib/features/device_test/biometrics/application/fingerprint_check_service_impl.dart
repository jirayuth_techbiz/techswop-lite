import 'package:techswop_lite/features/device_test/biometrics/domain/fingerprint_check.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/repository/fingerprint_check_repository.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/service/fingerprint_check_service.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
import 'package:techswop_lite/const/const.dart';

class FingerprintCheckServiceImpl implements FingerprintCheckService {
  final FingerprintCheckRepository _fingerprintCheckRepository;
  final TradeInSharePreferencesRepository _tradeInSharePreferencesRepository;

  FingerprintCheckServiceImpl(this._fingerprintCheckRepository,
      this._tradeInSharePreferencesRepository);

  @override
  Future<FingerprintCheck> isFingerPrintAvailable() async {
    var fingerprintCheck = const FingerprintCheck();

    try {
      final isFingerPrintAvailable =
          await _fingerprintCheckRepository.isFingerprintAvailable();
      
      fingerprintCheck = fingerprintCheck.copyWith(
          isFingerPrintAvailable: isFingerPrintAvailable, );

      return fingerprintCheck;
    } catch (e) {
      return fingerprintCheck;
    }
  }

  @override 
  Future<FingerprintCheck> isFingerPrintScanSuccess() async {
    var fingerprintCheck = const FingerprintCheck();

    try {
      final isFingerprintScanSuccess =
          await _fingerprintCheckRepository.authenticateFingerprint();
      
      fingerprintCheck = fingerprintCheck.copyWith(
          isFingerprintScanSuccess: isFingerprintScanSuccess, );

      return fingerprintCheck;
    } catch (e) {
      return fingerprintCheck;
    }
  }

  @override
  Future<bool?> getFingerPrintCheck() async {
    return await _tradeInSharePreferencesRepository.getBool(AppRoutes.biometricsCheckPage.titleAPI);
  }

  @override
  Future<void> saveFingerPrintCheck(bool value) async {
    await _tradeInSharePreferencesRepository.saveBool(AppRoutes.biometricsCheckPage.titleAPI, value);
  }
}
