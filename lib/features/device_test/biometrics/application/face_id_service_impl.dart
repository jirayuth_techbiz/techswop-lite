// import 'package:techswop_lite/features/device_test/biometrics/domain/face_id_check.dart';
// import 'package:techswop_lite/features/device_test/biometrics/domain/repository/face_id_repository.dart';
// import 'package:techswop_lite/features/device_test/biometrics/domain/service/face_id_service.dart';
// import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';
// import 'package:techswop_lite/const/const.dart';

// class FaceIdServiceImpl implements FaceIdService {
//   final FaceIdRepository _faceIdRepository;
//   final TradeInSharePreferencesRepository _tradeInSharePreferencesRepository;

//   FaceIdServiceImpl(
//       this._faceIdRepository, this._tradeInSharePreferencesRepository);

//   TradeInSharePreferencesRepository get prefs => _tradeInSharePreferencesRepository;

//   @override
//   Future<FaceIdCheck> isFaceIdAvailable() async {
//     var faceIdCheck = const FaceIdCheck();

//     try {
//       final isFaceIdAvailable = await _faceIdRepository.isFaceIdAvailable();

//       return faceIdCheck.copyWith(
//         isFaceIdAvailable: isFaceIdAvailable,
//       );
//     } catch (e) {
//       return faceIdCheck;
//     }
//   }

//   @override
//   Future<FaceIdCheck> isFaceIdCheck() async {
//     var faceIdCheck = const FaceIdCheck();

//     try {
//       final isFaceIdScannedSuccess = await _faceIdRepository.authenticateFaceId();

//       return faceIdCheck.copyWith(
//         isFaceIdScannedSuccess: isFaceIdScannedSuccess
//       );
      
//     } catch (e) {
//       return faceIdCheck;
//     }

//   }

//   @override
//   Future<void> saveFaceIdCheck(bool value) async {
//     await prefs.saveBool(AppRoutes.faceIdCheckPage.titleAPI, value);
//   }
  
//   @override
//   Future<bool?> getFaceIdCheck() async {
//     return await prefs.getBool(AppRoutes.faceIdCheckPage.titleAPI);
//   }
// }
