import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/features/device_test/base_layout/base_layout.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/test_result/test_failed_status_icon.dart';
import 'package:techswop_lite/features/device_test/biometrics/presentation/state/fingerprint_check_state.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/common_widgets/gradient_elevated_long_button.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';

class BiometricsCheckPage extends ConsumerWidget {
  const BiometricsCheckPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final translation = context.localization;

    final fingerPrintCheckState = ref.watch(fingerprintCheckStateProvider);
    final getBiometricsPageNumber = ref.watch(
        getCurrentPageNumberProvider(AppRoutes.biometricsCheckPage.path));
    final theme = context.theme;
    final biometricsCheckManager =
        ref.watch(biometricsCheckManagerProvider.notifier);

    ref.listen(fingerprintCheckStateProvider, (prev, next) {
      //เช็คว่า fingerprint มีหรือไม่
      if (!next.asData!.value.isFingerPrintAvailable) {
        _handlingFingerprintCheck(
          context: context,
          ref: ref,
          isCheck: false,
          failedReason: translation.biometricsPageFingerPrintNotFound,
        );
      }

      //ถ้ามี เช็คต่อว่าพอกดเริ่มแล้ว fingerprint สแกนสำเร็จหรือไม่
      if (next.asData!.value.isFingerprintScanSuccess) {
        _handlingFingerprintCheck(
          context: context,
          ref: ref,
          isCheck: true,
        );
      } else if (!next.asData!.value.isFingerprintScanSuccess &&
          prev!.asData!.value.isFingerPrintAvailable && !next.isLoading && !prev.isLoading) {
        _handlingFingerprintCheck(
          context: context,
          ref: ref,
          isCheck: false,
          failedReason: translation.biometricsPageFingerPrintNotFound,
        );
      }
    });

    return TestPageWidgetLayout(
      nextPage: getBiometricsPageNumber,
      processNumber: "${translation.layoutStep} $getBiometricsPageNumber",
      pictureAssetDirectory: CommonConst.CHECKLIST_PROPERTIES
          .firstWhere((element) =>
              element.appRoutesPage == AppRoutes.biometricsCheckPage)
          .icon,
      title: translation.biometricsPageTitle,
      cardBodyText: translation.biometricsPageDescription,
      underCard: AsyncValueWidget(
        value: fingerPrintCheckState,
        data: (fingerprintCheck) {
          if (fingerprintCheck.isFingerPrintAvailable &&
              fingerprintCheck.isFingerprintScanSuccess) {
            return Center(child: TestSuccessStatusIcon(theme: theme));
          } else if (!fingerprintCheck.isFingerPrintAvailable &&
              !fingerprintCheck.isFingerprintScanSuccess) {
            return Center(child: TestFailedStatusIcon(theme: theme));
          }

          return SharedElevatedButton(
            onPressed: () async {
              await ref
                  .read(fingerprintCheckStateProvider.notifier)
                  .startScanning();
            },
            text: translation.biometricsPageOpenScanningPage,
          );
        },
      ),
      onSkip: () async {
        await biometricsCheckManager.saveBiometricsCheck(false);
      },
    );
  }

  void _handlingFingerprintCheck({
    required BuildContext context,
    required WidgetRef ref,
    required bool isCheck,
    String? failedReason,
  }) async {
    final isExecuted = ref.watch(resultIsExecutedStateProvider);
    final biometricsCheckManager =
        ref.watch(biometricsCheckManagerProvider.notifier);
    final resultHandler = TestCheckHandler(
      context: context,
      ref: ref,
      isCheck: isCheck,
      appRoutes: AppRoutes.biometricsCheckPage,
      stateProvider: fingerprintCheckStateProvider,
      isExecuted: isExecuted,
    );

    if (isExecuted) return;
    Future.delayed(
            const Duration(milliseconds: AppConfig.futureSuccessAwaitDuration))
        .then(
      (_) async {
        await biometricsCheckManager.saveBiometricsCheck(isCheck);

        log("Fingerprint Check: ${await biometricsCheckManager.getBiometricsCheck()}");

        if (context.mounted) {
          await resultHandler.handlingStatus(
            failedReason: failedReason,
            onSkip: () async {
              await biometricsCheckManager.saveBiometricsCheck(false);
            },
          );
        }
      },
    );
  }
}
