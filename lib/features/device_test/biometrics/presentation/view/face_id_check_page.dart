// import 'package:app_settings/app_settings.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:techswop_lite/configs/app_configs.dart';
// import 'package:techswop_lite/features/device_test/biometrics/domain/service/face_id_service.dart';
// import 'package:techswop_lite/features/device_test/biometrics/presentation/state/face_id_state.dart';
// import 'package:techswop_lite/route/route.dart';
// import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
// import 'package:techswop_lite/shared/shared.dart';
// import 'package:techswop_lite/const/const.dart';
// import 'package:techswop_lite/shared/utils/test_check_handler/test_check_handler.dart';

// class FaceIdCheckPage extends ConsumerWidget {
//   const FaceIdCheckPage({super.key});

//   @override
//   Widget build(BuildContext context, WidgetRef ref) {
//     final translation = context.localization;
//     final getCurrentPageNumber = ref.watch(
//       getCurrentPageNumberProvider(AppRoutes.faceIdCheckPage.path));
//     final faceIdCheckState = ref.watch(faceIdCheckStateProvider);

//     return TestPageWidgetLayout(
//       processNumber:
//           "${translation.layoutStep} $getCurrentPageNumber",
//       title: translation.faceIdPageTitle,
//       nextPage: AppRoutes.batteryCheckPage,
//       pushNamedorGonamed: PushNamedOrGoNamed.go,
//       icondata: FontAwesomeIcons.faceViewfinder,
//       body: Text(
//         "${translation.faceIdPageDescriptionWeAreTestingiOS} \n\n ${translation.faceIdPagePleaseScan}",
//         textAlign: TextAlign.center,
//       ),
//       bodyUnderIcon: AsyncValueWidget(
//         value: faceIdCheckState,
//         data: (faceIdCheck) {
//           if (faceIdCheck.isFaceIdScannedSuccess) {
//             _handlingFaceIdCheck(
//               context: context,
//               ref: ref,
//               isCheck: true,
//             );
//           }

//           return FilledButton(
//             onPressed: () async {
//               final hasFaceID = await ref
//                   .read(faceIdCheckStateProvider.notifier)
//                   .checkIfFaceIdAvailable();

//               if ((hasFaceID == false && context.mounted) ||
//                   (hasFaceID == null && context.mounted)) {
//                 _handlingFaceIdCheck(
//                   context: context,
//                   ref: ref,
//                   isCheck: false,
//                   failedReason: translation.faceIdPageFaceIdNotFound,);
//               }

//               await ref
//                   .watch(faceIdCheckStateProvider.notifier)
//                   .startScanning();
//             },
//             child: Text(translation.faceIdPageOpenScanningPage),
//           );
//         },
//       ),
//     );
//   }

//   void _handlingFaceIdCheck({
//     required BuildContext context,
//     required WidgetRef ref, 
//     required bool isCheck, 
//     String? failedReason,
//   }) async {
//     final faceIdService = ref.watch(faceIdServiceProvider);

//     Future.delayed(Duration(milliseconds: AppConfig.futureSuccessAwaitDuration))
//         .then(
//       (value) async {
//         await faceIdService.saveFaceIdCheck(isCheck);

//         log("Face ID Check: ${await faceIdService.getFaceIdCheck()}");

//         if (context.mounted) {
//           await TestCheckHandler.handlingStatus(
//             appRoutes: AppRoutes.faceIdCheckPage,
//             context: context,
//             ref: ref,
//             stateProvider: faceIdCheckStateProvider,
//             isCheck: isCheck,
//             failedReason: failedReason,
//           );
//         }

//         // if (isCheck && context.mounted) {
//         //   await TestDialogService.testCompletePage(
//         //       context, AppRoutes.batteryCheckPage);
//         // } else if (!isCheck && context.mounted) {
//         //   await showDialog(
//         //     context: context,
//         //     builder: (context) {
//         //       return AlertDialog(
//         //         title: Text(
//         //           translation.faceIdPageFaceIdNotFound,
//         //           style: Theme.of(context).textTheme.titleLarge,
//         //         ),
//         //         content: Text(
//         //           translation.faceIdPageFaceIdNotFoundDescription,
//         //           textAlign: TextAlign.justify,
//         //         ),
//         //         actions: [
//         //           TextButton(
//         //             onPressed: () => context.pop(),
//         //             child: Text(
//         //                 translation.faceIdPageFaceIfinishedSettingFaceId,
//         //                 style: Theme.of(context).textTheme.bodyMedium!
//         //                 // .copyWith(color: Colors.blueGrey),
//         //                 ),
//         //           ),
//         //           TextButton(
//         //             onPressed: () => AppSettings.openAppSettings(),
//         //             child: Text(
//         //               translation.confirmTxt,
//         //               style: Theme.of(context)
//         //                   .textTheme
//         //                   .bodyMedium!
//         //                   .copyWith(color: Colors.blueGrey),
//         //             ),
//         //           ),
//         //         ],
//         //       );
//         //     },
//         //   );
//         // }
//       },
//     );
//   }
// }
