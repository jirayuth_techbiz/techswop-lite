// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fingerprint_check_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fingerprintCheckStateHash() =>
    r'bec206faf04423ac74627c9bbede28a54cedc612';

/// See also [FingerprintCheckState].
@ProviderFor(FingerprintCheckState)
final fingerprintCheckStateProvider = AutoDisposeAsyncNotifierProvider<
    FingerprintCheckState, FingerprintCheck>.internal(
  FingerprintCheckState.new,
  name: r'fingerprintCheckStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$fingerprintCheckStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FingerprintCheckState = AutoDisposeAsyncNotifier<FingerprintCheck>;
String _$biometricsCheckManagerHash() =>
    r'68beb9fa3d0e16be1511c851de4b59fb85b8697c';

/// See also [BiometricsCheckManager].
@ProviderFor(BiometricsCheckManager)
final biometricsCheckManagerProvider =
    AutoDisposeAsyncNotifierProvider<BiometricsCheckManager, void>.internal(
  BiometricsCheckManager.new,
  name: r'biometricsCheckManagerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$biometricsCheckManagerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$BiometricsCheckManager = AutoDisposeAsyncNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
