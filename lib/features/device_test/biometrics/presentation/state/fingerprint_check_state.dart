import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/fingerprint_check.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/service/fingerprint_check_service.dart';

part 'fingerprint_check_state.g.dart';

@riverpod
class FingerprintCheckState extends _$FingerprintCheckState {
  @override
  FutureOr<FingerprintCheck> build() async {
    final fingerprintCheckService = ref.watch(fingerprintCheckServiceProvider);
    final result = await fingerprintCheckService.isFingerPrintAvailable();
    return FingerprintCheck(
      isFingerPrintAvailable: result.isFingerPrintAvailable,
    );
  }

  Future<void> startScanning() async {
    final fingerprintCheckService = ref.watch(fingerprintCheckServiceProvider);
    final result = await fingerprintCheckService.isFingerPrintScanSuccess();
    state = state.whenData((data) => data.copyWith(
      isFingerprintScanSuccess: result.isFingerprintScanSuccess,
    ));
  }
}

@riverpod
class BiometricsCheckManager extends _$BiometricsCheckManager {
  @override 
  FutureOr<void> build() async {}

  Future<bool?> getBiometricsCheck() async {
    final fingerprintCheckService = ref.watch(fingerprintCheckServiceProvider);
    return await fingerprintCheckService.getFingerPrintCheck();
  }

  Future<void> saveBiometricsCheck(bool status) async {
    final fingerprintCheckService = ref.watch(fingerprintCheckServiceProvider);
    state = const AsyncLoading();
    state = await AsyncValue.guard(() => fingerprintCheckService.saveFingerPrintCheck(status));
  }
}
