// // ignore: depend_on_referenced_packages
// import 'package:riverpod_annotation/riverpod_annotation.dart';
// import 'package:techswop_lite/features/device_test/biometrics/domain/face_id_check.dart';
// import 'package:techswop_lite/features/device_test/biometrics/domain/service/face_id_service.dart';

// part 'face_id_state.g.dart';

// @riverpod
// class FaceIdCheckState extends _$FaceIdCheckState {
//   @override
//   FutureOr<FaceIdCheck> build() async {
//     return const FaceIdCheck();
//   }

//   checkIfFaceIdAvailable() async {
//     final faceIdCheckService = ref.watch(faceIdServiceProvider);
//     final result = await faceIdCheckService.isFaceIdAvailable();
//     state = state.whenData((data) => data.copyWith(
//       isFaceIdAvailable: result.isFaceIdAvailable,
//     ));
//   }

//   startScanning() async {
//     final faceIdCheckService = ref.watch(faceIdServiceProvider);
//     final result = await faceIdCheckService.isFaceIdCheck();
//     state = state.whenData((data) => data.copyWith(
//       isFaceIdScannedSuccess: result.isFaceIdScannedSuccess,
//     ));
//   }
// }

