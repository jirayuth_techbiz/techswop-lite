import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart' hide ServiceStatus;
import 'package:rxdart/rxdart.dart';
import 'package:techswop_lite/features/device_test/bluetooth/domain/repository/bluetooth_check_repository.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/repository/face_id_repository.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/repository/fingerprint_check_repository.dart';
import 'package:techswop_lite/features/device_test/geo_location/domain/repository/geo_location_repository.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/repositories/wifi_check_repository.dart';
import 'package:techswop_lite/features/preparation/domain/permission_status.dart';
import 'package:techswop_lite/features/preparation/domain/repository/permission_status_repository.dart';
import 'package:techswop_lite/features/preparation/domain/services/permission_status_service.dart';
import 'package:techswop_lite/shared/shared.dart';

class PermissionStatusServiceImpl implements PermissionStatusService {
  final Ref ref;
  final PermisisonStatusRepository permissionStatusRepository;
  final WifiCheckRepository wifiCheckRepository;
  final BluetoothCheckRepository bluetoothCheckRepository;
  final GeolocationCheckRepository geoLocationCheckRepository;
  final FingerprintCheckRepository biometricsCheckRepository;
  final FaceIdRepository faceIdRepository;
  final DeviceInfoRepository deviceInfoRepository;
  final LoggerRepository logger;

  PermissionStatusServiceImpl({
    required this.ref,
    required this.wifiCheckRepository,
    required this.bluetoothCheckRepository,
    required this.geoLocationCheckRepository,
    required this.permissionStatusRepository,
    required this.biometricsCheckRepository,
    required this.faceIdRepository,
    required this.deviceInfoRepository,
    required this.logger,
  });

  @override
  Stream<PermissionToCheckStatus> updatePermissionStatus() async* {
    BehaviorSubject<PermissionToCheckStatus> permissionStatusController =
        BehaviorSubject<PermissionToCheckStatus>();
    var permissiontoCheck = const PermissionToCheckStatus();
    StreamSubscription? isWifiEnableResult;
    StreamSubscription? isGeolocationEnableResult;
    StreamSubscription? isBluetoothAdapterStateOn;
    Timer checkPermissionTimer;

    try {

      isWifiEnableResult = wifiCheckRepository
          .connectivityResultStreamController()
          .listen((event) {
        logger.logInfo('wifi status = $event');
        permissiontoCheck = permissiontoCheck.copyWith(
            isWifiEnabled: event == ConnectivityResult.wifi);
        permissionStatusController.sink.add(permissiontoCheck);
      });

      isGeolocationEnableResult =
          geoLocationCheckRepository.isGPSEnabled().listen((event) {
        ///Use index because enum operator issue
        permissiontoCheck = permissiontoCheck.copyWith(
          isLocationServiceStatusEnabled:
              event.index == ServiceStatus.enabled.index,
        );
        permissionStatusController.sink.add(permissiontoCheck);
      });

      isBluetoothAdapterStateOn =
          bluetoothCheckRepository.isBluetoothEnabled().listen((event) {
        permissiontoCheck = permissiontoCheck.copyWith(
            isBluetoothAccessGranted: event == BluetoothAdapterState.on);
        permissionStatusController.sink.add(permissiontoCheck);
      });

      checkPermissionTimer =
          Timer.periodic(const Duration(seconds: 1), (timer) async {
        bool? hasFingerprint =
          await biometricsCheckRepository.isFingerprintAvailable();
        bool? hasFaceId = await faceIdRepository.isFaceIdAvailable();
        Permission storagePermission = await _getStoragePermission();
      var permissionRes =
            await permissionStatusRepository.getPermissionStatus(storagePermission: storagePermission);

        permissiontoCheck = permissiontoCheck.copyWith(
          isLocationAccessGranted:
              _isPermissionGranted(permissionRes, Permission.location) ||
                  (_isPermissionGranted(permissionRes, Permission.location) &&
                      _isPermissionGranted(
                          permissionRes, Permission.locationWhenInUse)),
          isMotionEnabled: _isPermissionGranted(
              permissionRes, Permission.activityRecognition),
          isBodySensorEnabled: _isPermissionGranted(
              permissionRes, Permission.sensors),
          isEnrolledBiometric: hasFingerprint || hasFaceId,
          isMicrophoneAccessGranted:
              _isPermissionGranted(permissionRes, Permission.microphone),
          isCameraAccessGranted:
              _isPermissionGranted(permissionRes, Permission.camera),
          isStorageAccessGranted:
              _isPermissionGranted(permissionRes, storagePermission),
        );
        permissionStatusController.sink.add(permissiontoCheck);
      });

      ref.onDispose(() async {
        await isWifiEnableResult?.cancel();
        await isGeolocationEnableResult?.cancel();
        await isBluetoothAdapterStateOn?.cancel();
        checkPermissionTimer.cancel();
        log('permission status service disposed');
      });

      yield* permissionStatusController.stream;
    } catch (e) {
      debugPrint(e.toString());
      rethrow;
    }
  }

  ///แต่ละ Platform และ OS Version จะใช้ Permission ขอเข้าถึง Storage ไม่เหมือนกัน
  ///จำเป็นต้องเช็ตและแยกแต่ละแพลตฟอร์มว่าจะใช้ Permission อะไร
  Future<Permission> _getStoragePermission() async {
    bool isAndroid = defaultTargetPlatform == TargetPlatform.android;
    bool isAPIBelow32 = await deviceInfoRepository
        .getDeviceInfo()
        .then((value) => value!.version <= 32);
    Permission storagePermission;
    if (isAndroid) {
      if (isAPIBelow32) {
        storagePermission = Permission.storage;
      } else {
        storagePermission = Permission.photos;
      }
    } else {
      storagePermission = Permission.photosAddOnly;
    }
    return storagePermission;
  }

  ///เช็คว่า Permission นั้นๆ ได้รับการอนุญาตหรือไม่
  bool _isPermissionGranted(
      Map<Permission, PermissionStatus> permissionRes, Permission permission) {
    return permissionRes[permission] != null &&
        permissionRes[permission]!.isGranted;
  }
}
