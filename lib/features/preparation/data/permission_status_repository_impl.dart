import 'package:permission_handler/permission_handler.dart';
import 'package:permission_handler/permission_handler.dart' as permissionhandler show openAppSettings;
import 'package:techswop_lite/features/preparation/domain/repository/permission_status_repository.dart';
import 'package:techswop_lite/shared/shared.dart';

class PermissionStatusRepositoryImpl
    implements PermisisonStatusRepository {
  final DeviceInfoRepository deviceInfoRepository;

  PermissionStatusRepositoryImpl(this.deviceInfoRepository);

  @override
  Future<Map<Permission, PermissionStatus>> getPermissionStatus({required Permission storagePermission}) async {
    final deviceInfo = await deviceInfoRepository.getDeviceInfo();
    Map<Permission, PermissionStatus> status = {};
    if (defaultTargetPlatform == TargetPlatform.android) {
      if (deviceInfo!.version >= 29 && deviceInfo.version <= 32) {
        status = await [
          Permission.activityRecognition,
          Permission.sensors,
          Permission.bluetoothScan,
          Permission.bluetoothAdvertise,
          Permission.location,
          Permission.locationWhenInUse,
          Permission.microphone,
          Permission.camera,
          storagePermission,
        ].request();
      } else if (deviceInfo.version < 29) {
        status = await [
          Permission.sensors,
          Permission.bluetooth,
          Permission.location,
          Permission.locationWhenInUse,
          Permission.microphone,
          Permission.camera,
          storagePermission,
        ].request();
      } else if (deviceInfo.version >= 33) {
        status = await [
          Permission.activityRecognition,
          Permission.sensors,
          Permission.bluetoothScan,
          Permission.bluetoothAdvertise,
          Permission.location,
          Permission.locationWhenInUse,
          Permission.microphone,
          Permission.camera,
          storagePermission,
        ].request();
      }
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      status = await [
        Permission.bluetooth,
        Permission.location,
        Permission.microphone,
        Permission.camera,
        storagePermission,
      ].request();
    }
    return status;
  }

  Future<PermissionStatus> status(Permission permission) async {
    return await permission.status;
  }

  Future<bool> openAppSettings() async {
    return permissionhandler.openAppSettings();
  }

  Future<PermissionStatus> request(Permission permission) async {
    return await permission.request();
  }
  
  @override
  Future<bool> isPermissionGranted(Permission permission) async {
    return await permission.isGranted;
  }

}
