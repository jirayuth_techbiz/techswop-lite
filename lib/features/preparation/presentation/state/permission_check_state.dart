import 'dart:async';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/preparation/domain/permission_status.dart';
import 'package:techswop_lite/features/preparation/domain/services/permission_status_service.dart';
import 'package:techswop_lite/shared/common_widgets/icons/check_widget.dart';
import 'package:techswop_lite/shared/common_widgets/icons/xmark_widget.dart';
import 'package:rxdart/rxdart.dart';

part 'permission_check_state.g.dart';

@riverpod
class PermissionCheckState extends _$PermissionCheckState {

  PermissionCheckState();

  @override
  Stream<PermissionToCheckStatus> build() async* {
    final permissionService = ref.watch(permissionStatusServiceProvider);
    BehaviorSubject<PermissionToCheckStatus> permissionStatusController =
        BehaviorSubject<PermissionToCheckStatus>();
    var permissionToCheckStatus = const PermissionToCheckStatus();
    permissionService.updatePermissionStatus().listen(
      (event) {
        permissionToCheckStatus = PermissionToCheckStatus(
          isWifiEnabled: event.isWifiEnabled,
          isMotionEnabled: event.isMotionEnabled,
          isBodySensorEnabled: event.isBodySensorEnabled,
          isBluetoothAccessGranted: event.isBluetoothAccessGranted,
          isLocationAccessGranted: event.isLocationAccessGranted,
          isLocationServiceStatusEnabled: event.isLocationServiceStatusEnabled,
          isBatteryCharging: event.isBatteryCharging,
          isEnrolledBiometric: event.isEnrolledBiometric,
          isMicrophoneAccessGranted: event.isMicrophoneAccessGranted,
          isCameraAccessGranted: event.isCameraAccessGranted,
          isStorageAccessGranted: event.isStorageAccessGranted,
        );
        permissionStatusController.sink.add(permissionToCheckStatus);
      },
    );

    yield* permissionStatusController.stream;
  }

  void checkWifiConnectivity() {
    final wifiStatus = state.value!.isWifiEnabled;
    if (!wifiStatus) {
      if (defaultTargetPlatform == TargetPlatform.android) {
        try {
          AppSettings.openAppSettingsPanel(AppSettingsPanelType.wifi);
        } on Exception {
          AppSettings.openAppSettings(type: AppSettingsType.wifi);
        }
      } else if (defaultTargetPlatform == TargetPlatform.iOS) {
        AppSettings.openAppSettings();
      } else {
        return;
      }
    }
  }

  void checkBlueToothConnectivity() async {
    final bluetoothStatus = state.value!.isBluetoothAccessGranted;
    if (!bluetoothStatus) {
      if (defaultTargetPlatform == TargetPlatform.android) {
        try {
          AppSettings.openAppSettings(type: AppSettingsType.bluetooth);
        } on Exception {
          AppSettings.openAppSettings();
        }
      } else if (defaultTargetPlatform == TargetPlatform.iOS) {
        AppSettings.openAppSettings();
      } else {
        return;
      }
    }
  }

  void checkLocationConnectivity() {
    final locationStatus = state.value!.isLocationAccessGranted;
    final locationServiceStatus = state.value!.isLocationServiceStatusEnabled;
    if (!locationStatus || !locationServiceStatus) {
      if (defaultTargetPlatform == TargetPlatform.android) {
        AppSettings.openAppSettings(type: AppSettingsType.location);
      } else if (defaultTargetPlatform == TargetPlatform.iOS) {
        AppSettings.openAppSettings();
      } else {
        return;
      }
    }
  }

  bool checkBatteryConnectivity() {
    final batteryStatus = state.value!.isBatteryCharging;
    return batteryStatus;
  }

  void checkBiometricsEnrollment() {
    final biometricStatus = state.value!.isEnrolledBiometric;
    if (!biometricStatus) {
      if (defaultTargetPlatform == TargetPlatform.android) {
        try {
          AppSettings.openAppSettings(type: AppSettingsType.biometrics);
        } on Exception {
          AppSettings.openAppSettings(type: AppSettingsType.security);
        }
      } else if (defaultTargetPlatform == TargetPlatform.iOS) {
        AppSettings.openAppSettings();
      } else {
        return;
      }
    }
  }

  void checkMicrophoneConnectivity() {
    final microphoneStatus = state.value!.isMicrophoneAccessGranted;
    if (!microphoneStatus) {
      AppSettings.openAppSettings(type: AppSettingsType.settings);
    }
  }

  Future<void> getSettingPages(
    PermissionToCheckStatusEnum permissionToCheckStatusEnum,
    PermissionToCheckStatus permissionState,
  ) async {
    switch (permissionToCheckStatusEnum) {
      case PermissionToCheckStatusEnum.isWifiEnabled:
        if (permissionState.isWifiEnabled == false) {
          checkWifiConnectivity();
        }
      case PermissionToCheckStatusEnum.isBluetoothAccessGranted:
        if (permissionState.isBluetoothAccessGranted == false) {
          checkBlueToothConnectivity();
        }
      case PermissionToCheckStatusEnum.isLocationAccessGranted:
      case PermissionToCheckStatusEnum.isLocationServiceStatusEnabled:
        if (permissionState.isLocationServiceStatusEnabled == false) {
          checkLocationConnectivity();
        } else if (permissionState.isLocationAccessGranted == false) {
          await openAppSettings();
        }
      case PermissionToCheckStatusEnum.isMicrophoneAccessGranted:
        if (permissionState.isMicrophoneAccessGranted == false) {
          checkMicrophoneConnectivity();
        }
      case PermissionToCheckStatusEnum.isCameraAccessGranted:
        if (permissionState.isCameraAccessGranted == false) {
          await openAppSettings();
        }
      case PermissionToCheckStatusEnum.isBodySensorEnabled:
      case PermissionToCheckStatusEnum.isMotionEnabled:
        if (permissionState.isMotionEnabled == false ||
            permissionState.isBodySensorEnabled == false) {
          await openAppSettings();
        }
      case PermissionToCheckStatusEnum.isBatteryCharging:
        if (permissionState.isBatteryCharging == false) {
          checkBatteryConnectivity();
        }
      case PermissionToCheckStatusEnum.isEnrolledBiometric:
        if (permissionState.isEnrolledBiometric == false) {
          checkBiometricsEnrollment();
        }
      case PermissionToCheckStatusEnum.isStorageAccessEnabled:
        if (permissionState.isStorageAccessGranted == false) {
          await openAppSettings();
        }
      default:
        await openAppSettings();
    }
  }

  Widget getIconBasedOnPermission(
    PermissionToCheckStatusEnum permissionToCheckStatusEnum,
    PermissionToCheckStatus permissionState,
  ) {
    switch (permissionToCheckStatusEnum) {
      case PermissionToCheckStatusEnum.isWifiEnabled:
        return permissionState.isWifiEnabled
            ? const CheckIconWidget()
            : const XMarkIconWidget();
      case PermissionToCheckStatusEnum.isBluetoothAccessGranted:
        return permissionState.isBluetoothAccessGranted
            ? const CheckIconWidget()
            : const XMarkIconWidget();
      case PermissionToCheckStatusEnum.isLocationAccessGranted:
      case PermissionToCheckStatusEnum.isLocationServiceStatusEnabled:
        return (permissionState.isLocationServiceStatusEnabled &&
                permissionState.isLocationAccessGranted)
            ? const CheckIconWidget()
            : const XMarkIconWidget();
      case PermissionToCheckStatusEnum.isMicrophoneAccessGranted:
        return permissionState.isMicrophoneAccessGranted
            ? const CheckIconWidget()
            : const XMarkIconWidget();
      case PermissionToCheckStatusEnum.isCameraAccessGranted:
        return permissionState.isCameraAccessGranted
            ? const CheckIconWidget()
            : const XMarkIconWidget();
      case PermissionToCheckStatusEnum.isBodySensorEnabled:
      case PermissionToCheckStatusEnum.isMotionEnabled:
        return (permissionState.isMotionEnabled &&
                permissionState.isBodySensorEnabled)
            ? const CheckIconWidget()
            : const XMarkIconWidget();
      case PermissionToCheckStatusEnum.isBatteryCharging:
        return permissionState.isBatteryCharging
            ? const CheckIconWidget()
            : const XMarkIconWidget();
      case PermissionToCheckStatusEnum.isEnrolledBiometric:
        return permissionState.isEnrolledBiometric
            ? const CheckIconWidget()
            : const XMarkIconWidget();
      case PermissionToCheckStatusEnum.isStorageAccessEnabled:
        return permissionState.isStorageAccessGranted
            ? const CheckIconWidget()
            : const XMarkIconWidget();
      default:
        return const XMarkIconWidget();
    }
  }
}

enum PermissionToCheckStatusEnum {
  isWifiEnabled,
  isMotionEnabled,
  isBodySensorEnabled,
  isBluetoothAccessGranted,
  isLocationAccessGranted,
  isLocationServiceStatusEnabled,
  isBatteryCharging,
  isEnrolledBiometric,
  isMicrophoneAccessGranted,
  isCameraAccessGranted,
  isStorageAccessEnabled,
}
