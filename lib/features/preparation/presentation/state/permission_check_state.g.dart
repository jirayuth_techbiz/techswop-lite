// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'permission_check_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$permissionCheckStateHash() =>
    r'fe087a38182846bafdbb2356782aa6aede59ed36';

/// See also [PermissionCheckState].
@ProviderFor(PermissionCheckState)
final permissionCheckStateProvider = AutoDisposeStreamNotifierProvider<
    PermissionCheckState, PermissionToCheckStatus>.internal(
  PermissionCheckState.new,
  name: r'permissionCheckStateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$permissionCheckStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$PermissionCheckState
    = AutoDisposeStreamNotifier<PermissionToCheckStatus>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
