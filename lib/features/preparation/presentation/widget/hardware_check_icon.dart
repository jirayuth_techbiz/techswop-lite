import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/features/preparation/domain/checklist_properties.dart';
import 'package:techswop_lite/features/preparation/presentation/state/permission_check_state.dart';
import 'package:techswop_lite/features/preparation/presentation/widget/vertical_divider_checklist.dart';
import 'package:techswop_lite/generated/assets.gen.dart';
import 'package:techswop_lite/shared/shared.dart';

class HardwareBasedWidget extends ConsumerWidget {
  const HardwareBasedWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final translation = context.localization;
    final permissionStatusState = ref.watch(permissionCheckStateProvider);
    final _scrollController =  ScrollController();

    final List<CheckListPermissionProperties> checkList = [
      CheckListPermissionProperties(
        icon: Assets.icon.techBizCustom.wifi.path,
        textUpper: translation.hardwareBasedWifi,
        textLower: translation.hardwareBasedWifiDescription,
        state: permissionStatusState,
        permissionStatusEnum: PermissionToCheckStatusEnum.isWifiEnabled,
      ),
      CheckListPermissionProperties(
        icon: Assets.icon.techBizCustom.bluetooth.path,
        textUpper: translation.hardwareBasedBluetooth,
        textLower: translation.hardwareBasedBluetoothDescription,
        permissionStatusEnum:
            PermissionToCheckStatusEnum.isBluetoothAccessGranted,
        state: permissionStatusState,
      ),
      CheckListPermissionProperties(
        icon: Assets.icon.techBizCustom.location.path,
        textUpper: translation.hardwareBasedGPS,
        textLower: translation.hardwareBasedGPSDescription,
        state: permissionStatusState,
        permissionStatusEnum:
            PermissionToCheckStatusEnum.isLocationServiceStatusEnabled,
      ),
      CheckListPermissionProperties(
        icon: Assets.icon.techBizCustom.camera.path,
        textUpper: translation.permissionBasedCamera,
        textLower: translation.permissionBasedCameraDescription,
        state: permissionStatusState,
        permissionStatusEnum: PermissionToCheckStatusEnum.isCameraAccessGranted,
      ),
      CheckListPermissionProperties(
        icon: Assets.icon.techBizCustom.microphone.path,
        textUpper: translation.permissionBasedMicrophone,
        textLower: translation.permissionBasedMicrophoneDescription,
        state: permissionStatusState,
        permissionStatusEnum:
            PermissionToCheckStatusEnum.isMicrophoneAccessGranted,
      ),
      CheckListPermissionProperties(
        icon: Assets.icon.techBizCustom.bio.path,
        textUpper: translation.hardwareBasedBiometrics,
        textLower: translation.hardwareBasedBiometricsDescription,
        state: permissionStatusState,
        permissionStatusEnum: PermissionToCheckStatusEnum.isEnrolledBiometric,
      ),
      CheckListPermissionProperties(
        icon: Assets.icon.techBizCustom.motion.path,
        textUpper: translation.hardwareBasedMotion,
        textLower: translation.hardwareBasedMotionDescription,
        state: permissionStatusState,
        permissionStatusEnum: PermissionToCheckStatusEnum.isMotionEnabled,
      ),
      CheckListPermissionProperties(
        icon: Assets.icon.techBizCustom.storage.path,
        textUpper: translation.permissionBasedStorage,
        textLower: translation.permissionBasedStorageDescription,
        state: permissionStatusState,
        permissionStatusEnum: PermissionToCheckStatusEnum.isStorageAccessEnabled,
      ),
    ];

    return ListView.separated(
        padding: EdgeInsets.only(bottom:100.h),
        scrollDirection: Axis.vertical,
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        controller: _scrollController,
        itemBuilder: (context, index) {
          return BasicCheckIcon(checkListProperties: checkList[index]);
        },
        separatorBuilder: (context, index) {
          return const SizedBox(child: DividerCheckList());
        },
        itemCount: checkList.length);
  }
}
