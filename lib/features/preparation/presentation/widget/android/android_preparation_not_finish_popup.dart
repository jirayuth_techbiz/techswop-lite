import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/route/state/route_state.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/shared.dart';

class AndroidPreparationNotFinishWarningPopup extends ConsumerWidget {
  const AndroidPreparationNotFinishWarningPopup({super.key, required this.route});
  final List<GoRoute> route;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final translation = context.localization;
    final theme = context.theme;
    
    return AlertDialog(
      title: Text(translation.preparationPopupWarning),
      content: RichText(
        text: TextSpan(
          children: [
            TextSpan(
                text:
                    "\n${translation.preparationPopupWarningAccessInComplete}",
                style: theme.textTheme.bodyMedium),
            TextSpan(
              text: translation.preparationPopupAreYouSureYouWantToContinues,
              style: theme.textTheme.bodyMedium,
            ),
            TextSpan(
                text: translation.preparationPopupIfYesPressContinues,
                style: theme.textTheme.bodyMedium),
            TextSpan(
                text: translation.preparationPopupOrYouWantToDoItManually,
                style: theme.textTheme.bodyMedium),
          ],
        ),
      ),
      actions: [
        TextButton(
          onPressed: () => openAppSettings(),
          child: Text(
            translation.preparationPopupGrantPermission,
            style: theme.textTheme.bodyMedium,
          ),
        ),
        TextButton(
          onPressed: () => context.pop(),
          child: Text(
            translation.returnTxt,
            style: theme.textTheme.bodyMedium,
          ),
        ),
        TextButton(
            onPressed: () => context.goNamed(route.first.path),
            child: Text(
              translation.confirmTxt,
              style: theme.textTheme.bodyMedium,
            ),
          ),
      ],
    );
  }
}
