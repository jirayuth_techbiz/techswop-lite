import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class DividerCheckList extends ConsumerWidget {
  const DividerCheckList({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return const Padding(
      padding: EdgeInsets.symmetric(vertical: 8),
      child: Opacity(
        opacity: .2,
        child: Divider(
          height: 2,
          color: Colors.black,
          thickness: .4,
        ),
      ),
    );
  }
}