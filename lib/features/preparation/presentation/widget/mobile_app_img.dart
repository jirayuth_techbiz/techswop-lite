import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/shared/shared.dart';

class MobileAppImage extends ConsumerWidget {
  final String img;
  const MobileAppImage({super.key, required this.img});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Image.asset(
      img,
      width: 200.w,
      height: 200.h,
    );
  }
}
