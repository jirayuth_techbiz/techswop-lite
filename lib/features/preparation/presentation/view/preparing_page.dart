import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/const/enums/routing_enums.dart';
import 'package:techswop_lite/features/preparation/presentation/state/permission_check_state.dart';
import 'package:techswop_lite/features/preparation/presentation/widget/android/android_preparation_not_finish_popup.dart';
import 'package:techswop_lite/features/preparation/presentation/widget/hardware_check_icon.dart';
import 'package:techswop_lite/features/preparation/presentation/widget/mobile_app_img.dart';
import 'package:techswop_lite/generated/assets.gen.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/route/state/route_state.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/common_widgets/gradient_elevated_long_button.dart';
import 'package:techswop_lite/shared/shared.dart';

class PreparingPage extends ConsumerStatefulWidget {
  const PreparingPage({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _PreparingPageState();
}

class _PreparingPageState extends ConsumerState<PreparingPage> {
  @override
  Widget build(BuildContext context) {
    final translation = context.localization;
    final theme = context.theme;
    final permissionStatusState = ref.watch(permissionCheckStateProvider);
    final route = ref.watch(routeStateProvider);
    String onboardingImg = Assets.design.preparationTh.path;

    final bool isiOS;
    if (defaultTargetPlatform == TargetPlatform.iOS) {
      isiOS = true;
    } else {
      isiOS = false;
    }

    return SafeArea(
      top: false,
      child: Scaffold(
        floatingActionButtonLocation:
            FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Container(
          // decoration: BoxDecoration(
          //   color: theme.colorScheme.surface,
          // ),
          width: MediaQuery.of(context).size.width.w,
          padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewPadding.bottom),
          child: AsyncValueWidget(
            value: permissionStatusState,
            data: (permissionStatusstate) {
              bool shouldVisible = false;
              List<bool> areAllPermissionGranted = [
                permissionStatusstate.isBluetoothAccessGranted,
                permissionStatusstate.isCameraAccessGranted,
                permissionStatusstate.isMicrophoneAccessGranted,
                permissionStatusstate.isWifiEnabled,
                permissionStatusstate.isEnrolledBiometric,
                permissionStatusstate.isMotionEnabled,
                permissionStatusstate.isLocationAccessGranted,
                permissionStatusstate.isLocationServiceStatusEnabled,
              ];
      
              for (var permission in areAllPermissionGranted) {
                if (permission == false) {
                  shouldVisible = false;
                  break;
                }
                shouldVisible = true;
              }
              return Container(
                padding: const EdgeInsets.symmetric(vertical: 20),
                margin: const EdgeInsets.symmetric(horizontal: 20),
                child: AsyncValueWidget(
                  value: route,
                  data: (route) => SharedElevatedButton(
                      onPressed: shouldVisible
                          ? () {
                              context.goNamed(route.first.name!);
                            }
                          : () {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return AndroidPreparationNotFinishWarningPopup(route: route);
                                },
                              );
                            },
                      text: translation.preparationLetsBegin,
                    ),
                ),
                
              );
            },
          ),
        ),
        appBar: AppBar(
          backgroundColor: theme.colorScheme.primary,
          centerTitle: true,
          title: Padding(
            padding: EdgeInsets.only(left: 7.0.w),
            child: Text(
              translation.preparationBeforeCheckTips,
              textAlign: TextAlign.justify,
              style: theme.textTheme.titleMedium!.copyWith(
                color: theme.colorScheme.onPrimary,
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 20.w),
                child: MobileAppImage(img: onboardingImg),
              ),
              Container(
                constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width / 1.2,
                ),
                margin: EdgeInsets.all(20.w),
                alignment: Alignment.center,
                child: Text(
                  translation.preparationPleaseAllowPermission,
                  textAlign: TextAlign.center,
                  style: theme.textTheme.bodyMedium!.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.sp,
                  ),
                  overflow: TextOverflow.visible,
                ),
              ),
              Container(
                constraints: BoxConstraints(
                  maxWidth: ScreenUtil().screenWidth.w / 1.2,
                ),
                alignment: Alignment.center,
                child: Text(
                  "${translation.preparationOtherwiseItWillAffectTheResult} \n ${translation.preparationIncaseYouForgotPermission}",
                  textAlign: TextAlign.center,
                  style: theme.textTheme.bodyMedium!,
                  overflow: TextOverflow.visible,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20.w),
                child: const Column(
                  children: [
                    HardwareBasedWidget(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
