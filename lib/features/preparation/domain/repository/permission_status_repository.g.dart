// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'permission_status_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$permissionStatusRepositoryHash() =>
    r'e3d318a2b299291386bd94b2bbbfd7d7708e32e0';

/// See also [permissionStatusRepository].
@ProviderFor(permissionStatusRepository)
final permissionStatusRepositoryProvider =
    AutoDisposeProvider<PermisisonStatusRepository>.internal(
  permissionStatusRepository,
  name: r'permissionStatusRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$permissionStatusRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef PermissionStatusRepositoryRef
    = AutoDisposeProviderRef<PermisisonStatusRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
