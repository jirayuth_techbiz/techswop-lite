import 'package:permission_handler/permission_handler.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/preparation/data/permission_status_repository_impl.dart';
import 'package:techswop_lite/shared/domain/repository/device_info_repository.dart';

part 'permission_status_repository.g.dart';

@riverpod
PermisisonStatusRepository permissionStatusRepository(
    PermissionStatusRepositoryRef ref) {
  final deviceInfoRepository = ref.watch(deviceInfoRepositoryProvider);
  return PermissionStatusRepositoryImpl(deviceInfoRepository);
}

abstract class PermisisonStatusRepository {
  Future<Map<Permission, PermissionStatus>> getPermissionStatus(
      {required Permission storagePermission});
  Future<bool> isPermissionGranted(Permission permission);
}
