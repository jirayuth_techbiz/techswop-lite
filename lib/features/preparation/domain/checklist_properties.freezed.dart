// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'checklist_properties.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CheckListPermissionProperties {
  String get icon => throw _privateConstructorUsedError;
  String get textUpper => throw _privateConstructorUsedError;
  String get textLower => throw _privateConstructorUsedError;
  PermissionToCheckStatusEnum? get permissionStatusEnum =>
      throw _privateConstructorUsedError;
  AsyncValue<PermissionToCheckStatus>? get state =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CheckListPermissionPropertiesCopyWith<CheckListPermissionProperties>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CheckListPermissionPropertiesCopyWith<$Res> {
  factory $CheckListPermissionPropertiesCopyWith(
          CheckListPermissionProperties value,
          $Res Function(CheckListPermissionProperties) then) =
      _$CheckListPermissionPropertiesCopyWithImpl<$Res,
          CheckListPermissionProperties>;
  @useResult
  $Res call(
      {String icon,
      String textUpper,
      String textLower,
      PermissionToCheckStatusEnum? permissionStatusEnum,
      AsyncValue<PermissionToCheckStatus>? state});
}

/// @nodoc
class _$CheckListPermissionPropertiesCopyWithImpl<$Res,
        $Val extends CheckListPermissionProperties>
    implements $CheckListPermissionPropertiesCopyWith<$Res> {
  _$CheckListPermissionPropertiesCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? icon = null,
    Object? textUpper = null,
    Object? textLower = null,
    Object? permissionStatusEnum = freezed,
    Object? state = freezed,
  }) {
    return _then(_value.copyWith(
      icon: null == icon
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as String,
      textUpper: null == textUpper
          ? _value.textUpper
          : textUpper // ignore: cast_nullable_to_non_nullable
              as String,
      textLower: null == textLower
          ? _value.textLower
          : textLower // ignore: cast_nullable_to_non_nullable
              as String,
      permissionStatusEnum: freezed == permissionStatusEnum
          ? _value.permissionStatusEnum
          : permissionStatusEnum // ignore: cast_nullable_to_non_nullable
              as PermissionToCheckStatusEnum?,
      state: freezed == state
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as AsyncValue<PermissionToCheckStatus>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CheckListPermissionPropertiesImplCopyWith<$Res>
    implements $CheckListPermissionPropertiesCopyWith<$Res> {
  factory _$$CheckListPermissionPropertiesImplCopyWith(
          _$CheckListPermissionPropertiesImpl value,
          $Res Function(_$CheckListPermissionPropertiesImpl) then) =
      __$$CheckListPermissionPropertiesImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String icon,
      String textUpper,
      String textLower,
      PermissionToCheckStatusEnum? permissionStatusEnum,
      AsyncValue<PermissionToCheckStatus>? state});
}

/// @nodoc
class __$$CheckListPermissionPropertiesImplCopyWithImpl<$Res>
    extends _$CheckListPermissionPropertiesCopyWithImpl<$Res,
        _$CheckListPermissionPropertiesImpl>
    implements _$$CheckListPermissionPropertiesImplCopyWith<$Res> {
  __$$CheckListPermissionPropertiesImplCopyWithImpl(
      _$CheckListPermissionPropertiesImpl _value,
      $Res Function(_$CheckListPermissionPropertiesImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? icon = null,
    Object? textUpper = null,
    Object? textLower = null,
    Object? permissionStatusEnum = freezed,
    Object? state = freezed,
  }) {
    return _then(_$CheckListPermissionPropertiesImpl(
      icon: null == icon
          ? _value.icon
          : icon // ignore: cast_nullable_to_non_nullable
              as String,
      textUpper: null == textUpper
          ? _value.textUpper
          : textUpper // ignore: cast_nullable_to_non_nullable
              as String,
      textLower: null == textLower
          ? _value.textLower
          : textLower // ignore: cast_nullable_to_non_nullable
              as String,
      permissionStatusEnum: freezed == permissionStatusEnum
          ? _value.permissionStatusEnum
          : permissionStatusEnum // ignore: cast_nullable_to_non_nullable
              as PermissionToCheckStatusEnum?,
      state: freezed == state
          ? _value.state
          : state // ignore: cast_nullable_to_non_nullable
              as AsyncValue<PermissionToCheckStatus>?,
    ));
  }
}

/// @nodoc

class _$CheckListPermissionPropertiesImpl
    extends _CheckListPermissionProperties {
  const _$CheckListPermissionPropertiesImpl(
      {required this.icon,
      this.textUpper = "",
      this.textLower = "",
      this.permissionStatusEnum,
      this.state})
      : super._();

  @override
  final String icon;
  @override
  @JsonKey()
  final String textUpper;
  @override
  @JsonKey()
  final String textLower;
  @override
  final PermissionToCheckStatusEnum? permissionStatusEnum;
  @override
  final AsyncValue<PermissionToCheckStatus>? state;

  @override
  String toString() {
    return 'CheckListPermissionProperties(icon: $icon, textUpper: $textUpper, textLower: $textLower, permissionStatusEnum: $permissionStatusEnum, state: $state)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckListPermissionPropertiesImpl &&
            (identical(other.icon, icon) || other.icon == icon) &&
            (identical(other.textUpper, textUpper) ||
                other.textUpper == textUpper) &&
            (identical(other.textLower, textLower) ||
                other.textLower == textLower) &&
            (identical(other.permissionStatusEnum, permissionStatusEnum) ||
                other.permissionStatusEnum == permissionStatusEnum) &&
            (identical(other.state, state) || other.state == state));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, icon, textUpper, textLower, permissionStatusEnum, state);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckListPermissionPropertiesImplCopyWith<
          _$CheckListPermissionPropertiesImpl>
      get copyWith => __$$CheckListPermissionPropertiesImplCopyWithImpl<
          _$CheckListPermissionPropertiesImpl>(this, _$identity);
}

abstract class _CheckListPermissionProperties
    extends CheckListPermissionProperties {
  const factory _CheckListPermissionProperties(
          {required final String icon,
          final String textUpper,
          final String textLower,
          final PermissionToCheckStatusEnum? permissionStatusEnum,
          final AsyncValue<PermissionToCheckStatus>? state}) =
      _$CheckListPermissionPropertiesImpl;
  const _CheckListPermissionProperties._() : super._();

  @override
  String get icon;
  @override
  String get textUpper;
  @override
  String get textLower;
  @override
  PermissionToCheckStatusEnum? get permissionStatusEnum;
  @override
  AsyncValue<PermissionToCheckStatus>? get state;
  @override
  @JsonKey(ignore: true)
  _$$CheckListPermissionPropertiesImplCopyWith<
          _$CheckListPermissionPropertiesImpl>
      get copyWith => throw _privateConstructorUsedError;
}
