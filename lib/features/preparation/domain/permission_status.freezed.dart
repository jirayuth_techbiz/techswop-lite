// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'permission_status.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$PermissionToCheckStatus {
  bool get isWifiEnabled => throw _privateConstructorUsedError;
  bool get isBluetoothAccessGranted => throw _privateConstructorUsedError;
  bool get isLocationAccessGranted => throw _privateConstructorUsedError;
  bool get isLocationServiceStatusEnabled => throw _privateConstructorUsedError;
  bool get isCameraAccessGranted => throw _privateConstructorUsedError;
  bool get isMicrophoneAccessGranted => throw _privateConstructorUsedError;
  bool get isEnrolledBiometric => throw _privateConstructorUsedError;
  bool get isBatteryCharging => throw _privateConstructorUsedError;
  bool get isBodySensorEnabled => throw _privateConstructorUsedError;
  bool get isMotionEnabled => throw _privateConstructorUsedError;
  bool get isStorageAccessGranted => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PermissionToCheckStatusCopyWith<PermissionToCheckStatus> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PermissionToCheckStatusCopyWith<$Res> {
  factory $PermissionToCheckStatusCopyWith(PermissionToCheckStatus value,
          $Res Function(PermissionToCheckStatus) then) =
      _$PermissionToCheckStatusCopyWithImpl<$Res, PermissionToCheckStatus>;
  @useResult
  $Res call(
      {bool isWifiEnabled,
      bool isBluetoothAccessGranted,
      bool isLocationAccessGranted,
      bool isLocationServiceStatusEnabled,
      bool isCameraAccessGranted,
      bool isMicrophoneAccessGranted,
      bool isEnrolledBiometric,
      bool isBatteryCharging,
      bool isBodySensorEnabled,
      bool isMotionEnabled,
      bool isStorageAccessGranted});
}

/// @nodoc
class _$PermissionToCheckStatusCopyWithImpl<$Res,
        $Val extends PermissionToCheckStatus>
    implements $PermissionToCheckStatusCopyWith<$Res> {
  _$PermissionToCheckStatusCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isWifiEnabled = null,
    Object? isBluetoothAccessGranted = null,
    Object? isLocationAccessGranted = null,
    Object? isLocationServiceStatusEnabled = null,
    Object? isCameraAccessGranted = null,
    Object? isMicrophoneAccessGranted = null,
    Object? isEnrolledBiometric = null,
    Object? isBatteryCharging = null,
    Object? isBodySensorEnabled = null,
    Object? isMotionEnabled = null,
    Object? isStorageAccessGranted = null,
  }) {
    return _then(_value.copyWith(
      isWifiEnabled: null == isWifiEnabled
          ? _value.isWifiEnabled
          : isWifiEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isBluetoothAccessGranted: null == isBluetoothAccessGranted
          ? _value.isBluetoothAccessGranted
          : isBluetoothAccessGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isLocationAccessGranted: null == isLocationAccessGranted
          ? _value.isLocationAccessGranted
          : isLocationAccessGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isLocationServiceStatusEnabled: null == isLocationServiceStatusEnabled
          ? _value.isLocationServiceStatusEnabled
          : isLocationServiceStatusEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isCameraAccessGranted: null == isCameraAccessGranted
          ? _value.isCameraAccessGranted
          : isCameraAccessGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isMicrophoneAccessGranted: null == isMicrophoneAccessGranted
          ? _value.isMicrophoneAccessGranted
          : isMicrophoneAccessGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isEnrolledBiometric: null == isEnrolledBiometric
          ? _value.isEnrolledBiometric
          : isEnrolledBiometric // ignore: cast_nullable_to_non_nullable
              as bool,
      isBatteryCharging: null == isBatteryCharging
          ? _value.isBatteryCharging
          : isBatteryCharging // ignore: cast_nullable_to_non_nullable
              as bool,
      isBodySensorEnabled: null == isBodySensorEnabled
          ? _value.isBodySensorEnabled
          : isBodySensorEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isMotionEnabled: null == isMotionEnabled
          ? _value.isMotionEnabled
          : isMotionEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isStorageAccessGranted: null == isStorageAccessGranted
          ? _value.isStorageAccessGranted
          : isStorageAccessGranted // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$PermissionToCheckStatusImplCopyWith<$Res>
    implements $PermissionToCheckStatusCopyWith<$Res> {
  factory _$$PermissionToCheckStatusImplCopyWith(
          _$PermissionToCheckStatusImpl value,
          $Res Function(_$PermissionToCheckStatusImpl) then) =
      __$$PermissionToCheckStatusImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool isWifiEnabled,
      bool isBluetoothAccessGranted,
      bool isLocationAccessGranted,
      bool isLocationServiceStatusEnabled,
      bool isCameraAccessGranted,
      bool isMicrophoneAccessGranted,
      bool isEnrolledBiometric,
      bool isBatteryCharging,
      bool isBodySensorEnabled,
      bool isMotionEnabled,
      bool isStorageAccessGranted});
}

/// @nodoc
class __$$PermissionToCheckStatusImplCopyWithImpl<$Res>
    extends _$PermissionToCheckStatusCopyWithImpl<$Res,
        _$PermissionToCheckStatusImpl>
    implements _$$PermissionToCheckStatusImplCopyWith<$Res> {
  __$$PermissionToCheckStatusImplCopyWithImpl(
      _$PermissionToCheckStatusImpl _value,
      $Res Function(_$PermissionToCheckStatusImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isWifiEnabled = null,
    Object? isBluetoothAccessGranted = null,
    Object? isLocationAccessGranted = null,
    Object? isLocationServiceStatusEnabled = null,
    Object? isCameraAccessGranted = null,
    Object? isMicrophoneAccessGranted = null,
    Object? isEnrolledBiometric = null,
    Object? isBatteryCharging = null,
    Object? isBodySensorEnabled = null,
    Object? isMotionEnabled = null,
    Object? isStorageAccessGranted = null,
  }) {
    return _then(_$PermissionToCheckStatusImpl(
      isWifiEnabled: null == isWifiEnabled
          ? _value.isWifiEnabled
          : isWifiEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isBluetoothAccessGranted: null == isBluetoothAccessGranted
          ? _value.isBluetoothAccessGranted
          : isBluetoothAccessGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isLocationAccessGranted: null == isLocationAccessGranted
          ? _value.isLocationAccessGranted
          : isLocationAccessGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isLocationServiceStatusEnabled: null == isLocationServiceStatusEnabled
          ? _value.isLocationServiceStatusEnabled
          : isLocationServiceStatusEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isCameraAccessGranted: null == isCameraAccessGranted
          ? _value.isCameraAccessGranted
          : isCameraAccessGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isMicrophoneAccessGranted: null == isMicrophoneAccessGranted
          ? _value.isMicrophoneAccessGranted
          : isMicrophoneAccessGranted // ignore: cast_nullable_to_non_nullable
              as bool,
      isEnrolledBiometric: null == isEnrolledBiometric
          ? _value.isEnrolledBiometric
          : isEnrolledBiometric // ignore: cast_nullable_to_non_nullable
              as bool,
      isBatteryCharging: null == isBatteryCharging
          ? _value.isBatteryCharging
          : isBatteryCharging // ignore: cast_nullable_to_non_nullable
              as bool,
      isBodySensorEnabled: null == isBodySensorEnabled
          ? _value.isBodySensorEnabled
          : isBodySensorEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isMotionEnabled: null == isMotionEnabled
          ? _value.isMotionEnabled
          : isMotionEnabled // ignore: cast_nullable_to_non_nullable
              as bool,
      isStorageAccessGranted: null == isStorageAccessGranted
          ? _value.isStorageAccessGranted
          : isStorageAccessGranted // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$PermissionToCheckStatusImpl extends _PermissionToCheckStatus {
  const _$PermissionToCheckStatusImpl(
      {this.isWifiEnabled = false,
      this.isBluetoothAccessGranted = false,
      this.isLocationAccessGranted = false,
      this.isLocationServiceStatusEnabled = false,
      this.isCameraAccessGranted = false,
      this.isMicrophoneAccessGranted = false,
      this.isEnrolledBiometric = false,
      this.isBatteryCharging = false,
      this.isBodySensorEnabled = false,
      this.isMotionEnabled = true,
      this.isStorageAccessGranted = false})
      : super._();

  @override
  @JsonKey()
  final bool isWifiEnabled;
  @override
  @JsonKey()
  final bool isBluetoothAccessGranted;
  @override
  @JsonKey()
  final bool isLocationAccessGranted;
  @override
  @JsonKey()
  final bool isLocationServiceStatusEnabled;
  @override
  @JsonKey()
  final bool isCameraAccessGranted;
  @override
  @JsonKey()
  final bool isMicrophoneAccessGranted;
  @override
  @JsonKey()
  final bool isEnrolledBiometric;
  @override
  @JsonKey()
  final bool isBatteryCharging;
  @override
  @JsonKey()
  final bool isBodySensorEnabled;
  @override
  @JsonKey()
  final bool isMotionEnabled;
  @override
  @JsonKey()
  final bool isStorageAccessGranted;

  @override
  String toString() {
    return 'PermissionToCheckStatus(isWifiEnabled: $isWifiEnabled, isBluetoothAccessGranted: $isBluetoothAccessGranted, isLocationAccessGranted: $isLocationAccessGranted, isLocationServiceStatusEnabled: $isLocationServiceStatusEnabled, isCameraAccessGranted: $isCameraAccessGranted, isMicrophoneAccessGranted: $isMicrophoneAccessGranted, isEnrolledBiometric: $isEnrolledBiometric, isBatteryCharging: $isBatteryCharging, isBodySensorEnabled: $isBodySensorEnabled, isMotionEnabled: $isMotionEnabled, isStorageAccessGranted: $isStorageAccessGranted)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PermissionToCheckStatusImpl &&
            (identical(other.isWifiEnabled, isWifiEnabled) ||
                other.isWifiEnabled == isWifiEnabled) &&
            (identical(
                    other.isBluetoothAccessGranted, isBluetoothAccessGranted) ||
                other.isBluetoothAccessGranted == isBluetoothAccessGranted) &&
            (identical(
                    other.isLocationAccessGranted, isLocationAccessGranted) ||
                other.isLocationAccessGranted == isLocationAccessGranted) &&
            (identical(other.isLocationServiceStatusEnabled,
                    isLocationServiceStatusEnabled) ||
                other.isLocationServiceStatusEnabled ==
                    isLocationServiceStatusEnabled) &&
            (identical(other.isCameraAccessGranted, isCameraAccessGranted) ||
                other.isCameraAccessGranted == isCameraAccessGranted) &&
            (identical(other.isMicrophoneAccessGranted,
                    isMicrophoneAccessGranted) ||
                other.isMicrophoneAccessGranted == isMicrophoneAccessGranted) &&
            (identical(other.isEnrolledBiometric, isEnrolledBiometric) ||
                other.isEnrolledBiometric == isEnrolledBiometric) &&
            (identical(other.isBatteryCharging, isBatteryCharging) ||
                other.isBatteryCharging == isBatteryCharging) &&
            (identical(other.isBodySensorEnabled, isBodySensorEnabled) ||
                other.isBodySensorEnabled == isBodySensorEnabled) &&
            (identical(other.isMotionEnabled, isMotionEnabled) ||
                other.isMotionEnabled == isMotionEnabled) &&
            (identical(other.isStorageAccessGranted, isStorageAccessGranted) ||
                other.isStorageAccessGranted == isStorageAccessGranted));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      isWifiEnabled,
      isBluetoothAccessGranted,
      isLocationAccessGranted,
      isLocationServiceStatusEnabled,
      isCameraAccessGranted,
      isMicrophoneAccessGranted,
      isEnrolledBiometric,
      isBatteryCharging,
      isBodySensorEnabled,
      isMotionEnabled,
      isStorageAccessGranted);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PermissionToCheckStatusImplCopyWith<_$PermissionToCheckStatusImpl>
      get copyWith => __$$PermissionToCheckStatusImplCopyWithImpl<
          _$PermissionToCheckStatusImpl>(this, _$identity);
}

abstract class _PermissionToCheckStatus extends PermissionToCheckStatus {
  const factory _PermissionToCheckStatus(
      {final bool isWifiEnabled,
      final bool isBluetoothAccessGranted,
      final bool isLocationAccessGranted,
      final bool isLocationServiceStatusEnabled,
      final bool isCameraAccessGranted,
      final bool isMicrophoneAccessGranted,
      final bool isEnrolledBiometric,
      final bool isBatteryCharging,
      final bool isBodySensorEnabled,
      final bool isMotionEnabled,
      final bool isStorageAccessGranted}) = _$PermissionToCheckStatusImpl;
  const _PermissionToCheckStatus._() : super._();

  @override
  bool get isWifiEnabled;
  @override
  bool get isBluetoothAccessGranted;
  @override
  bool get isLocationAccessGranted;
  @override
  bool get isLocationServiceStatusEnabled;
  @override
  bool get isCameraAccessGranted;
  @override
  bool get isMicrophoneAccessGranted;
  @override
  bool get isEnrolledBiometric;
  @override
  bool get isBatteryCharging;
  @override
  bool get isBodySensorEnabled;
  @override
  bool get isMotionEnabled;
  @override
  bool get isStorageAccessGranted;
  @override
  @JsonKey(ignore: true)
  _$$PermissionToCheckStatusImplCopyWith<_$PermissionToCheckStatusImpl>
      get copyWith => throw _privateConstructorUsedError;
}
