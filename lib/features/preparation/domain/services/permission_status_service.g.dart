// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'permission_status_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$permissionStatusServiceHash() =>
    r'b5a4457b12402ce6e6beb5480c170918a466dc0b';

/// See also [permissionStatusService].
@ProviderFor(permissionStatusService)
final permissionStatusServiceProvider =
    AutoDisposeProvider<PermissionStatusService>.internal(
  permissionStatusService,
  name: r'permissionStatusServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$permissionStatusServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef PermissionStatusServiceRef
    = AutoDisposeProviderRef<PermissionStatusService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
