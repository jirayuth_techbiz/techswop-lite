import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/bluetooth/domain/repository/bluetooth_check_repository.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/repository/face_id_repository.dart';
import 'package:techswop_lite/features/device_test/biometrics/domain/repository/fingerprint_check_repository.dart';
import 'package:techswop_lite/features/device_test/geo_location/domain/repository/geo_location_repository.dart';
import 'package:techswop_lite/features/device_test/wifi/domain/repositories/wifi_check_repository.dart';
import 'package:techswop_lite/features/preparation/application/permission_status_service_impl.dart';
import 'package:techswop_lite/features/preparation/domain/permission_status.dart';
import 'package:techswop_lite/features/preparation/domain/repository/permission_status_repository.dart';
import 'package:techswop_lite/shared/domain/repository/device_info_repository.dart';
import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';

part 'permission_status_service.g.dart';

@riverpod
PermissionStatusService permissionStatusService(
    PermissionStatusServiceRef ref) {
  final permissionStatusRepo = ref.watch(permissionStatusRepositoryProvider);
  final wifiRepo = ref.watch(wifiCheckRepositoryProvider);
  final bluetoothRepo = ref.watch(bluetoothCheckRepositoryProvider);
  final locationRepo = ref.watch(geoLocationCheckRepositoryProvider);
  final biometricsRepo = ref.watch(fingerprintCheckRepositoryProvider);
  final faceIdRepo = ref.watch(faceIdRepositoryProvider);
  final deviceInfoRepo = ref.watch(deviceInfoRepositoryProvider);
  final loggerRepository = ref.watch(loggerRepositoryProvider);
  
  return PermissionStatusServiceImpl(
    ref: ref,
    wifiCheckRepository: wifiRepo,
    bluetoothCheckRepository: bluetoothRepo,
    geoLocationCheckRepository: locationRepo,
    permissionStatusRepository: permissionStatusRepo,
    biometricsCheckRepository: biometricsRepo,
    faceIdRepository: faceIdRepo,
    deviceInfoRepository: deviceInfoRepo,
    logger: loggerRepository,
  );
}

abstract class PermissionStatusService {
  ///อัพเดตสถานะ Permission ส่งให้กับหน้า Preparation
  Stream<PermissionToCheckStatus> updatePermissionStatus();
}
