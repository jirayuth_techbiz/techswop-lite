import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/features/preparation/domain/permission_status.dart';
import 'package:techswop_lite/features/preparation/presentation/state/permission_check_state.dart';

part 'checklist_properties.freezed.dart';

@freezed
sealed class CheckListPermissionProperties with _$CheckListPermissionProperties {
  const factory CheckListPermissionProperties({
    required String icon,
    @Default("") String textUpper,
    @Default("") String textLower,
    PermissionToCheckStatusEnum? permissionStatusEnum,
    AsyncValue<PermissionToCheckStatus>? state,
  }) = _CheckListPermissionProperties;

  const CheckListPermissionProperties._();
}
