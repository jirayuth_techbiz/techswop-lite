import 'package:freezed_annotation/freezed_annotation.dart';

part 'permission_status.freezed.dart';

@freezed 
class PermissionToCheckStatus with _$PermissionToCheckStatus {
  const factory PermissionToCheckStatus({
    @Default(false) bool isWifiEnabled,
    @Default(false) bool isBluetoothAccessGranted,
    @Default(false) bool isLocationAccessGranted,
    @Default(false) bool isLocationServiceStatusEnabled,
    @Default(false) bool isCameraAccessGranted,
    @Default(false) bool isMicrophoneAccessGranted,
    @Default(false) bool isEnrolledBiometric,
    @Default(false) bool isBatteryCharging,
    @Default(false) bool isBodySensorEnabled,
    @Default(true) bool isMotionEnabled,
    @Default(false) bool isStorageAccessGranted,
  }) = _PermissionToCheckStatus;

  const PermissionToCheckStatus._();
}