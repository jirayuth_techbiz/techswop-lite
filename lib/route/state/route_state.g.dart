// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'route_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$routeStateHash() => r'dbb1e3d26a41f8a212ef29f4b87e50e65428de58';

/// See also [RouteState].
@ProviderFor(RouteState)
final routeStateProvider =
    AutoDisposeAsyncNotifierProvider<RouteState, List<GoRoute>>.internal(
  RouteState.new,
  name: r'routeStateProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$routeStateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$RouteState = AutoDisposeAsyncNotifier<List<GoRoute>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
