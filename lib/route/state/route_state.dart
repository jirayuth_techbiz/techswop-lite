import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/const/enums/routing_enums.dart';
import 'package:techswop_lite/route/route.dart';

part 'route_state.g.dart';

@riverpod
class RouteState extends _$RouteState {
  @override
  Future<List<GoRoute>> build() async {
    List<AppRoutes> path = [
      AppRoutes.wifiCheckPage,
      AppRoutes.bluetoothCheckPage,
      AppRoutes.geolocationCheckPage,
      ///TODO: Uncomment the following lines
      AppRoutes.frontCameraCheckPage,
      AppRoutes.backCameraCheckPage,
      AppRoutes.microphoneCheckPage,
      AppRoutes.biometricsCheckPage,
      AppRoutes.proximitySensorCheckPage,
      AppRoutes.volumeCheckPage,
      AppRoutes.motionCheckPage,
      AppRoutes.screenCheckStartPage
    ];
    List<GoRoute> result = [];

    for (var question in path) {
      List<GoRoute> childPath = [];
      if (question.titleAPI == AppRoutes.screenCheckStartPage.titleAPI) {
        childPath.add(
          GoRoute(
            path: AppRoutes.screenCheckPage.path,
            name: AppRoutes.screenCheckPage.name,
            pageBuilder: (context, state) {
              return buildPagewithDefaultTransition(
                context: context,
                state: state,
                child: AppRoutes.screenCheckPage.widgetPages,
              );
            },
          ),
        );
      }

      GoRoute route = GoRoute(
        name: question.name,
        path: question.path,
        pageBuilder: (context, state) {
          return buildPagewithDefaultTransition(
            context: context,
            state: state,
            child: question.widgetPages,
          );
        },
        routes: childPath,
      );

      result.add(route);
    }

    return result;
  }
}
