import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

part 'app_startup_state.g.dart';

@Riverpod(keepAlive: true)
Future<void> appStartup(AppStartupRef ref) async {
  await Future.wait([
  ref.watch(
    tradeInSharePreferencesRepositoryProvider.future,
  ),
  ]);
}
