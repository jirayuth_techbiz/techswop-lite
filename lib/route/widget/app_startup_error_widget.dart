import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/shared/shared.dart';

class AppStartupErrorWidget extends ConsumerWidget {
  const AppStartupErrorWidget(
      {super.key, required this.message, required this.onRetry});
  final String message;
  final VoidCallback onRetry;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final translation = context.localization;
    final theme = context.theme;

    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(message, style: theme.textTheme.headlineSmall),
            ElevatedButton(
              onPressed: onRetry,
              child: Text(translation.retryTxt),
            ),
          ],
        ),
      ),
    );
  }
}
