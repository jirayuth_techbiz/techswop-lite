import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/route/state/app_startup_state.dart';
import 'package:techswop_lite/route/widget/app_startup_error_widget.dart';
import 'package:techswop_lite/route/widget/app_startup_loading_widget.dart';

class AppStartupWidget extends ConsumerWidget {
  final WidgetBuilder onLoaded;
  const AppStartupWidget({super.key, required this.onLoaded});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appStartupState = ref.watch(appStartupProvider);
    return appStartupState.when(
        loading: () => const AppStartupLoadingWidget(),
        error: (error, stacktrace) => AppStartupErrorWidget(
              message: error.toString(),
              onRetry: () => ref.invalidate(appStartupProvider),
            ),
        data: (_) => onLoaded(context));
  }
}
