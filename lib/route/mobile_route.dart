import 'package:flutter/material.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/test_result/test_cancelling_widget.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/route/state/app_startup_state.dart';
import 'package:techswop_lite/const/const.dart';
import 'package:techswop_lite/route/state/route_state.dart';

part 'mobile_route.g.dart';

///Custom transition pages helper function
CustomTransitionPage buildPagewithDefaultTransition({
  required BuildContext context,
  required GoRouterState state,
  required Widget child,
  UniqueKey? key,
}) {
  return CustomTransitionPage(
    child: child,
    key: key ?? state.pageKey,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      return SlideTransition(
        position: animation.drive(
          Tween<Offset>(
            begin: const Offset(1, 0),
            end: Offset.zero,
          ).chain(
            CurveTween(curve: Curves.easeIn),
          ),
        ),
        child: child,
      );
    },
  );
}

@riverpod
class AppRouter extends _$AppRouter {
  @override
  GoRouter build() {
    final appStartupState = ref.watch(appStartupProvider);
    final routePathState = ref.watch(routeStateProvider);

    return GoRouter(
      debugLogDiagnostics: true,
      initialLocation: '/${AppRoutes.preparingPage.path}',
      redirect: (context, state) {
        if (appStartupState.isLoading || appStartupState.hasError) {
          return '/${AppRoutes.preparingPage.path}';
        }
        return null;
      },
      routes: [
        GoRoute(
          name: AppRoutes.preparingPage.name,
          path: "/${AppRoutes.preparingPage.path}",
          pageBuilder: (context, state) {
            return buildPagewithDefaultTransition(
              context: context,
              state: state,
              child: AppRoutes.preparingPage.widgetPages,
            );
          },
          routes: 
            routePathState.value ?? [], //isiOS ? iOSRoute : androidRoute,
        ),
        GoRoute(
              name: AppRoutes.resultPage.name,
              path: "/${AppRoutes.resultPage.path}",
              pageBuilder: (context, state) => buildPagewithDefaultTransition(
                context: context,
                state: state,
                child: AppRoutes.resultPage.widgetPages,
              ),
            ),
      ],
    );
  }
}

@riverpod
class GetCurrentPageNumber extends _$GetCurrentPageNumber {
  @override
  int build(String path) {
    late int currentPage;
    final routePathState = ref.watch(routeStateProvider);
    routePathState.whenData((value) {
      currentPage = value.indexWhere((element) => element.path == path);
    });
    return currentPage + 1;
  }
}

////////////////////////////////////////////////////////////////////////////////

extension GoRouterLocation on GoRouter {
  String get location {
    final RouteMatch lastMatch = routerDelegate.currentConfiguration.last;
    final RouteMatchList matchList = lastMatch is ImperativeRouteMatch
        ? lastMatch.matches
        : routerDelegate.currentConfiguration;
    return matchList.uri.toString();
  }
}
