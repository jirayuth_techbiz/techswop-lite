// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mobile_route.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$appRouterHash() => r'3a28ef1eb6255690b06caa13228d12fdc58fe374';

/// See also [AppRouter].
@ProviderFor(AppRouter)
final appRouterProvider =
    AutoDisposeNotifierProvider<AppRouter, GoRouter>.internal(
  AppRouter.new,
  name: r'appRouterProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$appRouterHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AppRouter = AutoDisposeNotifier<GoRouter>;
String _$getCurrentPageNumberHash() =>
    r'213807621aec2a433cfe63448e6bb572f8b5885f';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$GetCurrentPageNumber
    extends BuildlessAutoDisposeNotifier<int> {
  late final String path;

  int build(
    String path,
  );
}

/// See also [GetCurrentPageNumber].
@ProviderFor(GetCurrentPageNumber)
const getCurrentPageNumberProvider = GetCurrentPageNumberFamily();

/// See also [GetCurrentPageNumber].
class GetCurrentPageNumberFamily extends Family<int> {
  /// See also [GetCurrentPageNumber].
  const GetCurrentPageNumberFamily();

  /// See also [GetCurrentPageNumber].
  GetCurrentPageNumberProvider call(
    String path,
  ) {
    return GetCurrentPageNumberProvider(
      path,
    );
  }

  @override
  GetCurrentPageNumberProvider getProviderOverride(
    covariant GetCurrentPageNumberProvider provider,
  ) {
    return call(
      provider.path,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'getCurrentPageNumberProvider';
}

/// See also [GetCurrentPageNumber].
class GetCurrentPageNumberProvider
    extends AutoDisposeNotifierProviderImpl<GetCurrentPageNumber, int> {
  /// See also [GetCurrentPageNumber].
  GetCurrentPageNumberProvider(
    String path,
  ) : this._internal(
          () => GetCurrentPageNumber()..path = path,
          from: getCurrentPageNumberProvider,
          name: r'getCurrentPageNumberProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$getCurrentPageNumberHash,
          dependencies: GetCurrentPageNumberFamily._dependencies,
          allTransitiveDependencies:
              GetCurrentPageNumberFamily._allTransitiveDependencies,
          path: path,
        );

  GetCurrentPageNumberProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.path,
  }) : super.internal();

  final String path;

  @override
  int runNotifierBuild(
    covariant GetCurrentPageNumber notifier,
  ) {
    return notifier.build(
      path,
    );
  }

  @override
  Override overrideWith(GetCurrentPageNumber Function() create) {
    return ProviderOverride(
      origin: this,
      override: GetCurrentPageNumberProvider._internal(
        () => create()..path = path,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        path: path,
      ),
    );
  }

  @override
  AutoDisposeNotifierProviderElement<GetCurrentPageNumber, int>
      createElement() {
    return _GetCurrentPageNumberProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GetCurrentPageNumberProvider && other.path == path;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, path.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin GetCurrentPageNumberRef on AutoDisposeNotifierProviderRef<int> {
  /// The parameter `path` of this provider.
  String get path;
}

class _GetCurrentPageNumberProviderElement
    extends AutoDisposeNotifierProviderElement<GetCurrentPageNumber, int>
    with GetCurrentPageNumberRef {
  _GetCurrentPageNumberProviderElement(super.provider);

  @override
  String get path => (origin as GetCurrentPageNumberProvider).path;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
