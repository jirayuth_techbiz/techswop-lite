import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart' as intl;

import 'app_localizations_en.dart';
import 'app_localizations_th.dart';

/// Callers can lookup localized strings with an instance of AppLocalizations
/// returned by `AppLocalizations.of(context)`.
///
/// Applications need to include `AppLocalizations.delegate()` in their app's
/// `localizationDelegates` list, and the locales they support in the app's
/// `supportedLocales` list. For example:
///
/// ```dart
/// import 'generated/app_localizations.dart';
///
/// return MaterialApp(
///   localizationsDelegates: AppLocalizations.localizationsDelegates,
///   supportedLocales: AppLocalizations.supportedLocales,
///   home: MyApplicationHome(),
/// );
/// ```
///
/// ## Update pubspec.yaml
///
/// Please make sure to update your pubspec.yaml to include the following
/// packages:
///
/// ```yaml
/// dependencies:
///   # Internationalization support.
///   flutter_localizations:
///     sdk: flutter
///   intl: any # Use the pinned version from flutter_localizations
///
///   # Rest of dependencies
/// ```
///
/// ## iOS Applications
///
/// iOS applications define key application metadata, including supported
/// locales, in an Info.plist file that is built into the application bundle.
/// To configure the locales supported by your app, you’ll need to edit this
/// file.
///
/// First, open your project’s ios/Runner.xcworkspace Xcode workspace file.
/// Then, in the Project Navigator, open the Info.plist file under the Runner
/// project’s Runner folder.
///
/// Next, select the Information Property List item, select Add Item from the
/// Editor menu, then select Localizations from the pop-up menu.
///
/// Select and expand the newly-created Localizations item then, for each
/// locale your application supports, add a new item and select the locale
/// you wish to add from the pop-up menu in the Value field. This list should
/// be consistent with the languages listed in the AppLocalizations.supportedLocales
/// property.
abstract class AppLocalizations {
  AppLocalizations(String locale) : localeName = intl.Intl.canonicalizedLocale(locale.toString());

  final String localeName;

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  /// A list of this localizations delegate along with the default localizations
  /// delegates.
  ///
  /// Returns a list of localizations delegates containing this delegate along with
  /// GlobalMaterialLocalizations.delegate, GlobalCupertinoLocalizations.delegate,
  /// and GlobalWidgetsLocalizations.delegate.
  ///
  /// Additional delegates can be added by appending to this list in
  /// MaterialApp. This list does not have to be used at all if a custom list
  /// of delegates is preferred or required.
  static const List<LocalizationsDelegate<dynamic>> localizationsDelegates = <LocalizationsDelegate<dynamic>>[
    delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ];

  /// A list of this localizations delegate's supported locales.
  static const List<Locale> supportedLocales = <Locale>[
    Locale('en'),
    Locale('th')
  ];

  /// ชื่อแอพ
  ///
  /// In th, this message translates to:
  /// **'แอพเช็คสภาพเครื่องมือถือ'**
  String get title;

  /// ปุ่มยืนยัน
  ///
  /// In th, this message translates to:
  /// **'ยืนยัน'**
  String get confirmTxt;

  /// ปุ่มตกลง
  ///
  /// In th, this message translates to:
  /// **'ตกลง'**
  String get okTxt;

  /// ปุ่มย้อนกลับ
  ///
  /// In th, this message translates to:
  /// **'ย้อนกลับ'**
  String get returnTxt;

  /// ปุ่มยกเลิก
  ///
  /// In th, this message translates to:
  /// **'ยกเลิก'**
  String get cancelTxt;

  /// คำว่าผิดพลาด แจ้งเวลาเกิดข้อผิดพลาดใดๆ
  ///
  /// In th, this message translates to:
  /// **'ผิดพลาด'**
  String get errorTxt;

  /// ปุ่มลองอีกครั้ง
  ///
  /// In th, this message translates to:
  /// **'ลองอีกครั้ง'**
  String get retryTxt;

  /// ปุ่มปิดรายการหรือหน้าต่าง
  ///
  /// In th, this message translates to:
  /// **'ปิด'**
  String get closeTxt;

  /// ปุ่มเริ่มตรวจเช็คสภาพเครื่อง และเริ่มการเทรดเครื่อง
  ///
  /// In th, this message translates to:
  /// **'เริ่มประเมินสภาพกันเลย!'**
  String get startTradeButton;

  /// ปุ่มรีเฟรชหน้า
  ///
  /// In th, this message translates to:
  /// **'รีเฟรช'**
  String get refreshTxt;

  /// ปุ่มรีเซ็ตข้อมูลทุกอย่างที่เก็บไว้ในเครื่อง อาทิ Shared Preferences, Secure Storage ใช้เฉพาะตอน Debug เท่านั้น
  ///
  /// In th, this message translates to:
  /// **'(Debug) รีเซ็ต'**
  String get debugReset;

  /// ปุ่มข้ามหน้าตรวจเช็คสภาพเครื่องไปยังหน้าที่ต้องการ ใช้เฉพาะตอน Debug เท่านั้น
  ///
  /// In th, this message translates to:
  /// **'(Debug) ตรวจเช็คสภาพเครื่อง'**
  String get debugToCheckPage;

  /// ข้อความแจ้งว่าหน้านี้ยังไม่เสร็จสมบูรณ์ และอยู่ในระหว่างการพัฒนา ใช้เฉพาะตอน Debug เท่านั้น
  ///
  /// In th, this message translates to:
  /// **'หน้านี้ยังไม่เสร็จสมบูรณ์ และอยู่ในระหว่างการพัฒนา โปรดกดข้ามไปหน้าต่อไป'**
  String get debugUnderConstuction;

  /// คำว่าหน้าแรกตรง Bottom App Bar
  ///
  /// In th, this message translates to:
  /// **'หน้าแรก'**
  String get bottomAppBarHome;

  /// คำว่าหน้าผลการตรวจตรง Bottom App Bar
  ///
  /// In th, this message translates to:
  /// **'ผลการตรวจ'**
  String get bottomAppBarResultPage;

  /// คำว่าอุปกรณ์ที่ไม่รู้จัก ขึ้นตอนที่ไม่สามารถดึงข้อมูลจาก API ได้
  ///
  /// In th, this message translates to:
  /// **'อุปกรณ์ที่ไม่รู้จัก'**
  String get homePageUnknownDevice;

  /// ข้อความแจ้งว่าไม่พบรูปภาพหน้าหลัก และขอให้กดไอคอนเพื่อรีเฟรชหน้า
  ///
  /// In th, this message translates to:
  /// **'ไม่พบรูปภาพ โปรดกดที่ไอคอนด้านบนเพื่อรีเฟรชหน้า'**
  String get homePageImageNotFound;

  /// ข้อความถามผู้ใช้ว่าต้องการออกจากแอพหรือไม่
  ///
  /// In th, this message translates to:
  /// **'คุณต้องการออกจากแอพหรือไม่'**
  String get homePageExitPage;

  /// ข้อความแจ้งว่าอุปกรณ์ไม่รองรับ ขึ้นตอนที่ตัวเครื่องไม่มีข้อมูลใน Database
  ///
  /// In th, this message translates to:
  /// **'อุปกรณ์ของคุณอาจไม่รองรับ หรือระบบพบว่าไม่มีข้อมูล โปรดกดปุ่ม \'รีเฟรช\' เพื่อดึงข้อมูลใหม่\nหากยังพบหน้านี้ โปรดติดต่อเจ้าหน้าที่เพื่อขอความช่วยเหลือเพิ่มเติม'**
  String get homePageYourDeviceIsNotSupported;

  /// ข้อความแจ้งว่าอุปกรณ์ของคุณไม่ได้เข้าร่วมแคมเปญ ขึ้นในกรณีที่ชื่อเครื่องถูก List ไว้ในรายการที่จะไม่มีการ Trade
  ///
  /// In th, this message translates to:
  /// **'อุปกรณ์ของคุณไม่ได้เข้าร่วมแคมเปญ Trade-in โปรดติดต่อเจ้าหน้าที่เพื่อขอความช่วยเหลือเพิ่มเติม'**
  String get homePageNoDevice;

  /// ข้อความโปรโมชั่นบนหน้าหลัก
  ///
  /// In th, this message translates to:
  /// **'โปรโมชั่น'**
  String get homePagePromotion;

  /// ข้อความแจ้งว่าไม่มีโปรโมชั่นใดๆ ในขณะนี้ ขึ้นในกรณีที่ไม่มีโปรโมชั่นในขณะนั้นหรือ API ไม่ทำงาน
  ///
  /// In th, this message translates to:
  /// **'ไม่มีโปรโมชั่นใดๆ ในขณะนี้'**
  String get homePageNoPromotion;

  /// หัวข้อของหน้าเว็บวิว ขึ้นเวลา User กดเข้าไปดูโปรโมชั่น
  ///
  /// In th, this message translates to:
  /// **'โปรโมชั่น'**
  String get webViewHeader;

  /// ชื่อหน้าก่อนเริ่มการตรวจสอบ
  ///
  /// In th, this message translates to:
  /// **'คำแนะนำก่อนเริ่มตรวจสอบ'**
  String get preparationBeforeCheckTips;

  /// ข้อความขอให้ผู้ใช้กดอนุญาติเข้าถึงสิทธิ์ต่างๆ
  ///
  /// In th, this message translates to:
  /// **'เปิดการใช้งานอุปกรณ์ และให้สิทธิ์เข้าถึงอุปกรณ์ให้พร้อมก่อนเริ่มตรวจสอบ'**
  String get preparationPleaseAllowPermission;

  /// ข้อความแจ้งว่าถ้าไม่เปิดอาจจะส่งผลต่อการประเมินราคา
  ///
  /// In th, this message translates to:
  /// **'ถ้าเปิดการใช้งานอุปกรณ์หรือให้สิทธิ์เข้าถึงอุปกรณ์ไม่ครบ จะส่งผลต่อการประเมินราคา'**
  String get preparationOtherwiseItWillAffectTheResult;

  /// ข้อความแจ้งผู้ใช้ว่าถ้าไม่ได้กดอนุญาติแต่แรก จะสามารถกดขอได้ด้วยการกดปุ่ม X
  ///
  /// In th, this message translates to:
  /// **'หากกดปฏิเสธการให้สิทธิ์ ให้กดที่ไอคอน X แล้วเปิดสิทธิ์ให้หน้าการเข้าถึง'**
  String get preparationIncaseYouForgotPermission;

  /// ปุ่มเริ่มตรวจสอบ
  ///
  /// In th, this message translates to:
  /// **'เริ่มตรวจสอบกันเลย!'**
  String get preparationLetsBegin;

  /// หัวข้อความที่จะขึ้นเวลาผู้ใช้ยังไม่ได้กดอนุญาติให้สิทธิ์ครบ
  ///
  /// In th, this message translates to:
  /// **'คำเตือน!'**
  String get preparationPopupWarning;

  /// ข้อความเตือนบอกผู้ใช้ว่าระบบพบว่ายังไม่ได้เปิดสิทธิ์ครบ
  ///
  /// In th, this message translates to:
  /// **'คุณยังไม่ได้เปิดสิทธิ์อนุญาติแอพครบ\n'**
  String get preparationPopupWarningAccessInComplete;

  /// ข้อความถามผู้ใช้ว่ายังจะยืนยันที่จะทำต่อจริงๆ หรือไม่ ถ้ายืนยันจะทำต่อ อาจจะส่งผลต่อการประเมินราคา
  ///
  /// In th, this message translates to:
  /// **'หากดำเนินการต่ออาจจะส่งผลต่อการประเมินราคา ต้องการดำเนินการต่อหรือไม่\n'**
  String get preparationPopupAreYouSureYouWantToContinues;

  /// ข้อความยืนยันการดำเนินการต่อ ถ้าผู้ใช้ต้องการดำเนินการต่อจริงๆ
  ///
  /// In th, this message translates to:
  /// **'• หากใช่ โปรดกดยืนยัน\n'**
  String get preparationPopupIfYesPressContinues;

  /// ข้อความถามผู้ใช้ว่าอยากจะเข้าหน้าตั้งค่าไปกดให้สิทธิ์ก่อนไหม
  ///
  /// In th, this message translates to:
  /// **'• หากต้องการจะเปิดสิทธิ์เข้าถึงในแอปก่อนให้เลือก \'ให้อนุญาติสิทธิ์\''**
  String get preparationPopupOrYouWantToDoItManually;

  /// ปุ่มให้อนุญาติสิทธิ์ กดแล้วจะขึ้นหน้าสิทธิ์ของแอพ เพื่อให้ User สามารถกดเข้าไปให้อนุญาติได้
  ///
  /// In th, this message translates to:
  /// **'ให้อนุญาติสิทธิ์'**
  String get preparationPopupGrantPermission;

  /// คำว่า Wi-Fi
  ///
  /// In th, this message translates to:
  /// **'Wi-fi'**
  String get hardwareBasedWifi;

  /// คำอธิบายเกี่ยวกับการเปิด Wi-Fi
  ///
  /// In th, this message translates to:
  /// **'เปิด Wi-Fi ให้แอพสามารถเข้าถึง Wi-fi และตรวจสอบการเชื่อมต่อได้'**
  String get hardwareBasedWifiDescription;

  /// คำว่า Motion หรือการตรวจสอบการหมุนหน้าจอ
  ///
  /// In th, this message translates to:
  /// **'Motion'**
  String get hardwareBasedMotion;

  /// คำอธิบายเกี่ยวกับการตรวจสอบการหมุนหน้าจอ
  ///
  /// In th, this message translates to:
  /// **'เปิดการใช้งานหมุนหน้าจอ'**
  String get hardwareBasedMotionDescription;

  /// คำว่าแบตเตอรี่
  ///
  /// In th, this message translates to:
  /// **'Battery'**
  String get hardwareBasedBattery;

  /// คำอธิบายเกี่ยวกับการตรวจสอบสถานะแบตเตอรี่
  ///
  /// In th, this message translates to:
  /// **'เสียบแบตเตอรี่ระหว่างการทดสอบ เพื่อให้แอพสามารถตรวจสอบสถานะแบตเตอรี่ได้'**
  String get hardwareBasedBatteryDescription;

  /// คำว่า GPS (Location)
  ///
  /// In th, this message translates to:
  /// **'GPS'**
  String get hardwareBasedGPS;

  /// คำอธิบายเกี่ยวกับการตรวจสอบสถานะ GPS
  ///
  /// In th, this message translates to:
  /// **'เปิดการใช้งานการระบุตำแหน่ง GPS ให้แอพสามารถตรวจสอบสถานะ GPS ได้'**
  String get hardwareBasedGPSDescription;

  /// คำว่า Biometrics หรือการตรวจสอบระบบสแกนใบหน้า/ลายนิ้วมือ
  ///
  /// In th, this message translates to:
  /// **'Biometrics'**
  String get hardwareBasedBiometrics;

  /// คำอธิบายเกี่ยวกับการตรวจสอบระบบสแกนใบหน้า/ลายนิ้วมือ
  ///
  /// In th, this message translates to:
  /// **'เปิดการใช้งานระบบสแกนใบหน้า/ลายนิ้วมือ ให้แอพสามารถตรวจสอบระบบสแกนใบหน้า/ลายนิ้วมือ ได้'**
  String get hardwareBasedBiometricsDescription;

  /// คำว่า Bluetooth
  ///
  /// In th, this message translates to:
  /// **'Bluetooth'**
  String get hardwareBasedBluetooth;

  /// คำอธิบายเกี่ยวกับการตรวจสอบสถานะ Bluetooth
  ///
  /// In th, this message translates to:
  /// **'เปิดการใช้งาน Bluetooth ให้แอพสามารถตรวจสอบสถานะ Bluetooth ได้'**
  String get hardwareBasedBluetoothDescription;

  /// คำว่าไมโครโฟน
  ///
  /// In th, this message translates to:
  /// **'Microphone'**
  String get permissionBasedMicrophone;

  /// คำอธิบายเกี่ยวกับสิทธิ์ไมโครโฟน
  ///
  /// In th, this message translates to:
  /// **'เปิดให้แอพสามารถเข้าถึงสิทธิ์ไมโครโฟน'**
  String get permissionBasedMicrophoneDescription;

  /// คำว่ากล้อง
  ///
  /// In th, this message translates to:
  /// **'Camera'**
  String get permissionBasedCamera;

  /// คำอธิบายเกี่ยวกับสิทธิ์กล้อง
  ///
  /// In th, this message translates to:
  /// **'เปิดให้แอพสามารถเข้าถึงสิทธิ์กล้องถ่ายรูป'**
  String get permissionBasedCameraDescription;

  /// คำว่าพื้นที่เก็บข้อมูล
  ///
  /// In th, this message translates to:
  /// **'Storage'**
  String get permissionBasedStorage;

  /// คำอธิบายเกี่ยวกับการขอสิทธิ์พื้นที่เก็บข้อมูล
  ///
  /// In th, this message translates to:
  /// **'เปิดให้แอพสามารถเข้าถึงสิทธิ์พื้นที่เก็บข้อมูล'**
  String get permissionBasedStorageDescription;

  /// หัวชื่อหน้า Test
  ///
  /// In th, this message translates to:
  /// **'ตรวจสอบอุปกรณ์'**
  String get layoutPageTitle;

  /// ปุ่มข้ามหน้า
  ///
  /// In th, this message translates to:
  /// **'ข้ามขั้นตอน'**
  String get layoutSkipButton;

  /// ปุ่มเริ่มทดสอบ ใช้ในบางหน้าที่ต้องการให้ User ตอบคำถาม
  ///
  /// In th, this message translates to:
  /// **'เริ่มการทดสอบ'**
  String get layoutPageStartTest;

  /// ปุ่มเริ่มทดสอบใหม่เวลาที่ User ตอบคำถามผิดหรือต้องการทดสอบใหม่
  ///
  /// In th, this message translates to:
  /// **'ทดสอบใหม่อีกครั้ง'**
  String get layoutPageRestart;

  /// ข้อความแจ้งว่าถึงขั้นตอนที่เท่าไหร่แล้ว
  ///
  /// In th, this message translates to:
  /// **'ขั้นตอนที่ : '**
  String get layoutStep;

  /// หัวข้อความแนะนำวิธีทดสอบ
  ///
  /// In th, this message translates to:
  /// **'คำแนะนำ '**
  String get layoutTips;

  /// คำอธิบายของการนับถอยหลัง
  ///
  /// In th, this message translates to:
  /// **'นับถอยหลัง...'**
  String get countdownWidgetDescription;

  /// คำอธิบายของการนับถอยหลัง ว่านับเป็นวินาที
  ///
  /// In th, this message translates to:
  /// **'วินาที'**
  String get countdownWidgetSeconds;

  /// คำอธิบายของการรีเซ็ตเวลานับถอยหลัง
  ///
  /// In th, this message translates to:
  /// **'ทดสอบใหม่อีกครั้ง'**
  String get countdownWidgetReset;

  /// หัวข้อคำว่าทดสอบเสร็จสมบูรณ์
  ///
  /// In th, this message translates to:
  /// **'การทดสอบเสร็จสมบูรณ์'**
  String get testSuccess;

  /// หัวข้อคำว่ายกเลิกการทดสอบ
  ///
  /// In th, this message translates to:
  /// **'ยกเลิกการทดสอบ'**
  String get testCancel;

  /// ถามผู้ใช้ว่าต้องการยกเลิกจริงหรือ
  ///
  /// In th, this message translates to:
  /// **'ผลการทดสอบทั้งหมดจะถูกยกเลิก และจะต้องทดสอบใหม่ คุณต้องการยกเลิกการทดสอบเครื่องหรือไม่'**
  String get testCancelDescription;

  /// หัวข้อคำว่าทดสอบล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'การทดสอบล้มเหลว ต้องการทดสอบใหม่หรือไม่'**
  String get testFailure;

  /// ชื่อหน้า Wi-Fi
  ///
  /// In th, this message translates to:
  /// **'Wi-Fi'**
  String get wifiPageTitle;

  /// รายละเอียดดเกี่ยวกับการทดสอบ Wi-Fi
  ///
  /// In th, this message translates to:
  /// **'• ตรวจสอบว่าคุณได้เปิดใช้งานการเชื่อมต่อ Wi-Fi เรียบร้อยแล้ว\n• เชื่อมต่อกับ Wi-Fi เรียบร้อยแล้ว'**
  String get wifiPageDescription;

  /// ข้อความแจ้งว่าทดสอบ Wi-Fi ล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'เราไม่พบการเชื่อมต่อ Wi-Fi จากอุปกรณ์ของคุณ'**
  String get wifiPageTestFailed;

  /// ชื่อหน้า Bluetooth
  ///
  /// In th, this message translates to:
  /// **'Bluetooth'**
  String get bluetoothPageTitle;

  /// รายละเอียดดเกี่ยวกับการทดสอบ Bluetooth
  ///
  /// In th, this message translates to:
  /// **'• ตรวจสอบว่าคุณได้เปิดใช้งานการเชื่อมต่อ Bluetooth เรียบร้อยแล้ว\n• หากการทดสอบใช้เวลานานเกินไป ให้ลองปิดและเปิดการเชื่อมต่อ Bluetooth อีกครั้ง\n• ทำการเชื่อมต่อกับอุปกรณ์ไร้สายผ่าน Bluetooth (หากมี)'**
  String get bluetoothPageDescription;

  /// ข้อความแจ้งว่าทดสอบ Bluetooth ล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'เราไม่พบการเชื่อมต่อ Bluetooth จากอุปกรณ์ของคุณ'**
  String get bluetoothPageTestFailed;

  /// ชื่อหน้า GPS
  ///
  /// In th, this message translates to:
  /// **'GPS'**
  String get gpsPageTitle;

  /// รายละเอียดเกี่ยวกับการทดสอบ GPS
  ///
  /// In th, this message translates to:
  /// **'• ตรวจสอบว่าคุณได้เปิดใช้งานบริการค้นหาตำแหน่ง (Location) แล้ว\n• ลองขยับมือถือไปมาเล็กน้อยเพื่อปลุกตัวเซ็นเซอร์จับให้ทำงาน'**
  String get gpsPageDescription;

  /// ข้อความแจ้งว่าทดสอบ GPS ล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'เราไม่พบการระบุตำแหน่ง GPS จากอุปกรณ์ของคุณ'**
  String get gpsPageTestFailed;

  /// ชื่อหน้า Flashlight
  ///
  /// In th, this message translates to:
  /// **'Flashlight'**
  String get flashlightPageTitle;

  /// รายละเอียดดการทดสอบระบบไฟฉาย
  ///
  /// In th, this message translates to:
  /// **'• หลังกดปุ่มเริ่มทำการทดสอบแล้ว ให้มองไปที่ด้านหลังมือถือแล้วนับว่าไฟฉายกระพริบกี่ครั้ง จากนั้นให้ตอบคำถามด้านล่าง\n• หากคุณไม่เห็นไฟฉายหรือมองไม่ทัน ให้ลองเริ่มการทดสอบใหม่อีกครั้งโดยการกดปุ่ม \'เริ่มทดสอบใหม่\' ด้านล่าง'**
  String get flashlightPageDescription;

  /// ข้อความแจ้งว่าทดสอบ Flashlight ล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'การทดสอบเปิด-ปิดไฟฉายจากอุปกรณ์ของคุณล้มเหลว'**
  String get flashlightPageTestFailed;

  /// ชื่อหน้า Front Camera
  ///
  /// In th, this message translates to:
  /// **'Front Camera'**
  String get frontCameraPageTitle;

  /// รายละเอียดดเกี่ยวกับการทดสอบ Front Camera
  ///
  /// In th, this message translates to:
  /// **'• หันกล้องด้านหน้าไปยังใบหน้าเพื่อทำการทดสอบกล้อง\n• พยายามประคองกล้องให้อยู่ตรงกลางใบหน้า เพื่อให้การตรวจสอบง่ายขึ้น'**
  String get frontCameraPageDescription;

  /// ข้อความแจ้งว่าการทดสอบ Front Camera ล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'ไม่สามารถจับภาพจากกล้องด้านหน้าของคุณได้'**
  String get frontCameraPageTestFailed;

  /// ชื่อหน้า Back Camera
  ///
  /// In th, this message translates to:
  /// **'Back Camera'**
  String get backCameraPageTitle;

  /// รายละเอียดดเกี่ยวกับการทดสอบ Back Camera
  ///
  /// In th, this message translates to:
  /// **'• หันกล้องไปยังวัตถุเพื่อทำการทดสอบกล้องด้านหลัง\n• พยายามประคองกล้องให้อยู่ตรงกลางวัตถุ เพื่อให้การตรวจสอบง่ายขึ้น'**
  String get backCameraPageDescription;

  /// ชื่อหน้า Microphone
  ///
  /// In th, this message translates to:
  /// **'Microphone'**
  String get microphonePageTitle;

  /// รายละเอียดดเกี่ยวกับการทดสอบ Microphone
  ///
  /// In th, this message translates to:
  /// **'• กดปุ่ม \'เริ่มการทดสอบ\' แล้วพูดใส่บริเวณไมล์เพื่อทำการทดสอบไมโครโฟนก่อนเวลาจะหมด\n• เปล่งเสียงให้ดังจะมีโอกาสสำเร็จมากขึ้น'**
  String get microphonePageDescription;

  /// ข้อความแจ้งให้ผู้ใช้พูดต่อไปเพื่อทดสอบไมโครโฟนต่อ
  ///
  /// In th, this message translates to:
  /// **'กรุณาพูดต่อไปอีก'**
  String get microphoneKeepTalking;

  /// ข้อความแจ้งเวลาที่เหลือของการทดสอบไมโครโฟน
  ///
  /// In th, this message translates to:
  /// **'วินาที'**
  String get microphoneSeconds;

  /// ข้อความแจ้งว่าการทดสอบไมโครโฟนล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'ไม่สามารถจับเสียงจากไมโครโฟนของคุณได้'**
  String get microphoneTestFailed;

  /// ชื่อหน้า Biometrics
  ///
  /// In th, this message translates to:
  /// **'Biometrics'**
  String get biometricsPageTitle;

  /// รายละเอียดเกี่ยวกับการทดสอบ Biometrics
  ///
  /// In th, this message translates to:
  /// **'• กดปุ่ม \'เริ่มการทดสอบ\' แล้วจะมี Pop-up ให้สแกนลายนิ้วมือ (หรือสแกนใบหน้า) ขึ้นเพื่อเริ่มการทดสอบ\n• หากไม่เคยมีการตั้งค่าสแกนลายนิ้วมือ โปรดตั้งค่าสแกนให้เรียบร้อยก่อนดำเนินการต่อ'**
  String get biometricsPageDescription;

  /// ข้อความแจ้งว่าไม่พบลายนิ้วมือ
  ///
  /// In th, this message translates to:
  /// **'ไม่พบลายนิ้วมือ'**
  String get biometricsPageFingerPrintNotFound;

  /// ข้อความแจ้งว่าไม่พบลายนิ้วมือ และขอให้ลองใหม่อีกครั้ง
  ///
  /// In th, this message translates to:
  /// **'ไม่พบลายนิ้วมือในระบบ โปรดเข้าไปเพิ่มในหน้าการตั้งค่า -> ไบโอเมตริค'**
  String get biometricsPageFingerPrintNotFoundDescription;

  /// ข้อความแจ้งว่าเสร็จสิ้นการตั้งค่า Fingerprint แล้ว
  ///
  /// In th, this message translates to:
  /// **'ฉันตั้งค่าแล้ว!'**
  String get biometricsPageFaceIfinishedSettingFaceId;

  /// ปุ่มเปิดหน้าสแกนลายนิ้วมือ
  ///
  /// In th, this message translates to:
  /// **'เปิดหน้าสแกนไบโอรเมตริค'**
  String get biometricsPageOpenScanningPage;

  /// ข้อความแจ้งให้สแกนลายนิ้วมือ
  ///
  /// In th, this message translates to:
  /// **'กรุณาสแกนลายนิ้วมือ/Face ID'**
  String get biometricPagePleaseScanFingerPrint;

  /// ชื่อหน้า Face ID
  ///
  /// In th, this message translates to:
  /// **'Face ID'**
  String get faceIdPageTitle;

  /// รายละเอียดดเกี่ยวกับการทดสอบ Face ID
  ///
  /// In th, this message translates to:
  /// **'• กดปุ่ม \'เริ่มการทดสอบ\' แล้วจะมี Pop-up ให้สแกนใบหน้าผ่านระบบ Face ID ขึ้นเพื่อเริ่มการทดสอบ โปรดทำตามคำแนะนำที่ปรากฏขึ้น\n• หากไม่เคยมีการตั้งค่า Face ID โปรดตั้งค่าสแกนให้เรียบร้อยก่อนดำเนินการต่อ'**
  String get faceIdPageDescription;

  /// ปุ่มเปิดหน้าสแกนใบหน้า
  ///
  /// In th, this message translates to:
  /// **'เปิดหน้าสแกนไบโอรเมทริค'**
  String get faceIdPageOpenScanningPage;

  /// ข้อความแจ้งว่าไม่พบ Face ID หรือลายนิ้วมือ
  ///
  /// In th, this message translates to:
  /// **'ไม่พบ Face ID หรือลายนิ้วมือ'**
  String get faceIdPageFaceIdNotFound;

  /// ข้อความแจ้งว่าไม่พบ Face ID หรือลายนิ้วมือ และขอให้ลองใหม่อีกครั้ง
  ///
  /// In th, this message translates to:
  /// **'ไม่พบลายนิ้วมือในระบบ โปรดเข้าไปเพิ่มในหน้าการตั้งค่า -> ไบโอเมตริค'**
  String get faceIdPageFaceIdNotFoundDescription;

  /// ข้อความแจ้งว่าเสร็จสิ้นการตั้งค่า Face ID แล้ว
  ///
  /// In th, this message translates to:
  /// **'ฉันตั้งค่าแล้ว!'**
  String get faceIdPageFaceIfinishedSettingFaceId;

  /// ชื่อหน้า Battery
  ///
  /// In th, this message translates to:
  /// **'Battery'**
  String get batteryPageTitle;

  /// รายละเอียดเกี่ยวกับการทดสอบ Battery
  ///
  /// In th, this message translates to:
  /// **'• ตรวจสอบให้เรียบร้อยว่าตัวเครื่องกำลังเสียบกับสายชาร์จและเปิดใช้งานอยู่\n• หากใช้ที่ชาร์จไร้สาย โปรดตรวจสอบว่าตัวชาร์จกำลังเสีบลปลั๊กอยู่\n• หากค้างอยู่หน้าทดสอบนาน ให้ลองถอดและเสียบตัวชาร์จใหม่อีกครั้ง'**
  String get batteryPageDescription;

  /// ข้อความแจ้งว่าทดสอบ Battery ล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'เราไม่สามารถตรวจสอบสถานะแบตเตอรี่จากอุปกรณ์ของคุณได้'**
  String get batteryPageTestFailed;

  /// ชื่อหน้า Proximity Sensor
  ///
  /// In th, this message translates to:
  /// **'Proximity Sensor'**
  String get proximitySensorPageTitle;

  /// รายละเอียดเกี่ยวกับการทดสอบ Proximity Sensor
  ///
  /// In th, this message translates to:
  /// **'• เอามือเข้าไปใกล้ๆ จอแล้วค่อยๆ โบกมือช้าๆ เพื่อทำการทดสอบ\n• ตัวเซ็นเซอร์อาจจะใช้เวลาสักครู่เพื่อตรวจจับ หากเซ็นเซอร์ไม่ตอบรับ ให้ลองขยับมือเข้าไปใกล้ๆ จอมากขึ้น'**
  String get proximitySensorPageDescription;

  /// ข้อความแจ้งว่าทดสอบ Proximity Sensor ล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'เราไม่สามารถตรวจสอบ Proximity Sensor จากอุปกรณ์ของคุณได้'**
  String get proximitySensorPageTestFailed;

  /// ข้อความแจ้งว่ามืออยู่ใกล้เซ็นเซอร์
  ///
  /// In th, this message translates to:
  /// **'ตรวจพบ'**
  String get proximitySensorPageIsNear;

  /// ชื่อหน้า Volume Button
  ///
  /// In th, this message translates to:
  /// **'Volume'**
  String get volumeButtonPageTitle;

  /// รายละเอียดเกี่ยวกับการทดสอบ Volume Button
  ///
  /// In th, this message translates to:
  /// **'• กดปุ่มเพิ่ม-ลดเสียงด้านข้างอุปกรณ์ของคุณ จนกว่าจะเห็นสัญลักษณ์ผ่าน'**
  String get volumeButtonPageDescription;

  /// ปุ่มเพิ่มเสียง
  ///
  /// In th, this message translates to:
  /// **'ปุ่มเพิ่มเสียง'**
  String get volumeUpButton;

  /// ปุ่มลดเสียง
  ///
  /// In th, this message translates to:
  /// **'ปุ่มลดเสียง'**
  String get volumeDownButton;

  /// ข้อความว่าการทดสอบปุ่มปรับระดับเสียงล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'เราไม่สามารถทดสอบปุ่มปรับระดับเสียงจากอุปกรณ์ของคุณได้'**
  String get volumePageTestFailed;

  /// ชื่อหน้า Speaker
  ///
  /// In th, this message translates to:
  /// **'Speaker'**
  String get speakerPageTitle;

  /// รายละเอียดเกี่ยวกับการทดสอบ Speaker
  ///
  /// In th, this message translates to:
  /// **'• กด \'เริ่มการทดสอบ\' แล้วฟังเสียงที่ออกมาจากลำโพงของอุปกรณ์ของคุณ จากนั้นตอบคำถามด้านล่าง\n• ถ้าไม่ได้ยินเสียง ให้ลองปรับระดับเสียงให้ดังขึ้นแล้วทดสอบใหม่อีกครั้ง'**
  String get speakerPageDescription;

  /// ข้อความแจ้งว่าทดสอบ Speaker ล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'เสียงลำโพงไม่ได้ทำงานอย่างถูกต้อง'**
  String get speakerPageTestFailed;

  /// ชื่อหน้า Motion
  ///
  /// In th, this message translates to:
  /// **'Motion'**
  String get motionPageTitle;

  /// รายละเอียดเกี่ยวกับการทดสอบ Motion
  ///
  /// In th, this message translates to:
  /// **'• หมุนอุปกรณ์ของคุณไปทางแนวตั้งและแนวนอนเพื่อทดสอบเซ็นเซอร์การเคลื่อนไหวของอุปกรณ์\n• หากไม่มีอะไรเกิดขึ้น ให้ลองเขย่าอุปกรณ์ของคุณดู หรือเปิดฟังค์ชั่นหมุนหน้าจออัติโนมัติแล้วลองอีกครั้ง'**
  String get motionPageDescription;

  /// ข้อความแจ้งว่าทดสอบ Motion แนวตั้ง
  ///
  /// In th, this message translates to:
  /// **'แนวตั้ง'**
  String get motionPageHorizontal;

  /// ข้อความแจ้งว่าทดสอบ Motion แนวนอน
  ///
  /// In th, this message translates to:
  /// **'แนวนอน'**
  String get motionPageVertical;

  /// ชื่อหน้า Screen Capture
  ///
  /// In th, this message translates to:
  /// **'Screen Capture'**
  String get screenCapturePageTitle;

  /// ข้อความบอกให้ผู้ใช้กดปุ่มลดเสียงพร้อมกับปุ่มล็อคหน้าจอเพื่อถ่ายภาพหน้าจอ
  ///
  /// In th, this message translates to:
  /// **'• กดปุ่มลดเสียงพร้อมกับปุ่มล็อคหน้าจอเพื่อถ่ายภาพหน้าจอ\n• ถ้ากดแล้วการถ่ายหน้าจอไม่เกิดขึ้น อาจจะเป็นเพราะอุปกรณ์ของคุณใช้วิธีถ่ายภาพหน้าจอที่แตกต่างจากอุปกรณ์อื่นๆ'**
  String get screenCapturePagePressVolumeDownAndLockScreen;

  /// ข้อความบอกให้ผู้ใช้กดปุ่มเพิ่มเสียงพร้อมกับปุ่มล็อคหน้าจอเพื่อถ่ายภาพหน้าจอ
  ///
  /// In th, this message translates to:
  /// **'• กดปุ่มเพิ่มเสียงพร้อมกับปุ่มล็อคหน้าจอเพื่อถ่ายภาพหน้าจอ'**
  String get screenCapturePagePressVolumeUpAndLockScreen;

  /// ข้อความแจ้งว่าถ่ายภาพหน้าจอเสร็จสมบูรณ์หรือยัง
  ///
  /// In th, this message translates to:
  /// **'ถ่ายภาพหน้าจอเสร็จสมบูรณ์'**
  String get screenCapturePageTestStatus;

  /// ชื่อหน้า Phone Call Speaker
  ///
  /// In th, this message translates to:
  /// **'Phone Call Speaker'**
  String get phoneCallSpeakerPageTitle;

  /// รายละเอียดดเกี่ยวกับการทดสอบ Phone Call Speaker
  ///
  /// In th, this message translates to:
  /// **'• เอาโทรศัพท์แนบหูแล้วฟังเสียงที่ออกมาจากลำโพงแนบหูของอุปกรณ์ของคุณ จากนั้นตอบคำถามด้านล่าง\n• ถ้าไม่มีเสียงออกมา ให้ลองปรับระดับเสียงให้ดังขึ้นแล้วทดสอบใหม่อีกครั้ง'**
  String get phoneCallSpeakerPageDescription;

  /// ข้อความแจ้งว่าทดสอบ Phone Call Speaker ล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'เสียงลำโพงขณะโทรออกไม่ได้ทำงานอย่างถูกต้อง'**
  String get phoneCallSpeakerPageTestFailed;

  /// ชื่อหน้า Vibration
  ///
  /// In th, this message translates to:
  /// **'Vibration'**
  String get vibrationPageTitle;

  /// รายละเอียดเกี่ยวกับการทดสอบ Vibration
  ///
  /// In th, this message translates to:
  /// **'• กดปุ่ม \'เริ่มการทดสอบ\' แล้วตรวจสอบว่าโทรศัพท์สั่นไปแล้วทั้งหมดกี่ครั้ง จากนั้นตอบคำถามด้านล่าง\n• หากไม่มีการสั่นเกิดขึ้นหรือต้องการให้สั่นอีกครั้ง ให้กดปุ่ม \'ทดสอบใหม่อีกครั้ง\' ด้านล่าง'**
  String get vibrationPageDescription;

  /// ข้อความแจ้งว่าทดสอบการสั่นล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'การทดสอบระบบการสั่นล้มเหลว'**
  String get vibrationPageTestFailed;

  /// ชื่อหน้า Screen Touch Start
  ///
  /// In th, this message translates to:
  /// **'Screen Check'**
  String get screenTouchStartPageTitle;

  /// รายละเอียดเกี่ยวกับการทดสอบ Screen Touch Start
  ///
  /// In th, this message translates to:
  /// **'• กดปุ่ม \'เริ่มการทดสอบ\' แล้วระบายหน้าจอเพื่อลบสีที่ปรากฏจนกว่าทั้งหน้าจอจะเป็นสีขาวสะอาด'**
  String get screenTouchStartPageDescription;

  /// ข้อความแจ้งว่าทดสอบการสัมผัสหน้าจอล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'คุณไม่ได้ระบายหน้าจอภายในเวลาที่กำหนด'**
  String get screenTouchStartPageTestFailed;

  /// ปุ่มเริ่มทดสอบการสัมผัสหน้าจอ
  ///
  /// In th, this message translates to:
  /// **'กดปุ่ม \'เริ่มการทดสอบ\' แล้วระบายหน้าจอเพื่อลบสีที่ปรากฏ'**
  String get screenTouchStartTest;

  /// ข้อความแจ้งว่าผลการทดสอบอยู่ในระหว่างการประมวลผล
  ///
  /// In th, this message translates to:
  /// **'กำลังประมวลผลข้อมูล กรุณารอสักครู่...'**
  String get resultLoadingInProcess;

  /// ชื่อหน้า Result
  ///
  /// In th, this message translates to:
  /// **'ผลการตรวจสอบ'**
  String get resultPageTitle;

  /// ข้อความแจ้งให้ผู้ใช้แสดง QR Code ให้เจ้าหน้าที่
  ///
  /// In th, this message translates to:
  /// **'โปรดแสดง QR Code กับเจ้าหน้าที่'**
  String get resultPagePleaseShowQR;

  /// ข้อความแจ้งว่าไม่พบข้อมูล หรือข้อมูลผิดพลาด ให้เริ่มเทสใหม่
  ///
  /// In th, this message translates to:
  /// **'คุณยังไม่ได้เริ่มการประเมินสภาพเครื่อง โปรดกดปุ่มด้านล่างเพื่อเริ่มการประเมินสภาพเครื่อง'**
  String get resultPageNoDataError;

  /// ข้อความแจ้งว่าวันที่ตรวจสอบ
  ///
  /// In th, this message translates to:
  /// **'วันที่ตรวจสอบ'**
  String get resultPageDateOfTest;

  /// ข้อความเก็บ QR Code
  ///
  /// In th, this message translates to:
  /// **'บันทึก QR Code'**
  String get resultPageSaveQR;

  /// ข้อความปิด Hero Tag
  ///
  /// In th, this message translates to:
  /// **'ปิด'**
  String get resultPageClosedHero;

  /// ข้อความแจ้งให้ผู้ใช้สร้าง QR Code
  ///
  /// In th, this message translates to:
  /// **'โปรดแสดง QR Code กับเจ้าหน้าที่ หรือกดที่ QR เพื่อบันทึกรูปลงเครื่อง'**
  String get resultPageCreateQRCode;

  /// ข้อความผ่าน
  ///
  /// In th, this message translates to:
  /// **'ผ่านการตรวจสอบ'**
  String get resultPageTestPassed;

  /// ข้อความล้มเหลว
  ///
  /// In th, this message translates to:
  /// **'ไม่ผ่านการตรวจสอบ'**
  String get resultPageTestFailed;

  /// ข้อความว่ารายการ
  ///
  /// In th, this message translates to:
  /// **'รายการ'**
  String get resultPagePageTotal;

  /// ข้อความแจ้งว่า QR code ซ้ำกับที่เคยบันทึกไว้แล้ว
  ///
  /// In th, this message translates to:
  /// **'QR Code นี้ถูกบันทึกไปแล้ว'**
  String get resultPageDuplicatedQRSave;

  /// ข้อความแจ้งว่า QR Code ได้รับการบันทึกแล้ว
  ///
  /// In th, this message translates to:
  /// **'บันทึก QR Code สำเร็จ'**
  String get resultPageQRSaveSuccess;
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(lookupAppLocalizations(locale));
  }

  @override
  bool isSupported(Locale locale) => <String>['en', 'th'].contains(locale.languageCode);

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}

AppLocalizations lookupAppLocalizations(Locale locale) {


  // Lookup logic when only language code is specified.
  switch (locale.languageCode) {
    case 'en': return AppLocalizationsEn();
    case 'th': return AppLocalizationsTh();
  }

  throw FlutterError(
    'AppLocalizations.delegate failed to load unsupported locale "$locale". This is likely '
    'an issue with the localizations generation tool. Please file an issue '
    'on GitHub with a reproducible sample app and the gen-l10n configuration '
    'that was used.'
  );
}
