import 'app_localizations.dart';

/// The translations for Thai (`th`).
class AppLocalizationsTh extends AppLocalizations {
  AppLocalizationsTh([String locale = 'th']) : super(locale);

  @override
  String get title => 'แอพเช็คสภาพเครื่องมือถือ';

  @override
  String get confirmTxt => 'ยืนยัน';

  @override
  String get okTxt => 'ตกลง';

  @override
  String get returnTxt => 'ย้อนกลับ';

  @override
  String get cancelTxt => 'ยกเลิก';

  @override
  String get errorTxt => 'ผิดพลาด';

  @override
  String get retryTxt => 'ลองอีกครั้ง';

  @override
  String get closeTxt => 'ปิด';

  @override
  String get startTradeButton => 'เริ่มประเมินสภาพกันเลย!';

  @override
  String get refreshTxt => 'รีเฟรช';

  @override
  String get debugReset => '(Debug) รีเซ็ต';

  @override
  String get debugToCheckPage => '(Debug) ตรวจเช็คสภาพเครื่อง';

  @override
  String get debugUnderConstuction => 'หน้านี้ยังไม่เสร็จสมบูรณ์ และอยู่ในระหว่างการพัฒนา โปรดกดข้ามไปหน้าต่อไป';

  @override
  String get bottomAppBarHome => 'หน้าแรก';

  @override
  String get bottomAppBarResultPage => 'ผลการตรวจ';

  @override
  String get homePageUnknownDevice => 'อุปกรณ์ที่ไม่รู้จัก';

  @override
  String get homePageImageNotFound => 'ไม่พบรูปภาพ โปรดกดที่ไอคอนด้านบนเพื่อรีเฟรชหน้า';

  @override
  String get homePageExitPage => 'คุณต้องการออกจากแอพหรือไม่';

  @override
  String get homePageYourDeviceIsNotSupported => 'อุปกรณ์ของคุณอาจไม่รองรับ หรือระบบพบว่าไม่มีข้อมูล โปรดกดปุ่ม \'รีเฟรช\' เพื่อดึงข้อมูลใหม่\nหากยังพบหน้านี้ โปรดติดต่อเจ้าหน้าที่เพื่อขอความช่วยเหลือเพิ่มเติม';

  @override
  String get homePageNoDevice => 'อุปกรณ์ของคุณไม่ได้เข้าร่วมแคมเปญ Trade-in โปรดติดต่อเจ้าหน้าที่เพื่อขอความช่วยเหลือเพิ่มเติม';

  @override
  String get homePagePromotion => 'โปรโมชั่น';

  @override
  String get homePageNoPromotion => 'ไม่มีโปรโมชั่นใดๆ ในขณะนี้';

  @override
  String get webViewHeader => 'โปรโมชั่น';

  @override
  String get preparationBeforeCheckTips => 'คำแนะนำก่อนเริ่มตรวจสอบ';

  @override
  String get preparationPleaseAllowPermission => 'เปิดการใช้งานอุปกรณ์ และให้สิทธิ์เข้าถึงอุปกรณ์ให้พร้อมก่อนเริ่มตรวจสอบ';

  @override
  String get preparationOtherwiseItWillAffectTheResult => 'ถ้าเปิดการใช้งานอุปกรณ์หรือให้สิทธิ์เข้าถึงอุปกรณ์ไม่ครบ จะส่งผลต่อการประเมินราคา';

  @override
  String get preparationIncaseYouForgotPermission => 'หากกดปฏิเสธการให้สิทธิ์ ให้กดที่ไอคอน X แล้วเปิดสิทธิ์ให้หน้าการเข้าถึง';

  @override
  String get preparationLetsBegin => 'เริ่มตรวจสอบกันเลย!';

  @override
  String get preparationPopupWarning => 'คำเตือน!';

  @override
  String get preparationPopupWarningAccessInComplete => 'คุณยังไม่ได้เปิดสิทธิ์อนุญาติแอพครบ\n';

  @override
  String get preparationPopupAreYouSureYouWantToContinues => 'หากดำเนินการต่ออาจจะส่งผลต่อการประเมินราคา ต้องการดำเนินการต่อหรือไม่\n';

  @override
  String get preparationPopupIfYesPressContinues => '• หากใช่ โปรดกดยืนยัน\n';

  @override
  String get preparationPopupOrYouWantToDoItManually => '• หากต้องการจะเปิดสิทธิ์เข้าถึงในแอปก่อนให้เลือก \'ให้อนุญาติสิทธิ์\'';

  @override
  String get preparationPopupGrantPermission => 'ให้อนุญาติสิทธิ์';

  @override
  String get hardwareBasedWifi => 'Wi-fi';

  @override
  String get hardwareBasedWifiDescription => 'เปิด Wi-Fi ให้แอพสามารถเข้าถึง Wi-fi และตรวจสอบการเชื่อมต่อได้';

  @override
  String get hardwareBasedMotion => 'Motion';

  @override
  String get hardwareBasedMotionDescription => 'เปิดการใช้งานหมุนหน้าจอ';

  @override
  String get hardwareBasedBattery => 'Battery';

  @override
  String get hardwareBasedBatteryDescription => 'เสียบแบตเตอรี่ระหว่างการทดสอบ เพื่อให้แอพสามารถตรวจสอบสถานะแบตเตอรี่ได้';

  @override
  String get hardwareBasedGPS => 'GPS';

  @override
  String get hardwareBasedGPSDescription => 'เปิดการใช้งานการระบุตำแหน่ง GPS ให้แอพสามารถตรวจสอบสถานะ GPS ได้';

  @override
  String get hardwareBasedBiometrics => 'Biometrics';

  @override
  String get hardwareBasedBiometricsDescription => 'เปิดการใช้งานระบบสแกนใบหน้า/ลายนิ้วมือ ให้แอพสามารถตรวจสอบระบบสแกนใบหน้า/ลายนิ้วมือ ได้';

  @override
  String get hardwareBasedBluetooth => 'Bluetooth';

  @override
  String get hardwareBasedBluetoothDescription => 'เปิดการใช้งาน Bluetooth ให้แอพสามารถตรวจสอบสถานะ Bluetooth ได้';

  @override
  String get permissionBasedMicrophone => 'Microphone';

  @override
  String get permissionBasedMicrophoneDescription => 'เปิดให้แอพสามารถเข้าถึงสิทธิ์ไมโครโฟน';

  @override
  String get permissionBasedCamera => 'Camera';

  @override
  String get permissionBasedCameraDescription => 'เปิดให้แอพสามารถเข้าถึงสิทธิ์กล้องถ่ายรูป';

  @override
  String get permissionBasedStorage => 'Storage';

  @override
  String get permissionBasedStorageDescription => 'เปิดให้แอพสามารถเข้าถึงสิทธิ์พื้นที่เก็บข้อมูล';

  @override
  String get layoutPageTitle => 'ตรวจสอบอุปกรณ์';

  @override
  String get layoutSkipButton => 'ข้ามขั้นตอน';

  @override
  String get layoutPageStartTest => 'เริ่มการทดสอบ';

  @override
  String get layoutPageRestart => 'ทดสอบใหม่อีกครั้ง';

  @override
  String get layoutStep => 'ขั้นตอนที่ : ';

  @override
  String get layoutTips => 'คำแนะนำ ';

  @override
  String get countdownWidgetDescription => 'นับถอยหลัง...';

  @override
  String get countdownWidgetSeconds => 'วินาที';

  @override
  String get countdownWidgetReset => 'ทดสอบใหม่อีกครั้ง';

  @override
  String get testSuccess => 'การทดสอบเสร็จสมบูรณ์';

  @override
  String get testCancel => 'ยกเลิกการทดสอบ';

  @override
  String get testCancelDescription => 'ผลการทดสอบทั้งหมดจะถูกยกเลิก และจะต้องทดสอบใหม่ คุณต้องการยกเลิกการทดสอบเครื่องหรือไม่';

  @override
  String get testFailure => 'การทดสอบล้มเหลว ต้องการทดสอบใหม่หรือไม่';

  @override
  String get wifiPageTitle => 'Wi-Fi';

  @override
  String get wifiPageDescription => '• ตรวจสอบว่าคุณได้เปิดใช้งานการเชื่อมต่อ Wi-Fi เรียบร้อยแล้ว\n• เชื่อมต่อกับ Wi-Fi เรียบร้อยแล้ว';

  @override
  String get wifiPageTestFailed => 'เราไม่พบการเชื่อมต่อ Wi-Fi จากอุปกรณ์ของคุณ';

  @override
  String get bluetoothPageTitle => 'Bluetooth';

  @override
  String get bluetoothPageDescription => '• ตรวจสอบว่าคุณได้เปิดใช้งานการเชื่อมต่อ Bluetooth เรียบร้อยแล้ว\n• หากการทดสอบใช้เวลานานเกินไป ให้ลองปิดและเปิดการเชื่อมต่อ Bluetooth อีกครั้ง\n• ทำการเชื่อมต่อกับอุปกรณ์ไร้สายผ่าน Bluetooth (หากมี)';

  @override
  String get bluetoothPageTestFailed => 'เราไม่พบการเชื่อมต่อ Bluetooth จากอุปกรณ์ของคุณ';

  @override
  String get gpsPageTitle => 'GPS';

  @override
  String get gpsPageDescription => '• ตรวจสอบว่าคุณได้เปิดใช้งานบริการค้นหาตำแหน่ง (Location) แล้ว\n• ลองขยับมือถือไปมาเล็กน้อยเพื่อปลุกตัวเซ็นเซอร์จับให้ทำงาน';

  @override
  String get gpsPageTestFailed => 'เราไม่พบการระบุตำแหน่ง GPS จากอุปกรณ์ของคุณ';

  @override
  String get flashlightPageTitle => 'Flashlight';

  @override
  String get flashlightPageDescription => '• หลังกดปุ่มเริ่มทำการทดสอบแล้ว ให้มองไปที่ด้านหลังมือถือแล้วนับว่าไฟฉายกระพริบกี่ครั้ง จากนั้นให้ตอบคำถามด้านล่าง\n• หากคุณไม่เห็นไฟฉายหรือมองไม่ทัน ให้ลองเริ่มการทดสอบใหม่อีกครั้งโดยการกดปุ่ม \'เริ่มทดสอบใหม่\' ด้านล่าง';

  @override
  String get flashlightPageTestFailed => 'การทดสอบเปิด-ปิดไฟฉายจากอุปกรณ์ของคุณล้มเหลว';

  @override
  String get frontCameraPageTitle => 'Front Camera';

  @override
  String get frontCameraPageDescription => '• หันกล้องด้านหน้าไปยังใบหน้าเพื่อทำการทดสอบกล้อง\n• พยายามประคองกล้องให้อยู่ตรงกลางใบหน้า เพื่อให้การตรวจสอบง่ายขึ้น';

  @override
  String get frontCameraPageTestFailed => 'ไม่สามารถจับภาพจากกล้องด้านหน้าของคุณได้';

  @override
  String get backCameraPageTitle => 'Back Camera';

  @override
  String get backCameraPageDescription => '• หันกล้องไปยังวัตถุเพื่อทำการทดสอบกล้องด้านหลัง\n• พยายามประคองกล้องให้อยู่ตรงกลางวัตถุ เพื่อให้การตรวจสอบง่ายขึ้น';

  @override
  String get microphonePageTitle => 'Microphone';

  @override
  String get microphonePageDescription => '• กดปุ่ม \'เริ่มการทดสอบ\' แล้วพูดใส่บริเวณไมล์เพื่อทำการทดสอบไมโครโฟนก่อนเวลาจะหมด\n• เปล่งเสียงให้ดังจะมีโอกาสสำเร็จมากขึ้น';

  @override
  String get microphoneKeepTalking => 'กรุณาพูดต่อไปอีก';

  @override
  String get microphoneSeconds => 'วินาที';

  @override
  String get microphoneTestFailed => 'ไม่สามารถจับเสียงจากไมโครโฟนของคุณได้';

  @override
  String get biometricsPageTitle => 'Biometrics';

  @override
  String get biometricsPageDescription => '• กดปุ่ม \'เริ่มการทดสอบ\' แล้วจะมี Pop-up ให้สแกนลายนิ้วมือ (หรือสแกนใบหน้า) ขึ้นเพื่อเริ่มการทดสอบ\n• หากไม่เคยมีการตั้งค่าสแกนลายนิ้วมือ โปรดตั้งค่าสแกนให้เรียบร้อยก่อนดำเนินการต่อ';

  @override
  String get biometricsPageFingerPrintNotFound => 'ไม่พบลายนิ้วมือ';

  @override
  String get biometricsPageFingerPrintNotFoundDescription => 'ไม่พบลายนิ้วมือในระบบ โปรดเข้าไปเพิ่มในหน้าการตั้งค่า -> ไบโอเมตริค';

  @override
  String get biometricsPageFaceIfinishedSettingFaceId => 'ฉันตั้งค่าแล้ว!';

  @override
  String get biometricsPageOpenScanningPage => 'เปิดหน้าสแกนไบโอรเมตริค';

  @override
  String get biometricPagePleaseScanFingerPrint => 'กรุณาสแกนลายนิ้วมือ/Face ID';

  @override
  String get faceIdPageTitle => 'Face ID';

  @override
  String get faceIdPageDescription => '• กดปุ่ม \'เริ่มการทดสอบ\' แล้วจะมี Pop-up ให้สแกนใบหน้าผ่านระบบ Face ID ขึ้นเพื่อเริ่มการทดสอบ โปรดทำตามคำแนะนำที่ปรากฏขึ้น\n• หากไม่เคยมีการตั้งค่า Face ID โปรดตั้งค่าสแกนให้เรียบร้อยก่อนดำเนินการต่อ';

  @override
  String get faceIdPageOpenScanningPage => 'เปิดหน้าสแกนไบโอรเมทริค';

  @override
  String get faceIdPageFaceIdNotFound => 'ไม่พบ Face ID หรือลายนิ้วมือ';

  @override
  String get faceIdPageFaceIdNotFoundDescription => 'ไม่พบลายนิ้วมือในระบบ โปรดเข้าไปเพิ่มในหน้าการตั้งค่า -> ไบโอเมตริค';

  @override
  String get faceIdPageFaceIfinishedSettingFaceId => 'ฉันตั้งค่าแล้ว!';

  @override
  String get batteryPageTitle => 'Battery';

  @override
  String get batteryPageDescription => '• ตรวจสอบให้เรียบร้อยว่าตัวเครื่องกำลังเสียบกับสายชาร์จและเปิดใช้งานอยู่\n• หากใช้ที่ชาร์จไร้สาย โปรดตรวจสอบว่าตัวชาร์จกำลังเสีบลปลั๊กอยู่\n• หากค้างอยู่หน้าทดสอบนาน ให้ลองถอดและเสียบตัวชาร์จใหม่อีกครั้ง';

  @override
  String get batteryPageTestFailed => 'เราไม่สามารถตรวจสอบสถานะแบตเตอรี่จากอุปกรณ์ของคุณได้';

  @override
  String get proximitySensorPageTitle => 'Proximity Sensor';

  @override
  String get proximitySensorPageDescription => '• เอามือเข้าไปใกล้ๆ จอแล้วค่อยๆ โบกมือช้าๆ เพื่อทำการทดสอบ\n• ตัวเซ็นเซอร์อาจจะใช้เวลาสักครู่เพื่อตรวจจับ หากเซ็นเซอร์ไม่ตอบรับ ให้ลองขยับมือเข้าไปใกล้ๆ จอมากขึ้น';

  @override
  String get proximitySensorPageTestFailed => 'เราไม่สามารถตรวจสอบ Proximity Sensor จากอุปกรณ์ของคุณได้';

  @override
  String get proximitySensorPageIsNear => 'ตรวจพบ';

  @override
  String get volumeButtonPageTitle => 'Volume';

  @override
  String get volumeButtonPageDescription => '• กดปุ่มเพิ่ม-ลดเสียงด้านข้างอุปกรณ์ของคุณ จนกว่าจะเห็นสัญลักษณ์ผ่าน';

  @override
  String get volumeUpButton => 'ปุ่มเพิ่มเสียง';

  @override
  String get volumeDownButton => 'ปุ่มลดเสียง';

  @override
  String get volumePageTestFailed => 'เราไม่สามารถทดสอบปุ่มปรับระดับเสียงจากอุปกรณ์ของคุณได้';

  @override
  String get speakerPageTitle => 'Speaker';

  @override
  String get speakerPageDescription => '• กด \'เริ่มการทดสอบ\' แล้วฟังเสียงที่ออกมาจากลำโพงของอุปกรณ์ของคุณ จากนั้นตอบคำถามด้านล่าง\n• ถ้าไม่ได้ยินเสียง ให้ลองปรับระดับเสียงให้ดังขึ้นแล้วทดสอบใหม่อีกครั้ง';

  @override
  String get speakerPageTestFailed => 'เสียงลำโพงไม่ได้ทำงานอย่างถูกต้อง';

  @override
  String get motionPageTitle => 'Motion';

  @override
  String get motionPageDescription => '• หมุนอุปกรณ์ของคุณไปทางแนวตั้งและแนวนอนเพื่อทดสอบเซ็นเซอร์การเคลื่อนไหวของอุปกรณ์\n• หากไม่มีอะไรเกิดขึ้น ให้ลองเขย่าอุปกรณ์ของคุณดู หรือเปิดฟังค์ชั่นหมุนหน้าจออัติโนมัติแล้วลองอีกครั้ง';

  @override
  String get motionPageHorizontal => 'แนวตั้ง';

  @override
  String get motionPageVertical => 'แนวนอน';

  @override
  String get screenCapturePageTitle => 'Screen Capture';

  @override
  String get screenCapturePagePressVolumeDownAndLockScreen => '• กดปุ่มลดเสียงพร้อมกับปุ่มล็อคหน้าจอเพื่อถ่ายภาพหน้าจอ\n• ถ้ากดแล้วการถ่ายหน้าจอไม่เกิดขึ้น อาจจะเป็นเพราะอุปกรณ์ของคุณใช้วิธีถ่ายภาพหน้าจอที่แตกต่างจากอุปกรณ์อื่นๆ';

  @override
  String get screenCapturePagePressVolumeUpAndLockScreen => '• กดปุ่มเพิ่มเสียงพร้อมกับปุ่มล็อคหน้าจอเพื่อถ่ายภาพหน้าจอ';

  @override
  String get screenCapturePageTestStatus => 'ถ่ายภาพหน้าจอเสร็จสมบูรณ์';

  @override
  String get phoneCallSpeakerPageTitle => 'Phone Call Speaker';

  @override
  String get phoneCallSpeakerPageDescription => '• เอาโทรศัพท์แนบหูแล้วฟังเสียงที่ออกมาจากลำโพงแนบหูของอุปกรณ์ของคุณ จากนั้นตอบคำถามด้านล่าง\n• ถ้าไม่มีเสียงออกมา ให้ลองปรับระดับเสียงให้ดังขึ้นแล้วทดสอบใหม่อีกครั้ง';

  @override
  String get phoneCallSpeakerPageTestFailed => 'เสียงลำโพงขณะโทรออกไม่ได้ทำงานอย่างถูกต้อง';

  @override
  String get vibrationPageTitle => 'Vibration';

  @override
  String get vibrationPageDescription => '• กดปุ่ม \'เริ่มการทดสอบ\' แล้วตรวจสอบว่าโทรศัพท์สั่นไปแล้วทั้งหมดกี่ครั้ง จากนั้นตอบคำถามด้านล่าง\n• หากไม่มีการสั่นเกิดขึ้นหรือต้องการให้สั่นอีกครั้ง ให้กดปุ่ม \'ทดสอบใหม่อีกครั้ง\' ด้านล่าง';

  @override
  String get vibrationPageTestFailed => 'การทดสอบระบบการสั่นล้มเหลว';

  @override
  String get screenTouchStartPageTitle => 'Screen Check';

  @override
  String get screenTouchStartPageDescription => '• กดปุ่ม \'เริ่มการทดสอบ\' แล้วระบายหน้าจอเพื่อลบสีที่ปรากฏจนกว่าทั้งหน้าจอจะเป็นสีขาวสะอาด';

  @override
  String get screenTouchStartPageTestFailed => 'คุณไม่ได้ระบายหน้าจอภายในเวลาที่กำหนด';

  @override
  String get screenTouchStartTest => 'กดปุ่ม \'เริ่มการทดสอบ\' แล้วระบายหน้าจอเพื่อลบสีที่ปรากฏ';

  @override
  String get resultLoadingInProcess => 'กำลังประมวลผลข้อมูล กรุณารอสักครู่...';

  @override
  String get resultPageTitle => 'ผลการตรวจสอบ';

  @override
  String get resultPagePleaseShowQR => 'โปรดแสดง QR Code กับเจ้าหน้าที่';

  @override
  String get resultPageNoDataError => 'คุณยังไม่ได้เริ่มการประเมินสภาพเครื่อง โปรดกดปุ่มด้านล่างเพื่อเริ่มการประเมินสภาพเครื่อง';

  @override
  String get resultPageDateOfTest => 'วันที่ตรวจสอบ';

  @override
  String get resultPageSaveQR => 'บันทึก QR Code';

  @override
  String get resultPageClosedHero => 'ปิด';

  @override
  String get resultPageCreateQRCode => 'โปรดแสดง QR Code กับเจ้าหน้าที่ หรือกดที่ QR เพื่อบันทึกรูปลงเครื่อง';

  @override
  String get resultPageTestPassed => 'ผ่านการตรวจสอบ';

  @override
  String get resultPageTestFailed => 'ไม่ผ่านการตรวจสอบ';

  @override
  String get resultPagePageTotal => 'รายการ';

  @override
  String get resultPageDuplicatedQRSave => 'QR Code นี้ถูกบันทึกไปแล้ว';

  @override
  String get resultPageQRSaveSuccess => 'บันทึก QR Code สำเร็จ';
}
