import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get title => 'Trade-in Checking Mobile App';

  @override
  String get confirmTxt => 'Confirm';

  @override
  String get okTxt => 'Okay';

  @override
  String get returnTxt => 'Return';

  @override
  String get cancelTxt => 'Cancel';

  @override
  String get errorTxt => 'Error';

  @override
  String get retryTxt => 'Retry';

  @override
  String get closeTxt => 'Close';

  @override
  String get startTradeButton => 'Start evaluating now!';

  @override
  String get refreshTxt => 'Refresh';

  @override
  String get debugReset => '(Debug) Reset';

  @override
  String get debugToCheckPage => '(Debug) To Check Page';

  @override
  String get debugUnderConstuction => 'Under Construction, Please Skip the test.';

  @override
  String get bottomAppBarHome => 'Homepage';

  @override
  String get bottomAppBarResultPage => 'Result';

  @override
  String get homePageUnknownDevice => 'Unknown Device';

  @override
  String get homePageImageNotFound => 'An error occured. Press the icon above to refresh the page.';

  @override
  String get homePageExitPage => 'Would you like to exit the app?';

  @override
  String get homePageYourDeviceIsNotSupported => 'Your device may not supported. Or retriving data incorrectly, Try pressing \'Refresh\' to retrieve data again. \n If this issue persists, Please contact the staff for further assistance.';

  @override
  String get homePageNoDevice => 'Your device did not join the campaign. Please contact the staff for further assistance.';

  @override
  String get homePagePromotion => 'Promotion';

  @override
  String get homePageNoPromotion => 'No promotion available';

  @override
  String get webViewHeader => 'Promotion';

  @override
  String get preparationBeforeCheckTips => 'Preparation';

  @override
  String get preparationPleaseAllowPermission => 'Please grant permissions to allow the app to access your device information';

  @override
  String get preparationOtherwiseItWillAffectTheResult => 'If you don\'t allow the app access to your devices, it could affect the price evaluation. ';

  @override
  String get preparationIncaseYouForgotPermission => ' If you forgot to grant permission/enabling certain system services, you can press the X icon and grant the permission/go to setting page manually.';

  @override
  String get preparationLetsBegin => 'Let\'s Begin!';

  @override
  String get preparationPopupWarning => 'Warning!';

  @override
  String get preparationPopupWarningAccessInComplete => 'You haven\'t finished granting permission.\n';

  @override
  String get preparationPopupAreYouSureYouWantToContinues => 'If you continue, It could potentially affected the price evaluation. Are you sure you want to continue without granting permission?\n';

  @override
  String get preparationPopupIfYesPressContinues => '• If yes, Press \'Continue\'.\n';

  @override
  String get preparationPopupOrYouWantToDoItManually => '• If you want to grant permission manually, Press \'Grant Permission\'';

  @override
  String get preparationPopupGrantPermission => 'Grant Permission';

  @override
  String get hardwareBasedWifi => 'Wi-fi';

  @override
  String get hardwareBasedWifiDescription => 'Enable Wi-Fi and let the app access to your connection hardware.';

  @override
  String get hardwareBasedMotion => 'Motion';

  @override
  String get hardwareBasedMotionDescription => 'Enable automatic screen orientation.';

  @override
  String get hardwareBasedBattery => 'Battery';

  @override
  String get hardwareBasedBatteryDescription => 'Plug in your battery charger to let the app get access to your battery hardware.';

  @override
  String get hardwareBasedGPS => 'GPS';

  @override
  String get hardwareBasedGPSDescription => 'Enabling GPS to let the app get access and checking your GPS hardware.';

  @override
  String get hardwareBasedBiometrics => 'Biometrics';

  @override
  String get hardwareBasedBiometricsDescription => 'Enable face ID/fingerprint scanner to let the app get access to your biometrics hardware.';

  @override
  String get hardwareBasedBluetooth => 'Bluetooth';

  @override
  String get hardwareBasedBluetoothDescription => 'Enable Bluetooth to let the app get access to your Bluetooth hardware.';

  @override
  String get permissionBasedMicrophone => 'Microphone';

  @override
  String get permissionBasedMicrophoneDescription => 'Enable microphone access to allow the app to test your microphone device.';

  @override
  String get permissionBasedCamera => 'Camera';

  @override
  String get permissionBasedCameraDescription => 'Enable camera access to allow the app to test your camera hardware.';

  @override
  String get permissionBasedStorage => 'Storage';

  @override
  String get permissionBasedStorageDescription => 'Enable storage access.';

  @override
  String get layoutPageTitle => 'Device Testing';

  @override
  String get layoutSkipButton => 'Skip this step';

  @override
  String get layoutPageStartTest => 'Start the test';

  @override
  String get layoutPageRestart => 'Restart the test';

  @override
  String get layoutStep => 'Step : ';

  @override
  String get layoutTips => 'Tips ';

  @override
  String get countdownWidgetDescription => 'Counting Down...';

  @override
  String get countdownWidgetSeconds => 'Seconds';

  @override
  String get countdownWidgetReset => 'Reset';

  @override
  String get testSuccess => 'Test Passed';

  @override
  String get testCancel => 'Cancelling Test?';

  @override
  String get testCancelDescription => 'All of your test data will be lost, and you will need to restart. Are you sure you want to cancel the test?';

  @override
  String get testFailure => 'Test Failed';

  @override
  String get wifiPageTitle => 'Wi-Fi';

  @override
  String get wifiPageDescription => '• Make sure your Wi-Fi service you have in your device turned on.\n• Connect to a Wi-Fi network.';

  @override
  String get wifiPageTestFailed => 'We cannot detect any Wi-Fi connection.';

  @override
  String get bluetoothPageTitle => 'Bluetooth';

  @override
  String get bluetoothPageDescription => '• Make sure your Bluetooth service is turned on during the test.\n• If the test is taking too long, try turning Bluetooth service off and on again first.\n• If you have wireless device with Bluetooth capability, try connecting it to your device.';

  @override
  String get bluetoothPageTestFailed => 'We cannot detect any Bluetooth connection.';

  @override
  String get gpsPageTitle => 'GPS';

  @override
  String get gpsPageDescription => '• Make sure your GPS service is turned on during the test\n• Try moving your phone around a bit to give the GPS sensor activated.';

  @override
  String get gpsPageTestFailed => 'We cannot detect any GPS connection.';

  @override
  String get flashlightPageTitle => 'Flashlight';

  @override
  String get flashlightPageDescription => '• After start the test, Look behind your phone and count how many times the flash blinks. Then answer the question to continue.\n• If you don\'t see any flash or not sure about the answer, try restart the test one more times by pressing \'Restart The Test\' button below.';

  @override
  String get flashlightPageTestFailed => 'We cannot turn on/off the torch on your device.';

  @override
  String get frontCameraPageTitle => 'Front Camera';

  @override
  String get frontCameraPageDescription => '• Please turn your device\'s camera toward your face.\n• Try to keep the camera in the center of your face for easier detection.';

  @override
  String get frontCameraPageTestFailed => 'We cannot detect front camera available from your device\'s.';

  @override
  String get backCameraPageTitle => 'Back Camera';

  @override
  String get backCameraPageDescription => '• Please turn your device\'s camera toward any object in front of you.\n• Try to steady the camera for better detection.';

  @override
  String get microphonePageTitle => 'Microphone';

  @override
  String get microphonePageDescription => '• Press \'Start The Test\' and speak to your device\'s microphone before the time ran out.\n• Try speaking louder to increase your chance of success.';

  @override
  String get microphoneKeepTalking => 'Keep talking for';

  @override
  String get microphoneSeconds => 'Seconds';

  @override
  String get microphoneTestFailed => 'We cannot detect any sound from your device\'s microphone.';

  @override
  String get biometricsPageTitle => 'Biometrics';

  @override
  String get biometricsPageDescription => '• Press \'Start the Test\' there will be a pop-up asking for your biometrics data (such as your fingerprint) for authentication. Please follow the instruction to continue.\n• If you never setup your biometrics. Please go to Setting page and setup your biometrics before continue.';

  @override
  String get biometricsPageFingerPrintNotFound => 'Fingerprint not found';

  @override
  String get biometricsPageFingerPrintNotFoundDescription => 'We couldn\'t find your fingerprint. Please go into Setting -> Biometrics (Fingerprint) and set up.';

  @override
  String get biometricsPageFaceIfinishedSettingFaceId => 'I finished setting up Fingerprint ID';

  @override
  String get biometricsPageOpenScanningPage => 'Open Scanning Page';

  @override
  String get biometricPagePleaseScanFingerPrint => 'Please scan your fingerprint/Face ID';

  @override
  String get faceIdPageTitle => 'Face ID';

  @override
  String get faceIdPageDescription => '• Press \'Start the Test\' there will be a pop-up asking for your Face ID for authentication. Please follow the instruction to continue.\n• If you never setup your Face ID. Please go to Setting page and setup your Face ID before continue.';

  @override
  String get faceIdPageOpenScanningPage => 'Open Scanning Page';

  @override
  String get faceIdPageFaceIdNotFound => 'Face ID not found';

  @override
  String get faceIdPageFaceIdNotFoundDescription => 'We couldn\'t find your Face ID. Please go into Setting -> Biometrics (Face ID) and set up.';

  @override
  String get faceIdPageFaceIfinishedSettingFaceId => 'I finished setting up Face ID';

  @override
  String get batteryPageTitle => 'Battery';

  @override
  String get batteryPageDescription => '• Make sure your device is currently plugged-in to the charger.\n• If you are using a wireless charger, please make sure it is properly connected to the power source.\n• If the test is taking too long. Try unplug before plugging in again.';

  @override
  String get batteryPageTestFailed => 'We cannot detect any battery connection.';

  @override
  String get proximitySensorPageTitle => 'Proximity Sensor';

  @override
  String get proximitySensorPageDescription => '• Please cover your hand a few inches above your device\'s screen and wave slowly to test the proximity sensor.\n• It may take a few seconds for the sensor to detecting movement. If the sensor did not detect your movement, Try moving your hand closer toward screen.';

  @override
  String get proximitySensorPageTestFailed => 'We cannot detect any proximity sensor connection.';

  @override
  String get proximitySensorPageIsNear => 'Detected';

  @override
  String get volumeButtonPageTitle => 'Volume';

  @override
  String get volumeButtonPageDescription => '• Press the up and down volume button on the side of your device to test the volume button.';

  @override
  String get volumeUpButton => 'Volume Up';

  @override
  String get volumeDownButton => 'Volume Down';

  @override
  String get volumePageTestFailed => 'We cannot detect any volume being pressed.';

  @override
  String get speakerPageTitle => 'Speaker';

  @override
  String get speakerPageDescription => '• Press \'Start the Test\' and listen to the sound from your device\'s speaker. Then answer the question to continue.\n• If you don\'t hear any sound, try increasing the volume or restart the test by pressing \'Restart The Test\' button below.';

  @override
  String get speakerPageTestFailed => 'We cannot detect any sound from your device.';

  @override
  String get motionPageTitle => 'Motion';

  @override
  String get motionPageDescription => '• Rotate your device horizontally/vertically to test the motion sensor.\n• If nothing happened, Try shaking your device or enable auto-rotate feature on your device.';

  @override
  String get motionPageHorizontal => 'Horizontal';

  @override
  String get motionPageVertical => 'Vertical';

  @override
  String get screenCapturePageTitle => 'Screen Capture';

  @override
  String get screenCapturePagePressVolumeDownAndLockScreen => '• Press the volume down and lock screen button to continues.\n• If that doesn\'t work. Your device may use a different button/method to capture the screen.';

  @override
  String get screenCapturePagePressVolumeUpAndLockScreen => '• Press the volume up and lock screen button to continues.';

  @override
  String get screenCapturePageTestStatus => 'Screen Captured';

  @override
  String get phoneCallSpeakerPageTitle => 'Phone Call Speaker';

  @override
  String get phoneCallSpeakerPageDescription => '• Put your phone near your ear, listen to the sound then answer the question to continues\n• If you don\'t hear any sound, try increasing the volume or restart the test by pressing \'Restart The Test\' button below.';

  @override
  String get phoneCallSpeakerPageTestFailed => 'We cannot detect any sound from your phone call speaker.';

  @override
  String get vibrationPageTitle => 'Vibration';

  @override
  String get vibrationPageDescription => '• Press \'Start the Test\' and feel the vibration from your device. Then answer the question to continue.\n• If you don\'t feel any vibration or would like to try again, Try restart the test by pressing \'Restart The Test\' button below.';

  @override
  String get vibrationPageTestFailed => 'We cannot detect any vibration from your device.';

  @override
  String get screenTouchStartPageTitle => 'Screen Check';

  @override
  String get screenTouchStartPageDescription => '• Press \'Start The Test\' and clear all the color on your screen within the time limit.';

  @override
  String get screenTouchStartPageTestFailed => 'You did not clear the screen\'s color in time limit.';

  @override
  String get screenTouchStartTest => 'Press \'Start The Test\' and clear all the color on your screen within the time limit.';

  @override
  String get resultLoadingInProcess => 'Result is in process Please wait...';

  @override
  String get resultPageTitle => 'Result';

  @override
  String get resultPagePleaseShowQR => 'Please show QR Code image to the staff';

  @override
  String get resultPageNoDataError => 'You haven\'t start testing your devices yet. Please press the button below to start testing.';

  @override
  String get resultPageDateOfTest => 'Test Date';

  @override
  String get resultPageSaveQR => 'Save QR Code';

  @override
  String get resultPageClosedHero => 'Closed';

  @override
  String get resultPageCreateQRCode => 'Please show this QR code to the staff or tap the QR Code to save it to your device.';

  @override
  String get resultPageTestPassed => 'Test Passed';

  @override
  String get resultPageTestFailed => 'Test Failed';

  @override
  String get resultPagePageTotal => 'Pages';

  @override
  String get resultPageDuplicatedQRSave => 'You already saved this QR code.';

  @override
  String get resultPageQRSaveSuccess => 'QR Code has been saved to your device.';
}
