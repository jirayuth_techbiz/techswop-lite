/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vector_graphics/vector_graphics.dart';

class $AssetsAnimationGen {
  const $AssetsAnimationGen();

  /// File path: assets/animation/test-failed.json
  String get testFailed => 'assets/animation/test-failed.json';

  /// File path: assets/animation/test-passed.json
  String get testPassed => 'assets/animation/test-passed.json';

  /// List of all assets
  List<String> get values => [testFailed, testPassed];
}

class $AssetsDesignGen {
  const $AssetsDesignGen();

  /// File path: assets/design/branding.png
  AssetGenImage get branding =>
      const AssetGenImage('assets/design/branding.png');

  /// File path: assets/design/branding_temp.png
  AssetGenImage get brandingTemp =>
      const AssetGenImage('assets/design/branding_temp.png');

  /// File path: assets/design/i-phone-8.jpg
  AssetGenImage get iPhone8 =>
      const AssetGenImage('assets/design/i-phone-8.jpg');

  /// File path: assets/design/preparation_th.png
  AssetGenImage get preparationTh =>
      const AssetGenImage('assets/design/preparation_th.png');

  /// File path: assets/design/techbiz-logo_512_lite.png
  AssetGenImage get techbizLogo512Lite =>
      const AssetGenImage('assets/design/techbiz-logo_512_lite.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [branding, brandingTemp, iPhone8, preparationTh, techbizLogo512Lite];
}

class $AssetsFontsGen {
  const $AssetsFontsGen();

  /// File path: assets/fonts/sukhumvit_set_bold.ttf
  String get sukhumvitSetBold => 'assets/fonts/sukhumvit_set_bold.ttf';

  /// File path: assets/fonts/sukhumvit_set_light.ttf
  String get sukhumvitSetLight => 'assets/fonts/sukhumvit_set_light.ttf';

  /// File path: assets/fonts/sukhumvit_set_medium.ttf
  String get sukhumvitSetMedium => 'assets/fonts/sukhumvit_set_medium.ttf';

  /// File path: assets/fonts/sukhumvit_set_semi_bold.ttf
  String get sukhumvitSetSemiBold => 'assets/fonts/sukhumvit_set_semi_bold.ttf';

  /// File path: assets/fonts/sukhumvit_set_text.ttf
  String get sukhumvitSetText => 'assets/fonts/sukhumvit_set_text.ttf';

  /// File path: assets/fonts/sukhumvit_set_thin.ttf
  String get sukhumvitSetThin => 'assets/fonts/sukhumvit_set_thin.ttf';

  /// List of all assets
  List<String> get values => [
        sukhumvitSetBold,
        sukhumvitSetLight,
        sukhumvitSetMedium,
        sukhumvitSetSemiBold,
        sukhumvitSetText,
        sukhumvitSetThin
      ];
}

class $AssetsIconGen {
  const $AssetsIconGen();

  /// Directory path: assets/icon/tech_biz_custom
  $AssetsIconTechBizCustomGen get techBizCustom =>
      const $AssetsIconTechBizCustomGen();

  /// File path: assets/icon/test-exit.svg
  SvgGenImage get testExit => const SvgGenImage('assets/icon/test-exit.svg');

  /// List of all assets
  List<SvgGenImage> get values => [testExit];
}

class $AssetsSoundGen {
  const $AssetsSoundGen();

  /// File path: assets/sound/testsound.wav
  String get testsound => 'assets/sound/testsound.wav';

  /// List of all assets
  List<String> get values => [testsound];
}

class $AssetsIconTechBizCustomGen {
  const $AssetsIconTechBizCustomGen();

  /// File path: assets/icon/tech_biz_custom/Touch Screen.svg
  SvgGenImage get touchScreen =>
      const SvgGenImage('assets/icon/tech_biz_custom/Touch Screen.svg');

  /// File path: assets/icon/tech_biz_custom/back camera.svg
  SvgGenImage get backCamera =>
      const SvgGenImage('assets/icon/tech_biz_custom/back camera.svg');

  /// File path: assets/icon/tech_biz_custom/battery.svg
  SvgGenImage get battery =>
      const SvgGenImage('assets/icon/tech_biz_custom/battery.svg');

  /// File path: assets/icon/tech_biz_custom/bio.svg
  SvgGenImage get bio =>
      const SvgGenImage('assets/icon/tech_biz_custom/bio.svg');

  /// File path: assets/icon/tech_biz_custom/bluetooth.svg
  SvgGenImage get bluetooth =>
      const SvgGenImage('assets/icon/tech_biz_custom/bluetooth.svg');

  /// File path: assets/icon/tech_biz_custom/camera.svg
  SvgGenImage get camera =>
      const SvgGenImage('assets/icon/tech_biz_custom/camera.svg');

  /// File path: assets/icon/tech_biz_custom/fingerprint.svg
  SvgGenImage get fingerprint =>
      const SvgGenImage('assets/icon/tech_biz_custom/fingerprint.svg');

  /// File path: assets/icon/tech_biz_custom/flashlight.svg
  SvgGenImage get flashlight =>
      const SvgGenImage('assets/icon/tech_biz_custom/flashlight.svg');

  /// File path: assets/icon/tech_biz_custom/front_camera.svg
  SvgGenImage get frontCamera =>
      const SvgGenImage('assets/icon/tech_biz_custom/front_camera.svg');

  /// File path: assets/icon/tech_biz_custom/location.svg
  SvgGenImage get location =>
      const SvgGenImage('assets/icon/tech_biz_custom/location.svg');

  /// File path: assets/icon/tech_biz_custom/microphone.svg
  SvgGenImage get microphone =>
      const SvgGenImage('assets/icon/tech_biz_custom/microphone.svg');

  /// File path: assets/icon/tech_biz_custom/motion.svg
  SvgGenImage get motion =>
      const SvgGenImage('assets/icon/tech_biz_custom/motion.svg');

  /// File path: assets/icon/tech_biz_custom/proximity_sensor.svg
  SvgGenImage get proximitySensor =>
      const SvgGenImage('assets/icon/tech_biz_custom/proximity_sensor.svg');

  /// File path: assets/icon/tech_biz_custom/screen capture.svg
  SvgGenImage get screenCapture =>
      const SvgGenImage('assets/icon/tech_biz_custom/screen capture.svg');

  /// File path: assets/icon/tech_biz_custom/speaker.svg
  SvgGenImage get speaker =>
      const SvgGenImage('assets/icon/tech_biz_custom/speaker.svg');

  /// File path: assets/icon/tech_biz_custom/speaker_ear.svg
  SvgGenImage get speakerEar =>
      const SvgGenImage('assets/icon/tech_biz_custom/speaker_ear.svg');

  /// File path: assets/icon/tech_biz_custom/storage.svg
  SvgGenImage get storage =>
      const SvgGenImage('assets/icon/tech_biz_custom/storage.svg');

  /// File path: assets/icon/tech_biz_custom/vibration.svg
  SvgGenImage get vibration =>
      const SvgGenImage('assets/icon/tech_biz_custom/vibration.svg');

  /// File path: assets/icon/tech_biz_custom/volume.svg
  SvgGenImage get volume =>
      const SvgGenImage('assets/icon/tech_biz_custom/volume.svg');

  /// File path: assets/icon/tech_biz_custom/wifi.svg
  SvgGenImage get wifi =>
      const SvgGenImage('assets/icon/tech_biz_custom/wifi.svg');

  /// List of all assets
  List<SvgGenImage> get values => [
        touchScreen,
        backCamera,
        battery,
        bio,
        bluetooth,
        camera,
        fingerprint,
        flashlight,
        frontCamera,
        location,
        microphone,
        motion,
        proximitySensor,
        screenCapture,
        speaker,
        speakerEar,
        storage,
        vibration,
        volume,
        wifi
      ];
}

class Assets {
  Assets._();

  static const $AssetsAnimationGen animation = $AssetsAnimationGen();
  static const $AssetsDesignGen design = $AssetsDesignGen();
  static const $AssetsFontsGen fonts = $AssetsFontsGen();
  static const $AssetsIconGen icon = $AssetsIconGen();
  static const $AssetsSoundGen sound = $AssetsSoundGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName, {this.size = null});

  final String _assetName;

  final Size? size;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(
    this._assetName, {
    this.size = null,
  }) : _isVecFormat = false;

  const SvgGenImage.vec(
    this._assetName, {
    this.size = null,
  }) : _isVecFormat = true;

  final String _assetName;

  final Size? size;
  final bool _isVecFormat;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme? theme,
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture(
      _isVecFormat
          ? AssetBytesLoader(_assetName,
              assetBundle: bundle, packageName: package)
          : SvgAssetLoader(_assetName,
              assetBundle: bundle, packageName: package),
      key: key,
      matchTextDirection: matchTextDirection,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
      colorFilter: colorFilter ??
          (color == null ? null : ColorFilter.mode(color, colorBlendMode)),
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
