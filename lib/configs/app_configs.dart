class AppConfig {
  /// All the API endpoints are defined here
  static const String baseUrl = "https://tradein-samsung-mobile-api.techbusiness.co.th";
  static const String baseUrlDev = "https://tradein-samsung-mobile-api.techbusiness.co.th";
  static const String getAuth = "/api/v1/Auth/GenerateToken";
  static const String getHistory = "/api/v1/TradeIn/GetQuestionIssueResult";

  ///[Series] API Endpoint
  static const String postSeries = "/api/v1/Product/Model";
  static const String postModel = "/api/v1/Product/Choice/Hardware";
  static const String postCreateTradeinCheckResult = "/api/v1/TradeIn/Create";

  ///Default Value Config
  /// Timeout = 30
  static int defaultTimeout = 30;

  /// Countdown = 10
  static int defaultCountdown = 10;
  static int defaultFlashlightCountdown = 5;
  static int defaultScreenCheckCountdown = 60;

  /// Max Random Number = 4
  static const int defaultRandomMaxNumber = 4;

  //Language
  static const String defaultLang = "th";

  /// Miliseconds between certain [Future]
  /// 
  /// [futureSuccessAwaitDuration] = 500
  static const int futureSuccessAwaitDuration = 500;
  /// [futureFailedAwaitDuration] = 100
  static const int futureFailedAwaitDuration = 100;
  /// [futureDelayedTestAwaitDuration] = 1
  static const int futureDelayedTestAwaitDuration = 1;
  /// [resultLoadingFakeDelay] = 2
  static const int resultLoadingFakeDelay = 2;

  /// Default Decibel
  static const int defaultDecibel = 80;

  ///Minimum Height for LayoutBuilder
  static const double smallScreenHeight = 788;
}
