import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/shared/shared.dart';

class BackgroundApp extends ConsumerWidget {
  final Widget child;
  const BackgroundApp({
    super.key,
    required this.child,
  });
  // https://fluttergradientgenerator.com/
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Stack(
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width.w,
          height: MediaQuery.of(context).size.height.h,
          child: DecoratedBox(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [Color(0xffcfcfcf), Color(0xffffffff)],
                stops: [0.1, 1],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              ),
            ),
            position: DecorationPosition.background,
            child: child,
          ),
        ),
      ],
    );
  }
}
