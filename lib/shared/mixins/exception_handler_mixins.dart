import 'dart:io';

import 'package:http/http.dart';
import 'package:techswop_lite/shared/exceptions/http_exceptions.dart';
import 'package:techswop_lite/shared/utils/network_service/base/network_service.dart';
import 'package:techswop_lite/shared/domain/either.dart';
import 'package:techswop_lite/shared/domain/response.dart' as response;

mixin ExceptionHandlerMixin on NetworkService {
  Future<Either<AppException, response.Response>>
      handleException<T extends Object>(Future<Response> Function() handler,
          {String endpoint = ''}) async {
    try {
      final res = await handler();
      return Right(
        response.Response(
          resultCode: res.statusCode,
          resultData: res.body,
          developerMessage: res.reasonPhrase ?? 'OK',
        ),
      );
    } catch (e) {
      String message = '';
      String identifier = '';
      int statusCode = 0;
      switch (e.runtimeType) {
        case HttpException _:
          e as HttpException;
          message = 'Unable to connect to the server.';
          statusCode = StatusCode.httpException.value;
          identifier = 'Http Exception ${e.message}\n at  $endpoint';
          break;

        case FormatException _:
          e as FormatException;
          message = 'Invalid response from server';
          statusCode = StatusCode.jsonException.value;
          identifier = 'Json Exception ${e.message}\n at $endpoint';
          break;

        default:
          message = 'Unknown error occurred';
          statusCode = StatusCode.unknownException.value;
          identifier = 'Unknown error ${e.toString()}\n at $endpoint';
      }
      return Left(
        AppException(
          resultData: message,
          resultCode: statusCode,
          developerMessage: identifier,
        ),
      );
    }
  }
}

enum StatusCode {
  httpException(0),
  unknownException(1),
  jsonException(2);

  const StatusCode(this.value);
  final int value;
}
