import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/shared/shared.dart';

class CheckIconWidget extends ConsumerWidget {
  const CheckIconWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return const FaIcon(
      FontAwesomeIcons.check,
      size: 22,
      color: Colors.green,
    );
  }
}
