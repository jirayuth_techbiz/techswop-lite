import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/shared/shared.dart';

class XMarkIconWidget extends ConsumerWidget {
  const XMarkIconWidget({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return const FaIcon(
      FontAwesomeIcons.xmark,
      size: 22,
      color: Colors.red,
    );
  }
}
