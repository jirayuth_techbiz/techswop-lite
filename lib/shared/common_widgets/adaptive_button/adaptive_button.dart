import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/generated/app_localizations.dart';
import 'package:techswop_lite/shared/common_widgets/gradient_elevated_long_button.dart';
import 'package:techswop_lite/shared/shared.dart';

class NormalMainBodyButton extends ConsumerWidget {
  final AppLocalizations translation;
  final void Function() onPressed;

  const NormalMainBodyButton({
    super.key,
    required this.translation,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10.0.h),
          child: SharedElevatedButton(
            text: translation.startTradeButton,
            fontSize: 13.sp,
            fontWeight: FontWeight.w500,
            onPressed: onPressed,
          ),
        ),
      ],
    );
  }
}

class AdaptiveMainBodyButton extends ConsumerWidget {
  final AppLocalizations translation;
  final void Function() onPressed;
  const AdaptiveMainBodyButton({
    super.key,
    required this.translation,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        SharedElevatedButton(
          text: translation.startTradeButton,
          onPressed: onPressed,
        ),
      ],
    );
  }
}
