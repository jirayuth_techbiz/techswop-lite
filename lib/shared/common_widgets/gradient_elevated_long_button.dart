import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/shared/shared.dart';

class SharedElevatedButton extends ConsumerWidget {
  final ButtonStyle? style;
  final String text;
  final FontWeight? fontWeight;
  final double? fontSize;
  final void Function()? onPressed;
  const SharedElevatedButton({
    super.key,
    required this.onPressed,
    required this.text,
    this.fontWeight,
    this.fontSize,
    this.style,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = context.theme;
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10.r)),
        color: theme.colorScheme.primary,
      ),
      child: FilledButton(
        style: style ??
            ButtonStyle(
              backgroundColor: WidgetStateProperty.resolveWith(
                (states) => Colors.transparent,
              ),
            ),
        onPressed: onPressed,
        child: Text(
          text,
          style: theme.textTheme.bodyMedium!.copyWith(
            fontWeight: fontWeight ?? FontWeight.bold,
            fontSize: fontSize ?? 16.sp,
            color: theme.colorScheme.onPrimary,
          ),
        ),
      ),
    );
  }
}
