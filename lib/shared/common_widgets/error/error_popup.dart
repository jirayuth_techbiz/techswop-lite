import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/const/enums/routing_enums.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/shared.dart';

///Common Widget สำหรับแสดงผลข้อมูลจาก 
///
///- [AsyncValue.error]
///- [AppException]
class GeneralPopUpError extends ConsumerWidget {
  
  ///ชื่อของ Popup
  final String title;

  ///เนื้อหา Error ของ Popup
  final String content;

  const GeneralPopUpError(
      {super.key, required this.title, required this.content});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final translation = context.localization;
    
    return AlertDialog(
      title: Text(title),
      content: Text(content),
      actions: [
        TextButton(
          onPressed: () {
            try {
              context.pop();
            } on GoError {
              context.goNamed(AppRoutes.preparingPage.name);
            } catch (e) {
              log(e.toString());
            }
          },
          child: Text(translation.confirmTxt),
        ),
      ],
    );
  }
}