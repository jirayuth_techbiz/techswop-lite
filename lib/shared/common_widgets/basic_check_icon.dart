import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:techswop_lite/features/preparation/domain/checklist_properties.dart';
import 'package:techswop_lite/features/preparation/presentation/state/permission_check_state.dart';
import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';
import 'package:techswop_lite/shared/extensions/theme_extension.dart';

class BasicCheckIcon extends ConsumerWidget {
  final CheckListPermissionProperties checkListProperties;

  const BasicCheckIcon({
    super.key,
    required this.checkListProperties,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final themeData = context.theme;
    final permissionStatusStateNotifier =
        ref.read(permissionCheckStateProvider.notifier);

    return AsyncValueWidget(
      value: checkListProperties.state!,
      data: (result) => Material(
        type: MaterialType.transparency,
        child: InkWell(
          onTap: () {
            permissionStatusStateNotifier.getSettingPages(
              checkListProperties.permissionStatusEnum!,
              result,
            );
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 25.r,
                height: 25.r,
                constraints: BoxConstraints(
                  maxWidth: 30.w,
                ),
                margin: EdgeInsets.symmetric(horizontal: 25.w),
                child: SvgPicture.asset(
                  fit: BoxFit.fill,
                  checkListProperties.icon,
                  colorFilter:
                      ColorFilter.mode(themeData.primaryColor, BlendMode.srcIn),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        checkListProperties.textUpper,
                        textAlign: TextAlign.start,
                        style: themeData.textTheme.bodyMedium!.copyWith(
                          fontWeight: FontWeight.bold,
                          fontSize: 16.sp,
                        ),
                      ),
                      SizedBox(
                        child: Text(
                          checkListProperties.textLower,
                          softWrap: true,
                          maxLines: 3,
                          textAlign: TextAlign.start,
                          style: themeData.textTheme.bodyMedium,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                constraints: BoxConstraints(
                  maxWidth: 25.w,
                ),
                margin: EdgeInsets.symmetric(horizontal: 25.w),
                child: permissionStatusStateNotifier.getIconBasedOnPermission(
                    checkListProperties.permissionStatusEnum!, result),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
