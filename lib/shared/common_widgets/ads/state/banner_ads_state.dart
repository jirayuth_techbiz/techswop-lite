// import 'dart:io';

// import 'package:google_mobile_ads/google_mobile_ads.dart';
// import 'package:riverpod_annotation/riverpod_annotation.dart';
// import 'package:techswop_lite/shared/shared.dart';
// import 'package:techswop_lite/shared/utils/shared_env/ads_keys.dart';

// part 'banner_ads_state.g.dart';

// @riverpod
// class BannerAdsState extends _$BannerAdsState {
//   @override
//   Future<BannerAd?> build() async {
//     final LoggerRepository logger = ref.watch(loggerRepositoryProvider);
//     final adTestUnit = Platform.isAndroid
//         ? 'ca-app-pub-3940256099942544/6300978111'
//         : 'ca-app-pub-3940256099942544/2934735716';

//     ///เอามาจาก ads_keys.dart อยู่ใน shared/utils/shared_env/ads_keys.dart
//     final adRealUnit =
//         Platform.isAndroid ? AdsKey.adsKeyAndroid : AdsKey.adsKeyIos;

//     BannerAd? bannerAd = BannerAd(
//       size: AdSize.banner,
//       adUnitId: kDebugMode ? adTestUnit : adRealUnit,
//       request: const AdRequest(),
//       listener: BannerAdListener(
//         onAdLoaded: (Ad ad) {
//           logger.logInfo('Ad loaded: $ad');
//           ref.read(bannerAdsShowStateProvider.notifier).showAds(true);
//         },
//         onAdFailedToLoad: (Ad ad, LoadAdError error) {
//           ad.dispose();
//           logger.logError(
//             'Ad failed to load: $error',
//           );
//         },
//         onAdOpened: (Ad ad) => logger.logDebug('Ad opened: $ad'),
//         onAdClosed: (Ad ad) => logger.logDebug('Ad closed: $ad'),
//         onAdImpression: (Ad ad) => logger.logDebug('Ad impression: $ad'),
//         onAdClicked: (Ad ad) => logger.logDebug('Ad clicked: $ad'),
//       ),
//     );

//     ref.onDispose(() {
//       bannerAd.dispose();
//     });
    
//     return bannerAd;
//   }

//   loadAds() async {
//     state.whenData((ad) {
//       final LoggerRepository logger = ref.watch(loggerRepositoryProvider);
//       logger.logInfo('Loading ads');
//       ad?.load();
//     });
//   }
// }

// @riverpod
// class BannerAdsShowState extends _$BannerAdsShowState {
//   @override
//   bool build() {
//     return false;
//   }

//   showAds(bool status) {
//     state = status;
//   }
// }
