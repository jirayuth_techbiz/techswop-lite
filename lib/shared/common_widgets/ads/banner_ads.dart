// import 'package:flutter/material.dart';
// import 'package:flutter_riverpod/flutter_riverpod.dart';
// import 'package:google_mobile_ads/google_mobile_ads.dart';
// import 'package:techswop_lite/shared/common_widgets/ads/state/banner_ads_state.dart';
// import 'package:techswop_lite/shared/common_widgets/asyncvalue_widget.dart';

// class BannerAds extends ConsumerStatefulWidget {
//   const BannerAds({super.key});

//   @override
//   ConsumerState<ConsumerStatefulWidget> createState() => _BannerAdsState();
// }

// class _BannerAdsState extends ConsumerState<BannerAds> {
  
//   @override
//   void initState() {
//     super.initState();
//     ref.read(bannerAdsStateProvider.notifier).loadAds();
//   }

//   @override
//   Widget build(BuildContext context) {
//     final homepageAds = ref.watch(bannerAdsStateProvider);

//     return AsyncValueWidget(
//       value: homepageAds,
//       data: (ad) => Container(
//         alignment: Alignment.center,
//         padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
//         child: SafeArea(
//           child: SizedBox(
//             width: ad!.size.width.toDouble(),
//             height: ad.size.height.toDouble(),
//             child: AdWidget(
//               ad: ad,
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }