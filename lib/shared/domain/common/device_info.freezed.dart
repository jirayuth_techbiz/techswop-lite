// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'device_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$DeviceModelInfo {
  String get model => throw _privateConstructorUsedError;
  set model(String value) => throw _privateConstructorUsedError;
  String? get id => throw _privateConstructorUsedError;
  set id(String? value) => throw _privateConstructorUsedError;
  int get version => throw _privateConstructorUsedError;
  set version(int value) => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $DeviceModelInfoCopyWith<DeviceModelInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DeviceModelInfoCopyWith<$Res> {
  factory $DeviceModelInfoCopyWith(
          DeviceModelInfo value, $Res Function(DeviceModelInfo) then) =
      _$DeviceModelInfoCopyWithImpl<$Res, DeviceModelInfo>;
  @useResult
  $Res call({String model, String? id, int version});
}

/// @nodoc
class _$DeviceModelInfoCopyWithImpl<$Res, $Val extends DeviceModelInfo>
    implements $DeviceModelInfoCopyWith<$Res> {
  _$DeviceModelInfoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? model = null,
    Object? id = freezed,
    Object? version = null,
  }) {
    return _then(_value.copyWith(
      model: null == model
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as String,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      version: null == version
          ? _value.version
          : version // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$DeviceModelInfoImplCopyWith<$Res>
    implements $DeviceModelInfoCopyWith<$Res> {
  factory _$$DeviceModelInfoImplCopyWith(_$DeviceModelInfoImpl value,
          $Res Function(_$DeviceModelInfoImpl) then) =
      __$$DeviceModelInfoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String model, String? id, int version});
}

/// @nodoc
class __$$DeviceModelInfoImplCopyWithImpl<$Res>
    extends _$DeviceModelInfoCopyWithImpl<$Res, _$DeviceModelInfoImpl>
    implements _$$DeviceModelInfoImplCopyWith<$Res> {
  __$$DeviceModelInfoImplCopyWithImpl(
      _$DeviceModelInfoImpl _value, $Res Function(_$DeviceModelInfoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? model = null,
    Object? id = freezed,
    Object? version = null,
  }) {
    return _then(_$DeviceModelInfoImpl(
      model: null == model
          ? _value.model
          : model // ignore: cast_nullable_to_non_nullable
              as String,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      version: null == version
          ? _value.version
          : version // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$DeviceModelInfoImpl extends _DeviceModelInfo {
  _$DeviceModelInfoImpl({this.model = "", this.id = null, this.version = 0})
      : super._();

  @override
  @JsonKey()
  String model;
  @override
  @JsonKey()
  String? id;
  @override
  @JsonKey()
  int version;

  @override
  String toString() {
    return 'DeviceModelInfo(model: $model, id: $id, version: $version)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$DeviceModelInfoImplCopyWith<_$DeviceModelInfoImpl> get copyWith =>
      __$$DeviceModelInfoImplCopyWithImpl<_$DeviceModelInfoImpl>(
          this, _$identity);
}

abstract class _DeviceModelInfo extends DeviceModelInfo {
  factory _DeviceModelInfo({String model, String? id, int version}) =
      _$DeviceModelInfoImpl;
  _DeviceModelInfo._() : super._();

  @override
  String get model;
  set model(String value);
  @override
  String? get id;
  set id(String? value);
  @override
  int get version;
  set version(int value);
  @override
  @JsonKey(ignore: true)
  _$$DeviceModelInfoImplCopyWith<_$DeviceModelInfoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
