import 'package:freezed_annotation/freezed_annotation.dart';

part 'device_info.freezed.dart';

@unfreezed
class DeviceModelInfo with _$DeviceModelInfo {
  factory DeviceModelInfo({
    @Default("") String model, 
    @Default(null) String? id,
    @Default(0) int version,}) 
    = _DeviceModelInfo;

  DeviceModelInfo._();
}
