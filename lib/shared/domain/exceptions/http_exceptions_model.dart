import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:techswop_lite/shared/exceptions/http_exceptions.dart';

part 'http_exceptions_model.freezed.dart';

@freezed
class DeviceRetrievalException
    with _$DeviceRetrievalException
    implements AppException {
  const factory DeviceRetrievalException() = _DeviceRetrievalException;

  const DeviceRetrievalException._();

  @override
  String get resultData => 'ไม่สามารถดึงข้อมูลอุปกรณ์จาก Server ได้';

  @override
  String get developerMessage => 'Unable to retrieve device';

  @override
  int get resultCode => 404;

  @override
  // TODO: implement copyWith
  $AppExceptionCopyWith<AppException> get copyWith =>
      throw UnimplementedError();
}

@freezed
class HistoryRetrivedFailed
    with _$HistoryRetrivedFailed
    implements AppException {
  const factory HistoryRetrivedFailed() = _HistoryRetrivedFailed;

  const HistoryRetrivedFailed._();

  @override
  String get resultData => 'ไม่สามารถดึงข้อมูลประวัติการทดสอบได้';

  @override
  String get developerMessage => 'Unable to retrieve history';

  @override
  int get resultCode => 404;

  @override
  // TODO: implement copyWith
  $AppExceptionCopyWith<AppException> get copyWith =>
      throw UnimplementedError();
}

@freezed
class CacheFailureException
    with _$CacheFailureException
    implements AppException {
  const factory CacheFailureException() = _CacheFailureException;

  const CacheFailureException._();

  @override
  String get resultData => 'Cache failure exception';

  @override
  String get developerMessage => 'Unable to save user';

  @override
  int get resultCode => 100;

  @override
  // TODO: implement copyWith
  $AppExceptionCopyWith<AppException> get copyWith =>
      throw UnimplementedError();
}

@freezed
class ChecklistRetrivedFailed
    with _$ChecklistRetrivedFailed
    implements AppException {
  const factory ChecklistRetrivedFailed() = _ChecklistRetrivedFailed;

  const ChecklistRetrivedFailed._();

  @override
  String get resultData => 'ไม่สามารถดึงข้อมูล Checklist ได้';

  @override
  String get developerMessage => 'Unable to retrieve checklist';

  @override
  int get resultCode => 404;

  @override
  // TODO: implement copyWith
  $AppExceptionCopyWith<AppException> get copyWith =>
      throw UnimplementedError();
}
