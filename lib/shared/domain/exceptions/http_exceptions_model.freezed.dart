// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'http_exceptions_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$DeviceRetrievalException {}

/// @nodoc
abstract class $DeviceRetrievalExceptionCopyWith<$Res> {
  factory $DeviceRetrievalExceptionCopyWith(DeviceRetrievalException value,
          $Res Function(DeviceRetrievalException) then) =
      _$DeviceRetrievalExceptionCopyWithImpl<$Res, DeviceRetrievalException>;
}

/// @nodoc
class _$DeviceRetrievalExceptionCopyWithImpl<$Res,
        $Val extends DeviceRetrievalException>
    implements $DeviceRetrievalExceptionCopyWith<$Res> {
  _$DeviceRetrievalExceptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$DeviceRetrievalExceptionImplCopyWith<$Res> {
  factory _$$DeviceRetrievalExceptionImplCopyWith(
          _$DeviceRetrievalExceptionImpl value,
          $Res Function(_$DeviceRetrievalExceptionImpl) then) =
      __$$DeviceRetrievalExceptionImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DeviceRetrievalExceptionImplCopyWithImpl<$Res>
    extends _$DeviceRetrievalExceptionCopyWithImpl<$Res,
        _$DeviceRetrievalExceptionImpl>
    implements _$$DeviceRetrievalExceptionImplCopyWith<$Res> {
  __$$DeviceRetrievalExceptionImplCopyWithImpl(
      _$DeviceRetrievalExceptionImpl _value,
      $Res Function(_$DeviceRetrievalExceptionImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$DeviceRetrievalExceptionImpl extends _DeviceRetrievalException {
  const _$DeviceRetrievalExceptionImpl() : super._();

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DeviceRetrievalExceptionImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _DeviceRetrievalException extends DeviceRetrievalException {
  const factory _DeviceRetrievalException() = _$DeviceRetrievalExceptionImpl;
  const _DeviceRetrievalException._() : super._();
}

/// @nodoc
mixin _$HistoryRetrivedFailed {}

/// @nodoc
abstract class $HistoryRetrivedFailedCopyWith<$Res> {
  factory $HistoryRetrivedFailedCopyWith(HistoryRetrivedFailed value,
          $Res Function(HistoryRetrivedFailed) then) =
      _$HistoryRetrivedFailedCopyWithImpl<$Res, HistoryRetrivedFailed>;
}

/// @nodoc
class _$HistoryRetrivedFailedCopyWithImpl<$Res,
        $Val extends HistoryRetrivedFailed>
    implements $HistoryRetrivedFailedCopyWith<$Res> {
  _$HistoryRetrivedFailedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$HistoryRetrivedFailedImplCopyWith<$Res> {
  factory _$$HistoryRetrivedFailedImplCopyWith(
          _$HistoryRetrivedFailedImpl value,
          $Res Function(_$HistoryRetrivedFailedImpl) then) =
      __$$HistoryRetrivedFailedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$HistoryRetrivedFailedImplCopyWithImpl<$Res>
    extends _$HistoryRetrivedFailedCopyWithImpl<$Res,
        _$HistoryRetrivedFailedImpl>
    implements _$$HistoryRetrivedFailedImplCopyWith<$Res> {
  __$$HistoryRetrivedFailedImplCopyWithImpl(_$HistoryRetrivedFailedImpl _value,
      $Res Function(_$HistoryRetrivedFailedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$HistoryRetrivedFailedImpl extends _HistoryRetrivedFailed {
  const _$HistoryRetrivedFailedImpl() : super._();

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HistoryRetrivedFailedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _HistoryRetrivedFailed extends HistoryRetrivedFailed {
  const factory _HistoryRetrivedFailed() = _$HistoryRetrivedFailedImpl;
  const _HistoryRetrivedFailed._() : super._();
}

/// @nodoc
mixin _$CacheFailureException {}

/// @nodoc
abstract class $CacheFailureExceptionCopyWith<$Res> {
  factory $CacheFailureExceptionCopyWith(CacheFailureException value,
          $Res Function(CacheFailureException) then) =
      _$CacheFailureExceptionCopyWithImpl<$Res, CacheFailureException>;
}

/// @nodoc
class _$CacheFailureExceptionCopyWithImpl<$Res,
        $Val extends CacheFailureException>
    implements $CacheFailureExceptionCopyWith<$Res> {
  _$CacheFailureExceptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CacheFailureExceptionImplCopyWith<$Res> {
  factory _$$CacheFailureExceptionImplCopyWith(
          _$CacheFailureExceptionImpl value,
          $Res Function(_$CacheFailureExceptionImpl) then) =
      __$$CacheFailureExceptionImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CacheFailureExceptionImplCopyWithImpl<$Res>
    extends _$CacheFailureExceptionCopyWithImpl<$Res,
        _$CacheFailureExceptionImpl>
    implements _$$CacheFailureExceptionImplCopyWith<$Res> {
  __$$CacheFailureExceptionImplCopyWithImpl(_$CacheFailureExceptionImpl _value,
      $Res Function(_$CacheFailureExceptionImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CacheFailureExceptionImpl extends _CacheFailureException {
  const _$CacheFailureExceptionImpl() : super._();

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CacheFailureExceptionImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _CacheFailureException extends CacheFailureException {
  const factory _CacheFailureException() = _$CacheFailureExceptionImpl;
  const _CacheFailureException._() : super._();
}

/// @nodoc
mixin _$ChecklistRetrivedFailed {}

/// @nodoc
abstract class $ChecklistRetrivedFailedCopyWith<$Res> {
  factory $ChecklistRetrivedFailedCopyWith(ChecklistRetrivedFailed value,
          $Res Function(ChecklistRetrivedFailed) then) =
      _$ChecklistRetrivedFailedCopyWithImpl<$Res, ChecklistRetrivedFailed>;
}

/// @nodoc
class _$ChecklistRetrivedFailedCopyWithImpl<$Res,
        $Val extends ChecklistRetrivedFailed>
    implements $ChecklistRetrivedFailedCopyWith<$Res> {
  _$ChecklistRetrivedFailedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ChecklistRetrivedFailedImplCopyWith<$Res> {
  factory _$$ChecklistRetrivedFailedImplCopyWith(
          _$ChecklistRetrivedFailedImpl value,
          $Res Function(_$ChecklistRetrivedFailedImpl) then) =
      __$$ChecklistRetrivedFailedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ChecklistRetrivedFailedImplCopyWithImpl<$Res>
    extends _$ChecklistRetrivedFailedCopyWithImpl<$Res,
        _$ChecklistRetrivedFailedImpl>
    implements _$$ChecklistRetrivedFailedImplCopyWith<$Res> {
  __$$ChecklistRetrivedFailedImplCopyWithImpl(
      _$ChecklistRetrivedFailedImpl _value,
      $Res Function(_$ChecklistRetrivedFailedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ChecklistRetrivedFailedImpl extends _ChecklistRetrivedFailed {
  const _$ChecklistRetrivedFailedImpl() : super._();

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChecklistRetrivedFailedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;
}

abstract class _ChecklistRetrivedFailed extends ChecklistRetrivedFailed {
  const factory _ChecklistRetrivedFailed() = _$ChecklistRetrivedFailedImpl;
  const _ChecklistRetrivedFailed._() : super._();
}
