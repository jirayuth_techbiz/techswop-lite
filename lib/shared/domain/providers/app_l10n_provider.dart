import 'dart:ui' as ui;
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/generated/app_localizations.dart';

part 'app_l10n_provider.g.dart';

@riverpod
AppLocalizations appLocalizations(AppLocalizationsRef ref) =>
    lookupAppLocalizations(ui.PlatformDispatcher.instance.locale);
