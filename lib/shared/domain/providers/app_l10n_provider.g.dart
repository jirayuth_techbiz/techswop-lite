// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_l10n_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$appLocalizationsHash() => r'bc1bfbaf06c384b1172812f91981dd976e355951';

/// See also [appLocalizations].
@ProviderFor(appLocalizations)
final appLocalizationsProvider = AutoDisposeProvider<AppLocalizations>.internal(
  appLocalizations,
  name: r'appLocalizationsProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$appLocalizationsHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef AppLocalizationsRef = AutoDisposeProviderRef<AppLocalizations>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
