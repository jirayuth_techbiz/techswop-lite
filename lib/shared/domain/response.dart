import 'package:techswop_lite/shared/domain/either.dart';
import 'package:techswop_lite/shared/exceptions/http_exceptions.dart';

class Response {
  final int resultCode;
  final String developerMessage;
  final dynamic resultData;

  Response(
      {required this.resultCode, required this.developerMessage, this.resultData});
  @override
  String toString() {
    return 'statusCode=$resultCode\nstatusMessage=$developerMessage\n data=$resultData';
  }
}

extension ResponseExtension on Response {
  Right<AppException, Response> get toRight => Right(this);
}