import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:techswop_lite/shared/data/shared_preferences/share_preference_repository_impl.dart';
import 'package:techswop_lite/shared/domain/repository/base/share_preferences.dart';

part 'share_preferences_repository.g.dart';

@Riverpod(keepAlive: true)
Future<TradeInSharePreferencesRepository> tradeInSharePreferencesRepository(
    TradeInSharePreferencesRepositoryRef ref) async {
  final sharePrefs = await SharedPreferences.getInstance();
  return TradeInSharePreferencesRepositoryImpl(sharePrefs);
}

abstract class TradeInSharePreferencesRepository extends SharedPreferencesBase {
  Future<void> resetSharePrefs();
}
