import 'dart:async';

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/shared/data/test_timeout_timer/test_timeout_repository_impl.dart';

part 'test_timeout_repository.g.dart';

@riverpod
TestTimerRepository testTimerRepository(TestTimerRepositoryRef ref) {
  return TestTimerRepositoryImpl();
}

abstract class TestTimerRepository {
  FutureOr<void> startTimer();
  FutureOr<void> stopTimer();
  Stream<int> getTimer();
}
