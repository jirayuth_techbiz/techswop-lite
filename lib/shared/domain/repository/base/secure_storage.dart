// ignore_for_file: file_names
abstract class SecureStorageRepositoryBase {

  Future<Map<String, String>> getAllData();
  Future<String?> getDataByKey({required String key});

  Future<void> writeData({
    required String key,
    required String? value,
  });

  Future<void> deleteDataByKey({required String key});
  Future<void> deleteAllData();
}
