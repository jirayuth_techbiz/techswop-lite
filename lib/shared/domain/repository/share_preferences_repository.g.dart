// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'share_preferences_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$tradeInSharePreferencesRepositoryHash() =>
    r'3bb23475255d174c566f707b340f4f0ea1d2fd4c';

/// See also [tradeInSharePreferencesRepository].
@ProviderFor(tradeInSharePreferencesRepository)
final tradeInSharePreferencesRepositoryProvider =
    FutureProvider<TradeInSharePreferencesRepository>.internal(
  tradeInSharePreferencesRepository,
  name: r'tradeInSharePreferencesRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$tradeInSharePreferencesRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef TradeInSharePreferencesRepositoryRef
    = FutureProviderRef<TradeInSharePreferencesRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
