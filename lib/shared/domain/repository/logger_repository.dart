import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/shared/data/logger/logger_repository_impl.dart';
import 'package:logger/logger.dart';
import 'package:techswop_lite/shared/shared.dart';

part 'logger_repository.g.dart';

@riverpod
LoggerRepository loggerRepository(LoggerRepositoryRef ref) {
  var logger = Logger(
    filter: MyFilter(),
    printer: PrettyPrinter(),
    output: null,
  );

  return LoggerRepositoryImpl(logger);
}

class MyFilter extends LogFilter {
  @override
  bool shouldLog(LogEvent event) {
    return kReleaseMode || kDebugMode;
  }
}

abstract class LoggerRepository {
  void logTrace(String message);
  void logDebug(String message);
  void logInfo(String message);
  void logWarning(String message);
  void logError(String message);
  void logWTF(String message);
}
