import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/shared/data/secure_storage/trade_in_secure_storage_repository_impl.dart';
import 'package:techswop_lite/shared/domain/repository/base/secure_storage.dart';

part 'trade_in_secure_storage_repository.g.dart';

@Riverpod(keepAlive: true)
TradeinSecureStorageRepository tradeinSecureStorageRepository(TradeinSecureStorageRepositoryRef ref) {
  const secureStorage = FlutterSecureStorage(
    aOptions: AndroidOptions(
      encryptedSharedPreferences: true,
    ),
    iOptions: IOSOptions.defaultOptions,
  );
  
  return TradeinSecureStorageRepositoryImpl(storage: secureStorage);
}

abstract class TradeinSecureStorageRepository extends SecureStorageRepositoryBase {
  Future<String?> getCurrentPage(BuildContext context);
}
