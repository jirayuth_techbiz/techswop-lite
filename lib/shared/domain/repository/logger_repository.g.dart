// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'logger_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$loggerRepositoryHash() => r'8b7e761fc4a7130a91b1c4c17b3282bb36ddbf1a';

/// See also [loggerRepository].
@ProviderFor(loggerRepository)
final loggerRepositoryProvider = AutoDisposeProvider<LoggerRepository>.internal(
  loggerRepository,
  name: r'loggerRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$loggerRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef LoggerRepositoryRef = AutoDisposeProviderRef<LoggerRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
