// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'random_number_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$randomNumberRepositoryHash() =>
    r'a11f8ee8429b17a51bcf6d51eaa3e58675315336';

/// See also [randomNumberRepository].
@ProviderFor(randomNumberRepository)
final randomNumberRepositoryProvider =
    AutoDisposeProvider<RandomNumberRepository>.internal(
  randomNumberRepository,
  name: r'randomNumberRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$randomNumberRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef RandomNumberRepositoryRef
    = AutoDisposeProviderRef<RandomNumberRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
