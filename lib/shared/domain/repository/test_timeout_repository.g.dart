// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_timeout_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$testTimerRepositoryHash() =>
    r'74ee719ec47cbb893bcfc15d1d6d65395bf7d721';

/// See also [testTimerRepository].
@ProviderFor(testTimerRepository)
final testTimerRepositoryProvider =
    AutoDisposeProvider<TestTimerRepository>.internal(
  testTimerRepository,
  name: r'testTimerRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$testTimerRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef TestTimerRepositoryRef = AutoDisposeProviderRef<TestTimerRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
