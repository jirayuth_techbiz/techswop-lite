import 'package:android_id/android_id.dart';
import 'package:device_imei/device_imei.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/shared/data/device_info/device_info_repository_impl.dart';
import 'package:techswop_lite/shared/domain/common/device_info.dart';

part 'device_info_repository.g.dart';

@riverpod
DeviceInfoRepository deviceInfoRepository(DeviceInfoRepositoryRef ref) {
  final deviceImei = DeviceImei();
  final deviceInfo = DeviceInfoPlugin();
  const androidId = AndroidId();

  return DeviceInfoRepositoryImpl(
    deviceimei: deviceImei, 
    androidId: androidId,
    deviceInfoPlugin: deviceInfo,);
}

abstract class DeviceInfoRepository {
  Future<DeviceModelInfo?> getDeviceInfo();
  Future<double?> getDeviceTotalSpace();
  Future<String?> getIMEIidentifier();
}
