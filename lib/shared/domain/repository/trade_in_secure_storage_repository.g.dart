// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trade_in_secure_storage_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$tradeinSecureStorageRepositoryHash() =>
    r'13474d82c4b195684dd0bb2745b073f0833ae4e8';

/// See also [tradeinSecureStorageRepository].
@ProviderFor(tradeinSecureStorageRepository)
final tradeinSecureStorageRepositoryProvider =
    Provider<TradeinSecureStorageRepository>.internal(
  tradeinSecureStorageRepository,
  name: r'tradeinSecureStorageRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$tradeinSecureStorageRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef TradeinSecureStorageRepositoryRef
    = ProviderRef<TradeinSecureStorageRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
