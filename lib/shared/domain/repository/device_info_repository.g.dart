// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_info_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$deviceInfoRepositoryHash() =>
    r'8f2e01adfd4402946fa492e51435bb3481d98cf6';

/// See also [deviceInfoRepository].
@ProviderFor(deviceInfoRepository)
final deviceInfoRepositoryProvider =
    AutoDisposeProvider<DeviceInfoRepository>.internal(
  deviceInfoRepository,
  name: r'deviceInfoRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$deviceInfoRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef DeviceInfoRepositoryRef = AutoDisposeProviderRef<DeviceInfoRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
