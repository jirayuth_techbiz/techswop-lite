import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:techswop_lite/shared/data/random_number/random_number_repository_impl.dart';

part 'random_number_repository.g.dart';

@riverpod
RandomNumberRepository randomNumberRepository(RandomNumberRepositoryRef ref) {
  return RandomNumberRepositoryImpl();
}

abstract class RandomNumberRepository {
  int getRandomNumber();
  Map<int, bool> getRandomNumberMap(int newMapNumber);
}
