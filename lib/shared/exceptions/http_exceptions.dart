
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:techswop_lite/shared/domain/either.dart';
import 'package:techswop_lite/shared/domain/response.dart';

part 'http_exceptions.freezed.dart';

@freezed
class AppException with _$AppException implements Exception {
  const factory AppException({
    /// เอาไว้แสดงข้อความ Error
    required String resultData,

    /// เอาไว้แสดงรหัส Error
    required int resultCode,

    /// เอาไว้อธิบายสาเหตุที่ Error
    required String developerMessage,
  }) = _AppException;

  const AppException._();

  @override
  String toString() {
    return 'statusCode=$resultCode\nmessage=$developerMessage\nidentifier=$resultData';
  }
}

//  extension

extension HttpExceptionExtension on AppException {
  Left<AppException, Response> get toLeft => Left<AppException, Response>(this);
}
