// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'http_exceptions.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AppException {
  /// เอาไว้แสดงข้อความ Error
  String get resultData => throw _privateConstructorUsedError;

  /// เอาไว้แสดงรหัส Error
  int get resultCode => throw _privateConstructorUsedError;

  /// เอาไว้อธิบายสาเหตุที่ Error
  String get developerMessage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AppExceptionCopyWith<AppException> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppExceptionCopyWith<$Res> {
  factory $AppExceptionCopyWith(
          AppException value, $Res Function(AppException) then) =
      _$AppExceptionCopyWithImpl<$Res, AppException>;
  @useResult
  $Res call({String resultData, int resultCode, String developerMessage});
}

/// @nodoc
class _$AppExceptionCopyWithImpl<$Res, $Val extends AppException>
    implements $AppExceptionCopyWith<$Res> {
  _$AppExceptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? resultData = null,
    Object? resultCode = null,
    Object? developerMessage = null,
  }) {
    return _then(_value.copyWith(
      resultData: null == resultData
          ? _value.resultData
          : resultData // ignore: cast_nullable_to_non_nullable
              as String,
      resultCode: null == resultCode
          ? _value.resultCode
          : resultCode // ignore: cast_nullable_to_non_nullable
              as int,
      developerMessage: null == developerMessage
          ? _value.developerMessage
          : developerMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AppExceptionImplCopyWith<$Res>
    implements $AppExceptionCopyWith<$Res> {
  factory _$$AppExceptionImplCopyWith(
          _$AppExceptionImpl value, $Res Function(_$AppExceptionImpl) then) =
      __$$AppExceptionImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String resultData, int resultCode, String developerMessage});
}

/// @nodoc
class __$$AppExceptionImplCopyWithImpl<$Res>
    extends _$AppExceptionCopyWithImpl<$Res, _$AppExceptionImpl>
    implements _$$AppExceptionImplCopyWith<$Res> {
  __$$AppExceptionImplCopyWithImpl(
      _$AppExceptionImpl _value, $Res Function(_$AppExceptionImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? resultData = null,
    Object? resultCode = null,
    Object? developerMessage = null,
  }) {
    return _then(_$AppExceptionImpl(
      resultData: null == resultData
          ? _value.resultData
          : resultData // ignore: cast_nullable_to_non_nullable
              as String,
      resultCode: null == resultCode
          ? _value.resultCode
          : resultCode // ignore: cast_nullable_to_non_nullable
              as int,
      developerMessage: null == developerMessage
          ? _value.developerMessage
          : developerMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$AppExceptionImpl extends _AppException {
  const _$AppExceptionImpl(
      {required this.resultData,
      required this.resultCode,
      required this.developerMessage})
      : super._();

  /// เอาไว้แสดงข้อความ Error
  @override
  final String resultData;

  /// เอาไว้แสดงรหัส Error
  @override
  final int resultCode;

  /// เอาไว้อธิบายสาเหตุที่ Error
  @override
  final String developerMessage;

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AppExceptionImpl &&
            (identical(other.resultData, resultData) ||
                other.resultData == resultData) &&
            (identical(other.resultCode, resultCode) ||
                other.resultCode == resultCode) &&
            (identical(other.developerMessage, developerMessage) ||
                other.developerMessage == developerMessage));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, resultData, resultCode, developerMessage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AppExceptionImplCopyWith<_$AppExceptionImpl> get copyWith =>
      __$$AppExceptionImplCopyWithImpl<_$AppExceptionImpl>(this, _$identity);
}

abstract class _AppException extends AppException {
  const factory _AppException(
      {required final String resultData,
      required final int resultCode,
      required final String developerMessage}) = _$AppExceptionImpl;
  const _AppException._() : super._();

  @override

  /// เอาไว้แสดงข้อความ Error
  String get resultData;
  @override

  /// เอาไว้แสดงรหัส Error
  int get resultCode;
  @override

  /// เอาไว้อธิบายสาเหตุที่ Error
  String get developerMessage;
  @override
  @JsonKey(ignore: true)
  _$$AppExceptionImplCopyWith<_$AppExceptionImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
