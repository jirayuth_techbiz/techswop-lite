import 'package:techswop_lite/features/result/domain/model/issues_model.dart';

class ResultFormatUtils {
  ResultFormatUtils._();

  static String toFormat(Map<String, dynamic> map) {
    String format = map['code'];
    format += '|${map['created_at']}';
    final list = map['test_results'] as List<TestResultModel>;
    for(int i=0; i<list.length; i++) {
      final result = list[i];
      format += '|${result.hardware_id}:${result.result ? 1 : 0}';
    }
    return format;
  }
}
