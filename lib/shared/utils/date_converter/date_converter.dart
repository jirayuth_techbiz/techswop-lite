// ignore_for_file: non_constant_identifier_names
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';

String timeConverter({required String? db_date}) {;
  final DateTime localDate = DateFormat("yyyy-MM-dd hh:mm:ss.SSSSSS").parse(db_date ?? "01/01/1970 00:00:00 AM").toLocal();
  final String splitLocal =
      "${localDate.day}/${localDate.month}/${localDate.year + 543} ${localDate.hour.toString().padLeft(2, "0")}:${localDate.minute.toString().padLeft(2, "0")}:${localDate.second.toString().padLeft(2, "0")}";
  return splitLocal;
}

String generateFakeTime() {
  final DateTime now = DateTime.now();
  final DateFormat formatter = DateFormat('MM/dd/yyyy KK:mm:ss a');
  final String formatted = formatter.format(now);
  return formatted;
}
