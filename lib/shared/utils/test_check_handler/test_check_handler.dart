import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/const/enums/routing_enums.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/state/test_result_state.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/test_result/test_complete.dart';
import 'package:techswop_lite/features/device_test/base_layout/presentation/widgets/test_result/test_failed.dart';
import 'package:techswop_lite/features/result/domain/service/result_page_create_trade_in_service.dart';
import 'package:techswop_lite/features/result/domain/service/result_page_history_service.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/route/state/route_state.dart';
import 'package:techswop_lite/shared/shared.dart';

class TestCheckHandler {
  final BuildContext context;
  final WidgetRef ref;
  final bool isCheck;
  final AppRoutes appRoutes;
  final ProviderOrFamily stateProvider;
  final bool isExecuted;

  TestCheckHandler({
    required this.context,
    required this.ref,
    required this.isCheck,
    required this.appRoutes,
    required this.stateProvider,
    required this.isExecuted,
  });

  int waitDuration = 1;

  Future<void> handlingStatus({
    required void Function() onSkip,
    String? failedReason,
  }) async {
    final translation = context.localization;
    final extra = GoRouterState.of(context).extra;

    ///ถ้า User เข้าหน้านี้มาจากหน้า Result, Extra จะเป็น False ตลอด
    final isFromResultPage = extra is bool ? extra : false;

    final routeState = ref.watch(routeStateProvider.future);
    final getNextPageNumber =
        ref.watch(getCurrentPageNumberProvider(appRoutes.path));
    final routes = await routeState;

    if (isCheck && context.mounted) {
      if (getNextPageNumber >= routes.length) {
        return await _goToResultPage(
          context,
          isExecuted,
        );
      }
      return isFromResultPage
          ? await _goToResultPage(
              context,
              isExecuted,
            )
          : await _testComplete(
              routes[getNextPageNumber].name!,
              isExecuted,
            );
    } else if (!isCheck && context.mounted) {
      await _testFailed(
        isFromResultPage: isFromResultPage,
        isExecuted: isExecuted,
        titleAPI: appRoutes.titleAPI,
        nextPageRoute: 
        ///แก้ปัญหาเวลากดเข้าหน้าสุุดท้ายุจากหน้า Result แล้ว Error
        getNextPageNumber >= routes.length
            ? routes[getNextPageNumber - 1]
            : routes[getNextPageNumber],
        onNextPage: () {
          if (getNextPageNumber >= routes.length) {
            context.goNamed(AppRoutes.resultPage.name);
          } else {
            onSkip.call();
            context.goNamed(routes[getNextPageNumber].name!);
          }
        },
        failReason: failedReason ?? translation.testFailure,
        onRestart: () {
          ref.invalidate(stateProvider);
        },
      );
    }
  }

  Future<void> _testComplete(
    String nextPageRoute,
    bool isExecuted,
  ) async {
    final isExecutedNotifier = ref.read(resultIsExecutedStateProvider.notifier);

    if (!isExecuted) {
      isExecutedNotifier.setExecuted();
      await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (ctx) {
          Future.delayed(Duration(seconds: waitDuration), () {
            context.goNamed(
              nextPageRoute,
            );
            isExecutedNotifier.setExecuted();
          });
          return const TestCompletePage();
        },
      );
    }
  }

  Future<void> _testFailed({
    String? titleAPI,
    required GoRoute nextPageRoute,
    Function()? onNextPage,
    required String failReason,
    void Function()? onRestart,
    required bool isExecuted,
    required bool isFromResultPage,
  }) async {
    final isExecutedNotifier = ref.read(resultIsExecutedStateProvider.notifier);

    void onDialogClose() {
      isExecutedNotifier.setExecuted();
    }

    if (!isExecuted) {
      isExecutedNotifier.setExecuted();
      await showDialog(
        context: context,
        builder: (ctx) {
          return TestFailedPage(
            isFromResultPage: isFromResultPage,
            titleAPI: titleAPI,
            nextPageRoute: nextPageRoute,
            onNextPage: onNextPage,
            failReason: failReason,
            onRestart: onRestart,
            onDialogClose: onDialogClose,
          );
        },
      );
    }
  }

  Future<void> _goToResultPage(BuildContext context, bool isExecuted) async {
    final isExecutedNotifier = ref.read(resultIsExecutedStateProvider.notifier);

    if (!isExecuted) {
      isExecutedNotifier.setExecuted();
      await showDialog(
        context: context,
        builder: (ctx) {
          ref.invalidate(resultPageCreateTradeInServiceProvider);
          Future.delayed(Duration(seconds: waitDuration), () {
            context.goNamed(
              AppRoutes.resultPage.name,
            );
            isExecutedNotifier.setExecuted();
          });
          return const TestCompletePage();
        },
      );
    }
  }
}
