import 'package:envied/envied.dart';

part 'gitlab_token.g.dart';
///https://codewithandrea.com/articles/flutter-api-keys-dart-define-env-files/
@Envied(obfuscate: true, path: 'lib/shared/utils/shared_env/env/gitlab_token.env')
abstract class GitlabToken {
  @EnviedField(varName: "GITLAB_PROJECT_ID")
  static final String gitlabProjectId = _GitlabToken.gitlabProjectId;
  @EnviedField(varName: "GITLAB_FEEDBACK")
  static final String gitlabFeedbackToken = _GitlabToken.gitlabFeedbackToken;
}
