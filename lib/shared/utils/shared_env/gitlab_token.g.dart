// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gitlab_token.dart';

// **************************************************************************
// EnviedGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
final class _GitlabToken {
  static const List<int> _enviedkeygitlabProjectId = <int>[
    2100348311,
    2622258452,
    2479545141,
    2292342610,
    3935698198,
    283350485,
    2163302492,
    1176168793,
  ];

  static const List<int> _envieddatagitlabProjectId = <int>[
    2100348322,
    2622258469,
    2479545090,
    2292342626,
    3935698211,
    283350498,
    2163302506,
    1176168800,
  ];

  static final String gitlabProjectId = String.fromCharCodes(List<int>.generate(
    _envieddatagitlabProjectId.length,
    (int i) => i,
    growable: false,
  ).map(
      (int i) => _envieddatagitlabProjectId[i] ^ _enviedkeygitlabProjectId[i]));

  static const List<int> _enviedkeygitlabFeedbackToken = <int>[
    1375248449,
    1705210254,
    3084673175,
    2862153787,
    2238479294,
    3661403880,
    135970852,
    729280923,
    3454931007,
    559303350,
    1577868411,
    3312030314,
    330644121,
    374694934,
    1779296432,
    1682572526,
    3601356269,
    449572271,
    2637227095,
    4087429385,
    1718806125,
    295100220,
    3193258872,
    3808756001,
    3754763666,
    4099774280,
  ];

  static const List<int> _envieddatagitlabFeedbackToken = <int>[
    1375248422,
    1705210338,
    3084673255,
    2862153818,
    2238479306,
    3661403845,
    135970845,
    729280942,
    3454931022,
    559303419,
    1577868337,
    3312030215,
    330644192,
    374694994,
    1779296483,
    1682572450,
    3601356221,
    449572319,
    2637227014,
    4087429483,
    1718806024,
    295100286,
    3193258806,
    3808756080,
    3754763717,
    4099774211,
  ];

  static final String gitlabFeedbackToken = String.fromCharCodes(
      List<int>.generate(
    _envieddatagitlabFeedbackToken.length,
    (int i) => i,
    growable: false,
  ).map((int i) =>
          _envieddatagitlabFeedbackToken[i] ^
          _enviedkeygitlabFeedbackToken[i]));
}
