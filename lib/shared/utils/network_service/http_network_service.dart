import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/shared/utils/network_service/base/network_service.dart';

class HttpNetworkService extends NetworkService with ExceptionHandlerMixin {
  final http.Client client;

  HttpNetworkService({http.Client? client}) : client = client ?? http.Client();

  @override
  String get baseUrl => (kDebugMode) ? AppConfig.baseUrlDev : AppConfig.baseUrl;

  @override
  Map<String, String> get headers => {
        'Content-Type': 'application/json',
      };

  @override
  Future<Either<AppException, Response>> delete(String url,
      {Map<String, dynamic>? headers}) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<Either<AppException, Response>> get(String url,
      {required Map<String, String> header}) {
    final res = handleException(
      () => client.get(
        Uri.parse("$baseUrl$url"),
        headers: updateHeaders(header),
      ),
    );
    return res;
  }

  @override
  Future<Either<AppException, Response>> post(String url,
      {required Map<String, String> headers, Map<String, dynamic>? body}) {
    final res = handleException(
      () => client.post(Uri.parse("$baseUrl$url"),
          headers: updateHeaders(headers), 
          body: body != null ? jsonEncode(body) : null),
    );
    return res;
  }

  @override
  Future<Either<AppException, Response>> put(String url,
      {Map<String, dynamic>? headers, body}) {
    // TODO: implement put
    throw UnimplementedError();
  }

  @override
  Map<String, String> updateHeaders(Map<String, String> data) {
    final header = {...data, ...headers};
    return header;
  }
}
