import 'package:techswop_lite/shared/domain/either.dart';
import 'package:techswop_lite/shared/domain/response.dart';
import 'package:techswop_lite/shared/exceptions/http_exceptions.dart';

abstract class NetworkService {
  String get baseUrl;

  Map<String, String> get headers;

  Map<String, String> updateHeaders(Map<String, String> data);

  Future<Either<AppException, Response>> get(
    String url, {
    required Map<String, String> header,
  });

  Future<Either<AppException, Response>> post(
    String url, {
    required Map<String, String> headers,
    Map<String, dynamic>? body,
  });

  Future<Either<AppException, Response>> put(
    String url, {
    Map<String, dynamic>? headers,
    dynamic body,
  });

  Future<Either<AppException, Response>> delete(
    String url, {
    Map<String, dynamic>? headers,
  });
}
