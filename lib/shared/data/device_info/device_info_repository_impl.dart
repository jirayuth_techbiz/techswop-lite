import 'package:android_id/android_id.dart';
import 'package:device_imei/device_imei.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:disk_space_plus/disk_space_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:techswop_lite/shared/domain/common/device_info.dart';
import 'package:techswop_lite/shared/domain/repository/device_info_repository.dart';

class DeviceInfoRepositoryImpl implements DeviceInfoRepository {
  final DeviceImei deviceimei;
  final AndroidId androidId;
  final DeviceInfoPlugin deviceInfoPlugin;
  DeviceInfoRepositoryImpl({
    required this.deviceimei,
    required this.androidId,
    required this.deviceInfoPlugin,
  });

  @override
  Future<DeviceModelInfo?> getDeviceInfo() async {
    if (defaultTargetPlatform == TargetPlatform.android) {
      final AndroidDeviceInfo androidDeviceInfo = await deviceInfoPlugin.androidInfo;
      final String? androidId = await _getAndroidId();
      return DeviceModelInfo(
        model: androidDeviceInfo.model,
        id: androidId,
        version: androidDeviceInfo.version.sdkInt,
      );
    } else if (defaultTargetPlatform == TargetPlatform.iOS) {
      final IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
      return DeviceModelInfo(
        model: iosDeviceInfo.utsname.machine,
        id: iosDeviceInfo.identifierForVendor,
      );
    } else {
      return DeviceModelInfo(
        model: 'Unknown',
        id: 'Unknown',
      );
    }
  }

  @override
  Future<double?> getDeviceTotalSpace() async {
    double? diskSpace = await DiskSpacePlus.getTotalDiskSpace;
    return diskSpace;
  }

  @override
  Future<String?> getIMEIidentifier() async {
    String? identifier;

    try {
      var deviceImei = await deviceimei.getDeviceImei();
      identifier = deviceImei;
    } catch (e) {
      return "Error : $e";
    }

    return identifier;
  }

  Future<String?> _getAndroidId() async {
    final String? androidIdData = await androidId.getId();
    return androidIdData;
  }
}
