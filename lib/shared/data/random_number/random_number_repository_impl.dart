import 'dart:math';

import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/shared/domain/repository/random_number_repository.dart';

class RandomNumberRepositoryImpl implements RandomNumberRepository {
  int get _maxValue => AppConfig.defaultRandomMaxNumber;

  @override
  int getRandomNumber() {
    final intValue = Random().nextInt(_maxValue) + 1;
    return intValue;
  }

  @override
  Map<int, bool> getRandomNumberMap(int newMapNumber) {
    final Map<int, bool> numberMap = {
      1: false,
      2: false,
      3: false,
      4: false,
    };

    numberMap.forEach((key, value) {
      numberMap[key] = false;
    });

    numberMap[newMapNumber] = true;
    return numberMap;
  }
}
