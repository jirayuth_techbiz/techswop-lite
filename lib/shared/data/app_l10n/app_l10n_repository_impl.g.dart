// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_l10n_repository_impl.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$localeObserverHash() => r'0b710efb1ababe2d45ec961fc0f9665d4ec68fdc';

/// See also [localeObserver].
@ProviderFor(localeObserver)
final localeObserverProvider = AutoDisposeProvider<AppLocalizations>.internal(
  localeObserver,
  name: r'localeObserverProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$localeObserverHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef LocaleObserverRef = AutoDisposeProviderRef<AppLocalizations>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
