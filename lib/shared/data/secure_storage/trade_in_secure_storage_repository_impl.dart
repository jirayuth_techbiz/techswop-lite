// ignore_for_file: avoid_redundant_argument_values

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:techswop_lite/shared/shared.dart';

class TradeinSecureStorageRepositoryImpl
    implements TradeinSecureStorageRepository {
  final FlutterSecureStorage storage;

  TradeinSecureStorageRepositoryImpl({required this.storage});

  @override
  Future<String?> getCurrentPage(BuildContext context) async {
    final currentLocation = GoRouter.of(context).location.split("/").last;
    final result = await storage.read(key: currentLocation);

    return result;
  }

  @override
  Future<void> deleteAllData() async {
    await storage.deleteAll();
  }

  @override
  Future<void> deleteDataByKey({required String key}) async {
    await storage.delete(key: key);
  }

  @override
  Future<Map<String, String>> getAllData() async {
    final result = await storage.readAll();

    return result;
  }

  @override
  Future<String?> getDataByKey({required String key}) async {
    final result = await storage.read(key: key);

    return result;
  }

  @override
  Future<void> writeData({required String key, required String? value}) async {
    await storage.write(key: key, value: value);
  }
}
