import 'dart:async';

import 'package:techswop_lite/configs/app_configs.dart';
import 'package:techswop_lite/shared/domain/repository/test_timeout_repository.dart';
import 'package:techswop_lite/shared/shared.dart';

class TestTimerRepositoryImpl implements TestTimerRepository {
  Timer? timer;

  TestTimerRepositoryImpl();

  int get defaultTestTimeout => AppConfig.defaultTimeout;

  @override
  Stream<int> getTimer() {
    try {
      final seconds = timer!.tick;
      return Stream.periodic(const Duration(seconds: 1), (_) => seconds);
    } catch (e) {
      debugPrint(e.toString());
      return Stream.value(0);
    }
  }

  @override
  FutureOr<void> startTimer() {
    try {
      if (timer != null) {
        debugPrint("timer is not null!");
        timer?.cancel();
      }
      timer = Timer.periodic(const Duration(seconds: 1), (timer) {
        // debugPrint("Wifi Check Timer Started : ${timer.tick}");
        if (timer.tick >= defaultTestTimeout) {
          timer.cancel();
        }
      });
    } catch (e) {
      debugPrint(e.toString());
      rethrow;
    }
  }

  @override
  FutureOr<void> stopTimer() {
    try {
      if (timer != null) {
        timer?.cancel();
      }
    } catch (e) {
      debugPrint(e.toString());
      rethrow;
    }
  }
}
