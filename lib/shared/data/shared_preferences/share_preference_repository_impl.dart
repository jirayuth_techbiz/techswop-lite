// ignore_for_file: file_names

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:techswop_lite/const/enums/routing_enums.dart';
import 'package:techswop_lite/shared/domain/repository/share_preferences_repository.dart';

class TradeInSharePreferencesRepositoryImpl
    implements TradeInSharePreferencesRepository {
  final SharedPreferences prefs;
  TradeInSharePreferencesRepositoryImpl(this.prefs);

  ///TODO : ไม่ควรมาอยู่ใน Repository แต่เก็บไว้เพราะขี้เกียจย้ายตอนนี้
  ///เอาไว้ลบข้อมูลที่เกี่ยวกับการทดสอบทั้งหมด
  @override
  Raw<Future<void>> resetSharePrefs() async {
    List<String> keys = prefs.getKeys().toList();
    List<String> pageKeys = [
      AppRoutes.wifiCheckPage.titleAPI,
      AppRoutes.bluetoothCheckPage.titleAPI,
      AppRoutes.geolocationCheckPage.titleAPI,
      AppRoutes.frontCameraCheckPage.titleAPI,
      AppRoutes.backCameraCheckPage.titleAPI,
      AppRoutes.microphoneCheckPage.titleAPI,
      AppRoutes.biometricsCheckPage.titleAPI,
      AppRoutes.proximitySensorCheckPage.titleAPI,
      AppRoutes.volumeCheckPage.titleAPI,
      AppRoutes.motionCheckPage.titleAPI,
      AppRoutes.screenCheckStartPage.titleAPI,
    ];
    try {
      for (var k = 0; k < keys.length; k++) {
        if (keys.contains(pageKeys[k])) {
          await prefs.setBool(pageKeys[k], keys.contains(pageKeys[k]));
        } else {
          continue;
        }
      }
    } on RangeError catch (_) {
      ///Ignore so i don't have to rewrite half the app to handling skip cases.
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<bool> clear() async {
    return await prefs.clear();
  }

  @override
  Future<bool> containsKey(String key) async {
    return prefs.containsKey(key);
  }

  @override
  Future<Set<String>> getKeys() async {
    return prefs.getKeys();
  }

  

  @override
  Future<bool?> getBool(String key) async {
    return prefs.getBool(key);
  }

  @override
  Future<double?> getDouble(String key) async {
    return prefs.getDouble(key);
  }

  @override
  Future<int?> getInt(String key) async {
    return prefs.getInt(key);
  }

  @override
  Future<String?> getString(String key) async {
    return prefs.getString(key);
  }

  @override
  Future<bool> remove(String key) async {
    return prefs.remove(key);
  }

  @override
  Future<bool> saveBool(String key, bool value) async {
    return prefs.setBool(key, value);
  }

  @override
  Future<bool> saveDouble(String key, double value) async {
    return prefs.setDouble(key, value);
  }

  @override
  Future<bool> saveInt(String key, int value) async {
    return prefs.setInt(key, value);
  }

  @override
  Future<bool> saveString(String key, String value) async {
    return prefs.setString(key, value);
  }
}
