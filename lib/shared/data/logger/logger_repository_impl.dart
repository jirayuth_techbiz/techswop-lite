import 'package:techswop_lite/shared/domain/repository/logger_repository.dart';
import 'package:logger/logger.dart';

class LoggerRepositoryImpl implements LoggerRepository {
  final Logger logger;

  LoggerRepositoryImpl(this.logger);

  final DateTime dateTime = DateTime.now();
  
  @override
  void logDebug(String message) {
    return logger.d(message, time: dateTime);
  }
  
  @override
  void logError(String message) {
    return logger.e(message, time: dateTime);
  }
  
  @override
  void logWTF(String message) {
    return logger.f(message, time: dateTime);
  }
  
  @override
  void logInfo(String message) {
    return logger.i(message, time: dateTime,);
  }
  
  @override
  void logTrace(String message) {
    return logger.t(message, time: dateTime);
  }
  
  @override
  void logWarning(String message) {
    return logger.w(message, time: dateTime);
  }
}
