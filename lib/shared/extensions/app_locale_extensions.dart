import 'package:flutter/widgets.dart';
import 'package:techswop_lite/generated/app_localizations.dart';

extension AppLocaleExtensions on BuildContext {
  AppLocalizations get localization => AppLocalizations.of(this)!;
}