import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_displaymode/flutter_displaymode.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:techswop_lite/generated/app_localizations.dart';
import 'package:techswop_lite/route/route.dart';
import 'package:torch_controller/torch_controller.dart';
import 'package:techswop_lite/shared/shared.dart';
import 'package:techswop_lite/theme/theme_techbiz.dart';

void main() async {
  Future<void> initializingApp() async {
    ///ก่อนเริ่ม App ให้เอา Config ที่จำเป็นมาใส่ในนี้ก่อน

    final WidgetsBinding widgetsBinding =
        WidgetsFlutterBinding.ensureInitialized();
    
    ///iOS ไม่มี High Refresh Rate
    ///ถ้าไม่เช็คว่าเป็น Android ก่อนจะเจอ Exception
    final isAndroid = defaultTargetPlatform == TargetPlatform.android;
    if (isAndroid) {
      List<DisplayMode> supportedMode = await FlutterDisplayMode.supported;
      try {
        await FlutterDisplayMode.setHighRefreshRate();
      } catch (e) {
        await FlutterDisplayMode.setPreferredMode(supportedMode.first);
        log("Error : $e");
      }
      log("All Screen : $supportedMode");
    }

    ///Status Bar และ Navigation Bar ตั้งให้ขึ้นระหว่างใช้งานแอพ
    await SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);

    // await ScreenUtil.ensureScreenSize();

    ///Flutter จะลบ Splash Screen อัตโนมัติเมื่อเริ่ม App
    ///ถ้าอยากเก็บ และลบเฉพาะเวลาที่ต้องการ ให้ใช้ [FlutterNativeSplash.preserve]
    FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

    ///เรียก Mobile Ads ก่อนเริ่มแอพ
    ///https://developers.google.com/admob/flutter/quick-start?hl=th#ios
    // await MobileAds.instance.initialize();

    ///Torch Controller ต้อง Initialize ก่อนเริ่มแอพ
    TorchController().initialize();
  }

  runZonedGuarded<Future<void>>(() async {
    await initializingApp();

    return runApp(
      const ProviderScope(
        child: MainApp(),
      ),
    );
  }, (error, stack) {
    log("Error : $error");
    log("Stacktrace : $stack");
  });
}

class MainApp extends ConsumerWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final router = ref.watch(appRouterProvider);

    return ScreenUtilInit(
        designSize: Size(
          MediaQuery.of(context).size.width,
          MediaQuery.of(context).size.height,
        ),
        minTextAdapt: true,
        builder: (context, child) {
          FlutterNativeSplash.remove();
          return MaterialApp.router(
            debugShowCheckedModeBanner: false,
            theme: MaterialTechbizTheme().themeData,
            routerConfig: router,
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
          );
        });
  }
}
