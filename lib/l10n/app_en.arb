{
    "@@last_modified": "2024-05-31T10:42:50+0700",
    "@@author": "Jirayuth @ Tech Business",
    "@@context": "For Trade-in Checking Mobile App",
    "@@locale": "en",
    "@_GENERAL" : {},
    "@title" : {
        "description": "Title of the app"
    },
    "title": "Trade-in Checking Mobile App",
    "@confirmTxt" : {
        "description": "Confirm button"
    },
    "confirmTxt" : "Confirm",
    "@okTxt" : {
        "description": "OK button"
    },
    "okTxt" : "Okay",
    "@returnTxt" : {
        "description": "Return button"
    },
    "returnTxt" : "Return",
    "@cancelTxt" : {
        "description": "Cancel button"
    },
    "cancelTxt" : "Cancel",
    "@errorTxt" : {
        "description": "Error keyword"
    },
    "errorTxt" : "Error",
    "@retryTxt" : {
        "description": "Retry button"
    },
    "retryTxt" : "Retry",
    "@closeTxt" : {
        "description": "Close button"
    },
    "closeTxt" : "Close",
    "@startTradeButton" : {
        "description": "Button to start the trade-in process"
    },
    "startTradeButton" : "Start evaluating now!",
    "@refreshTxt" : {
        "description": "Refresh button"
    },
    "refreshTxt" : "Refresh",

    "@_DEBUG" : {},
    "@debugReset" : {
        "description": "Reset All Data in the local storage Eg. Shared Preferences, Secure Storage"
    },
    "debugReset" : "(Debug) Reset",
    "@debugToCheckPage" : {
        "description": "Skip to the specific check page. For debugging purpose only"
    },
    "debugToCheckPage" : "(Debug) To Check Page",
    "@debugUnderConstuction" : {
        "description": "Under Construction message"
    },
    "debugUnderConstuction" : "Under Construction, Please Skip the test.",

    "@_BOTTOM_APP_BAR" : {},
    "@bottomAppBarHome" : {
        "description": "Homepage Keyword"
    },
    "bottomAppBarHome" : "Homepage",
    "@bottomAppBarResultPage" : {
        "description": "Result Page keyword"
    },
    "bottomAppBarResultPage" : "Result",

    "@_HOME" : {},
    "@homePageUnknownDevice" : {
        "description": "Unknown device message"
    },
    "homePageUnknownDevice" : "Unknown Device",
    "@homePageImageNotFound" : {
        "description": "Image not found message"
    },
    "homePageImageNotFound" : "An error occured. Press the icon above to refresh the page.",
    "@homePageExitPage" : {
        "description": "Exit page message"
    },
    "homePageExitPage" : "Would you like to exit the app?",
    "@homePageYourDeviceIsNotSupported" : {
        "description": "Your device is not supported message"
    },
    "homePageYourDeviceIsNotSupported" : "Your device may not supported. Or retriving data incorrectly, Try pressing 'Refresh' to retrieve data again. \n If this issue persists, Please contact the staff for further assistance.",
    "@homePageNoDevice" : {
        "description": "Your device is not found message" 
    },
    "homePageNoDevice" : "Your device did not join the campaign. Please contact the staff for further assistance.",
    "@homePagePromotion" : {
        "description": "Promotion message"
    },
    "homePagePromotion" : "Promotion",
    "@homePageNoPromotion" : {
        "description": "No promotion message"
    },
    "homePageNoPromotion" : "No promotion available",
    "@_WEB_VIEW" : {},
    "@webViewHeader" : {
        "description": "Header to displayed when user open webview"
    },
    "webViewHeader" : "Promotion",

    "@_PREPARATION" : {},
    "@preparationBeforeCheckTips" : {
        "description": "Title page before start checking operation"
    },
    "preparationBeforeCheckTips" : "Preparation",
    "@preparationPleaseAllowPermission" : {
        "description": "Asking user to give the app permission access message"
    },
    "preparationPleaseAllowPermission" : "Please grant permissions to allow the app to access your device information",
    "@preparationOtherwiseItWillAffectTheResult" : {
        "description": "Otherwise it will affect the final result message"
    },
    "preparationOtherwiseItWillAffectTheResult" : "If you don't allow the app access to your devices, it could affect the price evaluation. ",
    "@preparationIncaseYouForgotPermission" : {
        "description": "In case you forgot to grant permission message"
    },
    "preparationIncaseYouForgotPermission" : " If you forgot to grant permission/enabling certain system services, you can press the X icon and grant the permission/go to setting page manually.",
    "@preparationLetsBegin" : {
        "description": "Let's begin button"
    },
    "preparationLetsBegin" : "Let's Begin!",
    
    "@_PREPARATION_POPUP" : {},
    "@preparationPopupWarning" : {
        "description": "Popup title warning the user that they haven't finished granting permission"
    },
    "preparationPopupWarning" : "Warning!",
    "@preparationPopupWarningAccessInComplete" : {
        "description": "Popup message warning the user that they haven't finished granting permission"
    },
    "preparationPopupWarningAccessInComplete" : "You haven't finished granting permission.\n",
    "@preparationPopupAreYouSureYouWantToContinues" : {
        "description": "Popup message asking the user if they want to continue without granting permission"
    },
    "preparationPopupAreYouSureYouWantToContinues" : "If you continue, It could potentially affected the price evaluation. Are you sure you want to continue without granting permission?\n",
    "@preparationPopupOrYouWantToDoItManually" : {
        "description": "Popup message asking the user if they want to grant permission manually"
    },
    "preparationPopupOrYouWantToDoItManually" : "• If you want to grant permission manually, Press 'Grant Permission'",
    "@preparationPopupIfYesPressContinues" : {
        "description": "Popup message asking the user if they want to continue without granting permission"
    },
    "preparationPopupIfYesPressContinues" : "• If yes, Press 'Continue'.\n",
    "@preparationPopupGrantPermission" : {
        "description": "Grant Permission button"
    },
    "preparationPopupGrantPermission" : "Grant Permission",

    "@_HARDWARE_BASED_WIDGET" : {
        "description": "Description as presented in each checklist tiles. Describing what the user need to do to pass the test smoothly."
    },
    "@hardwareBasedWifi" : {
        "description": "Wi-Fi keyword"
    },
    "hardwareBasedWifi" : "Wi-fi",
    "@hardwareBasedWifiDescription" : {
        "description": "Description about testing Wi-Fi"
    },
    "hardwareBasedWifiDescription" : "Enable Wi-Fi and let the app access to your connection hardware.",
    "@hardwareBasedMotion" : {
        "description": "Motion keyword (Orientation)"
    },
    "hardwareBasedMotion" : "Motion",
    "@hardwareBasedMotionDescription" : {
        "description": "Description about enabling automatic screen orientation"
    },
    "hardwareBasedMotionDescription" : "Enable automatic screen orientation.",
    "@hardwareBasedBattery" : {
        "description": "Battery keyword"
    },
    "hardwareBasedBattery" : "Battery",
    "@hardwareBasedBatteryDescription" : {
        "description": "Description about plugging in battery charger"
    },
    "hardwareBasedBatteryDescription" : "Plug in your battery charger to let the app get access to your battery hardware.",
    "@hardwareBasedGPS" : {
        "description": "คำว่า GPS"
    },
    "hardwareBasedGPS" : "GPS",
    "@hardwareBasedGPSDescription" : {
        "description": "Description about enabling GPS"
    },
    "hardwareBasedGPSDescription" : "Enabling GPS to let the app get access and checking your GPS hardware.",
    "@hardwareBasedBiometrics" : {
        "description": "Bio-metrics keyword"
    },
    "hardwareBasedBiometrics" : "Biometrics",
    "@hardwareBasedBiometricsDescription" : {
        "description": "Description about enabling face/fingerprint scanner"
    },
    "hardwareBasedBiometricsDescription" : "Enable face ID/fingerprint scanner to let the app get access to your biometrics hardware.",
    "@hardwareBasedBluetooth" : {
        "description": "Bluetooth keyword"
    },
    "hardwareBasedBluetooth" : "Bluetooth",
    "@hardwareBasedBluetoothDescription" : {
        "description": "Description about enabling Bluetooth"
    },
    "hardwareBasedBluetoothDescription" : "Enable Bluetooth to let the app get access to your Bluetooth hardware.",
    "@permissionBasedMicrophone" : {
        "description": "Microphone keyword"
    },
    "permissionBasedMicrophone" : "Microphone",
    "@permissionBasedMicrophoneDescription" : {
        "description": "Microphone access description message"
    },
    "permissionBasedMicrophoneDescription" : "Enable microphone access to allow the app to test your microphone device.",
    "@permissionBasedCamera" : {
        "description": "Camera keyword"
    },
    "permissionBasedCamera" : "Camera",
    "@permissionBasedCameraDescription" : {
        "description": "Camera access description message"
    },
    "permissionBasedCameraDescription" : "Enable camera access to allow the app to test your camera hardware.",
    "@permissionBasedStorage" : {
        "description": "Storage keyword"
    },
    "permissionBasedStorage" : "Storage",
    "@permissionBasedStorageDescription" : {
        "description": "Storage access description message"
    },
    "permissionBasedStorageDescription" : "Enable storage access.",

    "@_LAYOUT_PAGE" : {},
    "@layoutPageTitle" : {
        "description": "Title of the layout page"
    },
    "layoutPageTitle" : "Device Testing",
    "@layoutSkipButton" : {
        "description": "Skip button on the layout page"
    },
    "layoutSkipButton" : "Skip this step",
    "@layoutPageStartTest" : {
        "description": "Start button on the layout page"
    },
    "layoutPageStartTest" : "Start the test",
    "@layoutPageRestart" : {
        "description": "Restart button on the layout page"
    },
    "layoutPageRestart" : "Restart the test",
    "@layoutStep" : {
        "description": "Step message"
    },
    "layoutStep" : "Step : ",
    "@layoutTips" : {
        "description": "Tips message" 
    },
    "layoutTips" : "Tips ",

    "@_COUNTDOWN_WIDGET" : {},
    "@countdownWidgetDescription" : {
        "description": "Description of the countdown widget"
    },
    "countdownWidgetDescription" : "Counting Down...",
    "@countdownWidgetSeconds" : {
        "description": "Seconds keyword"
    },
    "countdownWidgetSeconds" : "Seconds",
    "@countdownWidgetReset" : {
        "description": "Reset button for the countdown widget"
    },
    "countdownWidgetReset" : "Reset",

    "@_TEST_SUCCESS" : {},
    "@testSuccess" : {
        "description": "Test Passed message"
    },
    "testSuccess" : "Test Passed",

    "@_TEST_CANCEL" : {},
    "@testCancel" : {
        "description": "Test cancel message"
    },
    "testCancel" : "Cancelling Test?",
    "@testCancelDescription" : {
        "description": "Test cancel description ask if user is really sure of it."
    },
    "testCancelDescription" : "All of your test data will be lost, and you will need to restart. Are you sure you want to cancel the test?",

    "@_TEST_FAILURE" : {},
    "@testFailure" : {
        "description": "Test failure message"
    },
    "testFailure" : "Test Failed",

    "@_WIFI_PAGE" : {},
    "@wifiPageTitle" : {
        "description": "Title of the Wi-Fi page"
    },
    "wifiPageTitle" : "Wi-Fi",
    "@wifiPageDescription" : {
        "description": "Description of the Wi-Fi page"
    },
    "wifiPageDescription" : "• Make sure your Wi-Fi service you have in your device turned on.\n• Connect to a Wi-Fi network.",
    "@wifiPageTestFailed" : {
        "description": "Wi-Fi test failed message"
    },
    "wifiPageTestFailed" : "We cannot detect any Wi-Fi connection.",

    "@_BLUETOOTH_PAGE" : {},
    "@bluetoothPageTitle" : {
        "description": "Title of the Bluetooth page"
    },
    "bluetoothPageTitle" : "Bluetooth",
    "@bluetoothPageDescription" : {
        "description": "Description of the Bluetooth page"
    },
    "bluetoothPageDescription" : "• Make sure your Bluetooth service is turned on during the test.\n• If the test is taking too long, try turning Bluetooth service off and on again first.\n• If you have wireless device with Bluetooth capability, try connecting it to your device.",
    "@bluetoothPageTestFailed" : {
        "description": "Bluetooth test failed message"
    },
    "bluetoothPageTestFailed" : "We cannot detect any Bluetooth connection.",
    
    "@_GPS_PAGE" : {},
    "@gpsPageTitle" : {
        "description": "Title of the GPS page"
    },
    "gpsPageTitle" : "GPS",
    "@gpsPageDescription" : {
        "description": "Description of the GPS page"
    },
    "gpsPageDescription" : "• Make sure your GPS service is turned on during the test\n• Try moving your phone around a bit to give the GPS sensor activated.",
    "@gpsPageTestFailed" : {
        "description": "GPS test failed message"
    },
    "gpsPageTestFailed" : "We cannot detect any GPS connection.",

    "@_FLASHLIGHT_PAGE" : {},
    "@flashlightPageTitle" : {
        "description": "Title of the Flashlight page"
    },
    "flashlightPageTitle" : "Flashlight",
    "@flashlightPageDescription" : {
        "description": "Description of the Flashlight page"
    },
    "flashlightPageDescription" : "• After start the test, Look behind your phone and count how many times the flash blinks. Then answer the question to continue.\n• If you don't see any flash or not sure about the answer, try restart the test one more times by pressing 'Restart The Test' button below.",
    "@flashlightPageTestFailed" : {
        "description": "Flashlight test failed message"
    },
    "flashlightPageTestFailed" : "We cannot turn on/off the torch on your device.",

    "@_FRONT_CAMERA_PAGE" : {},
    "@frontCameraPageTitle" : {
        "description": "Title of the Front Camera page"
    },
    "frontCameraPageTitle" : "Front Camera",
    "@frontCameraPageDescription" : {
        "description": "Description of the Front Camera page"
    },
    "frontCameraPageDescription" : "• Please turn your device's camera toward your face.\n• Try to keep the camera in the center of your face for easier detection.",
    "@frontCameraPageTestFailed" : {
        "description": "Front Camera test failed message"
    },
    "frontCameraPageTestFailed" : "We cannot detect front camera available from your device's.",

    "@_BACK_CAMERA_PAGE" : {},
    "@backCameraPageTitle" : {
        "description": "Title of the Back Camera page"
    },
    "backCameraPageTitle" : "Back Camera",
    "@backCameraPageDescription" : {
        "description": "Description of the Back Camera page"
    },
    "backCameraPageDescription" : "• Please turn your device's camera toward any object in front of you.\n• Try to steady the camera for better detection.",

    "@_MICROPHONE_PAGE" : {},
    "@microphonePageTitle" : {
        "description": "Title of the Microphone page"
    },
    "microphonePageTitle" : "Microphone",
    "@microphonePageDescription" : {
        "description": "Description of the Microphone page"
    },
    "microphonePageDescription" : "• Press 'Start The Test' and speak to your device's microphone before the time ran out.\n• Try speaking louder to increase your chance of success.",
    "@microphoneKeepTalking" : {
        "description": "Keep talking message"
    },
    "microphoneKeepTalking" : "Keep talking for",
    "@microphoneSeconds" : {
        "description": "Seconds Keyword"
    },
    "microphoneSeconds" : "Seconds",
    "@microphoneTestFailed" : {
        "description": "Microphone test failed message"
    },
    "microphoneTestFailed" : "We cannot detect any sound from your device's microphone.",

    "@_BIOMETRICS_PAGE" : {},
    "@biometricsPageTitle" : {
        "description": "Title of the Biometrics page"
    },
    "biometricsPageTitle" : "Biometrics",
    "@biometricsPageDescription" : {
        "description": "Description of the Biometrics page"
    },
    "biometricsPageDescription" : "• Press 'Start the Test' there will be a pop-up asking for your biometrics data (such as your fingerprint) for authentication. Please follow the instruction to continue.\n• If you never setup your biometrics. Please go to Setting page and setup your biometrics before continue.",
    "@biometricsPageFingerPrintNotFound" : {
        "description": "Fingerprint not found message"
    },
    "biometricsPageFingerPrintNotFound" : "Fingerprint not found",
    "@biometricsPageFingerPrintNotFoundDescription" : {
        "description": "Fingerprint not found description message"
    },
    "biometricsPageFingerPrintNotFoundDescription" : "We couldn't find your fingerprint. Please go into Setting -> Biometrics (Fingerprint) and set up.",
    "@biometricsPageFaceIfinishedSettingFaceId" : {
        "description": "Face ID finished setting message"
    },
    "biometricsPageFaceIfinishedSettingFaceId" : "I finished setting up Fingerprint ID",
    "@biometricsPageOpenScanningPage" : {
        "description": "Open Scanning page message"
    },
    "biometricsPageOpenScanningPage" : "Open Scanning Page",
    "@biometricPagePleaseScanFingerPrint" : {
        "description": "Please scan your fingerprint message"
    },
    "biometricPagePleaseScanFingerPrint" : "Please scan your fingerprint/Face ID",

    "@_FACE_ID_PAGE" : {},
    "@faceIdPageTitle" : {
        "description": "Title of the Face ID page"
    },
    "faceIdPageTitle" : "Face ID",
    "@faceIdPageDescription" : {
        "description": "Description of the Face ID page"
    },
    "faceIdPageDescription" : "• Press 'Start the Test' there will be a pop-up asking for your Face ID for authentication. Please follow the instruction to continue.\n• If you never setup your Face ID. Please go to Setting page and setup your Face ID before continue.",
    "@faceIdPageOpenScanningPage" : {
        "description": "Open Scanning page message"
    },
    "faceIdPageOpenScanningPage" : "Open Scanning Page",
    "@faceIdPageFaceIdNotFound" : {
        "description": "Face ID not found message"
    },
    "faceIdPageFaceIdNotFound" : "Face ID not found",
    "@faceIdPageFaceIdNotFoundDescription" : {
        "description": "Face ID not found description message"
    },
    "faceIdPageFaceIdNotFoundDescription" : "We couldn't find your Face ID. Please go into Setting -> Biometrics (Face ID) and set up.",
    "@faceIdPageFaceIfinishedSettingFaceId" : {
        "description": "Face ID finished setting message"
    },
    "faceIdPageFaceIfinishedSettingFaceId" : "I finished setting up Face ID",

    "@_BATTERY_PAGE" : {},
    "@batteryPageTitle" : {
        "description": "Title of the Battery page"
    },
    "batteryPageTitle" : "Battery",
    "@batteryPageDescription" : {
        "description": "Description of the Battery page"
    },
    "batteryPageDescription" : "• Make sure your device is currently plugged-in to the charger.\n• If you are using a wireless charger, please make sure it is properly connected to the power source.\n• If the test is taking too long. Try unplug before plugging in again.",
    "@batteryPageTestFailed" : {
        "description": "Battery test failed message"
    },
    "batteryPageTestFailed" : "We cannot detect any battery connection.",
    
    "@_PROXIMITY_SENSOR_PAGE" : {},
    "@proximitySensorPageTitle" : {
        "description": "Title of the Proximity Sensor page"
    },
    "proximitySensorPageTitle" : "Proximity Sensor",
    "@proximitySensorPageDescription" : {
        "description": "Description of the Proximity Sensor page"
    },
    "proximitySensorPageDescription" : "• Please cover your hand a few inches above your device's screen and wave slowly to test the proximity sensor.\n• It may take a few seconds for the sensor to detecting movement. If the sensor did not detect your movement, Try moving your hand closer toward screen.",
    "@proximitySensorPageTestFailed" : {
        "description": "Proximity Sensor test failed message"
    },
    "proximitySensorPageTestFailed" : "We cannot detect any proximity sensor connection.",
    "@proximitySensorPageIsNear" : {
        "description": "Proximity sensor is near message"
    },
    "proximitySensorPageIsNear" : "Detected",
    

    "@_VOLUME_BUTTON_PAGE" : {},
    "@volumeButtonPageTitle" : {
        "description": "Title of the Volume Button page"
    },
    "volumeButtonPageTitle" : "Volume",
    "@volumeButtonPageDescription" : {
        "description": "Description of the Volume Button page"
    },
    "volumeButtonPageDescription" : "• Press the up and down volume button on the side of your device to test the volume button.",
    "@volumeUpButton" : {
        "description": "Volume Up button"
    },
    "volumeUpButton" : "Volume Up",
    "@volumeDownButton" : {
        "description": "Volume Down button"
    },
    "volumeDownButton" : "Volume Down",
    "@volumePageTestFailed" : {
        "description": "Volume test failed message"
    },
    "volumePageTestFailed" : "We cannot detect any volume being pressed.",

    "@_SPEAKER_PAGE" : {},
    "@speakerPageTitle" : {
        "description": "Title of the Speaker page"
    },
    "speakerPageTitle" : "Speaker",
    "@speakerPageDescription" : {
        "description": "Description of the Speaker page"
    },
    "speakerPageDescription" : "• Press 'Start the Test' and listen to the sound from your device's speaker. Then answer the question to continue.\n• If you don't hear any sound, try increasing the volume or restart the test by pressing 'Restart The Test' button below.",
    "@speakerPageTestFailed" : {
        "description": "Speaker test failed message"
    },
    "speakerPageTestFailed" : "We cannot detect any sound from your device.",

    "@_MOTION_PAGE" : {},
    "@motionPageTitle" : {
        "description": "Title of the Motion page"
    },
    "motionPageTitle" : "Motion",
    "@motionPageDescription" : {
        "description": "Description of the Motion page"
    },
    "motionPageDescription" : "• Rotate your device horizontally/vertically to test the motion sensor.\n• If nothing happened, Try shaking your device or enable auto-rotate feature on your device.",
    "@motionPageHorizontal" : {
        "description": "Horizontal message"
    },
    "motionPageHorizontal" : "Horizontal",
    "@motionPageVertical" : {
        "description": "Vertical message"
    },
    "motionPageVertical" : "Vertical",

    "@_SCREEN_CAPTURE_PAGE" : {},
    "@screenCapturePageTitle" : {
        "description": "Title of the Screen Capture page"
    },
    "screenCapturePageTitle" : "Screen Capture",
    "@screenCapturePagePressVolumeDownAndLockScreen" : {
        "description": "Press the volume down and lock screen message"
    },
    "screenCapturePagePressVolumeDownAndLockScreen" : "• Press the volume down and lock screen button to continues.\n• If that doesn't work. Your device may use a different button/method to capture the screen.",
    "@screenCapturePagePressVolumeUpAndLockScreen" : {
        "description": "Press the volume up and lock screen message"
    },
    "screenCapturePagePressVolumeUpAndLockScreen" : "• Press the volume up and lock screen button to continues.",
    "@screenCapturePageTestStatus" : {
        "description": "Screen Capture test status message"
    },
    "screenCapturePageTestStatus" : "Screen Captured",

    "@_PHONE_CALL_SPEAKER_PAGE" : {},
    "@phoneCallSpeakerPageTitle" : {
        "description": "Title of the Phone Call Speaker page"
    },
    "phoneCallSpeakerPageTitle" : "Phone Call Speaker",
    "@phoneCallSpeakerPageDescription" : {
        "description": "Description of the Phone Call Speaker page"
    },
    "phoneCallSpeakerPageDescription" : "• Put your phone near your ear, listen to the sound then answer the question to continues\n• If you don't hear any sound, try increasing the volume or restart the test by pressing 'Restart The Test' button below.",
    "@phoneCallSpeakerPageTestFailed" : {
        "description": "Phone Call Speaker test failed message"
    },
    "phoneCallSpeakerPageTestFailed" : "We cannot detect any sound from your phone call speaker.",

    "@_VIBRATION_PAGE" : {},
    "@vibrationPageTitle" : {
        "description": "Title of the Vibration page"
    },
    "vibrationPageTitle" : "Vibration",
    "@vibrationPageDescription" : {
        "description": "Description of the Vibration page"
    },
    "vibrationPageDescription" : "• Press 'Start the Test' and feel the vibration from your device. Then answer the question to continue.\n• If you don't feel any vibration or would like to try again, Try restart the test by pressing 'Restart The Test' button below.",
    "@vibrationPageTestFailed" : {
        "description": "Vibration test failed message"
    },
    "vibrationPageTestFailed" : "We cannot detect any vibration from your device.",

    "@_SCREEN_TOUCH_START_PAGE" : {},
    "@screenTouchStartPageTitle" : {
        "description": "Title of the Screen Touch Start page"
    },
    "screenTouchStartPageTitle" : "Screen Check",
    "@screenTouchStartPageDescription" : {
        "description": "Description of the Screen Touch Start page"
    },
    "screenTouchStartPageDescription" : "• Press 'Start The Test' and clear all the color on your screen within the time limit.",
    "@screenTouchStartPageTestFailed" : {
        "description": "Screen Touch Start test failed message"
    },
    "screenTouchStartPageTestFailed" : "You did not clear the screen's color in time limit.",
    "@screenTouchStartTest" : {
        "description": "Start the test button"
    },
    "screenTouchStartTest" : "Press 'Start The Test' and clear all the color on your screen within the time limit.",

    "@_RESULT_LOADING_PAGE" : {},
    "@resultLoadingInProcess" : {
        "description": "Message telling user that the test result is in process"
    },
    "resultLoadingInProcess" : "Result is in process Please wait...",

    "@_RESULT_PAGE" : {},
    "@resultPageTitle" : {
        "description": "Title of the result page"
    },
    "resultPageTitle" : "Result",
    "@resultPagePleaseShowQR" : {
        "description": "Message telling user to show the QR code to the staff" 
    },
    "resultPagePleaseShowQR" : "Please show QR Code image to the staff",
    "@resultPageNoDataError" : {
        "description": "Message telling user that there is no data and they will need to test atleast once"
    },
    "resultPageNoDataError" : "You haven't start testing your devices yet. Please press the button below to start testing.",
    "@resultPageDateOfTest" : {
        "description": "Date of the test message"
    },
    "resultPageDateOfTest" : "Test Date",
    "@resultPageSaveQR" : {
        "description" : "Save QR code message"
    },
    "resultPageSaveQR" : "Save QR Code",
    "@resultPageClosedHero" : {
        "description": "Closed hero tag message"
    },
    "resultPageClosedHero" : "Closed",
    "@resultPageCreateQRCode" : {
        "description": "Create QR code message"
    },
    "resultPageCreateQRCode" : "Please show this QR code to the staff or tap the QR Code to save it to your device.",
    "@resultPageTestPassed" : {
        "description": "Test passed message"
    },
    "resultPageTestPassed" : "Test Passed",
    "@resultPageTestFailed" : {
        "description": "Test failed message"
    },
    "resultPageTestFailed" : "Test Failed",
    "@resultPagePageTotal" : {
        "description": "Pages message"
    },
    "resultPagePageTotal" : "Pages",
    "@resultPageDuplicatedQRSave" : {
        "description": "Duplicated QR code save message"
    },
    "resultPageDuplicatedQRSave" : "You already saved this QR code.",
    "@resultPageQRSaveSuccess" : {
        "description": "QR code save description message"
    },
    "resultPageQRSaveSuccess" : "QR Code has been saved to your device."
}